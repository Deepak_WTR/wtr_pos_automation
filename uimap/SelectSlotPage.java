package uimap;

public class SelectSlotPage {

/////24-July-13
		//CheckBox(Nanditha-Book A Slot Page)
		public static String chkBoxNewUser = "pass-0";
		public static String chkBoxNewUser_Loc = "id";


//CheckBox(Nanditha-Book A Slot Page)(Updated)
	public static String chkBoxRegisteredUser = "pass-1";
	public static String chkBoxRegisteredUser_Loc = "id";
	
		
		
		//Groceries Collection Slot Tab(Nanditha - Created on 06-Aug-13)
		public static final String tabGroceryCollectionSlot = "//a[contains(text(),'Groceries Collection')]";
		public static final String tabGroceryCollectionSlot_Loc = "xpath";

		//Deliver To New Address button(Created on 20-Aug-13)
			public static final String btnDeliverToNewAddress = "//a[contains(text(),'Deliver to a new address')]";
			public static final String btnDeliverToNewAddress_Loc = "xpath";


	//Enter new postcode text box(Nanditha - Created on 21-Aug-13)
			public static final String txtBoxEnterNewPostCode = "//input[@id='edit_postcode_existing_address']";
			public static final String txtBoxEnterNewPostCode_Loc = "xpath";
			
			public static final String txtDay_loc="xpath";
			public static final String txtDay="//div[@class='days']/a[*]/div";

						//yes we deliver
						public static final String txtwedeliver="//p[contains(text(),'Yes - we deliver to ')]";
						public static final String txtwedeliver_loc="xpath";
			//we ll take in check out
						public static final String txtwetakechk="//div[@class='address is-selected new-address']/p[3]";
						public static final String txtwetakechk_loc="xpath";

			//
						//Order reference number for barcnch
						public static final String txtOrdRef="//*[@id='content']/div/table/tbody/tr[1]/td";
						public static final String txtOrdRef_loc="xpath";
						

						//Check postcode button (Nanditha - Updated on 03-Sep-13)
			public static final String btnChkPostCode = "//div[@id='account-signed-in']/div/form/fieldset/div[2]/input";
		public static final String btnChkPostCode_Loc  = "xpath";
		

		//WE Collection Slot Tab(Nanditha - Created on 05-Sep-13)
						public static final String tabWECollectionSlot = "//a[contains(text(),'Entertaining Collection')]";
						public static final String tabWECollectionSlot_Loc = "xpath";


						//Delivery Service(Nanditha - Created on 18-Sep-13)
												public static final String lnkDeliveryService = "//a[contains(text(),'shop in branch, we deliver')]";
												public static final String lnkDeliveryService_Loc = "xpath";

												//Get user name
												public static final String txtusername="//form[@id='existing-user-address-select']/fieldset/label/div/p[1]";
												public static final String txtusername_loc="xpath";	
												
												
													
													

													//Check postcode button Updated(Nanditha - Updated on 5-Dec-13)(Updated by Vaishnavi on 20/5/2015)
													public static final String btnChkPostCode1 = "(//input[@value='Check postcode'])[2]";//"//body/div[2]/div/div/div/div[@class='l-content']/div[8]/div/form/fieldset/div[@class='button submit-button']/input[@value='Check postcode']";
													public static final String btnChkPostCode1_Loc  = "xpath";
													

													//Import now
													public static final String btnImportProduct="(//a[contains(text(),'Import now')])[3]";
													public static final String btnImportProduct_loc="xpath";
													
													//new page after book a slot
													//button to book deliver
													public static final String btndeliver="//input[@id='delivery']";
													public static final String btndeliver_loc="xpath";
																										
													
													//Shop Online You Collect link(Nanditha - Created on 06-Aug-13)
													public static final String lnkCollectionService = "//a[contains(text(),'shop online, you collect')]";
													public static final String lnkCollectionService_Loc = "xpath";
													
													//shop in branch we deliver
													public static final String linkShpInStoreWDel="shop in store and we will deliver";
													public static final String linkShpInStoreWDel_loc="linkText";
													
													//shop in branch, we deliver link updated on 240514
												    public static final String lnkShpInBrnchWeDelvr="//a[contains(text(),'shop in branch,we deliver')]";
													
												    //a[contains(text(),'shop in branch, we deliver')]";
												    public static final String lnkShpInBrnchWeDelvr_loc="xpath";
												    

public static final String btncollect="//input[@id='chooseCollectionLocation']";
public static final String btncollect_loc="xpath";

//change Home delivery
public static final String linkChangeHomeDelivery="//a[contains(text(),'Change to home delivery')]";
public static final String linkChangeHomeDelivery_loc="xpath";

public static final String txtNoOfDays_loc="xpath";
public static final String txtNoOfDays="//div[@class='days']/a";



//List of branches for collection postcode -- Kathir(created on 09102014)
//public static final String lst_Branch="returned-branches";
public static final String lst_Branch="//div[@id='returned-branches']/div";
public static final String lst_Branch_loc="xpath";

//Change to home delivery button -- Kathir(created on 09102014)
public static final String btn_ChangeToHomeDel="Change to home delivery";
public static final String btn_ChangeToHomeDel_loc="linkText";

//View next 5 button -- Kathir(created on 09102014)
public static final String btn_ViewNext="//div[@id='load-more']/a[text()='View next 5']";
public static final String btn_ViewNext_loc="xpath";

//Map -- Kathir(created on 09102014)
public static final String img_map="//div[@class='map_canvas gmap']";
public static final String img_map_loc="xpath";	

//Locker link for collection locker -- Kathir(created on 09102014)
public static final String lnk_locker1="//div[@id='returned-branches']/div[";

//div[@class='branch-logo EXTERNAL Gatwick']
public static final String lnk_locker2="]//div[@class='branch-logo EXTERNAL Gatwick']";//"]//div[contains(@class,'Shell')]"; 
		//"]//div[@class='branch-logo LOCKER Shell']";
public static final String lnk_locker_loc="xpath";
//Manned collections
public static final String lnk_manned1="//div[@id='returned-branches']/div[";
public static final String lnk_manned2="]//div[@class='branch-logo EXTERNAL John Lewis Store']";
public static final String lnk_manned_loc="xpath";
//manned image
public static final String imgManned="//div[@class='branch-logo EXTERNAL John Lewis Store']";
public static final String imgManned_loc="xpath";
//about this collection point
public static final String linkAboutColle="//a[@class='EXTERNAL locker about-collection-location']";
public static final String linkAboutColle_loc="xpath";
//Grocery collection
public static final String tabGrcColl="//a[@id='genTab']";
public static final String tabGrcColl_loc="xpath";
//Address details
public static final String txtAddress="//div[@class='branch-address']/p/Strong";
public static final String txtAddress_loc="xpath";
//Manned image in overlay
public static final String imgMannedOverlay="(//div[@class='logo EXTERNAL John Lewis Store'])[2]";
public static final String imgMannedOverlay_loc="xpath";

//Manned collection - text Age limit
public static final String textAgelimit="//div[@class='branch result-branch result-branch-1']//p[contains(text(),'Please note as you have chosen to collect from a locker you must be aged 18 or over and pay for your order with a credit card.')]";
public static final String textAgelimit_loc="xpath";




//Map -- Kathir(created on 09102014)
public static final String btn_CollectFrmHere="//div[@id='returned-branches']/div[1]//a[text()='Collect from here']";
public static final String btn_CollectFrmHere_loc="xpath";	

//view address link -- Kathir(created on 09102014)
public static final String lnk_ViewAddress="//div[@id='returned-branches']/div[1]//a[@class='viewAddress']";
public static final String lnk_ViewAddress_loc="xpath";

//Mobile number textbox for collection locker -- Kathir(created on 09102014)
public static final String txt_MobileNo="(//input[@id='mobilePhone'])[2]";
public static final String txt_MobileNo_loc="xpath";

//Day textbox for collection locker -- Kathir(created on 09102014)
public static final String txt_Day="(//input[@id='dayOfBirth'])[2]";
public static final String txt_Day_loc="xpath";

//Month textbox for collection locker -- Kathir(created on 09102014)
public static final String txt_Month="(//input[@id='monthOfBirth'])[2]";
public static final String txt_Month_loc="xpath";

//Year textbox for collection locker -- Kathir(created on 09102014)
public static final String txt_Year="(//input[@id='yearOfBirth'])[2]";
public static final String txt_Year_loc="xpath";

//Year textbox for collection locker -- Kathir(created on 09102014)
public static final String chk_IsPrimaryMobile="(//input[@id='isPrimaryMobilePhone'])[2]";
public static final String chk_IsPrimaryMobile_loc="xpath";



//NewYear Slot Selection(Created by Jayandran on 17 nov 2014 )
public static final String linkNewYearSlot_loc ="partialLinkText";
public static final String linkNewYearSlot="New Year";


//add delivery note

public static final String txtAddDeliveryNote="deliveryNote";
public static final String txtAddDeliveryNote_loc="id";

//verify delivery note

public static final String txtverifyAddnote="//table/tbody/tr/th[contains(text(),'Delivery Note')]/../td";
public static final String txtverifyAddnote_loc="xpath";

//date time

public static final String txtDateTime="//table/tbody/tr/th[contains(text(),'Delivery time and date')]/../td";
public static final String txtDateTime_loc="xpath";

//Christmas Slot Selection(Updated on 021214)

public static final String linkChristmas_loc ="linkText";
//public static final String linkChristmas="Christmas Slots";
public static final String linkChristmas="Christmas";
//delivery charge overlay



//delivery charge overlay(updated by vaishnavi)
public static final String txtDeliveryOverlay="//h2[@class='interruptionMsgHeading']";//"(//h2[contains(text(),'Delivery charging')])[2]";
public static final String txtDeliveryOverlay_loc="xpath";

//Continue button in delivery overlay

public static final String deliveryContinue="(//a[contains(text(),'Continue')])[2]";
public static final String deliveryContinue_loc="xpath";


//Delivery Charge Text In Slot Selection Page (Jayandran on 29/12/2014)(

public static final String textDeliveryCharge_More="//*[contains(text(),'if you spend more than')]";
public static final String textDeliveryCharge_More_loc="xpath";

public static final String textDeliveryCharge_Less="//*[contains(text(),'if you spend less than')]";
public static final String textDeliveryCharge_Less_loc="xpath";

//Delivery Charge Text In Slot Selection Overlay (Jayandran on 29/12/2014)

public static final String OverlayTextDeliveryCharge_More="(//span[contains(text(),'more than �')])[2]";
public static final String OverlayTextDeliveryCharge_More_loc="xpath";

public static final String OverlayTextDeliveryCharge_Less="(//span[contains(text(),'less than �')])[2]";
public static final String OverlayTextDeliveryCharge_Less_loc="xpath";


//delivery overlay close
public static final String linkClose="//a[contains(text(),'Close')]";
public static final String linkClose_loc="xpath";
//delivery overlay value
public static final String txtDeliveryOverlayValue="//div[@class='lightbox-container content-wrapper']/p";
public static final String txtDeliveryOverlayValue_loc="xpath";
//delivery banner value
public static final String txtbannerValue="(//div[@class='deliverChargeBanner'])[1]/h1";
public static final String txtbannerValue_loc="XPATH";
//delivery charge value in slot selected page
public static String txtDeliveryChargevalue="";
//mevhanism tesxt
public static final String txtDCMechanism="//p[@class='delivery-charge-msg-warning']";
public static final String txtDCMechanism_loc="xpath";//DC AND BANNER VALUE IN Slot confirmation overlay
public static final String txtdcBvalue="(//p[@class='delivery-details'])[4]";
public static final String txtdcBvalue_loc="xpath";

//delivery banner value in slot confirmation overlay
public static final String txtbannerChangeSlotvalue="(//div[@class='deliverChargeBanner'])[11]/h1";
public static final String txtbannerChangeSlotvalue_loc="xpath";





//shopping method
public static final String txtShngMethod="//div[@id='content']/h1";
public static final String txtShngMethod_loc="xpath";


// Delivery charge value 

public static String txtDCAmt = "";

// Confirm Slot change 

public static String btnconfirmslotchange = "(//a[contains(text(),'Confirm slot change')])[2]";
public static String btnconfirmslotchange_Loc = "xpath";

// Dc for slot header

public static String txtDc = "//*[@id='headerDelivery']/div/section[3]/p[2]/text()[2]";

public static String txtDc_Loc = "xpath";

// fastrack option - slot selection page

public static final String radiofastrackoption = "//input[@id='fasttrack']";
public static final String radiofastrackoption_Loc = "xpath";

//DC AND BANNER VALUE IN change Slot confirmation overlay
public static final String txtdcBvalueSlotchange="(//p[@class='delivery-details'])[6]";
public static final String txtdcBvalueSlotchange_loc="xpath";


// slot details in slot confirmation overlay

public static final String txtslotdetails = "(//span[@class='date-time-details'])[4]";
public static final String txtslotdetails_Loc = "xpath";


//slot details in slot confirmation overlay

public static final String txtslotdetailsFrom = "(//span[@class='date-time-details'])[5]";
public static final String txtslotdetailsFrom_Loc = "xpath";


//slot details in slot confirmation overlay

public static final String txtslotdetailsTo = "(//span[@class='date-time-details'])[6]";
public static final String txtslotdetailsTo_Loc = "xpath";


// slot details variable 

public static  String varSlotDetails = "";


//slot details in slot confirmation overlay

public static  String xpathslotreserved ="";

/*********************Mobile xpaths********************/

//Mobile click and collect
public static final String btnClickCollect="//span[@class='spanCollect']";
public static final String btnClickCollect_loc="xpath";
//post code filed
public static final String txtPostCode="//div[@class='WRgridItem4b']/input";
public static final String txtPostCode_loc="xpath";
//find button
public static final String btnFind="(//button[contains(text(),'Find')])[3]";
public static final String btnFind_loc="xpath";
//cOLLECTION Heading 
public static final String txtCollectHedng="//p[contains(text(),'Nearest Collection Points to')]";
public static final String txtCollectHedng_loc="xpath";
//welcomedesk
public static final String txtWelcomeDesk="(//p[@class='mediumSummary storeType'])";
public static final String txtWelcomeDesk_loc="xpath";
//WelcomedeskVary
public static final String txtWelcomeDeskVary="(//p[@class='mediumSummary storeType'])[index]";
public static final String txtWelcomeDeskVary_loc="xpath";
//notsiutable slot heading
public static final String txtNotSuitable="(//div[contains(text(),'No suitable slot?')])[2]";
public static final String txtNotSuitable_loc="xpath";

//delivery banner value in slot confirmation overlay
public static final String txtbannerSlotvalue="(//div[@class='deliverChargeBanner'])[10]/h1";
public static final String txtbannerSlotvalue_loc="xpath";


//slot confirmation overlay - Old Slot Reservation Message
public static final String txtOldSLotReservationMessage="(//*[contains(text(),'This slot will no longer be reserved for you')])";
public static final String txtOldSLotReservationMessage_loc="xpath";


//slot confirmation overlay - New Slot Reservation Message
public static final String txtNewSlotReservationMessage="(//*[contains(text(),'This slot will be reserved for 2 hours')])";
public static final String txtNewSlotReservationMessage_loc="xpath";

//slot confirmation overlay - From Delivery Address
public static final String txtFromDeliveryAddress="(//div[@class='from-section']//p[@class='address-details'])";
public static final String txtFromDeliveryAddress_loc="xpath";

//slot confirmation overlay - To Delivery Address
public static final String txtToDeliveryAddress="(//div[@class='to-section']//p[@class='address-details'])";
public static final String txtToDeliveryAddress_loc="xpath";

//Book slot - Service Selection Page - Left 
public static final String txtLeftSideTable="//div[@class='l-content']//h2[contains(text(),'Click & Collect')]";
public static final String txtLeftSideTable_Loc="xpath";

//Book slot - Service Selection Page - Right
public static final String txtRightSideTable="//div[@class='r-content']//h2[contains(text(),'Delivery')]";
public static final String txtRightSideTable_Loc="xpath";


//slot confirmation overlay - address

public static final String txtAddressInOVerlay =  "(//p[@class='address-detail'])[2]";

public static final String txtAddressInOVerlay_Loc =  "xpath";


//slot confirmation overlay - Heading

public static final String headingslotconfirmationoverlay =  "(//h2[contains(text(),'Confirmation of your delivery Slot')])[2]";

public static final String headingslotconfirmationoverlay_Loc =  "xpath";


public static final String slothighlighted = "//li[contains(@class,'selected')]";
public static final String slothighlighted_Loc = "xpath";

//Slot Table
		public static final String slotTable = "//div[@class='slots']";
		public static final String slotTable_Loc = "xpath";
	
// collection locker text 
		
		public static final String txtcollectionLockerinfo = "//p[contains(text(),'Please note as you have chosen to collect from a locker you must be aged 18 or over')]";
		public static final String txtcollectionLockerinfo_Loc = "xpath";

	// Alcohol restriction overlay header
		
		public static final String txtalcoholrestrictionheader = "//h2[contains(text(),'Alcohol can not be purchased in this slot')]";
		public static final String txtalcoholrestrictionheader_loc = "xpath";

		public static final String btncontinuealcohol = "//a[@id='continue']";
		public static final String btncontinuealcohol_loc = "xpath";
		
}
