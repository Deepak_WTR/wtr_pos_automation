package uimap;

public class JotterFrame {

	
			
			
			
			//Update results link-JotterList
			public static final String linkUpdateResults = "//a[contains(text(),'Update results')]";
			public static final String linkUpdateResults_Loc = "xpath";
			
			//Cancel link-JotterList
			public static final String linkCancel = "//a[contains(text(),'Update results')]";
			public static final String linkCancel_Loc = "xpath";
			
			
			
			//Add to List button-JotterList
			public static final String buttonAddToList = "//input[@value='Add']";
			public static final String buttonAddToList_Loc = "xpath";
			
			//Jotter List-JotterList
//			public static final String linkJotterList = "//div[@class='jotter-pad']/ul/li";
		//	public static final String linkJotterList_Loc = "xpath";
			
			//Name of Jotter List products-JotterList
			public static final String linkJotterListProductName = "//div[@id='content']/div[2]/div/div/ul/li[Index]/a[1]";
			public static final String linkJotterListProductName_Loc = "xpath";
			
			
			
			       //JotterNotePad (Nanditha-Jotter Landing Page)
					public static final String txtJotterNotePad="jotter-search";
					public static final String txtJotterNotePad_Loc="name";
					
					//SearchButton-JotterNotepad(Nanditha-Jotter Landing Page)
				//	public static final String buttonJotterSearch="//input[@value='Search']";
					public static final String buttonJotterSearch="//input[@value='Search these items']";
					public static final String buttonJotterSearch_Loc="xpath";
					

				    //Jotter Notepad Error message(Nanditha-Jotter Landing Page)
					public static final String txtJotterWarningMsg = "//div[@class='error-msg']/p";
					public static final String txtJotterWarningMsg_Loc = "xpath";
					
					//JotterList(Nanditha-Jotter Search Summary Page)
					public static final String linkJotterList = "//div[@class='jotter-pad']/ul/li[ListCount]";
					public static final String linkJotterList_Loc = "xpath";
					
					//Jotter Carousel Product(Nanditha-Jotter Search Summary Page)
					public static  String JotterCarouselProduct = "";
								    
					
					
					//Jotter Load More Button(Nanditha-Jotter Search Summary Page)
					public static final String buttonJotterLoadMore = "//div[@class='load-more']/div[@class='content-button no-image button']/a";
					public static final String buttonJotterLoadMore_Loc = "xpath";
					
					//Jotter Scroll Image(Nanditha-Jotter Search Summary Page)
					public static final String imgCarouselLoadButton = "//div[@class='panel'/img[@alt='[loading]'";
					public static final String imgCarouselLoadButton_Loc = "xpath";
					
					
					
					  
					 //Product cell-JotterSearchSummary Page
				    public static final String linkProduct="/div[@class='m-product-cell']";
				    public static final String linkProduct_Loc = "xpath";
				    
				    //Add Button-Jotter-can be used with JotterCarouselProduct(Nanditha-Jotter Search Summary Page)
				    public static final String buttonAdd="/div[@class='button add-button']/a";
				    public static final String buttonAdd_Loc="xpath";
				    
				    //Mini trolley-JotterSearchSummary Page
				    public static final String linkMiniTrolley="trolley-item-image";
				    public static final String linkMiniTrolley_Loc = "className";
				    
				 			 
				   
				 
				    public static final String linkHeartIconFilled="//div[@class='m-product-labels']/a[@class='label favourite is-favourite']";
				    public static final String linkHeartIconFilled_Loc = "xpath";
				    
				    //Jotter Product Grid-JotterSearchSummary Page
				    public static final String linkJotterCarousel = "//div[@class='jotter-result products products-grid']";
				    public static final String linkJotterCarousel_Loc = "xpath";
				    
				    //Jotter Carousel items-JotterSearchSummary Page
				    public static final String linkJotterCarouselItems = "/div/div/div/div[@class='carousel-items']";
				    public static final String linkJotterCarouselItems_Loc = "xpath";
				    
				    //Jotter Search Summary Heading(Nanditha-JotterSearchSummary Page)
				    public static final String headerJotterSearch ="//div[@class='r-content']/h1";
				    public static final String headerJotterSearch_Loc = "xpath";
				    
				    //Jotter Carousel product-JotterSearchSummary Page
				    public static final String linkCarouselProduct = "//div[@class='jotter-result products products-grid'][carouselindex]/div/div/div/div[@class='carousel-items']/div[@class='m-product-cell'][";
				    public static final String linkCarouselProduct_Loc = "xpath";
				    
				    //See All Link - Jotter Search Summary Page
				  //  public static final String linkSeeAllProducts = "//div[@class='jotter-result products products-grid']/p[@class='products-link']/a[@class='secondary-right']";
				    public static final String linkSeeAllProducts_Loc = "xpath";
				    public static final String linkSeeAllProducts = "//a[contains(text(),'See all 226 results for Name')]";
				  
				    //Jotter Guide-Previous Tab
				    public static final String tabPrevious = "//li[@class='before']/a/span";
				    public static final String tabPrevious_Loc = "xpath";
				    
				    //Jotter Guide-Current Tab
				    public static final String tabCurrent = "//li[@class='is-active']/a/span";
				    public static final String tabCurrent_Loc = "xpath";
				    
				    //Jotter Guide-Next Tab
				    public static final String tabNext = "//li[@class='after']/a/span";
				    public static final String tabNext_Loc = "xpath";
				    
				    //Jotter Product Name (can be used along with JotterCarouselProduct)
				    public static final String JotterProductName = "/div/div/div[2]/a";
				    public static final String JotterProductName_Loc = "xpath";
				  //Jotter Carousel name-(Nanditha-Jotter Search Summary Page)(Updated)
					public static final String linkCarouselName = "//div[@class='r-content']/div[iCheckCount]";
					public static final String linkCarouselName_Loc = "xpath";
					public static final String CarouselNameAttribute = "id";
					//////////////////
//23-July-13    
				    //Jotter Search Summary Page Heading(Nanditha-JotterSearchSummaryPage)
				    public static  String JotterSearchSummaryHeading = "jotter-results";//"Jotter multi-search results";
				    
				    //Carousel Product append qty edit box(Can be used along with Jotter Carousel Product)
				    public static final String editBoxJotterAppendQty = "/div/div[2]/div/div/input";
				    public static final String editBoxJotterAppendQty_Loc ="xpath";
				    
				  //Jotter Product Name (can be used along with JotterCarouselProduct)
				    public static final String JotterProductId = "/div/div/a/img";
				    public static final String JotterProductId_Loc = "xpath";
    
				    //Carousel Product select option
				    public static final String btnJotterSelectOption = "(//a[contains(text(),'Select option')])[Index]";
				    public static final String btnJotterSelectOption_Loc = "xpath";
				    
				    //Carousel Product select option -radio button
				    public static final String chkboxJotterSelectOption = "//input[@id='r2']";
				    public static final String chkboxJotterSelectOption_Loc = "xpath";
				    
				    //Variation product -Add to Trolley button
				    public static final String btnAddToTrolley = "//a[contains(text(),'Add to trolley')]";
				    public static final String btnAddToTrolley_Loc ="xpath";





//text box-JotterList(Updated)
			public static final String editboxAddToList = "(//input[@type='text'])[Index]";
			public static final String editboxAddToList_Loc = "xpath";
			public static String editboxAddToListIndex = "4";
			//Amend order button(Nanditha - Created on 07-Aug-13)
			public static final String btnAmendOrder = "//a[contains(text(),'Amend order')]";
			public static final String btnAmendOrder_Loc = "xpath";



		


			//click view items
			public static final String linkviewItems = "View items";
			public static final String linkviewItems_Loc = "linkText";

			//click all pending orders
			public static final String linkviewAllpedingOrders = "View all pending orders";
			public static final String linkviewAllpedingOrders_Loc = "linkText";
			


			 //Carousel Product select UOM(Nanditha -Updated on 03-Sep-13)
			
		public static final String drpdwnJotterUOMAttribute = "tagName";


		//Change my List link-JotterList(Nanditha -Updated on 20-sep-13)
					public static final String linkChangeList = "Edit Jotter list";
					public static final String linkChangeList_Loc = "linkText";


					//Date Layer -linkHeartIcon-Mini Trolley(Nanditha - created on 26-Sep-13)
									    public static final String lnkHeartIconMiniTrolley = "//div[3]/div/a";
									    public static final String lnkHeartIconMiniTrolley_Loc = "xpath";
									    



public static final String btnContinuToAmendOrder = "(//a[contains(text(),'Continue')])[2]";
public static final String btnConitnueToAmendOrder_Loc = "xpath";

//To favourite
public static final String linkHeartIconUnfilled_Loc = "xpath";
public static final String linkHeartIconUnfilled="//div[8]/div/div[2]/div/a";


public static final String drpdwnJotterUOM = "//div[2]/div/div[2]/div/span";
public static final String drpdwnJotterUOMvalue = "//a[contains(text(),'Each')]" ;
public static final String drpdwnJotterUOM_Loc = "xpath";

//delete link of Jotter List products-JotterList(Updated)
public static final String linkJotterListProduct = "//div[@id='content']/div[2]/div/div/ul/li[Index]/a[2]";
public static final String linkJotterListProduct_Loc = "xpath";
public static String IndexJotterListProduct = "47";


//incrementing product qty
public static final String  btnincr_Loc="linkText";
public static final String  btnincr="+";

// load more

public static final String btnloadmore = "//span[contains(text(),'Load more')]";
public static final String btnloadmore_Loc = "xpath";

//See All Products

public static final String linkSeeAllProduct = "//a[contains(text(),'See all')]";
public static final String linkSeeAllProduct_Loc = "xpath";



}
