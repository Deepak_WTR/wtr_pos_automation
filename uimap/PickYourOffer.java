package uimap;

import java.util.ArrayList;

public class PickYourOffer {

	
		//Introduction Text - By:prabhakaran Sankar
	//	public static final String intText = "//p[contains(text(),'As a myWaitrose member we want to give you more of what you love, so you can now')]";
		public static final String intText = "//p[contains(text(),'These offers are available')]";
		public static final String intText_Loc = "xpath";
		
		//Waitrose brand Image 1 - By: prabhakaran Sankar
		public static final String imageWaitroseCard = "//img[@class='myWaitroseCard'] ";
		public static final String imageWaitroseCard_Loc = "xpath";
		
		//Waitrose brand Image 2 - By: prabhakaran Sankar
		public static final String imageWaitroseBasket = "//img[@class='proOfferBasket']";
		public static final String imageWaitroseBasket_Loc = "xpath";	
		
		//FAQ - By:prabhakaran Sankar
		public static final String linkFAQ="FAQ";
		public static final String linkFAQ_Loc="LinkText";
		
		//Terms & Conditions - prabhakaran Sankar
		public static final String linkTermsandConditions="Terms & Conditions";
		public static final String linkTermsandConditions_Loc="LinkText";
		
		//verify MiniTrolley - prabhakaran Sankar
		public static final String checkMinitrolley="//div[@id='trolley-wrapper']";
		public static final String checkMinitrolley_Loc="xpath";
		
		//verify BreadCrumb_Home - prabhakaran Sankar
	//			public static final String breadCrumb_Home="(//a[contains(text(),'Home')])[2]";
				public static final String breadCrumb_Home="//ul[@class='breadcrumb']//a[contains(text(),'Home')]";
				public static final String breadCrumb_Home_Loc="xpath";
				
		//verify BreadCrumb_Groceries - prabhakaran Sankar
	//			public static final String breadCrumb_Groceries="(//a[contains(text(),'Groceries')])[3]";
				public static final String breadCrumb_Groceries="//ul[@class='breadcrumb']//a[contains(text(),'Groceries')]";
				public static final String breadCrumb_Groceries_Loc="xpath";
				
		//verify BreadCrumb_MyFavourites - prabhakaran Sankar
	//			public static final String breadCrumb_MyFavourites="(//a[contains(text(),'My Favourites ')])[1]";
				public static final String breadCrumb_MyFavourites="//ul[@class='breadcrumb']//span[contains(text(),'Pick Your Own offers')]";
				public static final String breadCrumb_MyFavourites_Loc="xpath";
		
		//verify BreadCrumb_MyOffer - prabhakaran Sankar
				public static final String breadCrumb_MyOffers="//span[contains(text(),'Pick Your Own offers')]";
				public static final String breadCrumb_MyOffers_Loc="xpath";
				
		//SelectOfferButton - prabhakaran Sankar
		public static final String btnSelectOffer="//div[@class='l-content']//a[@title='Pick Offer']";
		public static final String btnSelectOffer_Loc="xpath";
		
		//SelectOfferButton from Recommendation- prabhakaran Sankar
				public static final String btnSelectOfferSuggestions="//div[@class='products-grid']//a[@title='Pick Offer']";//"//div[@class='recommendedOffers']//a[@title='Pick Offer']";
				public static final String btnSelectOfferSuggestions_Loc="xpath";
				
		//SelectOfferButton - prabhakaran Sankar
				public static final String offerSelectedSuggestions="//div[@class='currentPYOOfferStatus']//span[contains(text(),'Picked')]";//"//div[@class='recommendedOffers-grid']//span[contains(text(),'Picked')]";
				public static final String offerSelectedSuggestions_Loc="xpath";
		
		//SelectOfferButton - prabhakaran Sankar
		public static final String btnSelectOfferDisabled="(//div[@class='button content-button select-offer is-disabled']//a[@class='selectOffer'])[11]";
		public static final String btnSelectOfferDisabled_Loc="xpath";
		
		//SelectOfferButton - prabhakaran Sankar
		public static final String offerSelected="//div[@class='l-content']//span[contains(text(),'Picked')]";
		public static final String offerSelected_Loc="xpath";
		
		//Added PYO Item Name - prabhakaran Sankar
		public static final String addedPYOItemName="//div[@class='pyoOfferSelected']//div[@class='m-product-details-container']";
		public static final String addedPYOItemName_Loc="xpath";	
		
		//Added PYO Item UOM - prabhakaran Sankar
		public static final String addedPYOItemUMO="//div[@class='pyoOfferSelected']//div[@class='m-product-volume']";
		public static final String addedPYOItemUMO_Loc="xpath";	
		
		//Added PYO Item Was Value - prabhakaran Sankar
		public static final String addedPYOItemWasValue="//div[@class='pyoOfferSelected']//span[@class='was-price']";
		public static final String addedPYOItemWasValue_Loc="xpath";
		
		//Added PYO Item trolley Price - prabhakaran Sankar
		public static final String addedPYOItemTrolleyPrice="//div[@class='pyoOfferSelected']//span[@class='price trolley-price']";
		public static final String addedPYOItemTrolleyPrice_Loc="xpath";
		
		// Link for viewing Product Detail Page
		public static final String linkAddedPYOoffer = "//div[@class='pyoOfferSelected']//div[@class='offerDetails']//a[@title='PYO Offer Detail']";
		public static final String linkAddedPYOoffer_Loc = "xpath";
				
		//Add Button-EIHB
		public static final String buttonAdd = "button add-button";
		public static final String buttonAdd_Loc = "className";
	
		//FavoriteOffer Tab
		public static final String linkFavOnOffer="Favourites on offer";
		public static final String linkFavOnOffer_loc="LinkText";
								
		//FavoriteOffer Tab
		public static final String linkFreshOffer="Fresh";
		public static final String linkFreshOffer_loc="LinkText";


		//Brand price check box
		public static final String chkBrandPrice="Brand price match";
		public static final String chkBrandPrice_loc="name";
	
public static String FavItemName ="";	
public static ArrayList selectedItem = new ArrayList(10);


public static ArrayList removeItem = new ArrayList(10);

// submit selection 

public static final String btnSubmitSelection = "//a[@class='disableSave']";
public static final String btnSubmitSelection_Loc = "xpath";



//header In submit selection overlay 

//public static final String headerSaveSelectionOverlay = "//div[@class='saveConfirm']//h2[contains(text(),'Are you happy with your selection?')]";
//public static final String headerSaveSelectionOverlay_Loc = "xpath";

public static final String headerSaveSelectionOverlay = "//div[@class='saveConfirm aligned']";
public static final String headerSaveSelectionOverlay_Loc = "xpath";

// Overlay Yes confirmation 

//public static final String btnYesconfirmation = "//a[contains(text(),'Yes')]";
//public static final String btnYesconfirmation_Loc = "xpath";

public static final String btnYesconfirmation = "//div[@class='saveConfirm aligned']//a[contains(text(),'Confirm')]";
public static final String btnYesconfirmation_Loc = "xpath";

//Overlay Yes confirmation 

public static final String btnCancelconfirmation = "//div[@class='button content-button']//a[contains(text(),'Cancel')]";
public static final String btnCancelconfirmation_Loc = "xpath";


//selected offer products

public static final String tabSelectedOfferProduct = "//div[@class='pyoOfferSelected']";
public static final String tabSelectedOfferProduct_Loc = "xpath";




//pick all button from suggested picks

public static final String btnPickAll = "//a[@class='addAll']";
public static final String btnPickAl_Loc = "xpath";


//offers available to pick 

public static final String tabofferProducts = "//div[@class='l-content pyo-all-offers']";
public static final String tabofferProducts_Loc = "xpath";

//Suggestion for you  

public static final String tabSuggestionForYou = "//div[@class='recommendedOffers']";
public static final String tabSuggestionForYou_Loc = "xpath";

//suggestion for you picked 


public static final String txtSuggestionPicked = "//div[@class='recommendedOffers']//span[@class='picked']";
public static final String txtSuggestionPicked_Loc = "xpath";

//offers -  picked 


public static final String txtOffersPicked = "//div[@class='l-content pyo-all-offers']//span[@class='picked']";
public static final String txtOffersPicked_Loc = "xpath";

//Register your card button

public static final String btnRegisterYourCard = "//div[@class='button content-button register-card']";
public static final String btnRegisterYourCard_Loc = "xpath";

//join-waitrose button

public static final String btnJoinMyWaitrose = "//div[@class='button content-button join-waitrose']";
public static final String btnJoinMyWaitrose_Loc = "xpath";

//Suggestions count

public static final String PYOSuggestions = "//div[@class='recommendedOffers']//div[@class='m-product-cell']";
public static final String PYOSuggestions_Loc = "xpath";
						
//Add button list
	public static final String btnAddlist="//div[@class='products-grid pyo-selected-offers']//div[@class='button add-button']/a";
	public static final String btnAddlist_loc="xpath";
	//Qty list
	public static final String txtqtylist="//div[@class='products-grid pyo-selected-offers']//div[@class='quantity-append with-cta']/input";
	public static final String txtqtylist_loc="xpath";
	
	public static final String linkEspot="//div[@class='pyo-offer-submission']//div[@class='genericESpot']";
	public static final String linkEspot_loc="xpath";
	
	public static final String breadcrumbGroceries="//span[contains(text(),'Groceries')]";
	public static final String breadcrumbGroceries_loc="xpath";
}
