package uimap;

public class UserRegisteration {
	//Grocery Link
	public static final String linkGrocery="Groceries";
	public static final String linkGrocery_loc="linkText";
	//Grocery tab
	public static final String linkGrocerytab="//div[@id='headerShoppingSection']//a[contains(.,'Groceries')]";
	public static final String linkGrocerytab_loc="xpath";

	//Registration page Heading
	public static final String txtRegHdng="//h2[contains(text(),'Enter your details to get started')]";
	public static final String txtRegHdng_loc="xpath";
	
	//Email Lookup overlay page Heading
//	public static final String txtEmailLookupOverlay = "//h1[contains(text(),'Enter your email to sign in or register')]";
	public static final String txtEmailLookupOverlay = "logon-email";
	public static final String txtEmailLookupOverlay_loc="id";
	
	//Email text field
	public static final String txtEmail="logon-email";
	public static final String txtEmail_loc="id";
	// Radio button of No-like to register.
	public static final String rdbliketoReg="pass-0";//"password_group"
	public static final String rdbliketoReg_loc="id";//"name"; 
	// Waitrose account Heading
	public static final String txtRegAccHdng="//h2[contains(text(),'Register for a Waitrose.com account')]";
	public static final String txtRegAccHdng_loc="xpath";
	//Confirm Email Password text field
	public static final String txtConfirmEmail="email-confirm";
	public static final String txtConfirmEmail_loc="id";
	//Password text field
	public static final String txtPassword="logon-password-choose";
	public static final String txtPassword_loc="id";
	
	//Login password
	  public static final String txtLoginPassword = "logon-password";
	    public static final String txtLoginPassword_loc ="id";
	//Checkbox of Terms&Conditions
	public static final String chkTerms="terms";
	public static final String chkTerms_loc="id";

	//Welcome message text
	public static final String txtWelcomeMsg="//h4[@id='welcomeback']";
	public static final String txtWelcomeMsg_loc="xpath";
	//Not You Link
	public static final String lnkNotYou="//h4[@id='welcomeback']/a[contains(text(),'Not you?')]";
	public static final String lnkNotYou_loc="xpath";
	//Text message for New to Waitrose.com?
	public static final String txtNewtoWaitorse="//h4[contains(text(),'New to Waitrose.com?')]";
	public static final String txtNewtoWaitorse_loc="xpath";

	//Yes radio button (Created on 050813)
	public static final String rdbYes="pass-1";
	public static final String rdbYes_loc="id";



	//New UserRegistration Link(Nanditha Updated on 20-Sep-13)
	//public static final String lnkNewUserReg="New to Waitrose? Register";
	public static final String lnkNewUserReg="Register";
	public static final String lnkNewUserReg_loc="linkText";
	
	public static final String btnSign_Register = "//div[@id='headerSignInRegister']/button[@title='Sign in/Register']";
	public static final String btnSign_Register_loc = "xpath";

	
	//new over lay start shopping image - 6.2 Changes
	public static final String startShopping_overlay = "//*[@id='js-browse-shop-guide']";
	public static final String startShopping_overlay_loc = "xpath";
	
	public static final String btnstartShopping_Register = "//a[contains(text(),'Start shopping')]";
	public static final String btnstartShopping_Register_loc = "xpath";

	public static String txtmyWaitroseCard_loc = "id";
	public static String txtmyWaitroseCard = "logon-myWaitroseCard";

	public static String rdoTempCard_loc = "id";
	public static String rdoTempCard = "temp_card";

	public static String rdoPermanentCard_loc = "id";
	public static String rdoPermanentCard = "perm_card";

	// DetailsPage Heading in Test3 Env (Nirmal-4-Oct-13)

	public static String txtUserDetailsHdng_loc = "xpath";
	public static String txtUserDetailsHdng = "//h1[contains(text(),'Please Confirm and Complete your Details')]";

	public static String txtScreenName_loc = "id";
	public static String txtScreenName = "screenName";

	public static String btnWtrAddrSubmit_loc = "id";
	public static String btnWtrAddrSubmit = "myWaitroseAddressSubmit";
	
	public static String btnWtrAddrSaveNCont_loc = "id";
	public static String btnWtrAddrSaveNCont = "myWaitroseActivateSubmit";

	public static String btnWtrContinue_loc = "id";
	public static String btnWtrContinue = "myWaitroseCont";

	public static String txtMyWtrRegHdng_loc = "xpath";
	public static String txtMyWtrRegHdng = "//h1[contains(text(),'You are now registered for myWaitrose online')]";

	//No Thanks radio button
	public static final String rdoNothanks="//label[@for='logon-waitrose-2']";
	public static final String rdoNothanks_loc="xpath";




	//Register Your card Updated(Nanditha - Created on 15-Oct-13)
	public static final String btnRegisterYourCardUpdated_Loc = "//div[@id='content']/div[2]/div[3]/div/div[3]/div/div/div/map/area[@href='/shop/MyWaitroseCard']";
	public static final String btnRegisterYourCardUpdated = "xpath";

		//Nanditha -Updated on 15-Oct-13)
	public static String txtLinkMyWtrHdng_loc = "xpath";
	public static String txtLinkMyWtrHdng = "//div/h1[contains(text(),'Your temporary myWaitrose card is now registered online')]";

	//No Thanks rado button Reg User (Nanditha - created on 21-Oct-13)
	public static final String chkBoxNoThanks = "pass-2";
	public static final String chkBoxNoThanks_Loc = "id";													
	//Sumbit No Thanks for Reg User (Nanditha - created on 21-Oct-13)
	public static final String btnSub = "myWaitroseActivationSubmit";
	//Continue button for mywaitrose
	public static final String btnContinuemyWaitrose="Continue";
	public static final String btnContinuemyWaitrose_loc="linkText";	
	public static final String btnSub_Loc = "id";

	public static String txtTempCardHdng_loc = "xpath";
	public static String txtTempCardHdng = "//h1[contains(text(),'Print your temporary myWaitrose card')]";//h1[contains(text(),'Print your temparory myWaitrose card')]";

	//mike OF
	public static String rdoSignmeUp_loc = "xpath";
	public static String rdoSignmeUp = "//label[@for='logon-waitrose-0']";
	public static String rdoHaveWtrCard_loc = "xpath";
	public static String rdoHaveWtrCard = "//label[@for='logon-waitrose-1']";	



	//Continue button Updated
	public static final String btnContinueUpdated = "//div[@class='waitrose-card-overlay']//a[contains(text(),'Continue')]";					
	public static final String btnContinueUpdated_Loc = "xpath";

	//Continue button updated on 060514
	public static final String btnContinue="logon-button-register";
	public static final String btnContinue_loc="id";
	
	//Sigin button
	public static final String btnSignIn="logon-button-sign-in";
	public static final String btnSignIn_loc="id";



	//Continue button updated on 060514
	public static final String btnContinue1="myWaitroseActivationSubmit";
	public static final String btnContinue1_loc="id";

	//No Thanks Radio button
	public static String rdoNoThanks_loc = "id";
	public static String rdoNoThanks = "logon-waitrose-2";


	public static String rdoNoThanksOF_loc = "id";
	public static String rdoNoThanksOF = "pass-2";

	//Updated button register your card.(created Lekshmi July)
	public static String btnNewRegisterYourCard_loc = "xpath";
	public static String btnNewRegisterYourCard = "//a[contains(@href, '/shop/MyWaitroseCard')]";

	//Pending Order Warning Message(Kathir - Created on 300714)
	public static final String txt_PendingOrderError = "//div[@class='address-warning pendingOrderMessage']/p";
	public static final String txt_PendingOrderError_loc = "xpath"; 																																	

	// Waitrose Online Registration in Test3 Env (Nirmal- 4-Oct-13)
	public static String btnPrint_loc = "linkText";
	public static String btnPrint = "Print temporary card";



	public static String btnRegisterYourCard_loc = "xpath";
	public static String btnRegisterYourCard = "//img[@title='Register myWaitrose']";
	


	// waitrose registration promotional text

	public static final String txtpromotionalinfo = "//div[@class='terms']//p[contains(text(),'At Waitrose, we have exciting offers and news about our products')]";
	public static final String txtpromotionalinfo_Loc = "xpath";

	// waitrose Email option information text

	public static final String txtemailinfo = "//div[@class='terms']//p[contains(text(),'If you would prefer not to hear from us, you can stop receiving our updates at any time by')]";
	public static final String txtemailinfo_Loc = "xpath";


	//waitrose checkbox - marketing waitrose 

	public static final String inputmarketingwaitrose = "//input[@id='marketingPrefWaitrose']";
	public static final String inputmarketingwaitrose_Loc = "xpath";

	//waitrose checkbox - marketing johnlewis 

	public static final String inputmarketingjohnlewis = "//input[@id='marketingPrefJohnLewis']";
	public static final String inputmarketingjohnlewis_Loc = "xpath";

	//waitrose checkbox - marketing finance 

	public static final String inputmarketingfinance = "//input[@id='marketingPrefFinance']";
	public static final String inputmarketingfinance_Loc = "xpath";

	// waitrose - johnlewis membership card text

	public static final String txtjlmembership = "//p[contains(text(),'If you have a my John Lewis membership card')]";
	public static final String txtjlmembership_Loc = "xpath";

	public static String lnkcontinue_loc="xpath";
	public static String lnkcontinue="//input[@value='Continue']";
	
	public static final String passowordOverlay = "logon-password";
	public static final String passowordOverlay_loc = "id";
	
	public static final String btnSignInOverlay = "logon-button-sign-in";
	public static final String btnSignInOverlay_loc = "id";
	
	//PostCode Modal 
	
		public static final String inputPostcodeModal = "//input[@class='input-field postcode js-postcode-lookup-text']";
		public static final String inputPostcodeModal_loc = "xpath";
		
		public static final String btnPostcodeModal = "//input[@class='postcode-check primary-cta button js-postcode-lookup']";
		public static final String btnPostcodeModal_loc = "xpath";
		
		public static final String btnPostcodePage = "//input[@class='button primary-cta postcode-check']";
		public static final String btnPostcodePage_loc = "xpath";
		
		public static final String inputPostcodePage = "logon-postcode";
		public static final String inputPostcodePage_loc = "id";
		
		
		public static final String btnPostcodeModalBookSlot = "book-slot";
		public static final String btnPostcodeModalBookSlot_loc = "id";
		
		public static final String btnPostcodeModalRegisterAndBookSlot = "book-slot";
		public static final String btnPostcodeModalRegisterAndBookSlot_loc = "id";
		
}

