package uimap;

public class BranchFinder {
		
	//Standard Opening Timing ( created by Jayandran on 8/1/2015)

	public static final String txtStandardOpeningTiming="//a[text()='Standard opening times']";
	public static final String txtStandardOpeningTiming_loc="xpath";

	//SeeMyBranchOnMap ( created by Jayandran on 8/1/2015)
	public static final String linkSeeMyBranchOnMap="//a[text()='See my branch on a map']";
	public static final String linkSeeMyBranchOnMap_loc="xpath";
	
	//Services Available Text ( created by Jayandran on 8/1/2015)
	public static final String textServicesAvailable="//*[contains(text(),'Services available')]";
	public static final String textServicesAvailable_loc="xpath";
	
	//Click And Collect Link ( created by Jayandran on 8/1/2015)
	public static final String linkClickAndCollect="//div[@class='jl-c-and-c-content']";
	public static final String linkClickAndCollect_loc="xpath";
	
	//Services And Facilities Text ( created by Jayandran on 8/1/2015)
	public static final String textServicesAndFacility="//*[text()='Services and facilities']";
	public static final String textServicesAndFacility_loc="xpath";
	
	//Customer Notice Text ( created by Jayandran on 8/1/2015)
	public static final String txtCustomerNotice="//div[@class='customer-notice']";
	public static final String txtCustomerNotice_loc="xpath";
	
	
	
	//Delivery Information Text ( created by Jayandran on 8/1/2015)
	public static final String txtDeliveryInformation="//*[text()='Delivery information']";
	public static final String txtDeliveryInformation_loc="xpath";
	
	
}
