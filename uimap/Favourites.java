package uimap;

public class Favourites {


	//Content Management Text 1-EIHB
	public static final String txtContentManagement1 = "//div[@class='l-content']/p";
	public static final String txtContentManagement1_Loc = "xpath";
	
	//Content Management Text 2-EIHB
		public static final String txtContentManagement2 = "//div[@class='r-content']/p[1]";
		public static final String txtContentManagement2_Loc = "xpath";
	
		//Content Management Text 3-EIHB
		public static final String txtContentManagement3 = "//div[@class='r-content']/p[2]";
		public static final String txtContentManagement3_Loc = "xpath";
		
		//Add Button-EIHB
		public static final String buttonAdd = "button add-button";
		public static final String buttonAdd_Loc = "className";
		
		
		//ProductGrid-MyFavourites
		public static final String gridFavouriteProducts = "jotter-result products products-grid";
		public static final String gridFavouriteProducts_Loc = "className";
		
		
		
		//Everything I Have Bought tab
		public static final String tabEIHB = "//div[@id='favourites-page']/div[@class='tabs general-tabs']/ul[@class='tab-panel']/li[2]";
		public static final String tabEIHB_Loc = "xpath";
		
	    //My favourites Products
		public static final String linkMyFavouritesProduct = "//div[@id='favourites-page']/div/div/div[2]/div[iCategoryIndex]/div/div/div/div/div/a[2]";
		public static final String linkMyFavouritesProduct_Loc = "xpath";

		//HeartIcon-MyFavourites
		public static final String linkFillHeartIcon="(//a[contains(text(),'Add to Favourites')])[Index]";
		public static final String linkFillHeartIcon_Loc= "xpath";
		
		
		public static final String linkUnFillHeartIcon="/a[contains(text(),'Remove from Favourites')]";
		public static final String linkUnFillHeartIcon_Loc="xpath";
		
		//ProductName-Regular Expression
		public static final String linkFavProductName="//a[contains(text(),'Name')]";
		public static final String linkFavProductName_Loc = "xpath";
		
		//EIHB Product Row
		public static final String linkEIHBProductRow = "//div[@class='jotter-result products products-grid']/div";
		public static final String linkEIHBProductRow_Loc = "xpath";
		
		//EIHB Products
		public static final String linkEIHBProducts = "//div[@class='jotter-result products products-grid']/div[RowIndex]/div[CellIndex]";
		public static final String linkEIHBProducts_Loc = "xpath";
		
		//EIHB Add button
		public static final String buttonAddEIHB="(//a[contains(text(),'Add')])[Index]";
		public static final String buttonAddEIHB_Loc = "xpath";
		
        //EIHB Product Name 
		public static final String linkEIHBProductName = "//div[@class='jotter-result products products-grid']/divRowIndex]/div[CellIndex]/div/div/div[2]/a";
		public static final String linkEIHBProductName_Loc = "xpath";
		
		//Did You Forget Interstitial
		public static String ProductFromInterstitial = "";
		
		//Did you forget Interstitial Heading
		public static final String txtInterstitialHeading = "//div[@class='IntHeaderMsg']/h1";
		public static final String txtInterstitialHeading_Loc = "xpath";
		
		//Did you forget Interstitial Product
		public static final String linkInterstitialProductGrid = "//div[@id='content']/div/div/div[2]/tr/div/div[RowIndex]/div[CellIndex]/div/div/a/img";
		public static final String linkInterstitialProductGrid_Loc = "xpath";
		
		//Interstitial Add button
		public static final String buttonAddInterstitial="(//a[contains(text(),'Add')])[Index]";
		public static final String buttonAddInterstitial_Loc = "xpath";
		
		//Favourite Prd Count (Nanditha Created on 30-July-13)
				public static String FavPrdCount ="";
				
				
				//Product Line Number-My Favourites(Nanditha - Created on 02-Aug-13)
				public static final String txtProductLineNumber = "//div[@class='jotter-result products products-grid'][GridIndex]/div[@class='products-row'][RowIndex]/div[@class='m-product-cell'][CellIndex]/div/div/a";
				public static final String txtProductLineNumber_Loc = "xpath";
				public static final String txtProductLineNumberAttribute = "src";






				//My Favourites-Product Grid (Nanditha - Created on 02-Aug-13)
				public static final String linkMyFavProductGrid = "//div[@class='jotter-result products products-grid']";
				public static final String linkMyFavProductGrid_Loc = "xpath";
				
				//My Favourites-Product Row (Nanditha - Created on 02-Aug-13)
				public static final String linkMyFavProductRow = "//div[@class='jotter-result products products-grid'][GridIndex]/div[@class='products-row']";
				public static final String linkMyFavProductRow_Loc = "xpath";



				//HeartIcon to be filled (Nanditha - created on 02-Aug-13)
				public static final String linkFillHeartIcon1="(//a[contains(text(),'Add to Favourites')])";
				public static final String linkFillHeartIcon1_Loc= "xpath";




				//Heart icon filled -Single Product(Nanditha - Created on 02-Aug-13)
				public static final String linkUnFillHeartIcon2="//a[contains(text(),'Remove from Favourites')]";
				public static final String linkUnFillHeartIcon2_Loc="xpath";
				
				//HeartIcon-MyFavourites
				public static final String linkFavHeartIcon="(//a[contains(text(),'Add to Favourites')])";
				public static final String linkFavHeartIcon_Loc= "xpath";
				//Heart icon filled -Product with Index (Nanditha -created on 06-Aug-13)
				public static final String linkUnFillHeartIcon1="//a[contains(text(),'Remove from Favourites')][Index]";
				public static final String linkUnFillHeartIcon1_Loc="xpath";
				
				//Heart icon for specific product(14082013)
				public static final String imgHeartForSpecificPrd="//div[@class='m-product-details-container']/a[@class='m-product-open-modal'][contains(text(),'ValToReplace')]/../../div[@class='m-product-labels']/a";
				public static final String imgHeartForSpecificPrd_loc="xpath";
				//QTY TEXT
				public static final String txtQty="//input[@class='quantity-input'][@value='1']";
				public static final String txtQty_loc="xpath";	
				
				public static final String linkUnFillHeartIconmytrolley="//a[contains(text(),'Toggle favourite')]";
				//unFav product in My Trolley(Nanditha - Created on 24-Sep-13)
				public static final String unFavPrd = "//div[@class='m-product-labels']/a[@class='label favourite']";
				public static final String unFavPrd_Loc = "xpath";


				//HeartIcon to be filled (Nanditha - created on 02-Dec-13)
				public static final String linkFillHeartIcon2="Add to Favourites";
				public static final String linkFillHeartIcon2_Loc= "link";	
				

				
				//Import now button(Nanditha - Created on 13-Jan-14)
				public static final String btnImportProducts = "import-products";
				public static final String btnImportProducts_Loc = "id";
				
				public static final String buttonAddFav="//a[@title='Add a single item to trolley']";//Add";
				public static final String buttonAddFav_Loc="xpath";

public static final String linkAddMyFavourites="//div/a[@title='Add a single item to trolley']";
		public static final String linkAddMyFavourites_Loc = "xpath";

//My Products
				public static final String linkMyproducts="My Products";
				public static final String linkMyproducts_loc="xpath";
				
				//Num product count Update(Update Gajapathy on 040814)
				public static final String prdcount="//div[@class='m-product-cell']";//div[@class='jotter-result products products-grid']/div/div[@class='m-product-cell']";
				public static final String prdcount_loc="xpath";
				

				//My favorite tab
				
								public static final String linkMyFav="My Favourites";
								public static final String linkMyFav_loc="linkText";
								//My fav on Offer
								public static final String linkOffonFav="Favourites on offer";
								public static final String linkOffonFav_loc="linkText";
								//Explore By category
								public static final String txtExploreByCatg="(//h3[contains(text(),'Explore by category')])[1]";
								public static final String txtExploreByCatg_loc="xpath";
								//Refined by
								public static final String txtRefinedBy="(//h3[contains(text(),'Refine by')])[1]";
								public static final String txtRefinedBy_loc="xpath";
								
								//Offer filter section
								public static final String txtOfferFilter="(//div[@id='filter-section-rolled'])[1]/ul/li/label";
								public static final String txtOfferFilter_loc="xpath";
								//clear all
								public static final String linkClearAll="(//a[contains(text(),'clear all')])[1]";
								public static final String linkClearAll_loc="xpath";
								

								//Offer Filter(Updated By Jayandran 3/12/2014)
								public static final String chkOfferFilter="(//input[@name='On_Offer'])[1]";//(//input[@id='prop_8192'])[1]";//(//input[@class='tickable' and @name='On Offer'])[1]";";
								public static final String chkOfferFilter_loc="xpath";
								
								//specific Category Type links
								public static final String linkspecifiCategorytype="(//a[contains(text(),'data')])[1]";
								public static final String linkspecifiCategorytype_loc="xpath";
								
								//user generated fav
								public static final String usrGenFav="//div[@class='l-content']/div/div[@class='products-row']/div";
								public static final String usrGenFav_loc="xpath";

								//USER AND SYSTEM GENERATED PRODUCTS
								public static final String sysusrFav="//div[@class='l-content']/div/div[@class='products-row']/div";
								public static final String sysusrFav_loc="xpath";

								//User generated favorites
								public static int userGenFavts=0;

								//system and user generated favoutites
								public static int numUsersysGenFav=0;
								//
								public static String userGenFav="";
								//user generated offer products
								public static int userGenOfferFavts=0;
								

								

								//First product Favourite label
								public static final String linkFirstHeartLabel="(//a[contains(text(),'Remove from Favourites')])[1]";
								public static final String linkFirstHeartLabel_loc="xpath";

								//Offer Hearrt Label
								public static final String linkOfferHeart="//div[@class='l-content']/div/div[@class='products-row']/div//a[@title='Add to Favourites']";
								public static final String linkOfferHeart_loc="xpath";

								//FavoriteOffer Tab
								public static final String linkFavOnOffer="Favourites on offer";
								public static final String linkFavOnOffer_loc="LinkText";
								
								//Favorite on Offer Tab
								public static final String linkFavOnOffertab="//a[contains(text(),'Favourites on offer')]";
								public static final String linkFavOnOffertab_loc="xpath";
								
								//FavoriteOffer Tab
								public static final String linkFreshOffer="//nav[@class='refinement offers']//a[contains(text(),'Fresh')]";
								public static final String linkFreshOffer_loc="xpath";

								//offer bread crumb created on 05-nov-2014
								public static final String linkOfferBreadcrumb="//div[@class='breadcrumbs standard']/ul/li[4]";
								public static final String linkOfferBreadcrumb_loc="xpath";


								//Offer products
								public static final String Ofrprdcount="//a[@class='label offer']";
								public static final String Ofrprdcount_loc="xpath";

								//Brand price check box
								public static final String chkBrandPrice="Brand price match";
								public static final String chkBrandPrice_loc="name";
								
								
								public static int categoryProducts;
								
								//specific category text

								
								
								//specific category text

								public static final String txtSpecificCategory="(//h4[@class='current'])[1]";
								public static final String txtSpecificCategory_loc="xpath";

								//ADDED specific category PRODUCTS

								public static int tXTtTotSpecificCategoryPrds=0;
								
								//specific product
								public static final String linkProd="(//div[@class='m-product-cell'])";
								public static final String linkProd_loc="xpath";
								
								//specific product qty text
								public static final String txtPrd="//div/div[2]/div/div[@class='quantity-append with-cta']/input";
								public static final String txtPrd_loc="xpath";
								
								//specific add button
								public static final String btnaDD="//div/div[2]/div/div[@class='button add-button']/a";
								public static final String btnaDD_LOC="XPATH";
								
								//List Products 
								public static final String myListProduct="//div[@class='m-product-title title']//a";
								public static final String myListProduct_LOC="XPATH";
								public static String[] productNames;
								
								//Pick Your Own Offers Tab 
								public static final String pickYourOwnOffers="//a[@class ='pyoOffer']";
								public static final String pickYourOwnOffers_Loc="XPATH";
								
								//filter option in Fav tab
								public static final String filterdropdown="//select[@id='favtype']";
								public static final String filterdropdown_Loc="XPATH";
								
								//Explore By category panel in PYO tab
								public static final String pyoExploreByCategorySection="//div[@id='pyoOffer']/div[3]/div/nav/ul/li";
								public static final String pyoExploreByCategorySection_Loc="XPATH";
								
								
}
