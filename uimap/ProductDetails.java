package uimap;

import java.util.ArrayList;
import java.util.List;

public class ProductDetails {

	//Product Detail Page UI
	//product quantity 
	public static final String txtDetailQty = "quantity";
	public static final String txtDetailQty_Loc ="name";
	//increase product
	public static final String btnDetailIncrease ="//*[@id='plus-1']";
	public static final String btnDetailIncrease_Loc = "xpath";
	//decrease product
	public static final String btnDetailDecrease = "//*[@id='minus-1']";
	public static final String btnDetailDecrease_Loc ="xpath";

	//select unit of measure
	public static final String ddbtnDetail="unitOfMeasure";
	public static final String ddbtnDetail_Loc="id";
	
	
	//default unit of measure
			public static final String defaultUnitOfMeasure="//select[@id='unitOfMeasure']//option[@selected]";
			public static final String defaultUnitOfMeasure_Loc="xpath";
	//close 
	public static final String linkClose="Close";
	public static final String linkClose_Loc="linkText";


	public static final String linkProductGrid="//div[@class='jotter-result products products-grid']/div[@class='products-row'][RowIndex]/div[@class='m-product-cell'][CellIndex]";
	public static final String linkProductGrid_Loc = "xpath";




	//Product Name -Product Data Page(Nanditha -Created on 06-Aug-13)
	public static final String txtProductName = "//div[@class='l-content']/h1/em";
	public static final String txtProductName_Loc ="xpath";
	//Remove from mY favorite link
	public static final String linkRemoveFromMyFav="//a[contains(text(),'Remove from My Favourites')]";
	public static final String linkRemoveFromMyFav_loc="xpath";


	public static final String txtSearchResult="//div[@class='r-content']/p";
	public static final String txtSearchResult_loc="xpath";


	//OnOffer filter
	public static final String chkOfferfilter="On_Offer";//div[@id='filter-section-rolled']/ul/li/input[@id='On_Offer/true']";
	public static final String chkOfferfilter_loc="name";

	// add to favourites updated on 130913
	public static final String linkAddtomyFavourites ="Add to Favourites";
	public static final String linkAddtomyFavourites_Loc ="linkText";



	//Heart Icon(Nanditha Created on 26-July-13)(Gajapathy updated on160913)
	public static final String linkProductUnFav="Remove from Favourites";
	public static final String linkProductUnFav_Loc = "linkText";

	//Offer link in product details page
	public static final String linkOffer="//a[@class='offer'][@title]";
	public static final String linkOffer_loc="xpath";



	//deliver to new address
	public static final String linkDeliver="Deliver to a new address";
	public static final String linkDeliver_Loc="partialLinkText";
	//deliver new postcode
	public static final String txtDeliverPostCode = "edit_postcode_existing_address";
	public static final String txtDeliverPostCode_Loc ="id";
	//check postcode
	public static final String linkCheckPostCode = "(//input[@value='Check postcode'])[2]";
	public static final String linkCheckPostCode_Loc ="xpath";

	//Product Data Page Favourite Link(updated on 24-Sep-13)
	public static final String linkProductFav="//a[contains(text(),'Add to Favourites')]";
	public static final String linkProductFav_Loc = "xpath";


	//my Waitrose promotion (Nanditha - Created on 09-Oct-13)
	public static final String imgMyWaitrose = "//a[@class='myWaitrose offer']";
	public static final String imgMyWaitrose_Loc = "xpath";


	//my Waitrose prd Selling price(Nanditha - Created on 07-Nov-13)
	public static final String prdSellingPrice = "//p[@class='price']/strong";
	public static final String prdSellingPrice_Loc = "xpath";
	

	public static int prd1SP = 0;
	public static int prd2SP =0;
	public static int prd3SP =0;

	//my Waitrose prd Name (Nanditha - Created on 07-Nov-13)
	public static final String prdName = "//h1/em";
	public static final String prdName_Loc = "xpath";
	public static String prdName1 = "";
	public static String prdName2 = "";

	//add to trolley updated
	public static final String btnDetailAddtoTrolley ="//a[contains(text(),'Add to trolley')]";//"Add to trolley";
	public static final String btnDetailAddtoTrolley_Loc="xpath";//"linkText";

	//link 2013
	public static final String linkSlot="201";
	public static final String linkSlot_Loc="partialLinkText";

	//Product Name link-Mini Trolley Flyout(Nanditha - Created on 06-Aug-13)(updated Lekshmi june 2014)
	public static final String lnkProductName1 = "//div[8]/div/div/div/p/a";
	public static final String lnkProductName1_Loc = "xpath";
																																											
																																											
																																											//Select unit of measure
																																											public static final String linkUOM="unitOfMeasure";
																																											public static final String linkUOM_loc="id";
																																											


	//Ask A question link
	public static final String btnAskaQuestion="//div/button[contains(text(),'Ask a question')]";
	public static final String btnAskaQuestion_loc="xpath";
	//Ask a question form
	public static final String formAskQtn="//div[@id='bv-mbox-questionSubmission1-list']";//h2[@class='bv-mbox-breadcrumb']/span[2]";
	public static final String formAskQtn_loc="xpath";
	//Cancel Qtn form
	public static final String linkCloseQtnForm="//button[@name='Cancel']";
	public static final String linkCloseQtnForm_loc="xpath";
	//QuestionSummary
	public static final String txtQtnSummary="questionsummary";
	public static final String txtQtnSummary_loc="name";
	//Nick Name
	public static final String txtNickName="usernickname";
	public static final String txtNickName_loc="name";
	//user location
	public static final String txtLocation="userlocation";
	public static final String txtLocation_loc="name";
	//user Mail
	public static final String txtuserMail="useremail";
	public static final String txtuserMail_loc="name";
	//Post Question Button
	public static final String btnPostQtn="//button[contains(text(),'Post Question')]";
	public static final String btnPostQtn_loc="xpath";



	//Back link above product image(Kathir- Created on 10-jul-14)
	public static final String lnkbackaboveproduct = "//div[@class='two-col product-detail']//a[@title='Back']";				
	public static final String lnkbackaboveproduct_loc = "xpath";

	//Nutrition table(Kathir - created on 17-jul-14)
	public static final String tblNutrition = "//table[@class='nutrition']";
	public static final String tblNutrition_loc = "xpath";

	//Nutrition table Column Name (Kathir - created on 17-jul-14)
	public static final String trcolmnName = "//table[@class='nutrition']/tbody/tr[1]/th";

	//Nutrition table first row (Kathir - created on 17-jul-14)
	public static final String trFirstRowNut = "//table[@class='nutrition']/tbody/tr[2]/*";

	//Ingredients title (Kathir - Created on 18-jul-14)
	public static final String titleIngredients = "//div[@class='l-content product-info']/h2[text()='Ingredients']";
	public static final String titleIngredients_loc = "xpath";

	//Ingredients contents (Kathir - Created on 18-jul-14)
	public static final String txtIngredients = "//div[@class='l-content product-info']/h2[text()='Ingredients']/following-sibling::p[1]";
	public static final String txtIngredients_loc = "xpath";

	// Product image (Kathir - Created on 21-jul-14)
	public static final String imgProduct = "//img[@class='main-image']";
	public static final String imgProduct_loc = "xpath";

	//At a glance title (Kathir - Created on 21-jul-14)
	public static final String titleAtAGlance = "//h2[text()='At a glance']";
	public static final String titleAtAGlance_loc = "xpath";

	//At a glance contents (Kathir - Created on 21-jul-14)
	public static final String txtAtAGlance = "//h2[text()='At a glance']/following-sibling::ul[1]/li[1]/span";
	public static final String txtAtAGlance_loc = "xpath";

	//First Age label's (Kathir - Created on 21-jul-14)
	public static final String lblAge = "(//dd[@class='bv-author-userinfo'])[1]/ul/li[1]/span";
	public static final String lblAge_loc = "xpath";

	//First Gender Label's (Kathir - Created on 21-jul-14)
	public static final String lblGender = "(//dd[@class='bv-author-userinfo'])[1]/ul/li[2]/span";
	public static final String lblGender_loc = "xpath";					

	//First Author name link(Kathir - Created on 21-jul-14)
	public static final String lnkAuthor = "(//span[@itemprop='author'])[1]/h3";
	public static final String lnkAuthor_loc = "xpath";

	//Number of Review text in Overlay(Kathir - Created
	public static final String txtReview = "//div[@class='bv-control-bar-count']/span";
	public static final String txtReview_loc = "xpath";


	//Offer logo

	public static final String imgOfferLogo="//div/a[@class='offer']";
	public static final String imgOfferLogo_loc="xpath";
	//multiple promotions for waitrose
	public static final String linkMywaitroseOff="//p/a[@class='myWaitrose offer']";
	public static final String linkMywaitroseOff_loc="xpath";
	//multiple promotions for buy some get some
	public static final String linkAddsomeget="(//p[@class='offer-container']/a)[2]";
	public static final String linkAddsomeget_loc="xpath";

	//future product offer link
	public static final String linkfuturePrd="//p[@class='offer-container']/a";
	public static final String linkfuturePrd_loc="xpath";
	//Available future offer
	public static final String txtAvlFutOffPrdDate="//div[@class='heading']/p";
	public static final String txtAvlFutOffPrdDate_loc="xpath";


	//Feature label
	public static final String txtFeatureLabel="//div[@class='labels']/a[@class='feaOffer']";
	public static final String txtFeatureLabel_loc="xpath";
	//Feature link
	public static final String linkFeature="(//a[contains(text(),'Featured Product')])";
	public static final String linkFeature_loc="xpath";
	//message Offer
	public static final String linkMesgOffer="//p[@class='offer-container']/a[2]";
	public static final String linkMesgOffer_loc="xpath";
	//firstofferlink
	public static final String linkFOffer="//p[@class='offer-container']/a[1]";
	public static final String linkFOffer_LOC="xpath";
	//multi promotion section(Updated on 070814)
	public static final String linkPromosec="//p[@class='offer-container']/a";
	public static final String linkPromosec_loc="xpath";

	//Shelf Life Image in PDP(Created by Jayandran Nov 2014)				
	public static final String imgShelfLife = "//*[contains(@class,'shelf')]";
	public  static final String imgShelfLife_Loc = "xpath";

	//Use By Date Field in PDP(Created by Jayandran Nov 2014)				
	public static final String txtUseByDate = "//div[@class=usebydate']";
	public  static final String txtUseByDate_Loc = "xpath";

	//Select Product Size Radio Button in PDP(Created by Jayandran Nov 2014)				
	public static final String btnProductSize = "(//fieldset[@class='radiogroup']//input)[1]";
	public  static final String btnProductSize_Loc = "xpath";
	
	


//Created By Jayandran
	public static double pdpPrice =0;


// Offer Valid till date text in PDP page(Created by Jayandran Jan 2015) - product details page
	
	public static final String txtValidtilldateoffer = "//p[contains(text(),'Available for delivery or collection until')]";
	public static final String txtValidtilldateoffer_Loc = "xpath";

	// Item value
	public static final String Itemamount = "//p[contains(text(),'Available for delivery or collection until')]";
	public static final String Itemamount_Loc = "xpath";
	
	// Storing Variable 
	public static double presistentItemPrice;
	public static double trolleyPrice;
	
	
	public static final String listdefaultUOM = "//select[@id='unitOfMeasure']//option[@selected]";
	public static final String listdefaultUOM_Loc = "xpath";
	

	public static final String tabNutritionTable = "//table[@class='nutrition nutrition-table']//tr";
	public static final String tabNutritionTable_Loc = "xpath";

	public static List<String> nutAttribute = new ArrayList();
	

public static final String txtunavailable = "//p[@class='product-unavailable']";
public static final String txtunavailable_loc = "xpath";


public static final String tabNutrition = "//table[@class='nutrition nutrition-table']";
public static final String tabNutritiosn_Loc = "xpath"; 

public static final String txtcountryoforigin = "//table[@class='nutrition nutrition-table']";
public static final String txtcountryoforigin_Loc = "xpath"; 

public static final String txtcooking = "//table[@class='nutrition nutrition-table']";
public static final String txtcooking_Loc = "xpath"; 

public static final String breadCrumb = "//span[contains(text(),'Search results:')]";
public static final String breadCrumb_Loc = "xpath"; 
	
public static final String btnContinueShoppingOverlay = "Continue shopping";
public static final String btnContinueShoppingOverlay_loc = "linkText";

public static final String btnBookSlotOverlay = "Book a slot";
public static final String btnBookSlotOverlay_loc = "linkText";

public static final String clickSeasonalSlot= "New Year";
public static final String clickSeasonalSlot_loc = "linkText";

}
