package uimap;

public class ProductModalModule {

	
	//decrease product
	public static final String btnModalDecrease ="//a[contains(@id, 'minus-')]";
	public static final String btnModalDecrease_Loc ="xpath";
	//select unit of measure
	public static final String ddbtnModal="//select[contains(@class, 'quantity-select')]";
	public static final String ddbtnModal_Loc="xpath";
	//Add personal shopper
	public static final String txtAreaModalPersonalShopper ="//textarea";
	public static final String txtAreaModalPersonalShopper_Loc ="xpath";
	//Save
	public static final String linkSave ="Save";
	public static final String linkSave_Loc ="linkText";
	//Remove from trolley
	public static final String linkRemoveFromTrolley = "Remove from trolley";
	public static final String linkRemoveFromTrolley_Loc = "linkText";
	
	//Close
	public static final String linkClose ="Close";
	public static final String linkClose_Loc ="linkText";

	
	//Heat Icon(created on 26-July-13)
		public static final String linkFillHeartIcon="(//a[contains(text(),'Add to Favourites')])";
		public static final String linkFillHeartIcon_Loc= "xpath";
		
		
		public static final String linkUnFillHeartIcon="(//a[contains(text(),'Remove from Favourites')])";
		public static final String linkUnFillHeartIcon_Loc="xpath";
		
		//Product Name (Nanditha - created on 26-July-13)
		public static final String linkProductName = "//em";
		public static final String linkProductName_Loc = "xpath";
		//Add to my favorites
				public static final String linkAddtoFavorite="//*[@id='trolley-information']/p[2]/a";
				public static final String linkAddtoFavorite_loc="xpath";
				
				//Add to Trolley button(Nanditha - Updated on 20-Aug-13) updated mike
				public static final String btnAddToTrolley = "Add to trolley";
				public static final String btnAddToTrolley_Loc = "linkText";
				
				//product quantity
				public static final String txtModalQtyReg ="quantity";
				public static final String txtModalQtyReg_Loc="name";

	//click product
	public static final String linkProduct_Loc="xpath";
	public static final String linkProduct="//a[contains(text(),'prodname')]";
	

	//increase product (Updated on 09-Sep-13)
		public static final String btnModalIncrease ="//a[contains(@id, 'plus-1')]";
		public static final String btnModalIncrease_Loc ="xpath";



		

		//Producct qty in model
			public static final String txtQtyModel="//*[@id='trolley-information']/div[1]/input";
			public static final String txtQtyModel_loc="xpath";

		//to be updated 0n140813 updated on 130913
						public static final String linkFillHeartIconUpdated = "//*[@id='trolley-information']/p[2]/a";//(//a[contains(text(),'Add to My Favourites')])";
						public static final String linkFillHeartIconUpdated_Loc = "xpath";

						//Remove from mY favorite link updated on 180913
										public static final String linkRemoveFromMyFav="//p/a[contains(text(),'Remove from Favourites')]";//a[contains(text(),'Remove from My Favourites')]";
										public static final String linkRemoveFromMyFav_loc="xpath";

										

											//product quantity
											public static final String txtModalQty ="//input[@name='quantity']";//"//input[contains(@class, 'quantity-input')]";
											public static final String txtModalQty_Loc="xpath";	

											//Gajapathy -Updated on 270114
											public static final String lnkFillHeartIconUpdated1 = "//p/a[@title='Add to Favourites']";
											public static final String lnkFillHeartIconUpdated1_Loc = "xpath";
											

											
											
											//To get product Modal page first product name(Kathir - created on 14-jul-14)
											public static String str_prevFirstprod = "";
											
											
											
											//Lazy load image icon(Kathir - created on 15-Jul-14)
											public static final String imgLazyLoad = "//div[@class='load-more-button']/div/div[@class='loading panel']/img[@alt='[loading]']";
											public static final String imgLazyLoad_Loc = "xpath";
											
											//Product in Product Modal Page(Kathir - created on 15-Jul-14)
											public static final String individualProduct = "//div[@class='m-product-cell']";
											
											//link to view product details page(Kathir - Updated on 07-Aug-14)
											public static final String lnkViewProductDetails = "//a[@id='viewproduct']/em";
											public static final String lnkViewProductDetails_Loc = "xpath";
																			

											

											//Feature label
																						public static final String txtFeatureLabel="//div[@class='labels']/a[@class='feaOffer']";
																						public static final String txtFeatureLabel_loc="xpath";
																						//Feature link
																						public static final String linkFeature="(//a[contains(text(),'Featured Product')])[3]";
																						public static final String linkFeature_loc="xpath";
																						//message Offer
																						public static final String linkMesgOffer="//div[@class='price-box']/a[2]";
																						public static final String linkMesgOffer_loc="xpath";
																						//Offer logo
																					    public static final String imgOfferLogo="(//div/a[@class='offer'])[2]";
																						public static final String imgOfferLogo_loc="xpath";
																						//Offer link
																						//message Offer
																						public static final String linkOffer="//div[@class='price-box']/a[1]";
																						public static final String linkOffer_loc="xpath";
																						
																						//Element to verify disable of product in grid(Kathir created on 170914)
																						public static final String disableProduct = "(//div[@class='m-product-cell'])[1]/div";
																						
																						//first product link from product list(Kathir - Updated on 18-Sep-14)
//																						public static final String lnkFirstProduct = "(//div[@class='m-product-details-container'])[1]/a[@class='m-product-open-modal']";
																						public static final String lnkFirstProduct = "(//div[@class='m-product-details-container'])[1]/a[@class='m-product-open-details']";
																						public static final String lnkFirstProduct_Loc = "xpath";
													


}
