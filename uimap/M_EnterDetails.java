package uimap;

public class M_EnterDetails {

	public static final String txtTitle = "(//div[@class='inputHolder']/input)[6]";
	public static final String txtTitle_loc = "xpath";
	
	public static final String txtFirstname = "(//div[@class='inputHolder']/input)[7]";
	public static final String txtFirstname_loc = "xpath";
	
	public static final String txtLastname = "(//div[@class='inputHolder']/input)[8]";
	public static final String txtLastname_loc = "xpath";
	
	public static final String txtnickname = "(//div[@class='inputHolder']/input)[9]";
	public static final String txtnickname_loc = "xpath";
	
	public static final String txtPostcode = "//input[@class='inputBox']";
	public static final String txtPostcode_loc = "xpath";
	
	public static final String btnAddr = "//button[contains(text(),'Find address')]";
	public static final String btnAddr_loc = "xpath";
	
	public static final String selectaddress = "//span[contains(text(),'1')]";
	public static final String selectaddress_loc = "xpath";
	
	public static final String txtPhoneNo = "(//div[@class='inputHolder']/input)[16]";
	public static final String txtPhoneNo_loc = "xpath";
	
	public static final String btnJoinmyWr = "//button[contains(text(),'Join myWaitrose')]";
	public static final String btnJoinmyWr_loc = "xpath";
	
	public static final String txtTitlenew = "(//div[@class='inputHolder']/input)[4]";
	public static final String txtTitlenew_loc = "xpath";
	
	public static final String txtFirstnamenew = "(//div[@class='inputHolder']/input)[5]";
	public static final String txtFirstnamenew_loc = "xpath";
	
	public static final String txtLastnamenew = "(//div[@class='inputHolder']/input)[6]";
	public static final String txtLastnamenew_loc = "xpath";
	
	public static final String txtnicknamenew = "(//div[@class='inputHolder']/input)[7]";
	public static final String txtnicknamenew_loc = "xpath";
	
	public static final String txtPostcodenew = "//input[@class='inputBox']";
	public static final String txtPostcodenew_loc = "xpath";
	
	public static final String btnAddrnew = "//button[contains(text(),'Find address')]";
	public static final String btnAddrnew_loc = "xpath";
	
	public static final String selectaddressnew = "//span[contains(text(),'4')]";
	public static final String selectaddressnew_loc = "xpath";
	
	public static final String txtPhoneNonew = "(//div[@class='inputHolder']/input)[14]";
	public static final String txtPhoneNonew_loc = "xpath";
	
	public static final String btnSaveAddressnew = "//button[contains(text(),'Save address')]";
	public static final String btnSaveAddressnew_loc = "xpath";
	
}
