package uimap;

public class M_SearchProduct {

	public static final String btnStartShopping = "//button[contains(text(),'Start Shopping')]";
	public static final String btnStartShopping_loc = "xpath";
	
	public static final String btnSearch = "(//button[@class='search'])[2]";
	public static final String btnSearch_loc = "xpath";
	
	public static final String txtSearch = "//input[@id='dojox_mobile_TextBox_0']";
	public static final String txtSearch_loc = "xpath";
	
	public static final String btnadd = "(//button[contains(text(),'Add')])[1]";
	public static final String btnadd_loc = "xpath";
	
	public static final String btnno = "(//input[@class='mblTextBox'])[1]";
	public static final String btnno_loc = "xpath";
	
	public static final String btnaddproduct = "//i[@class='fa fa-plus']";//"(//span[@class='mblColorDefault mblToolBarButtonBody mblToolBarButtonLightText mblToolBarButtonText btnPrimaryCTA incrementButton'])[1]";
	public static final String btnaddproduct_loc = "xpath";
	
	public static final String btncheckout = "(//button[@class='btnCheckout'])[2]";
	public static final String btncheckout_loc = "xpath";
}
