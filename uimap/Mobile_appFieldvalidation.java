package uimap;

public class Mobile_appFieldvalidation {

	public static final String itemprice = "//android.widget.LinearLayout[@index='3']/android.widget.TextView[@index='0']";
	public static final String itemprice_Loc = "xpath";
	
	public static final String itemName = "//android.widget.TextView[@index='1']";
	public static final String itemName_loc = "xpath";
	
	public static final String itempricefield = "//android.widget.LinearLayout[@index='2']/android.widget.TextView[@index='0']";
	public static final String itempricefield_loc = "xpath";
	
	public static final String savingsfield = "//android.widget.LinearLayout[@index='2']/android.widget.TextView[@index='1']";
	public static final String savingsfield_loc = "xpath";
	
	public static final String itemtotalfield = "//android.widget.LinearLayout[@index='2']/android.widget.TextView[@index='2']";
	public static final String itemtotalfield_loc = "xpath";
	
	public static final String overallTotalfield = "//android.widget.LinearLayout[@index='7']/android.widget.TextView[@index='1']";
	public static final String overallTotalfield_loc = "xpath";
	
	public static final String overallsavingsfield = "//android.widget.LinearLayout[@index='7']/android.widget.LinearLayout[@index='0']/android.widget.TextView[@index='1']";
	public static final String overallsavingsfield_loc = "xpath";
	
	public static final String overallsavingsfieldtrolley = "//android.widget.LinearLayout[@index='3']/android.widget.LinearLayout[@index='0']/android.widget.TextView[contains(@text,'Savings')]";
	public static final String overallsavingsfieldtrolley_loc = "xpath";
	
	
	//android.widget.LinearLayout[@index='7']/android.widget.LinearLayout[@index='0']/android.widget.TextView[@index='1']";
	public static final String itemQty = "//android.widget.LinearLayout[@index='7']/android.widget.LinearLayout[@index='0']/android.widget.TextView[@index='0']";
	public static final String itemQty_loc = "xpath";
	//android.widget.TextView[contains(text(),'item')]
	public static final String itemQtyTrolleypage = "//android.widget.LinearLayout[@index='0']/android.widget.LinearLayout[@index='3']/android.widget.LinearLayout[@index='0']/android.widget.TextView[contains(@text,'Item')]";
	public static final String itemQtyTrolleypage_loc = "xpath";
	
	public static final String itemsaving = "//android.widget.LinearLayout[@index='3']/android.widget.TextView[@index='1']";
	public static final String itemsaving_Loc = "xpath";
	
	public static final String itemTotal = "//android.widget.LinearLayout[@index='3']/android.widget.TextView[@index='2']";
	public static final String itemTotal_Loc = "xpath";
	
	public static final String overalltotal = "//android.widget.LinearLayout[@index='7']/android.widget.TextView[@index='1']";
	public static final String overalltotal_Loc = "xpath";
	
	public static final String trolleypageoveralltotal = "//android.widget.LinearLayout[@index='3']/android.widget.TextView[@index='1']";
	public static final String trolleypageoveralltotal_Loc = "xpath";
	
	public static final String offerDescription = "//android.widget.LinearLayout[@index='0']/android.widget.TextView[@index='5']";
	public static final String offerDescription_loc = "xpath";
	
}
