package uimap;

public class OFStockAllocatedLineMaintenance {
	
		//Christmas Option	
		public final static String dropdownChristmas="//*[contains(text(),'Christmas')]";
		public final static String dropdownChristmas_Loc="xpath";
		
		//NewYear Option	
		public final static String dropdownNewYear="//*[contains(text(),'New Year')]";
		public final static String dropdownNewYear_Loc="xpath";
		
		//Select Dropdown
		public final static String dropdownSlot ="seasonId";
		public final static String dropdownSlot_Loc ="name";
		
		//LineNumber Field
				public final static String txtLineNumber="itemLineNumber";
				public final static String txtLineNumber_Loc="id";			
			
				//searchButton
				public final static String btnSearchLineNumber="search";
				public final static String btnSearchLineNumber_Loc="name";
				
				//NumberofCases Field
				public final static String txtNumberOfCases="numberOfCases";
				public final static String txtNumberOfCases_Loc="id";
			
				//Value of the NumberofCases
				public static String txtNumberOfCasesValue = "0";				
				
				//Save Button
				public final static String btnSave ="save";
				public final static String btnSave_Loc ="name";

}
