package uimap;



public class OfHome {

	//customer search link
	public final static String linkCustsearch="Customer Search";
	public final static String linkCustsearch_Loc="linkText";
		
	//search order by slot
	public final static String linkSearchOrderBySlot="Search / Orders By Slot";
	public final static String linkSearchOrderBySlot_Loc="linkText";
	
	 
	
	//WaitroseEntertaining
		public static final String linkWtrEntertain="WaitroseEntertaining";
		public static final String linkWtrEntertain_loc="linkText";
		
		
		//home page link
		public final static String linkHomepage="//a[contains(text(),'Home')]";
		public final static String linkHomepage_Loc="xpath";
		
//		//customer search link
//		public final static String linkDailySlot="Daily Slots";
//		public final static String linkDailySlot_Loc="linkText"; 
				
				
				
				public final static String linkSignOff="//a[contains(@href, '/wdeliver/app/admin?op=logoff')]";
				public final static String linkSignOff_Loc="xpath";

		
				
		//WaitroseEntertaining
				public final static String linkWaitroseEntertaining = "WaitroseEntertaining";
				public final static String linkWaitroseEntertaining_Loc = "linkText";

		// Find Messages link (Created by : Prabhakaran Sankar)
				public final static String linkFindMessage = "Find Messages";
				public final static String linkFindMessage_Loc = "linkText";
				

				//home page - Shop in branch we deliver link(Jayandran on 27/11/2014)(updated by kathir on 22/02/2016)
								/*public final static String linkShopInBranchWeDeliver="delivery-service";
								public final static String linkShopInBranchWeDeliver_Loc="id";*/
				public final static String linkShopInBranchWeDeliver="//a[text()='Shop in Branch we deliver']";
				public final static String linkShopInBranchWeDeliver_Loc="xpath";

					//EquipmentLoanOrder
								public final static String linkEquipmentLoanOrder="Equipment Loan Order";
								public final static String linkEquipmentLoanOrder_Loc="linkText";

//WaitroseEntertaining phone order
								public static final String linkWtrEntertainphoneorder="WaitroseEntertaining Phone Order";
								public static final String linkWtrEntertainphoneorder_loc="linkText";
								
								// Shop in Branch we deliver Phone Order
								
								public static final String linkWtrShopinBranchwedeliverPhoneOrder = "Shop in Branch we deliver Phone Order";								

								public static final String linkWtrShopinBranchwedeliverPhoneOrder_Loc = "linkText";
								
								// Waitrose entertaining search - Prabhakaran Sankar - 14/March/2015
								
								public static final String linkWaitroseEntertainingSearch = "WtrEntertaining Line Search";								

								public static final String linkWaitroseEntertainingSearch_Loc = "linkText";
								
								// Branch Information Link - Prabhakaran Sankar - 19/March/2015
								
								public static final String linkBranchInformation = "Branch Information";								

								public static final String linkBranchInformation_Loc = "linkText";

								
								//customer search link
								public final static String linkDailySlot="Daily Slots";
								public final static String linkDailySlot_Loc="linkText";								

//Delivery slot maintanance
public static final String linkDeliverySlotMaintenance="Delivery Slot Maintenance";
public static final String linkDeliverySlotMaintenance_loc="linkText";
//Heading Delivery slot maintanance
public static final String hdngDelSlotMaintenance="//td[contains(text(),'Slot Admin Homepage')]";
public static final String hdngDelSlotMaintenance_loc="xpath";


//search order by Route
public final static String linkSearchOrderByRoute="Search / Orders By Route";
public final static String linkSearchOrderByRoute_Loc="linkText";

//delivery Dashboard
public final static String linkDeliveryDashboard="* DELIVERY DASHBOARD (ADMIN) - DRIVER HANDSET *";
public final static String linkDeliveryDashboard_Loc="linkText";

//public final static String webPortalUrl="https://ws6.microlise.com/WAITROSE_WEB_PORTAL/";
public final static String webPortalUrl="https://wswaitrose.microlise.com/WAITROSE_WEB_PORTAL/";


}
