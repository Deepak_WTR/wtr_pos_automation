package uimap;

public class M_PaymentDetails {
	 
	public static final String btnContinue = "//button[contains(text(),'Continue to payment')]";
	public static final String btnContinue_loc = "xpath";
	
	public static final String txtcardno = "//div[contains(text(),'Number on the front of the card')]/preceding-sibling::input";
	public static final String txtcardno_loc = "xpath";

	public static final String dropdownmonth = "(//select[@class='btnPrimaryDrpDwn'])[3]";
	public static final String dropdownmonth_loc = "xpath";
	
	public static final String dropdownyear = "(//select[@class='btnPrimaryDrpDwn'])[4]";
	public static final String dropdownyear_loc = "xpath";
	
	public static final String txtcardname = "//div[contains(text(),'Name as printed on card')]/preceding-sibling::input";
	public static final String txtcardname_loc = "xpath";
	
	public static final String placeorder = "//button[contains(text(),'Place your order')]";
	public static final String placeorder_loc = "xpath";
	
	public static final String enablecard = "//div[contains(text(),'Save new payment card details')]/preceding-sibling::div";
	public static final String enablecard_loc = "xpath";
	
	public static final String orderno = "//button[contains(text(),'Place your order')]";
	public static final String orderno_loc = "xpath";
	
	public static final String getReferenceno = "(//div[@class='WRgrid2 sectionSubHead']//div[contains(text(),'Order reference')])[2]//following-sibling::div//div";
	public static final String getReferenceno_loc = "xpath";
}


