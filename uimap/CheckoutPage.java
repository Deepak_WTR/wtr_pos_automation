package uimap;

public class CheckoutPage {

	
	//Text message
	public static final String txtHdngInCheckout="//h2[contains(text(),'Please select an address for delivery')]";
	public static final String txtHdngInCheckout_loc="xpath";
	

	//contiune shopping
	public static final String btnContinue="Continue shopping";
	public static final String btnContinue_loc="linkText";

		


		//Link Checkout(Nanditha - Updated on 22-Jan-14)
			public static final String btnCheckout="//input[@value='Checkout']";
			public static final String btnCheckout1 = "//div[@id='button-checkout']/a";
			public static final String btnCheckout_loc="xpath";
			
}
