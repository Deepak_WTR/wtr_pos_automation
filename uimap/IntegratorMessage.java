package uimap;

public class IntegratorMessage {

	//Select Text field to enter date
		public static final String textfieldEnterDate ="//input[@value='ccyy-mm-dd']";
		public static final String textfieldEnterDate_Loc ="xpath";

	//Select Text field to enter date
		public static final String textfieldEnterOrderNumber ="//input[@name='messageContent']";
		public static final String textfieldEnterOrderNumber_Loc ="xpath";

	//Select Text field to enter date
		public static final String selectSearchButton ="(//input[@value='search'])[2]";
		public static final String selectSearchButton_Loc ="xpath";
	
	//verify Integrator message
		public static final String verifyMessage ="//td[contains(text(),'No Error Found')]";
		public static final String verifyMessage_Loc ="xpath";
	
	//click error message number link
		public static final String linkErrorMessageNo ="//td[@class='text']//a";
		public static final String linkErrorMessageNo_Loc ="xpath";
		
		//Select Message type
		public static final String messageType="messageType";
		public static final String messageType_Loc="name";
	
	//verify order number in xml
	
		public static final String textOrderreferancenumber = "//textarea[@name='messageContent']";
		//public static final String textOrderreferancenumber = "//span[contains(text(),'Your order confirmation - " +Allocator.GlblVar+"')]";
		public static final String textOrderreferancenumber_Loc ="xpath";
	
}
