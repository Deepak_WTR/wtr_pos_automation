package uimap;

import allocator.Allocator;

public class OF_SearchOrderByRoute {

	// Select Dropdown - Branch Name
	public final static String txtBranchNameDropdown = "//select[@name='branch_rn']";
	public final static String txtBranchNameDropdown_Loc = "xpath";

	// Select Dropdown - Branch Name - Option values
	public final static String txtBranchNameOptionDropdown = "//select[@name='branch_rn']//option";
	public final static String txtBranchNameOptionDropdown_Loc = "xpath";
	
	// Select No routes Image Icon
		public final static String noRoutePlusImageIcon = "//img[contains(@id,'togRouteImg_-1')]";
		public final static String noRoutePlusImageIcon_Loc = "xpath";
		

		// Collection Image Icon
			public final static String collectPlusImageIcon = "//img[contains(@id,'togRouteImg_-2')]";
			public final static String collectPlusImageIcon_Loc = "xpath";
		
		public static String verifyOrderNumber = "//a[contains(text(),'" +Allocator.GlblVar+"')]"; 
		
		public static String tableDataList = "//*[@id='route_ords_-1']/table";
		public static String tableDataList_Loc = "xpath";
		
		//Search Order
		public static final String btnSrchOrderRoute="//img[@title='Click here to start your search.']";
		public static final String btnSrchOrderRoute_loc="xpath";
		

	
}
