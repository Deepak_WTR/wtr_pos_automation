package uimap;

public class Mobile_appBackofficeLogin {

	public static final String branchNo = "branchNumber";
	public static final String branchNo_Loc = "name";
	
	public static final String userNo = "username";
	public static final String userNo_Loc = "name";
	
	public static final String pwd = "username";
	public static final String pwd_Loc = "name";
	
	public static final String clickButton = "buttonClass";
	public static final String clickButton_Loc = "class";
	
	public static final String clickAuditButton = "//div[contains(text(),'AUDIT')]";
	public static final String clickAuditButton_Loc = "xpath";
	
	public static final String clickEODRbutton = "//div[contains(text(),'END OF DAY REDUCTIONS')]";
	public static final String clickEODRbutton_Loc = "xpath";
	
	public static final String clickEODRMainbutton = "//div[contains(text(),'End Of Day Reductions Maintenance')]";
	public static final String clickEODRMainbutton_Loc = "xpath";
	
	public static final String clickAddbutton = "addButton";
	public static final String clickAddbutton_Loc = "id";
	
	public static final String eodrName = "name";
	public static final String eodrName_Loc = "id";
	
	public static final String clickNextButton = "//input[@name='nextButton']";
	public static final String clickNextButton_Loc = "xpath";
	
	public static final String enterTime = "startTime";
	public static final String enterTime_Loc = "id";
	
}
