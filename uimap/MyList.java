package uimap;

public class MyList {

	//MyList Tab
			public static final String tabMyLists ="//a[contains(text(),'My Lists')]";
			public static final String tabMyLists_Loc = "xpath";
			
			//Content Management Text
			public static final String txtContentManagement = "//div[@class='lists-container']/p";
			public static final String txtContentManagement_Loc = "xpath";
		    public static final String txtContentManagementActual = "Create custom lists of your favourite items to make shopping quicker. You will then be able to add one or all of these products to your trolley in one go.";
		    
		  
			
			//Add button - List confirmation overlay
			public static final String buttonAddToList = "//a[contains(text(),'Add to list')]";
			public static final String buttonAddToList_Loc = "xpath";
			
			//Close link - List confirmation overlay
			public static final String linkClose = "//a[contains(text(),'Close')]";
			public static final String linkClose_Loc = "xpath";
			
			//Product Name - List confirmation overlay
			public static final String linkProductName = "//div[@class='top-wrapper']/h1/a";
			public static final String linkProductName_Loc = "xpath";
			
		    //Ok Button - List confirmation overlay
			public static final String buttonOk = "//div[@class='button content-button no-image']/a";
			public static final String buttonOk_Loc = "xpath";
			
			//Product Added To List
			public static String ProductAddedToList1 ="";
			
			//Product Added To List
			public static  String ProductAddedToList2 = "";
			
			//Create List edit box 
			public static final String editboxCreateList= "//form[@id='list-create']/fieldset/div/input";
			public static final String editboxCreateList_Loc = "xpath";
			
			//Create List button
			public static final String buttonCreateList = "//input[@value='Create list']";
			public static final String buttonCreateList_Loc = "xpath";
			
			
			
			//List name table
			public static final String linkListTable = "//div[@class='lists-container']/table/tbody/tr";
			public static final String linkListTable_Loc = "xpath";
			
			//List name table
			public static final String linkListTable1 = "//div[@class='lists-container']/table/tbody";
			public static final String linkListTable1_Loc = "xpath";
			
		    //Add all to List - Trolley
			public static final String linkAddAllToList = "//a[contains(text(),'Add all to a List')]";
			public static final String linkAddAllToList_Loc = "xpath";
			
			//List confirmation overlay - create new list radio button
			public static final String radiobuttonNewList = "//input[@id='new_list']";
			public static final String radiobuttonNewList_Loc = "xpath";
			
			//List confirmation overlay - new list edit box
			public static final String editboxNewList = "//input[@id='create_new_list']";
			public static final String ediboxNewList_Loc = "xpath";
			
			//Delete button - My List Page ( can be concatenated with'linkListtable')
			public static final String buttonDelete = "/td[@class='actions']/div[2]/a";
			public static final String buttonDelete_Loc = "xpath";
			
			//List details page
			
			public static final String linkListItem = "//div[@class='products-grid products-list'][CategoryIndex]/div[RowIndex]/div[CellIndex]/div/div/div/div[@class='m-product-title title']/label/a";
			public static final String linkListItem_Loc = "xpath";
			
			//View Button - My List Page ( can be concatenated with'linkListtable')
			public static final String buttonView = "/td[@class='actions']/div[1]/a";
			public static final String buttonView_Loc ="xpath";
			
			

//Add to List Link(Gajapathy Updated on 01/08/13)
			public static final String linkAddToList = "//a[contains(text(),'Add to a list')]";
			public static final String linkAddToList_Loc = "xpath";

//OK Button - List confirmation overlay
			public static final String btnOK = "//a[@class='cancel-list-button']";
			public static final String btnOK_loc = "xpath";
			


			//Delete Button
			public static final String btnDelete="//div[@class='available-actions-bottom']/form/fieldset/div[@class='button submit-button no-image delete_selected secondary-button']/input";
			public static final String btnDelete_loc="xpath";
			//Number of products in list
			public static final String txtNumProductsInList="//div[@class='available-actions-bottom']/form/fieldset/p";
			public static final String txtNumProductsInList_loc="xpath";




//Newlist radio button in overlay
			public static final String btnNewCreateList="//input[@id='new_list']";
			public static final String btnNewCreateList_loc="xpath";
			//Create new list text area
			public static final String txtNewList="//input[@id='create_new_list']";
			public static final String txtNewList_loc="xpath";
			//List Name (Nanditha - Created on 23-Aug-13)
			public static final String lnkListName_Loc  = "linkText";

			//Add All Items in list to trolley (Nanditha - Created on 23-Aug-13)
			public static final String btnAddAllItemsInListToTrolley = "Add all items in list to trolley";
			public static final String btnAddAllItemsInListToTrolley_Loc = "linkText";
			


			//Product title in List
						public static final String txtProdTitle="//div[@class='m-product-title title']/a[contains(.,'ValueSearch')]";
						public static final String txtProdTitle_loc="xpath";
					

			//table listname
						public static final String linkLists="//tbody[@class='shoppingList-container']/tr/td/a[contains(text(),'ValuetoSearch')]";
						public static final String linkLists_loc="xpath";
						//List radio button - List confirmation overlay(Gajapathy Updated on 29/08/13)
						public static final String buttonListChoice = "//div[@class='existing-list-container list-length-short']/div[1]/input";
						public static final String buttonListChoice_Loc = "xpath";

			//First check box (updated on290813)
						public static final String chkFirstItem="//div[@id='list-ItemDetail']/div[1]/div/div/div/div/div/input";
						public static final String chkFirstItem_loc="xpath";

			//dupicate list
									public static final String txtDuplicateList="//div[@class='error-msg-static duplicate-list-error']/p";
									public static final String txtDuplicateList_loc="xpath";

			//Delete button of first list
					public static final String btnFDelete="//tbody/tr[1]/td[@class='actions']/div[2]/a";
					public static final String btnFDelete_loc="xpath";
			//btnDeleteListInPopup
				    public static final String btnDeleteListInPopup="//a[contains(text(),'Delete list')]";
				    public static final String btnDeleteListInPopup_loc="xpath";

				  //Ok Button-List confirmation overlay(Updated on 05-Sep-13)
				  			public static final String buttonOk1 = "//a[contains(text(),'Ok')]";
				  			public static final String buttonOk1_Loc ="xpath";

				  		
				  		//add to trolley disabled updated on160913
				  									
				  		public static final String txtDisableADD="//div[@class='available-actions-bottom']/form/fieldset/div[1]";//form/fieldset/div[1]";
				  	//Addto Trolley button updated on 300514
				  		public static final String btnAddtoTrolley="(//form/fieldset/div[@class='button submit-button no-image add_selected secondary-button']/input)[1]";
				  		public static final String btnAddtoTrolley_loc="xpath";
				  		
				  	//listname heading
				  		public static final String txtlistNameHd="//tr/th[1]";
				  		public static final String txtlistNameHd_loc="xpath";
				  	//listname heading
				  		public static final String txtCreatedlist="//tr/th[1]";
				  		public static final String txtCreatedlist_loc="xpath";
				  	//listname heading
				  		public static final String txtqty="//tr/th[1]";
				  		public static final String txtqty_loc="xpath";
				  		
				  	//Add All Items in list to trolley 
						public static final String btnAddAllItemsInList = "//*[text()='Add to trolley']";
						public static final String btnAddAllItemsInList_Loc = "xpath";
						
					
				  		
				  		
}
