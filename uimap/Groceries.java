package uimap;

public class Groceries {
	
	
	public static final String btnAdd="Add";
	public static final String btnAdd_Loc="linkText";
	//Shop from this order button (Nanditha - Created on 08-Aug-13)
		public static final String btnShopFromThisOrder = "//a[contains(text(),'Shop from this order')]";
		public static final String btnShopFromThisOrder_Loc = "xpath";





		//Add selected to Trolley(Shop from this order overlay)(Nanditha - Created on 08-Aug-13)
		public static final String btnAddSelectedToTrolley = "//input[@value='Add selected to trolley']";
		public static final String btnAddSelectedToTrolley_Loc = "xpath";
		
		public static final String txtprodQTY1="(//input[@value='1'])[2]";
		public static final String txtprodQTY1_Loc="xpath";

					//OrderConfirmationPage
					//total savings
					public static final String varTotalSavings="//table[2]/tbody/tr[2]/td";
					public static final String varTotalSavings_Loc="xpath";
					
					public static final String txtprodQTY="(//input[@class='quantity-input'])";
					public static final String txtprodQTY_Loc="xpath";
					

					//Check box -particular product(Shop from this order overlay)(Nanditha - Updated on 17-Oct-13)
							public static final String selectCheckBox = "//div[@class='checkbox-container']/input";
							public static final String selectCheckBox_Loc = "xpath";
							
							

							//Import now link (Nanditha - Updated on 12-Feb-14)
													public static final String lnkImportProducts = "//a[@id='importproducts']/span";
													public static final String lnkImportProducts_Loc = "xpath";

													//adding from BYG
													public static final String txtprodQTYBYG="(//input[@value='1'])[5]";
													public static final String txtprodQTYBYG_Loc="xpath";
}
