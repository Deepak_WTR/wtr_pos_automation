package uimap;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

public class OrderConfirmation {

	//OrderConfirmationPage
	//order reference number

	//click order
		public static String varOrderRefNum ="View order no. ";
		public static String varOrderRefNum_Loc="linkText";
		
		//Log out button(Nanditha - Created on 1-Aug-13)
				public static final String btnLogOut = "//a[contains(text(),'Log out')]";
				public static final String btnLogOut_Loc = "xpath";
				//Order Number (Nanditha -created on 05-Aug-13)
				public static String OrderNumber = "";
				
				
				
				// to be updated by Mike
				public static final String varTotalSavings = "//div[@id='content']/div/div/table[2]/tbody/tr[2]/td";
				public static final String varTotalSavings_Loc = "xpath";
				
				//purchase number
				public static final String txtPurchaseNumber="(//input[@id='purchaseInput'])[2]";
				public static final String txtPurchaseNumber_Loc="xpath";
				
				//continue
				public static final String btnContinue="Continue";
				public static final String btnContinue_Loc="linkText";

				
							


							//LoanItem details in order confirmaiton page (Nanditha - created on 09-Sep-13)
														public static final String txtLoanItem = "//div[@id='order-details']/div[@class='details']/p";
														public static final String txtLoanItem_Loc = "xpath";




							//Loan Item Name in order confirmation page (Nanditha - created on 11-Sep-13)
														public static final String txtloanItemName = "//div[@id='order-details']/div[@class='details']/table/tbody/tr/td";
														public static final String txtloanItemName_Loc = "xpath";



														//Estimated totals
														public static final String txtEstiTot="//h2[contains(Text(),'Estimated Totals')]";
														public static final String txtEstiTot_loc="xpath";

														
														

														//Delivery Service-Delivery Note(Nanditha - created on 18-Sep-13)
													public static final String lnkDeliveryNote = "deliveryNote";
													public static final String lnkDeliveryNote_Loc = "id";
																												
													//Order Confirmation button(Nanditha - Created on 18-Sep-13)
													public static final String btnOrderConfirmation  = "//div/div/div/div/a";
													public static final String btnOrderConfirmation_Loc = "xpath";
													
													// order Referenceno for Seasonal items collection in order Confirmation page
													public static final String VarSeasonOrderRefNum_loc = "xpath";
													public static final String VarSeasonOrderRefNum = "//*[@id='content']/div[3]/div[1]/div[2]/p[2]";
													//sub total value
													public static final String txtSubtotal="//table[2]/tbody/tr[1]/td";
													public static final String txtSubtotal_loc="xpath";
													
//MySavings
public static final String txtmySavingsP="//table[2]/tbody/tr[2]";
public static final String txtmySavingsP_loc="xpath";

public static final String txtmySavingsP_V="//table[2]/tbody/tr[2]/td";
public static final String txtmySavingsP_V_loc="xpath";
//Promocode
public static final String txtmPromocode="//table[2]/tbody/tr[4]";
public static final String txtmPromocode_loc="xpath";

public static final String txtmPromocode_V="//table[2]/tbody/tr[4]/td";
public static final String txtmPromocode_V_loc="xpath";
//Total
public static final String txtmyTotalP_V="//table[2]/tbody/tr[5]/td";
public static final String txtmyTotalP_V_loc="xpath";
//Final Total
public static final String txtmyFinalTotalP_V="//table[2]/tbody/tr[5]/td";
public static final String txtmyFinalTotalP_V_loc="xpath";

//total value
public static final String txtTotal="//table[2]/tbody/tr[2]/td";
public static final String txtTotal_loc="xpath";


//MIKE OF
public static String varOrderRefNum1_Loc="partialLinkText";	
//

//subtotal
public static final String txtSubtotalHDng="((//table[@class='order-information'])[2])/tbody/tr[1]/th";
public static final String txtSubtotalHDng_loc="xpath";
//total
public static final String txttotalHDng="((//table[@class='order-information'])[2])/tbody/tr[2]/th";
public static final String txttotalHDng_loc="xpath";
//Vochers
public static final String txtVochersHDng="((//table[@class='order-information'])[2])/tbody/tr[3]/th";
public static final String txtVochersHDng_loc="xpath";
//Giftcardtotal
public static final String txtGiftcardHDng="((//table[@class='order-information'])[2])/tbody/tr[4]/th";
public static final String txtGiftcardHDng_loc="xpath";
//Giftcardredemed
public static final String txtGiftcardRedeem="((//table[@class='order-information'])[2])/tbody/tr[5]/th";
public static final String txtGiftcardRedeem_loc="xpath";
//finalpayment
public static final String txtfinalpay="((//table[@class='order-information'])[2])/tbody/tr[6]/th";
public static final String txtfinalpay_loc="xpath";

//subtotal value
public static final String txtSubtotalValue="((//table[@class='order-information'])[2])/tbody/tr/th[contains(text(),'Subtotal:')]/../td";
public static final String txtSubtotalValue_loc="xpath";
//total value
public static final String txttotalValue="((//table[@class='order-information'])[2])/tbody/tr/th[contains(text(),'Total:')]/../td";
public static final String txttotalValue_loc="xpath";
//Vochers value
public static final String txtVocherValue="((//table[@class='order-information'])[2])/tbody/tr/th[contains(text(),'Vouchers:')]/../td";
public static final String txtVocherValue_loc="xpath";
//Giftcardtotal value
public static final String txtGiftcardTotalValue="((//table[@class='order-information'])[2])/tbody/tr/th[contains(text(),'Gift card total:')]/../td";
public static final String txtGiftcardTotalValue_loc="xpath";
//Giftcardredemed value
public static final String txtGiftcardRedeemValue="((//table[@class='order-information'])[2])/tbody/tr/th[contains(text(),'Gift card redeemed:')]/../td/span[2]";
public static final String txtGiftcardRedeemValue_loc="xpath";
//finalpayment value
public static final String txtfinalpayValue="((//table[@class='order-information'])[2])/tbody/tr/th[contains(text(),'Final Payment:')]/../td";
public static final String txtfinalpayValue_loc="xpath";


//sub Promocode value
public static final String txtPromocode="//table[2]/tbody/tr[2]/td";
public static final String txtPromocode_loc="xpath";
//prototal value
public static final String txtTotalPro="//table[2]/tbody/tr[3]/td";
public static final String txtTotalPro_loc="xpath";

//FINAL total value (updated on 100614
public static final String txtfinalPayment="//table[2]/tbody/tr[3]/td";
public static final String txtfinalPayment_loc="xpath";

//proFinal total value(Updated on 100614
public static final String txtfinalPaymentPro="//table[2]/tbody/tr[4]/td";
public static final String txtfinalPaymentPro_loc="xpath";

//Shops_since_90_days dom path(kathir - created on 010714
public static final String txtShopsSince90DaysDOM = "window.tmParam.shops_since_90_days";

//MYWaitroseCustomer dom path(kathir - created on 030714
public static final String txtMYWaitroseCustomerDOM = "window.tmParam.myWaitrose_Customer";

//Delivery Address(Kathir - Created on 280714)
public static final String tdDeliveryAddress="//th[text()='Delivery Address:']/following-sibling::td[1]";
public static final String tdDeliveryAddress_loc="xpath";

//Billing Address(Kathir - Created on 280714)
public static final String tdBillingAddress="//th[text()='Billing Address']/following-sibling::td[1]";
public static final String tdBillingAddress_loc="xpath";	

public static String str_DeliveryAddress = "";
public static String str_BillingAddress = "";

//Personal Shopper Note (Nanditha - Created on 09-Sep-13)

public static final String txtPersonalShopperNote = "//div[@id='order-details']/div[2]/table/tbody/tr[Index]/td[3]/p";
public static final String txtPersonalShopperNote_Loc = "xpath";



											


//order reference numberOF

	public static final String varOrderNumberOF="//div[@id='content']/div[1]/div[1]/table[1]/tbody/tr[1]/td";
	public static final String varOrderNumberOF_Loc="xpath";
	
	//Order Confirmation button DElivery conformation pafe (Prabhakara - Created on 18-Nov-14)
		public static final String btnOrderConfirmationDeliveryConformation  = "//div[@id='content']/div/table/tbody/tr/td";
		public static final String btnOrderConfirmationDeliveryConformation_Loc = "xpath";
		

		//Order Confirmation Page - Slot Details (Jayandran Sampath)
						public static String txtSlotDetails;
						
						//Order Confirmation Page - Your Name (Jayandran Sampath)
						public static String txtYourName;



			//UseByDate Validation (Jayandran - Created on 11-Nov-14)
			public static final String txtUseByDate = "(//*[contains(text(),'Use by')])[1]";
			public static final String txtUseByDate_Loc = "xpath";

			//Size & Serve Field Validation (Jayandran - Created on 11-Nov-14)
			public static final String txtSizeAndServe = "(//*[contains(text(),'Serves')])[1]";
			public static final String txtSizeAndServe_Loc = "xpath";

			//Line Number Field Validation (Jayandran - Created on 11-Nov-14)
			public static final String txtLineNumber = "(//tr/td/div)[1]";
			public static final String txtLineNumber_Loc = "xpath";

			//Max Price Field Validation (Jayandran - Created on 11-Nov-14)
				public static final String txtMaxPrice = "(//*[contains(text(),'Max')])[1]";
				public static final String txtMaxPrice_Loc = "xpath";
				
				//Total Num of Products
				public static final String txtTotalProducts="//td[@class='product-name']";
				public static final String txtTotalProducts_loc="xpath";
				
				//Gift Voucher value

				public static final String txtVoucher="(//tr/th[contains(text(),'Vouchers')]/../td)[1]";
				public static final String txtVoucher_loc="xpath";
				
				//Collection note
				public static final String txtCollectionNote="(//tr/th[contains(text(),'Collection Note')]/../td)[1]";
				public static final String txtCollectionNote_loc="xpath";
				
				
				//Total value
				public static final String txtItemTotal="(//tr/th[@class='nowrap' and contains(text(),'Total')]/../td)[1]";//"(//tr/th[contains(text(),'Total')]/../td)[1]";
				public static final String txtItemTotal_loc="xpath";
				
			public static String total="";
				

				//Promo code value in Order conf

				public static final String txtPromo="(//tr/th[contains(text(),'Promotion Code')]/../td)[1]";
				public static final String txtPromo_loc="xpath";


				public static String latestOrder="";

				public static String txtdatetime="";
				
				//Delivery Note(Jayandran on 17/12/2014)

				public static final String txtDeliveryNote="(//tr/th[contains(text(),'Delivery Note:')]/../td)[1]";
				public static final String txtDeliveryNote_loc="xpath";
				
				//APD HEADING
				public static final String hdngAPD="//div[@id='checkout_page']/div/h2";
				public static final String hdngAPD_LOC="xpath";
				
				//APD MESSAGE
				public static final String txtAPD="//div[@id='checkout_page']/div/p/span";
				public static final String txtAPD_LOC="//div[@id='checkout_page']/div/p/span";
				
				
				public static final String varOrderNumber="//tr//th[contains(text(),'reference number')]/../td";//"//div[@id='content']/div/div/table/tbody/tr/td";
				
				public static final String varOrderNumber_Loc="xpath";
				
				
				

				// order confirmation product list
							
							public static final String tabproductorderconfirmation  = "//*[@class='details']";
							public static final String tabproductorderconfirmation_Loc  = "xpath";


				// order confirmation total amount 
							
							public static String totalpayment = "";
							//Deliver charge value
						public static String deliveryvalue="";


				// return date for loan items
							
							public static final String txtreturndateforloanitems = "(//div[@class='details']//*[contains(text(),'loan items to be collected with the rest of your shopping. Please return them')])[1]";
							public static final String txtreturndateforloanitems_Loc = "xpath";
							

							// partner discount 
										
										public static final String txtpartnerdiscount = "//h2[contains(text(),'Partner discount')]";
										public static final String txtpartnerdiscount_Loc = "xpath";
										
// carry bag  
										
										public static final String txtcarrybag = "//th[contains(text(),'Carrier bag charge:')]";
										public static final String txtcarrybag_Loc = "xpath";
				// delivery charge 
										
										public static final String txtDCinOC ="//span[contains(text(),'Delivery charge')]";
										public static final String txtDCinOC_Loc = "xpath";
										
										//delivery charge value
										
										public static final String txtdcvalue="(//tr/th/span[contains(text(),'Delivery charge')]/../../td)[1]";
										public static final String txtdcvalue_loc="xpath";
										
										//Order Confirmation - Item Total										
										public static final String txtItemTotalValue="//tr//th[text()='Estimated total*:']/../td";//"//tr//th[text()='Item Total:']/../td";
										public static final String txtItemTotalValue_loc="xpath";
										
										//Order Confirmation - Order Summary Table
//										public static final String txtOrderSummaryTable="(//tr//td[@class='product-name'])";//"//div[@id='order-details']//tbody//tr";
										public static final String txtOrderSummaryTable="//div[@id='order-details']//tbody//tr";
										public static final String txtOrderSummaryTable_loc="xpath";
										
										//Order Confirmation - Order Summary Table
										public static final String txtOrderSummaryTableProductCategory="(//tr//td[@class='product-name'])/../td[4]";
										public static final String txtOrderSummaryTableProductCategory_loc="xpath";
										
										public static String[] productQty = new String[25];
										
		//Order number 
										
										public static  String txtFirstOrder="";
										public static  String txtSecondOrder="";
										public static  String txtThirdOrder="";
										public static  String txtForthOrder="";
										
										// order number for seasonal
										
										public static String orderNumberForSeasonal = "//p[@class='delivery-date']";
										public static String orderNumberForSeasonal_loc = "xpath";
										
										// txtMyoffersavings
										
										
//Carrier bag charge
public static String carrierBagChargeInOrderSummary="//table[@class='order-information'][2]/tbody/tr";								
public static String carrierBagChargeInOrderSummary_loc="xpath";										
										
										
										
										
										
}
