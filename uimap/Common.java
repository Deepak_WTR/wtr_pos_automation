package uimap;

public class Common {
	//Edit Order pin bar (Nanditha - Created on 07-Aug-13)
		public static final String lnkEditOrderPinBar = "//div[@class='pinbar-inner']";//div[@class='pinbar active']";
		public static final String lnkEditOrderPinBar_Loc ="xpath";
		
	
		
		//Cancel changes link in edit order pin bar(Nanditha - Created on 07-Aug-13)
		public static final String lnkCancelChanges = "//a[contains(text(),'Cancel changes')]";
		public static final String lnkCancelChanges_Loc = "xpath";
	
		//Empty Trolley link(Nanditha - Created on 08-Aug-13)
		public static final String lnkEmptyTrolley = "//div[@id='headerBasket']/a/span";
		public static final String lnkEmptyTrolley_Loc = "xpath";
		
		//NickName-Delivery Address details(Nanditha -Created on 31-Aug-13)
				public static final String lnkNickName = "//input[@id='nickName']";
				public static final String lnkNickName_Loc = "xpath";
				
				
				//Title-Delivery Address details(Nanditha -Created on31-Aug-13)
				public static final String lnkTitle = "//input[@id='title']";
				public static final String lnkTitle_Loc = "xpath";
				
				//FirstName-Delivery Address details(Nanditha -Created on 31-Aug-13)
				public static final String lnkFirstName = "//input[@id='firstName']";
				public static final String lnkFirstName_Loc = "xpath";
				
				//LastName-Delivery Address details(Nanditha -Created on 31-Aug-13)
				public static final String lnkLastName = "//input[@id='lastName']";
				public static final String lnkLastName_Loc = "xpath";
				
				
				//Find my adderss link-Delivery Address details(Nanditha -Created on 31-Aug-13)
				public static final String lnkFindMyAddress = "//a[contains(text(),'Find my address')]";
				public static final String lnkFindMyAddress_Loc = "xpath";
				
				
				
				//Phone-Delivery Address details(Nanditha -Created on 31-AUg-13)
				public static final String lnkPhone = "//input[@id='phone1']";
				public static final String lnkPhone_Loc = "xpath";
				
				//SaveandContinue-Delivery Address details(Nanditha -Created on 31-Aug-13)
				public static final String btnSaveAndContinue = "//input[@value='Save and continue']";
				public static final String btnSaveAndContinue_Loc = "xpath";
				

						

						//Checkout button in edit order pin bar(Nanditha - Updated on 12-Sep-13)
						public static final String btnCheckoutFromPinBar = "//div[2]/div/div/div[3]/div/div/a";
						public static final String btnCheckoutFromPinBar_Loc ="xpath";

						//Phone-Delivery Address details(Nanditha -Created on 31-AUg-13)
						public static final String lnkzipCode = "zipCode";
						public static final String lnkzipCode_Loc = "id";
						
						//Delivery Note(Nanditha - Created on 14-Jan-14)
						public static final String txtDeliveryNote = "notes";
						public static final String txtDeliveryNote_Loc = "id";


						//IMPORT PRODUCTS HEADING
						public static final String txtImortPrd="//div[@id='import-products']/h1";
						public static final String txtImortPrd_loc="xpath";
						//import products text message
						public static final String msgimpPrd="//div[@class='getStarted']/p";
						public static final String msgimpPrd_loc="xpath";
						//my Supermarket
						public static final String imgSuperMarket="//div[@class='getStarted']/img[@title='my Supermarket']";
						public static final String imgSuperMarket_loc="xpath";
																								

						//Get Started button - Import Product Page (Nanditha - Created on 12-Feb-14)
											public static final String btnGetStarted = "//a[contains(text(),'Get started')]";
											public static final String btnGetStarted_Loc = "xpath";



			
											
											
											//Sainsbury Image - Import Product Page (Nanditha - Created on 12-Feb-14)
											public static final String imgSainsburys = "//div[@class='logos']/a[@title='Import Products from sainsburys']";
											public static final String imgSainsburrys_Loc = "xpath";
											
											
											//Ocado Image - Import Product Page (Nanditha - Created on 12-Feb-14)
											public static final String imgOcado = "//div[@class='logos']/a[@title='Import Products from ocado']";
											public static final String imgOcado_Loc = "xpath";
											
											//ASDA Image-Import Product Page (Nanditha - Created on 12-Feb-14)
											public static final String imgASDA = "//div[@class='logos']/a[@title='Import Products from asda']";
											public static final String imgASDA_Loc = "xpath";
											

											//Tesco image - Import Product Page (Nanditha - Created on 12-Feb-14)
																public static final String imgTesco = "//div[@class='logos']/a[1]/img";
																public static final String imgTesco_Loc = "xpath";
																
																//Choose Address - Delivery Address details(Nanditha -Updated on 18-Mar-13)
//																public static final String lnkChooseAddress = "//div[4]/div/div/ul/li[1]";
																public static final String lnkChooseAddress = "//div[6]/div/div/ul/li[1]";
																public static final String lnkChooseAddress_Loc = "xpath";

														



public static final String btnContinue="continue";
public static final String btnContinue_loc="id";
public static final String btnGetstarted="getStarted";
public static final String btnGetstarted_loc="id";
public static final String btnTesco="tesco_1";
public static final String btnTesco_loc="id";
public static final String btnSains="sainsburys_3";
public static final String btnSains_loc="id";
public static final String btnOcado="ocado_4";
public static final String btnOcado_loc="id";
public static final String btnAsda="asda_2";
public static final String btnAsda_loc="id";


//Import products left panel
						public static final String linkImportPrdsLeft="//h2[contains(text(),'Import products')]";//Import Products";
						public static final String linkImportPrdsLeft_loc="xpath";

public static final String linkImportPrd="onImportClick";//a[contains(text(),'Import Products')]";
						public static final String linkImportPrd_loc="id";
						
						//For Importing products(Updated Lekshmi June 2014)
						//mailid
						public static final String txtEmail="//input[@id='Email']";
						public static final String txtEmail_loc="xpath";
						
						//btn
						public static final String btnImportfromMarket="//a[@id='StoreRegisterText']";
						public static final String btnImportfromMarket_loc="xpath";
						
						public static final String txtPassword="//input[@id='PasswordLogin']";
						public static final String txtPassword_loc="xpath";	
						
						//Confirm Cancel Chages (Nanditha -Updated Lekshmi June 2014)				
						public static final String btnConfirmCancel = "(//a[contains(text(),'Yes-cancel changes')])[2]";
						public  static final String btnConfirmCancel_Loc = "xpath";
						
						// Import till receipt in import products

						public static final String btntillreceiptgetstarted = "getStartedTillReceipt";
						public static final String btntillreceiptgetstarted_Loc = "id";

						// receipt ID 

						public static final String txtreceiptidtxtbox1 = "receipt_field1";
						public static final String txtreceiptidtxtbox1_Loc = "id";
						public static final String txtreceiptidtxtbox2 = "receipt_field2";
						public static final String txtreceiptidtxtbox2_Loc = "id";
						public static final String txtreceiptidtxtbox3 = "receipt_field3";
						public static final String txtreceiptidtxtbox3_Loc = "id";
						public static final String txtreceiptidtxtbox4 = "receipt_field4";
						public static final String txtreceiptidtxtbox4_Loc = "id";


						// receipt date 

						public static final String txtreceiptdate = "receipt_date";
						public static final String txtreceiptdate_Loc = "id";

						// receipt time 

						public static final String listreceipttimeHH = "//select[@id='receipt_time_hh']";
						public static final String listreceipttimeHH_Loc = "xpath";
						public static final String listreceipttimeMM = "//select[@id='receipt_time_mm']";
						public static final String listreceipttimeMM_Loc = "xpath";

						// Till receipt import button

						public static final String btnreceiptimport = "import-tillReceipt";
						public static final String btnreceiptimport_Loc = "id";

						// till receipt default list name 

						public static final String txtreceiptlistname = "//input[@id='create_new_list']";
						public static final String  txtreceiptlistname_Loc = "xpath";

						// Till receipt no of products

						public static final String txtreceiptproductcount = "//div[@class='lightbox-container content-wrapper']//h2";
						public static final String txtreceiptproductcount_Loc = "xpath";

						// import till receipt continue overlay

						public static final String btnreceiptcontinuealert = "continue";
						public static final String btnreceiptcontinuealert_Loc = "id";

						// List page product 

						// List name in list page 

						public static final String txtlistname = "//div[@class='lists-container']//h2[@class='large']";
						public static final String txtlistname_Loc = "xpath";

						// List product count 


						public static final String txtlistproductcount = "(//span[@class='total-prod-in-list'])[1]";
						public static final String txtlistproductcount_Loc = "xpath";

						// List product list checkbox 

						public static final String favlistproduct = "//a[@class='label favourite is-favourite']";
						public static final String favlistproduct_Loc = "xpath";


						// Product not available when adding from list to trolley

						public static final String btnlistproductunavailablealert = "(//a[contains(text(),'Ok')])[8]";
						public static final String btnlistproductunavailablealert_Loc = "xpath";

						// List name already exists error message 

						public static final String msglistalreadyexists = "//p[@class='error-msg']";
						public static final String msglistalreadyexists_Loc = "xpath";


						// Product offer in list 

						public static final String lnkproductoffer = "//a[@class='offer']";
						public static final String lnkproductoffer_Loc = "xpath";

						// Product quantity in list 

						public static final String txtproductquantity = "//input[@class='quantity-input']";
						public static final String txtproductquantity_Loc = "xpath";

						// product UOM in list

						public static final String listproductunit = "//a[@class='txt']";
						public static final String listproductunit_Loc = "xpath";


						// product container in list

						public static final String tabproductlist = "//div[@class='products-row']";
						public static final String tabproductlist_Loc = "Xpath";

						// Product not available error message product list in list page 

						public static final String errproductunavailable = "//p[@class='msg-unavailable']";
						public static final String errproductunavailable_Loc = "xpath";

						// Product imported in favourites page

						public static final String tabfavproduct = "//div[@class='m-product-padding']";
						public static final String tabfavproduct_Loc = "xpath";


						// variable for import till receipt 

						public static String productCount = "";
						public static String defaultListName  = "";
						public static String defaultListtimestampName  = "";
						public static String ListProductCount = "";
						public static String Listname = "";
						
						
						public static String txtDeliveryCharrge="";
						
						
						//FirstName-Delivery Address details(Nanditha -Created on 31-Aug-13)
						public static final String linkBackToTop = "//a[@class='back-to-top']";
						public static final String linkBackToTop_Loc = "xpath";
						
						//Import from another supermarket
						public static final String btnImportSuperMarket="//button[contains(text(),'Import from another supermarket')]";
						public static final String btnImportSuperMarket_loc="xpath";
						//super market text
						public static final String txtSupermarketHdg="(//div[@class='sectionHeadBold'])[1]";
						public static final String txtSupermarketHdg_loc="xpath";
						//Till Receipt
						public static final String btnTillReceipt="//button[contains(text(),'Import from a Waitrose till receipt')]";
						public static final String btnTillReceipt_loc="xpath";
						//Till receipt heading
						public static final String txtTillRcptHdng="//div[contains(text(),'Enter your Waitrose till receipt details')]";
						public static final String txtTillRcptHdng_loc="xpath";
						//dONT IMPORT
						public static final String btnDntimport="(//div[@class='WRvMargin10'])[3]";
						public static final String btnDntimport_loc="xpath";
						//Ocado
						public static final String btnOcada="//button[@class='btnSecondaryCTA ocado']";
						public static final String btnOcada_loc="xpath";
						//Tesco
						public static final String btnTESCO="//button[@class='btnSecondaryCTA tesco']";
						public static final String btnTESCO_LOC="xpath";
						//Sainsburys
						public static final String btnSainsbury="//button[@class='btnSecondaryCTA sainsburys']";
						public static final String btnSainsbury_loc="xpath";
						//ASDA
						public static final String btnASDA="//button[@class='btnSecondaryCTA asda']";
						public static final String btnASDA_LOC="xpath";
						//OCADO account details
						public static final String txtOAccountHdng="//div[contains(text(),'Enter your Ocado account details')]";
						public static final String txtOAccountHdng_loc="xpath";
						//Tesco account details
						public static final String txtTescoAccountHdng="//div[contains(text(),'Enter your Tesco account details')]";
						public static final String txtTescoAccountHdng_loc="xpath";
						//Sainsbury account details
						public static final String txtSainAccountHdng="//div[contains(text(),'Enter your Sainsbury')]";
						public static final String txtSainAccountHdng_loc="xpath";
						//ASDA account details
						public static final String txtASDAAccountHdng="//div[contains(text(),'Enter your Asda account details')]";
						public static final String txtASDAAccountHdng_LOC="xpath";
						//Supermarket email
						public static final String txtSuperEmail="//input[@id='Email']";
						public static final String txtSuperEmail_loc="xpath";
						//Super PASSWORD
						public static final String txtSuperPassword="//input[@id='PasswordLogin']";
						public static final String txtSuperPassword_loc="xpath";
						
						
						
								
						
						
						
						
						
						
						
						
						
}
