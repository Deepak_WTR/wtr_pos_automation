package uimap;



public class OfBranchInformation {

	//branch information link
	public final static String linkBranchInformation="Branch Information";
	public final static String linkBranchInformation_Loc="linkText";
		
	//View_EditPostcodes_Areas link
	public final static String linkView_EditPostcodes_Areas="view/edit postcodes/areas";
	public final static String linkView_EditPostcodes_Areas_Loc="linkText";
	
	//branch information link
	public final static String linkBranchArea="create branch area";
	public final static String linkBranchArea_Loc="linkText";
	
	//branch area
	public final static String txtBranchArea="area_name";
	public final static String txtBranchArea_Loc="name";
	
	//branch description
	public final static String txtBranchDesc="area_desc";
	public final static String txtBranchDesc_Loc="name";
	
	//Save link
	public final static String linkSave="save";
	public final static String linkSave_Loc="linkText";
	
	//Click branch
	public final static String linkViewDetails="//tr[@class='normal']/td[contains(text(),'Auto1')]//..//td[4]/a";
	public final static String linkViewDetails_Loc="xpath";
	
	//click post code services
	public final static String linkPostCodeService="postcode services";
	public final static String linkPostCodeService_Loc="linkText";
	
	//click new post code 
	public final static String linkPostCodeNew="add new postcode";
	public final static String linkPostCodeNew_Loc="linkText";
	
	//enter post code
	public final static String txtNewPostcode="postcode";
	public final static String txtNewPostcode_Loc="name";
	
	//click save changes
	public final static String linkSaveChanges="save changes";
	public final static String linkSaveChanges_Loc="linkText";
	
	//click back to summary
	public final static String linkBackToSummary="back to summary";
	public final static String linkBackToSummary_Loc="linkText";
	
	//click WE Home checkbox
	public final static String chbWEHOME="new*3";
	public final static String chbWEHOME_Loc="name";
	
	//click delete branch area
	public final static String linkDeleteBranchArea="delete branch area";
	public final static String linkDeleteBranchArea_Loc="linkText";
	
	//click delete post code
	public final static String linkDeletePostCode="edit postcodes";
	public final static String linkDeletePostCode_Loc="linkText";
	
	//un check WEHOME
	public final static String chbWEHOMEUn="//tr[@class='normal']/td[contains(text(),'GU199')]//..//td[4]";
	public final static String chbWEHOMEUn_Loc="xpath";
	
	//click back to branch details
	public final static String linkBackToBranchDetails="back to branch details";
	public final static String linkBackToBranchDetails_Loc="linkText";
	
	//click back to branch info
	public final static String linkBackToBranchInfo="back to branch info";
	public final static String linkBackToBranchInfo_Loc="linkText";
	
	//click View/Edit Collection Location button
	public final static String btnViewEditBranchServices="//td[@class='buttonCenter']//a[@title='View/edit branch services']";
	public final static String btnViewEditBranchServices_Loc="xpath";
	
	
	public final static String btnViewEditPostServices="//td[@class='buttonCenter']//a[@title='View/edit branch postcodes']";
	public final static String btnViewEditPostServices_Loc="xpath";
	
	public final static String btnCreateBranchservices="//td[@class='buttonCenter']//a[contains(text(),'create')]";
	public final static String btnCreateBranchservices_Loc="xpath";
	
}
