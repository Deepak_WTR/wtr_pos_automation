package uimap;

public class M_BookSlot {

	public static final String btnmyW = "(//button[contains(text(),'Go to myWaitrose')])[1]";
	public static final String btnmyW_loc = "xpath";
	
	public static final String btnbookaslot = "(//div[contains(text(),'BOOK A SLOT')])[2]";
	public static final String btnbookaslot_loc = "xpath";
	
	public static final String btnbookdelivery = "//span[contains(text(),'Book Delivery')]";
	public static final String btnbookdelivery_loc = "xpath";
	
	public static final String dropdowndeliveryadd = "//select[@class='btnSecondaryCTA btnAddressDropDown']";
	public static final String dropdowndeliveryadd_loc = "xpath";
	
	public static final String btndeliveryslot = "//div[@class='WRgridItem2b']/button[@class='mblButton btnPrimaryCTA unselectable']";
	public static final String btndeliveryslot_loc = "xpath";
	
	public static final String btnEditslot = "//button[contains(text(),'Edit Slot')]";
	public static final String btnEditslot_loc = "xpath";
	
	public static final String btnbookaslotchoice = "//button[@class='btnCalSlot']";
	public static final String btnbookaslotchoice_loc = "xpath";

}
