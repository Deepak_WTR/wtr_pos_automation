package uimap;

import java.util.List;

import org.openqa.selenium.WebElement;

public class MainTrolley {

	//Heading in Main trolley page.
	public static final String txtmsgInShpngpage="//h1[contains(text(),'Your Shopping Trolley')]";
	public static final String txtmsgInShpngpage_loc="xpath";

	//Total value
 //	public static final String txtTotalvalue="//div[@class='totals']/span[1]";
//	public static final String txtTotalvalue="//div[@id='trolleyEstimatedTotal']//strong";
	public static final String txtTotalvalue="//span[@class='trolley-total']";
	public static final String txtTotalvalue_loc="xpath";
	
	//Saving value
	public static final String txtSavingvalue="//div[@class='totals']/span[2]";
	public static final String txtSavingvalue_loc="xpath";
	//tesx message for in pop up for no amend order
	public static final String txtNoAmendOrd_Popup="html/body/div[4]/div/div[2]/div/div[1]/p";
	public static final String txtNoAmendOrd_Popup_loc="xpath";
	//Confirm button in Amend order
	public static final String btnNoAmendOrd_Confirm_Popup="tml/body/div[4]/div/div[2]/div/div[2]/div/a";
	public static final String btnNoAmendOrd_Confirm_Popup_loc="xpath";
	//Main trolley product list
	public static final String txtProductList="//div[@id='headerBasket']/a/span";
	public static final String txtProductList_loc="xpath";
	//Cancel Amend order link
	public static final String txtcancelAmendOrdHdng="//h1[contains(text(),'Shopping online at Waitrose.com is quick, easy and great value')]";
	public static final String txtcancelAmendOrdHdng_loc="xpath";

	public static final String txtproductname="//div[@class='m-product-cell m-product-cell-manipulation']/div/div/div[2]/div/a[1]";
	public static final String remove="//div[@class='m-product-cell m-product-cell-manipulation']/div[ItemIndex]/div/div[3]/p[2]/a/span[2]";


	//Shoppers notes
	public static final String txtShoppersNotes="//div/form/textarea";//textarea[@placeholder='Please give us any special instructions about how you want this item picked (E.g. pick green bananas, or if unavailable, substitute with...)']";
	public static final String txtShoppersNotes_loc="xpath";
	//shoppers text area
	public static final String txtShopersTextarea="//a[contains(text(),'Save note')]";
	public static final String txtShopersTextarea_loc="xpath";

	//shopper notes link with iter
	public static final String lnkShopperNoteIter="//div[1]/div[ItemIndex]/div/div/div[3]/p[3]/a";//div[ItemIndex]/div/div[3]/p[3]/a";
	public static final String lnkShopperNoteIter_loc="xpath";


	//cancel link
	public static final String linkCancel="Cancel";
	public static final String linkCancel_Loc="linkText";
	//confirm link
	public static final String linkConfirm="Confirm";
	public static final String linkConfirm_Loc="linkText";


	//Add all to favourites link
	public static final String linkAddAllToFav = "//a[contains(text(),'Add all to Favourites')]";
	public static final String linkAddAllToFav_Loc = "xpath";

	//increase product
//	public static final String btnDetailIncrease ="(//a[contains(@id, 'plus-')])[ItemIndex]";
	public static final String btnDetailIncrease ="//a[contains(@id, 'plus-')]";
	public static final String btnDetailIncrease_Loc = "xpath";
	
	public static final String verifybtnDetailIncrease ="//a[contains(@id, 'plus-')]";
	public static final String verifybtnDetailIncrease_Loc = "xpath";
	//decrease product
	public static final String btnDetailDecrease = "//a[contains(@id, 'minus-')]";
	public static final String btnDetailDecrease_Loc ="xpath";

	//+Button for single product
	public static final String linkPlus="//a[contains(text(),'+')]";
	public static final String linkPlus_loc="xpath";	

	// - Button for single product
	public static final String linkMinus="//a[contains(text(),'-')]";
	public static final String linkMinus_loc="xpath";		


	//Single Product Qty Text
	public static final String txtSinglePrdQty="//div[@class='quantity-append']/input";
	public static final String txtSinglePrdQty_loc="xpath";




	//Add more shopping button(Nanditha -Created on 07-Aug-13)
	public static final String btnAddMoreShopping = "//a[contains(text(),'Add more shopping')]";
	public static final String btnAddMoreShopping_Loc = "xpath";

	//Groceries landing page (Nanditha - Created on 07-Aug-13)
	public static final String lnkAddMoreShoppingRedirected = "//div[@class='mega-menu has-trolley']/ul/li/a";
	public static final String lnkAddMoreShoppingRedirected_Loc ="xpath";






	//Product Name in Trolley(Nanditha - Updated on 13-Aug-13)
	public static String lnkTrolleyPrdName = "";////Data retrieved from the dataTable

	public static final String lnkTrolleyPrdName_Loc = "linkText";



	//Trolley product Name (Nanditha - Created on 16-Aug-13)
	public static final String lnkTrolleyPrdTitle = "//div[@id='my-trolley']/div/div[RowIndex]/div[CellIndex]/div/div/div[2]/div/a[1]";
	public static final String lnkTrolleyPrdTitle_Loc = "xpath";


	public static final String divgrid = "//div[contains(id(),'grid-product-id')])[2]";
	public static final String divgrid_Loc = "xpath";





	//Enter List name edit box(Nanditha - Created on 22-Aug-13)
	public static final String txtBoxListName = "//input[@id='create_new_list']";
	public static final String txtBoxListName_Loc = "xpath";





	//Add To List Button(Nanditha - Created on 22-Aug-13)
	public static final String btnAddToList = "Add to list";
	public static final String btnAddToList_Loc = "linkText";



	//Empty trolley
	public static final String txtEmptyTrolley="//p[contains(text(),'Empty')]";
	public static final String txtEmptyTrolley_loc="xpath";


	public static final  String lnkOk_Loc = "xpath";
	public static final String lnkOk = "//a[contains(text(),'Ok')]";



	//Create New list
	public static final String txtNewList="create_new_list";
	public static final String txtNewList_loc="id";
	//Ok button
	public static final String btnOk = "Ok";
	public static final String btnOk_Loc = "linkText";
	//New list
	public static final String rdonewList="new_list";
	public static final String rdonewList_loc="id";
	//Empty trolley enable or disable
	public static final String linkenbdsbofEmptyTrolley="//div[@class='trolley-actions']/ul/li[4]";
	public static final String linkenbdsbofEmptyTrolley_loc="xpath";

	//Personal Shopper Note link  (Nanditha - Created on 09-Sep-13)
	public static final String lnkAddPersonalShopperNotes = "//a[contains(text(),'Add personal shopper notes')]";
	public static final String lnkAddPersonalShopperNotes_Loc = "xpath";



	//Close link in Personnal Shopper overlay (Nanditha - Created on 09-Sep-13)
	public static final String lnkClosePersonalShopperOverlay = "//body/div[3]/a";
	public static final String lnkClosePersonalShopperOverlay_Loc = "xpath";


	//Zero  items (Updated on 030913)
	public static final String txtZeroItem ="//p[@id='total-items']";//p[contains(text(),'0 items in trolley')]";
	public static final String txtZeroItem_loc = "xpath";

	//Add to Personal Shopper Note link:(Nanditha - created on 24Sep13)
	public static final String lnkAddPersonalShopperNote = "Add personal shopper notes";
	public static final String lnkAddPersonalShopperNote_Loc = "linkText";








	//SaveNote (Nanditha - created on 24Sep13)
	public static final String btnSaveNote = "//a[contains(text(),'Save note')]";
	public static final String btnSaveNote_Loc = "xpath";



	//Add all To List Link(Nanditha -Updated on 24-Sep-13)
	public static final String lnkAddAllToList = "//a[contains(text(),'Add all to a List')]";
	public static final String lnkAddAllToList_Loc = "xpath";




	//Checkout button(Nanditha - Updated on 16-Oct-13)
	public static final String btnChkOutButton = "//div[@id='button-checkout']/a";
	public static final String btnChkOutButton_Loc = "xpath";



	//trolley Estimate Price
	public static final String txtEstPrice="//div[@id='trolleyEstimatedTotal']/ul/li[1]/strong";
	public static final String txtEstPrice_loc="xpath";
	//Trolley Estimate Savings
	public static final String txtEstSaving="//div[@id='trolleyEstimatedTotal']/ul/li[2]/span";
	public static final String txtEstSaving_loc="xpath";							


	//estimated total in trolley(Nanditha-Created on 14-Nov-13)
	public static final String txtEstimatedTrolleyTotal = "//div[@id='trolleyEstimatedTotal']/ul/li[1]/strong";
	public static final String txtEstimatedTrolleyTotal_Loc = "xpath";

	//savings in trolley (Nanditha - Created on 14-Nov-13)
	public static final String txtTrolleySavings = "//div[@id='trolleyEstimatedTotal']/ul/li[2]/strong";
	public static final String txtTrolleySavings_Loc = "xpath";

	//savings in Header trolley icon(Nanditha - Created on 14-Nov-13)
	public static final String txtSavingsInHeader = "//div[@id='headerBasket']/div/span[@class='savings']/br";
	public static final String txtSavingsInHeader_Loc = "xpath";

	//total in Header trolley icon (Nanditha - Created on 14-Nov-13)
	public static final String txtHeaderTotal = "//div[@id='headerBasket']/div/span[@class='value']";
	public static final String txtHeaderTotal_Loc = "xpath";



	//Product Grid Product Title (Nanditha - Updated on 2-Dec-13)
	public static final String lnkGridPrdTitle = "//div[@class='l-content']/div/div[RowIndex]/div[CellIndex]/div/div/div/div[2]/a";
	public static final String lnkGridPrdTitle_Loc = "xpath";


	//shoppers note link(Nanditha-Updated 09-Dec-13)
	public static final String lnkShoppersNote="//a[contains(text(),'Add note for shopper')]";
	public static final String lnkShoppersNote_loc="xpath";


	//Substitution Preference check box(Nanditha - Updated on 09-Dec-13)



	public static final String txtPrice = "";

	//mike OF

	//trolley Estimate Price
	public static final String linkPersGreeting="//a[contains(text(),'Edit personal greeting')]";
	public static final String linkPersGreeting_loc="xpath";
	public static final String txtGreeting="//textarea[@class='greeting']";
	public static final String txtGreeting_loc="xpath";
	public static final String linkSaveGrtng="Save greeting";
	public static final String linkSaveGrtng_loc="linkText";
	//


	//Add personal shopper note link (Nanditha-Updated  on 22-Jan-14)
	public static final String personalShopperNote1 = "//div[@id='my-trolley']/div[Index]/div/div[@class='products-row']/div/div/div/div[3]/p[@class='add-personal-shopper-note cta-personal-shopper-note']";
	public static final String personalShopperNote2 = "//div[@class='my-trolley']/div[@id='my-trolley']/div[Index]/div/div/div/div/div[@class='product']/p[@class='add-personal-shopper-note cta-personal-shopper-note']";
	public static final String personalShopperNote1_Loc = "xpath";


	public static final String remove1="//div[@title='ValToReplace']//..//..//span[contains(text(),'Remove')]";

	public static final String lnkGridPrdOffer = "//div[RowIndex]/div[CellIndex]/div/div/div[2]/div[3]/a";
	public static final String lnkGridPrdOffer_Loc = "xpath";
	//Text area (Nanditha - created on 24Sep13)
	public static final String txtAreaPersonalShopperNote = "//div[@class='shopper']/form/textarea[@id='trolleyText']";
	public static final String txtAreaPersonalShopperNote_Loc = "xpath";

	//empty trolley link
	public static final String linkEmptyTrolley="//a[contains(text(),'Empty trolley')]";
	public static final String linkEmptyTrolley_Loc="xpath";


	//Add all to pending orders link(Nanditha - Created on 5-Aug-13)(updated Lekshmi)
	public static final String lnkAddAllToPendingOrders = "//a[contains(text(),'Add all to pending order')]";
	public static final String lnkAddAllToPendingOrders_Loc = "xpath";




	public static final String linkPlaceYourOrder_Loc = "id";

	//Substitution Preference check box(Nanditha - Updated Lekshmi on June2014)
	public static final String chkBoxSubstitutionPreference = "//div[2]/input";
	public static final String chkBoxSubstitutionPreference_Loc = "xpath";

	//Trolley product offer  (updated Lekshmi June 2014)

	public static final String lnkTrolleyPrdOffer ="//div[2]/div/div[2]/a";
	public static final String lnkTrolleyPrdOffer_Loc = "xpath";


	public static final String chkBoxSubstitutionoFFaLL = "(//input[@name='substitute'])[ItemIndex]";
	public static final String chkBoxSubstitutionoFFaLL_loc = "xpath";
	//Confirm button on overlay(Gajapathy updated on 210314)(updatyed Lekshmi(May 2014)
	public static final String btnConfirmOverlay = "(//a[contains(text(),'Confirm')])[3]";
	public static final String btnConfirmOverlay_Loc = "xpath";


	// savings
	public static final String txtEstSavings="//div[@id='trolleyEstimatedTotal']/ul/li[2]/span";
	public static final String txtEstSavings_loc="xpath";




	//Remove link of first Product(Nanditha - Created on 13-Aug-13)
	public static final String lnkRemovePrd = "//div[@id='my-trolley']/div[1]/div/div/div/div/div[3]/p[2]/a/span[2]";
	public static final String lnkRemovePrd_Loc = "xpath";



	//Remove link of first Product(Updated on 09102014)
	public static final String lnkRemoveloan = "(//span[contains(text(),'Remove from trolley')])[2]";
	public static final String lnkRemoveloan_Loc = "xpath";



	//Confirmation Text for changing slot (Created by Jayandran on 19/11/2014)
	public static final String txtChangeSlotConfirmation = "(//span[contains(text(),'Your slot has been changed')])";
	public static final String txtChangeSlotConfirmation_Loc = "xpath";


	//CloseButton for changing slot Overlay(Created by Jayandran on 19/11/2014)
	public static final String btnCloseInOverlay = "(//div[@class='toolbox-close']/a[text()='Close'])";
	public static final String btnCloseInOverlay_Loc = "xpath";




	// Main trolley specific item

	public static final String tabmaintrolleyitem = "(//div[@class='m-product-cell m-product-cell-manipulation'])";
	public static final String tabmaintrolleyitem_loc = "xpath";

	//main trolley remove specific product 
	public static final String lnkmaintrolleyremove = "//span[contains(text(),'Remove from trolley')]"; 
	public static final String lnkmaintrolleyremove_Loc  = "xpath"; 

	// Trolley amount from main trolley

	public static final String txttrolleyamount = "//div[@id='trolleyEstimatedTotal']//strong";
	public static final String txttrolleyamount_Loc  = "xpath";
	public static  String vartrolleyamt = "";

	// Trolley amount from main trolley

	public static final String txttrolleyslot = "//p[contains(text(),'slot')]//span";
	public static final String txttrolleyslot_Loc  = "xpath";
	public static  String vartrolleyslot = "";

	// saving from main trolley

	public static final String txttrolleysavings = "//div[@id='trolleyEstimatedTotal']//span";
	public static final String txttrolleysavings_Loc  = "xpath";
	public static  String varsavingsamt = "";


	// saving from main trolley

	public static final String txttrolleyproductcount = "//span[@class='items']";
	public static final String txttrolleyproductcount_Loc  = "xpath";
	public static  String varproductcount = "";

	//Main trolley link(Gajapathy - Updated on 10-Dec-13)

//	public static final String lnkmainTrolley="//div[@id='headerBasket']/div/a/span";//div[@id='headerBasket']/a/span";
	public static final String lnkmainTrolley="//div[@id='headerMiniTrolley']/div/a/span";
	public static final String lnkmainTrolley_loc="xpath";


	//Items in my trolley
//	public static final String txtNoProd="//div[@id='headerBasket']/div/a/span"; 
	public static final String txtNoProd="(//div[@id='headerMiniTrolley']/div/a/span/span)[2]";
	public static final String txtNoProd_loc="xpath";
	//Amend order text
	public static final String txtAmendOrder="//h1[@id='my-trolley-heading']";
	public static final String txtAmendOrder_loc="xpath";



	//Edit shoppers note link - Prabhakaran Sankar

	public static final String lnkeditShoppersNote="//a[contains(text(),'Edit note for shopper')]";
	public static final String lnkeditShoppersNote_loc="xpath";



	//Un check aLlow substitute preference - Prabhakaran Sankar

	public static final String checkboxAllowSubstitute="(//input[@class='substitute'])[1]";
	public static final String checkboxAllowSubstitute_loc="xpath";


	// Loan item free cost in My trolley

	public static final String txtLoanFreecost = "(//span[contains(text(),'FREE')])[1]";
	public static final String txtLoanFreecost_Loc = "xpath";


	// trolley with delivery charge 

	public static final String txtdeliverycharge = "//*[@id='trolleySlotDetails']/div/text()[2]";
	public static final String txtdeliverycharge_Loc = "xpath";
	//delivery charge
	public static final String txtdcmsg="//div[@class='deliveryCharge']";
	public static final String txtdcmsg_loc="xpath";

	//dc value
	public static final String txtdcvalue="//div[@class='deliveryCharge']/strong[1]";
	public static final String txtdcvalue_loc="xpath";
	// banner value
	public static final String txtbvalue="//div[@class='deliveryCharge']/text()[2]";
	public static final String txtbvalue_loc="//div[@class='deliveryCharge']/strong[2]";

	//product titles
	public static final String txtTitle="(//div[@class='m-product-title title']/a)[index]";
	public static final String txtTitle_loc="xpath";



	// UOM of product in trolley

	public static final String txtProductUOM = "//select[@class='quantity-select']//option[@selected]";
	public static final String txtProductUOM_Loc = "xpath";

	// UOM of product in trolley

	public static final String listProductchangeUOM = "//select[@class='quantity-select']";
	public static final String listProductchangeUOM_Loc = "xpath";


	// Qty of the products


	public static final String txtProductQty = "//input[@class='quantity-input']";
	public static final String txtProductQty_Loc = "xpath";
	public static List<WebElement> arrayproductamt = null ;
	public static List<WebElement> arrayproductUOM = null ;
	public static List<WebElement> arrayproductQty = null;			

	// product amt in trolley 

	public static final String txtProductAmt = "//span[@class='price trolley-price']";
	public static final String txtProductAmt_Loc = "xpath";
	
	// PYO Item amt in trolley 

		public static final String txtPYOItemAmt = "((//p[@class='m-product-details'])[1]//span[@class='price'])[1]";
		public static final String txtPYOItemAmt_Loc = "xpath";
		
	// PYO Item QTY in trolley 

			public static final String txtPYOItemQTY = "//div[@class='quantity-append']//input";
			public static final String txtPYOItemQTY_Loc = "xpath";	
			
	// Estimated Tot in trolley 

			public static final String estimatedToT = "//div[@id='trolleyEstimatedTotal']//strong";
			public static final String estimatedToT_Loc = "xpath";	
			
	// Estimated offer Saving in trolley 

			public static final String offerSaving = "//div[@id='trolleyEstimatedTotal']//span";
			public static final String offerSaving_Loc = "xpath";			
	
	// saving Amount Variable
	
	public static String savingsInTrolley = ""; 
	
	// Offer Savings Amount Variable
	
	public static String OfferSavings = ""; 
	
	// my offer savings 
	
	public static String txtMyoffersavings = "//li[contains(text(),'My Offers Savings')]//span";
	public static String txtMyoffersavings_Loc = "xpath";
	

}
