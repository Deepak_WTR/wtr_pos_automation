package uimap;

public class Payment {

	
	//Payment Page
	//Gift Voucher
	public static final String txtSerialNumber="serialNumberId";
	public static final String txtSerialNumber_Loc="id";

	public static final String txtSecurityNumber="securityCodeId";
	public static final String txtSecurityNumber_Loc="id";
	
	//card number
		public static final String txtCardNumber ="cardNumber";
		public static final String txtCardNumber_Loc = "id";
		//issue month
		public static final String ddbtnIssueMonth ="startMonth";
		public static final String ddbtnIssueMonth_Loc ="id";
		//issue year
		public static final String ddbtnIssueYear ="startYear";
		public static final String ddbtnIssueYear_Loc ="id";	
		//Expire Month
		public static final String ddbtnExpireMonth ="endMonth";
		public static final String ddbtnExpireMonth_Loc ="id";
		//Expire Year
		public static final String ddbtnExpireYear ="endYear";
		public static final String ddbtnExpireYear_Loc ="id";
		//Card Name
		public static final String txtCardName ="nameOnCard";
		public static final String txtCardName_Loc = "id";
	
		//pay with different card
				public static final String linkPaywithdiffcard ="Pay with a different card";
				public static final String linkPaywithdiffcard_Loc = "linkText";
				//issuenumber
				public static final String txtissueNumber="issueNumber";
				public static final String txtissueNumber_Loc="id";
				// 3d pin number
				public static final String txtPinNumber ="pin";
				public static final String txtPinNumber_Loc = "name";
				//submit
				public static final String btnSubmit="//input[@type='image']";
				public static final String btnSubmit_Loc = "xpath";
				
				//image Logic Group
				
				public static final String imgLogicGroup = "//div[@id='CompanyLogo']";
				public static final String imgLogicGroup_loc = "xpath";
				
				//Save Card (Nanditha - Created on 06-Sep-13)
				public static final String lnkSaveCard = "//div[7]/input";
				public static final String lnkSaveCard_Loc = "xpath";
				//Dont want Claim
				public static final String btnDontwantClaim="I don't want to claim discount";
				public static final String btnDontwantClaim_loc="linkText";
				//Nees Claim
				public static final String btnNeedClaim="acceptCard";//"I want to claim discount";
				public static final String btnNeedClaim_loc="id";//"linkText";

				//errror message for invalid payment
				public static final String txtErrPayment="//div[@class='lightbox-container content-wrapper']";
				public static final String txtErrPayment_loc="xpath";
				//Close link
				public static final String linkClose="Close";
				public static final String linkClose_loc="linkText";
								
																		
														
														
														//View link
														
														public static final String linkGiftCard="view-gift-card";
														public static final String linkGiftCard_loc="id";
														public static final String linkView="View";
														public static final String linkView_loc="linkText";
														//giftCardNumber
														public static final String txtGiftcardNum="giftCardNumber";
														public static final String txtGiftcardNum_loc="id";
														//giftCardPIN
														public static final String txtGiftPin="giftCardPIN";
														public static final String txtGiftPin_loc="id";
														//apply giftcard
														public static final String btnApplyGift="add-gift-card";
														public static final String btnApplyGift_loc="id";
														//radio button to choose different billing address (Nanditha - created on 13-Jan-14)
														public static final String btnSavedAddress = "savedDelivAddress";
														public static final String btnSavedAddress_Loc = "id";
														//different billing address from drop down (Nanditha -created on 13-Jan-14)
														public static final String lnkDeliveryAddress = "delivery-addresses";
														public static final String lnkDeliveryAddress_Loc = "id";
														public static final String lnkDeliveryAddressValue = "addressValue";
														//view gift voucher updated on 180214
														public static final String linkGVView="//a[@id='view-gift-voucher']";//view-gift-voucher";
														public static final String linkGVView_loc="xpath";
														//removegiftcard
														public static final String linkRemoveGiftCard="removeGiftCard";
														public static final String linkRemoveGiftCard_loc="id";
														
														//duplicate GC Message
														public static final String txtDuplicateGC="//div[@class='error-msg']/p[contains(text(),'This card has already been applied to the order')]";
														public static final String txtDuplicateGC_loc="xpath";
														//apply voucher
														public static final String btnApplyVoucher="add-gift-voucher";
														public static final String btnApplyVouche_locr="id";
														//removespecific gc
														public static final String removeCardLink="//a[@data-giftcard='ValuetoReplace']";
														public static final String removeCardLink_loc="xpath";
														//removelinks
														public static final String linkremovelinks="(//a[contains(text(),'Remove this gift card')])[ItemIndex]";
														public static final String linkremovelinks_loc="xpath";
														//insuffiesnt balance
														public static final String txtNoBalance="//div[@class='error-msg']/p[contains(text(),'Insufficient funds on gift card(s). Please remove gift cards(s) indicated above.')]";
														public static final String txtNoBalance_loc="xpath";
														//remove voucher
														public static final String linkRemoveGV="//a[contains(text(),'Remove this voucher')]";
														public static final String linkRemoveGV_loc="xpath";
														//balanceon GC
														public static final String linkGVbalance="checkBalance";
														public static final String linkGVbalance_loc="id";
														
														
														


														//invalidGC error message (Nanditha - created on 18-Feb-14)
																												public static final String invalidGCErrorMsg = "//div[@class='error-msg']/p";
																												public static final String invalidGCErrorMsg_Loc = "xpath";



																												//link place your order updated on 6/3/14(Nanditha -Updated on 18-Mar-14)
																												//public static final String linkPlaceYourOrder ="//div[3]/div/div/a";
																												//div[@id='content']/div[@class='checkout']/div[@class='buttons buttons-payment']/div[@class='button content-button right']/a";
																												//"//a[contains(text(),'Place your order')][1]";//Place your order";
																												//public static final String linkPlaceYourOrder_Loc = "xpath";


																												//remove giftcard popup
																												public static final String giftCardErrorPop="//div[@class='lightbox-container content-wrapper'][contains(text(),'Order total has been exceeded')]";
																												public static final String giftCardErrorPop_loc="xpath";
																												//close remove giftcard popup
																												public static final String linkcGCClose="//div[@class='lightbox-container content-wrapper']/a";																												
																												public static final String linkcGCClose_loc="xpath";
																												//link place your order updated on 04/03/14
																												
																												
																												//analagesic prd
																												 public static final String txtanalagic="//div[@class='user-msg']";
																												public static final String txtanalagic_loc="xpath";
																												//analogic close link
																												public static final String linkCloseana="(//a[contains(text(),'Close')])[5]";
																												public static final String linkCloseana_loc="xpath";
																												
																												public static final String linkPlaceYourOrderOF ="//a[contains(text(),'Place your order')][1]";//Place your order;
																												public static final String linkPlaceYourOrderOF_Loc = "xpath";
																												

public static final String linkPlaceYourOrderLoan ="//a[contains(text(),'Place your order')]";//Place your order;
//link place your order updated on 04/06/14 ( updated by vaishnavi )
	public static final String linkPlaceYourOrder ="place-order-upper";
	public static final String linkPlaceYourOrder_Loc = "id";
	
	
	
	//invalid Giftcard Number(Updated on 170614)
	public static final String txtInvalidGiftCards="(//div[@class='error-msg']/p)[1]";
	public static final String txtInvalidGiftCards_loc="xpath";
	//invalid Giftvoucher Updated on 170614
	public static final String txtInvalidGiftVoucher="(//div[@class='error-msg']/p)[1]";
	public static final String txtInvalidGiftVoucher_loc="xpath";
	//Invalid GiftVPin
	public static final String txtInvalidGiftVoucherPin="(//div[@class='error-msg']/p)[4]";
	public static final String txtInvalidGiftVoucherPin_loc="xpath";
	
	//to be updated by gaja
	public static String txt10gcErr_loc;
	public static String txt10gcErr;
	public static String BillingAddressName;
	
	//ERRORR MESSAGE(Lekshmi June 2014)
		public static final String txtInvalidGiftPin="//fieldset[@class='giftvoucher']/div[@class='error-msg']/p";
		public static final String txtInvalidGiftPin_loc="xpath";
		//Place order link for seasonal(Kathir created on 050814)
				public static final String linkPlaceYourOrderSeasonal ="processnext";
				public static final String linkPlaceYourOrderSeasonal_Loc = "id";
				
				//Pay on collection

				public static final String rdoPayOnCollection="payOnCollection";
				public static final String rdoPayOnCollection_loc="id";
				//Pay on collection
				

				// invalid card number error message
								
								public static final String errinvalidcardnumber = "//div[@class='error-msg']//p[contains(text(),'Please enter a valid credit card number')]";
								public static final String errinvalidcardnumber_Loc = "xpath";

//Pay on collection

	public static final String textForSecurityReason="//div[contains(text(),'For security reasons, as you are using a new address')]";
	public static final String textForSecurityReason_loc="xpath";			

	public static final String linkPayWithYourExistingCard="Pay with your existing card";
	public static final String linkPayWithYourExistingCard_loc="linkText";
}
