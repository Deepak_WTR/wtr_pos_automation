package uimap;

public class OfCustomerDetails 
{
	
 public static String customerNumber =""; 
 
  public static final String latestOrder="(//tr/td[1]/a)[1]";
  public static final String latestOrder_loc="xpath";
 
		  
//image
  public static final String imgdelivery="(//tr/td[2]/img)[1]";
  public static final String imgdelivery_loc="xpath";
  //sign on as customer
  public static final String btnSignonCust="//input[@value='sign on as customer']";
  public static final String btnSignonCust_loc="xpath";

  // Equipment Loan Order Page - Place order
   	public static final String btnPlaceOrder="//input[@value='Place Order']";
   	public static final String btnPlaceOrder_loc="xpath";

   // Waitrose Deliver Services Page - book delivey
   	public static final String btnbookDelivery="//input[@value='book delivery']";
   	public static final String btnbookDelivery_loc="xpath";
 
   	
  //Book Delivery Button
    public static final String btnBookDelivery="//input[@value='book delivery']";
    public static final String btnBookDelivery_loc="xpath";

    //We collect Button
    public static final String btnWeCollect="//input[@value='WE Collection']";
    public static final String btnWeCollect_loc="xpath";
    
    //We collect Button
    public static final String btnWeDelivery="//input[@value='WE Delivery']";
    public static final String  btnWeDelivery_loc="xpath";
  
}
