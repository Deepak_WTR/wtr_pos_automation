package uimap;

import java.util.List;

import org.openqa.selenium.WebElement;

public class OrderReview {
	

	//ReviewYourOrder
	//Delivery Note
	public static final String txtDeliveryNote="checkout-form-delivery-note";
	public static final String txtDeliveryNote_Loc="id";
	//Click
			public static final String clickBlank="(//div/div/ul)[2]";
			public static final String clickBlank_Loc="xpath";
	//Collection Note
	public static final String txtCollectionNote="checkout-form-delivery-note";
	public static final String txtCollectionNote_loc="id";
	//Magazine 
	public static final String cbMagazine="wfimagazine";
	public static final String cbMagazine_Loc="id";
	//Promo Code
//	public static final String txtPromoCode="add-promocode";
	public static final String txtPromoCode="addIncentiveCode";
	public static final String txtPromoCode_Loc="id";

	//Your Order Heading
	public static final String txtYourOrder="//h2[contains(text(),'Your order')]";
	public static final String txtYourOrder_loc="xpath";


	//Review Products Quantity
	public static final String txtQuantity="//div[@id='sortable']/div[@class='sortableitem inload']/div/div[@class='quantity']";
	public static final String txtQuantity_loc="xpath";



	/*//continue to payment screen
	public static final String linkcontinuetopayment="Continue to payment details";
	public static final String linkcontinuetopayment_Loc="linkText";*/

	//continue to payment screen
	public static final String linkcontinuetopayment="//input[@class='continue-to-payment-button']";
	public static final String linkcontinuetopayment_Loc="xpath";


	//	//continue to payment screen
	//	public static final String linkcontinuetopayment="//input[@class='continue-to-payment-button']";
	//	//public static final String linkcontinuetopayment="//a[contains(text(),'Continue to payment details']";
	//	public static final String linkcontinuetopayment_Loc="xpath";


	//Return By Date(Nanditha - Created on 06-Sep-13)
	public static final String txtUseByDate = "//div[@class='useby']/p[2]";
	public static final String txtUseByDate_Loc = "xpath";



	//SlotDate (Nanditha -Created on 06-Sep-13)
	public static final String txtSlotDate = "//div[@class='l-content']/p[2]";
	public static final String txtSlotDate_Loc = "xpath";




	//Error message for invalid promo code
//	public static final String txtInvalidPromoCode="//div[@class='lightbox-container content-wrapper']/div/div[1]/p";
	public static final String txtInvalidPromoCode="//p[contains(text(),'Please enter a valid Promotion Code or leave blank.')]";
	public static final String txtInvalidPromoCode_loc="xpath";
	// promo code in Alert message
	public static final String txtPromoCodeInAlert="//div[@class='lightbox-container content-wrapper']/div/div[1]/p/b";
	public static final String txtPromoCodeInAlert_loc="xpath";
	//Close link
	public static final String linkClose="OK";
	public static final String linkClose_loc="linkText";



	//*************Before Promo code for normal products**********************// 	
	//Item total
	public static final String txtItemTotal="//tr/th[contains(text(),'Estimated total')]/../td";
	public static final String txtItemTotal_loc="xpath";



	//promotion discount
	public static final String txtPromoDiscount="//tr/th[contains(text(),'Promotion discount')]/../td";
	public static final String txtPromoDiscount_loc="xpath";




	//REMOVE PROMO CODE LINK
	public static final String linkremovePromo="//a[@title='Remove promotion code']";
	public static final String linkremovePromo_loc="xpath";


	//place order
	public static final String linkPlaceOrder="place-order-upper";
	public static final String linkPlaceOrder_loc="id";

	public static final String lnkContinue="Continue";
	public static final String lnkContinue_loc="linkText";

	//*************Before Promo code for mywaitrose  products**********************// 






	//Promo Code Add button
	public static final String btnAddPromoCode="//input[@id='addIncentiveCodeButton']";//"//input[@id='submitbutton']";
	public static final String btnAddPromoCode_Loc="xpath";

	//delivery charges before promocode(updated on 050614)
	public static final String txtDeliCharge="//tr/th[contains(text(),'Delivery charge')]/../td";//table/tbody/tr[2]/td[1]";
	public static final String txtDeliCharge_loc="xpaths";
	//Total before(updated on 050614)
	public static final String txtTotal="//tr[@id='TotalRow']/th[contains(text(),'Total')]/../td";//table/tbody/tr[4]/td[1]";
	//delivery charges after promocode(updated on 060614);
	public static final String txtDeliChargeAfterPromo="//tr/th[contains(text(),'Delivery charge')]/../td";//table/tbody/tr[3]/td[1]";
	public static final String txtDeliChargeAfterPromo_loc="xpath";
	//Total after(uPDATED ON 050614)

	public static final String txtTotalAfterPromo_loc="xpath";			 			 			 			 			 		 	public static final String txtTotal_loc="xpath";
	// total before discount 
	public static  float total ;
	//Review products Price
	public static final String txtPrice="//div[@class='sortableitem inload']/div/div[@class='cost']";//"//div[@id='sortable']/div[@class='sortableitem inload']/div/div[@class='cost']";
	public static final String txtPrice_loc="xpath";

	//Review Products Names
	public static final String txtProductNames="//div[@class='sortableitem inload']/div/div/p[@class='title']";//"//div[@id='sortable']/div[@class='sortableitem inload']/div/div/p[@class='title']";//div[@id='sortable']/div[@class='sortableitem inload']/div/div[@class='title']";
	public static final String txtProductNames_loc="xpath";

	//business account holder (Updated on 170614)
	public static final String txtBusinessAcc="(//input[@title='Purchase Order Number'])[2]";//purchaseInput";
	public static final String txtBusinessAcc_loc="xpath";
	//bus_acc_continue (Updated on 170614)
	public static final String btnBusContinue="(//a[@id='bus_acc_continue'])[2]";
	public static final String btnBusContinue_id="xpath";
	////////////////////////////////gaja my waitrose

	//Mywaitrose savings before promocode(Updated on 010714)
	public static final String txtMyWSavings="//table/tbody/tr[3]/td[1]";
	public static final String txtMyWSavings_loc="xpaths";
	//mysavings
	public static final String txtmysavings="//table/tbody/tr[2]/td[1]";
	public static final String txtmysavings_loc="xpath";
	//Estimate total
	public static final String txtEstTotal="//table/tbody/tr[5]/td[1]";
	public static final String txtEstTotal_loc="xpath";


	//delivery charges before promocode(Updated on 010714)
	public static final String txtMwSDeliCharge="//table/tbody/tr[6]/td[1]";
	public static final String txtMwSDeliCharge_loc="xpaths";

	//Total before
	public static final String txtMwSTotal="//table/tbody/tr[7]/td[1]";
	public static final String txtMwSTotal_loc="xpath";

	//promotion discount for MwSaving(uPDATED ON 010714)
	public static final String txtMwSPromoDiscount="//table/tbody/tr[4]/td[1]";
	public static final String txtMwSPromoDiscount_loc="xpath";
	//aFTER MYSAVINGS
	public static final String txtMysavings="//table/tbody/tr[2]/td[1]";
	public static final String txtMysavings_loc="xpath";
	//estimatea total after
	public static final String txtEstAfter="//table/tbody/tr[6]/td[1]";
	public static final String txtEstAfter_loc="xpath";




	//Mywaitrose Savings(uPDATED ON 010714)
	public static final String txtMwSaAfterPromo="//table/tbody/tr[3]/td[1]";
	public static final String txtMwSaAfterPromo_loc="xpath";


	//delivery charges after promocode(updated on 011714)
	public static final String txtDeliMwSChargeAfterPromo="//table/tbody/tr[7]/td[1]";
	public static final String txtDeliMwSChargeAfterPromo_loc="xpaths";

	//Total after(Updated on 010714)
	public static final String txtTotaMwSlAfterPromo="//table/tbody/tr[8]/td[1]";
	public static final String txtTotaMwSlAfterPromo_loc="xpath";

	//My wait rose txt
	public static final String txtMywaitroseSavings="//th[contains(text(),'myWaitrose Savings')]";
	public static final String txtMywaitroseSavings_loc="xpath";

	//my Waitrose savings in Order review page (Nanditha- creatd on 14-Nov-13)
	public static final String txtMWSavingsValue = "//table/tbody/tr[2]/td";
	public static final String txtMWSavingsValue_Loc = "xpath";

	//Place Your Order Button for Collection
	public static final String btnPlaceYourOrderInLoanItem = "(//a[text()='Place your order'])";
	public static final String btnPlaceYourOrderInLoanItem_Loc = "xpath";

	//Use By Date for Product in Order review page (Jayandran - creatd on 11-Nov-14)
	public static final String txtUseByDateForProduct = "(//*[contains(text(),'Use by')])[1]";
	public static final String txtUseByDateForProduct_Loc = "xpath";

	//Place Your Order button for Reservation Order in Order review page (Jayandran - creatd on 14-Nov-14)
	public static final String btnPlaceYourOrderInReservation = "seasoncheckoutform";
	public static final String btnPlaceYourOrderInReservation_Loc = "id";


	//Reservation Order
	public static final String txtReservationDetails="//h2[contains(text(),'Reservation Details')]";	         public static final String txtReservationDetails_loc="xpath";


	//Reservation delivery date
	public static final String txtResDeliveryDate="//p[@class='delivery-date']";
	public static final String txtResDeliveryDate_loc="xpath";


	//Reservation Address

	public static final String txtResAddress="//p[@class='delivery-address']";
	public static final String txtResAddress_loc="xpath";




	//Text News paper offer
	public static final String textNewsPapperOffer="//*[contains(text(),'Newspaper')]";
	public static final String textNewsPapperOffer_loc="xpath";

	//verify personal shopper note - prabhakaran

	public static final String textPersonalShopperNote="//p[@class='note-edit']";
	public static final String textPersonalShopperNoter_loc="xpath";


	//Total after(uPDATED ON 050614)(Jayandran on 16/12/2014)
	public static final String txtTotalAfterPromo="//tr[@id='TotalRow']//td";






	// mywaitrose offer savings in order review page 

	public static final String txtMywaitroseOfferSavings = "//tr/th[contains(text(),'myWaitrose')]/../td";
	public static final String txtMywaitroseOfferSavings_Loc = "xpath";

	// product list in order preview page

	public static final String tabproductname = "//div[@class='preview-item']";
	public static final String tabproductname_Loc = "xpath";

	// offer savings in order review page(Jayandran 8/1/2015) 

	public static final String txtOfferSavings = "//tr/th[text()='Savings']/../td";
	public static final String txtOfferSavings_Loc = "xpath";



	// buy carry bag 



	public static final String radiobtnbuycarrybag = "//input[@id='buybag']";
	public static final String radiobtnbuycarrybag_Loc = "xpath";


	// No buy carry bag 



	public static final String radiobtnnocarrybag = "//input[@id='nobag']";
	public static final String radiobtnnocarrybag_Loc = "xpath";

	// delivery charge 

	public static final String txtDC = "//th[contains(text(),'Delivery charge')]";
	public static final String txtDC_Loc = "xpath";
	//delivery value
	public static final String txtDV="(//tr/th[contains(text(),'Delivery charge')]/../td)[1]";
	public static final String txtDV_loc="xpath";

	// Qty of the product

	public static final String txtPRoductQty = "//div[@class='quantity']";

	public static final String txtPRoductQty_Loc = "xpath";



	// Amount of the product

	public static final String txtPRoductAmt= "//div[@class='cost']";
	public static final String txtPRoductAmt_Loc = "xpath";

	public static List<WebElement> arrayproductamtOR = null ;
	public static List<WebElement> arrayproductQtyOR = null;

	public static final String txtproduct = "(//div[contains(@class,'preview-item')])";
	public static List <Double> TRolleyAmt = null;
	public static List <String>TRolleyUOM = null;
	
	// My offer savings in order preview page

	public static final String MyoffferSavings = "//tr/th[contains(text(),'My Offers Savings')]/../td";
	public static final String MyoffferSavings_Loc = "xpath";

	// my offer savings 

	public static String varMyofferSaving = "";
	public static String click_loc;
	
	//Carrier bag charge

	public static final String btnCarrierBagChargePanel= "carrierBagCharge";
	public static final String btnCarrierBagChargePanel_Loc = "id";

	public static final String btnCarrierBagChargeAmount= "carrierBagChargeAmountDisplay";
	public static final String btnCarrierBagChargeAmount_Loc = "id";

	public static final String btnCarrierBagChargeToolTip= "//a[@class='help-content']";
	public static final String btnCarrierBagChargeToolTip_Loc = "xpath";

	public static final String btnCarrierBagChargeToolTipContent= "//a[@class='help-content']/span";
	public static final String btnCarrierBagChargeToolTipContent_Loc = "xpath";
	
	public static final String btnCarrierBagChargeOption= "//div[@class='content carrier-bags']";
	public static final String btnCarrierBagChargeOption_Loc = "xpath";
	
	public static final String btnCarrierBagChargeOptionBuyBag = "buybag";
	public static final String btnCarrierBagChargeOptionBuyBag_Loc = "id";
	
	public static final String btnCarrierBagChargeOptionNoBag = "nobag";
	public static final String btnCarrierBagChargeOptionNoBag_Loc = "id";

	//get notifications by mobile
	public static final String txtGetNotificationsByMobile = "phone2";
	public static final String txtGetNotificationsByMobile_Loc = "id";
	
}
