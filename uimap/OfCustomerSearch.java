package uimap;



public class OfCustomerSearch {

	//title
	public final static String txtTitle ="personTitle";
	public final static String txtTitle_Loc ="name";
	
	//lastname
	public final static String txtLastName ="lastName";
	public final static String txtLastName_Loc ="name";
	
	//firstname
	public final static String txtFirstName ="firstName";
	public final static String txtFirstName_Loc ="name";
	
	//postcode
	public final static String txtPostCode ="postCode";
	public final static String txtPostCode_Loc ="name";
	
	//email
	public final static String txtCustEmail ="logonId";
	public final static String txtCustEmail_Loc ="name";

	//number
	public final static String txtCustNumber ="customer_Id";
	public final static String txtCustNumber_Loc ="name";
	
	//search customer
	public final static String linkSearchCustomer ="search";
	public final static String linkSearchCustomer_Loc ="name";
	
	//clear search
	public final static String linkClearSearch ="clear";
	public final static String linkClearSearch_Loc ="name";
	
	//customer number
	public final static String txtCustomerNumber_Loc ="linkText";
	
	//customer search result
	public final static String tblCustomerResult_Loc ="xpath";
	public final static String tblCustomerResult ="//div[@id='searchResults']";

	//Search by details
		public static final String txtSrchDetails="(//div[@class='item_field_layout item_align']/span)[1]";
		public static final String txtSrchDetails_loc="xpath";
		//personTitle
		//public static final String txtTitle="personTitle";
		public static final String txtTitle_loc="name";
		//lastName
		public static final String txtLname="lastName";
		public static final String txtLname_loc="name";
		//firstName
		public static final String txtFname="firstName";
		public static final String txtFname_Loc="name";
		//postCode
		//public static final String txtPostCode="postCode";
		public static final String txtPostCode_loc="name";
			//Search by Email
		public static final String txtSrchbyEmail="(//div[@class='item_field_layout item_align']/span)[2]";
		public static final String txtSrchbyEmail_loc="xpath";
		//txtEmail
		public static final String txtEmail="logonId";
		public static final String txtEmail_loc="name";
		//Search by Customer Number
		public static final String txtSrchcustNumber="(//div[@class='item_field_layout item_align']/span)[3]";
		public static final String txtSrchcustNumber_loc="xpath";
		//customer_Id
		public static final String txtCustNum="customer_Id";
		public static final String txtCustNum_loc="name";
		//Customer search
		public static final String btnCustSearch="search";
		public static final String btnCustSearch_loc="name";
		////input[@value='WE Delivery']
		public static final String btnWeDeliver="//input[@value='WE Delivery']";
		public static final String btnWeDeliver_loc="xpath";
		

		public static final String btnWeCollection="//input[@value='WE Collection']";
		public static final String btnWeCollection_loc="xpath";
		
	////place order
			public static final String btnPlaceOrder="//input[@value='Place Order']";
			public static final String btnPlaceOrder_loc="xpath";
			

			//customer email
			public final static String txtCustomerEmail_Loc ="xpath";
			public final static String txtCustomerEmail="//a[contains(text(),'@')]";


			//customer Registeration 
			public final static String linkCustomerRegisteration_Loc ="xpath";
			public final static String linkCustomerRegisteration="//a[text()='Customer Registration']";

}
