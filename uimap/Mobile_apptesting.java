package uimap;

public class Mobile_apptesting {

	public static final String clickScanbutton = "//android.widget.TextView[@text='Scan']";
	public static final String clickScanbutton_Loc = "xpath";
	//android.widget.LinearLayout[@index='7']/android.widget.LinearLayout[@index='0']/android.widget.TextView[@index='1']
	//android.widget.RelativeLayout[@index='8']/android.widget.LinearLayout[@index='0']/android.widget.LinearLayout[@index='1']/android.widget.TextView[@index='0']
	public static final String clickScanQRcode = "//android.widget.TextView[@text='Scan QR code' and @index='1']";
	public static final String clickScanQRcode_Loc = "xpath";
	
	public static final String clickRemovebutton = "//android.widget.TextView[@text='Remove Item']";
	public static final String clickRemovebutton_Loc = "xpath";
}
