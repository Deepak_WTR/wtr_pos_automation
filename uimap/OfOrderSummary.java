package uimap;

public class OfOrderSummary {
public static final String txtOrder="//td[@class='textBold']";
public static final String txtOrder_loc="xpath";
public static final String imgIcomn="//td[@class='text']/img";
public static final String imgIcomn_loc="xpath";
public static final String linkSignOFF="//a[@class='headingLink'][contains(text(),'Sign')]";
public static final String linkSignOFF_LOC="xpath";

///img[@title='Click here to edit this order.']
public static final String btnEditOrder="//img[@title='Click here to edit this order.']";
public static final String btnEditOrder_loc="xpath";
//Click here to cancel this order.
public static final String btnCancelOrder="//img[@title='Click here to cancel this order.']";
public static final String btnCancelOrder_loc="xpath";

//CANCEL MESSAGE
public static final String txtCancelMsg="//td[@class='text' and contains(text(),'Cancelled')]";////tr[@class='review']/td[7]";
public static final String txtCancelMsg_loc="xpath";	


//est total amount 

public static final String txtesttotalamt = "(//tr/td[contains(text(),'est')]/../td)[2]";
public static final String txtesttotalamt_Loc = "xpath";


//partner discount  

public static final String txtpartnerdiscount = "(//tr/td[contains(text(),'partner discount')]/../td)[2]";
	public static final String txtpartnerdiscount_Loc = "xpath";
	
	// phone symbol
	
	public static final String imgPhoneSymbol = "//img[@title='Placed by phone']";
	public static final String imgphoneSymbol_Loc = "xpath";
	
	// Delivery charge 
	public static final String txtDCInOF = "//td[contains(text(),'Service Charge')]";
	public static final String txtDCInOF_Loc = "xpath";
	
	//ESTIMATED TOTAL
	public static final String txtEstTotal="(//tr/td[@class='label' and contains(text(),'est. order total')]/../td)[3]";
	public static final String txtEstTotal_loc="xpath";
	
	//TOTAL
	public static String txtOFTotal="";
	
	//Link Branch delivery date 
		public static final String linkBranchDelivery="Branch Delivery Dates";
		public static final String linkBranchDelivery_Loc="linkText";
	
		public static final String txtproduct = "//tr[@class='normal']";


		// save status for an order 

		public static final String btnsave =  "//form[@name='statuschange']//input[@name='submit']";
		public static final String btnsave_Loc = "xath";

		// change status  

		public static final String drpdwnstatus =  "//select[@name='new_status']";
		public static final String drpdwnstatus_Loc = "xath";


		// picking list 

		public static final String lnkpickinglist = "//a[contains(text(),'Picking List')]";
		public static final String lnkpickinglist_Loc = "xpath";


		// slot details  - date 

		public static final String txtSlotDate = "/html/body/table[1]/tbody/tr[4]/td[4]";
		public static final String txtSlotDate_Loc  = "xpath";

		// slot details  - Time 

		public static final String txtSlotTime = "/html/body/table[1]/tbody/tr[5]/td[4]";
		public static final String txtSloTime_Loc  = "xpath";

		// slot details  - Time 

		public static final String txtCancelled= "//td[@valign='middle']";
		public static final String txtCancelled_Loc  = "xpath";

		// My Waitrose Label

		public static final String txtMyWaitroseheading= "//tr[13]/td[1]";
		public static final String txtMyWaitroseheading_Loc  = "xpath";
		// My waitrose flag

		public static final String txtMyWaitroseFlag= "//tr[13]/td[2]";
		public static final String txtMyWaitrosFlag_Loc  = "xpath";


		//NUmber of shops label

		public static final String txtNoOfShopsheading= "//tr[12]/td[1]";
		public static final String txtNoOfShopsheading_Loc  = "xpath";
		// NO of Shops flag

		public static final String txtNoOfShopsFlag= "//tr[12]/td[2]";
		public static final String txtNoOfShopsFlag_Loc  = "xpath";
		
		// NO of Shops flag

		public static final String lnkDeliveryNote= "//a[contains(text(),'Delivery Note')]";
		public static final String lnkDeliveryNote_Loc  = "xpath";
		
		

		// Slot details - date variable

		public static  String varSlotDate = "";
		// Slot details - Time variable

		public static  String varSlotTime = "";
		
		//Complete MESSAGE
		
		public static final String completeMsg="If the selected order(s) are for Waitrose Deliver or Delivery Service, they will receive driver activated complete messages. Completing here will affect this process. Do you wish to continue?";
		public static final String txtCompleteMsg="//td[@class='text' and contains(text(),'If the selected order(s) are for Waitrose Deliver or Delivery Service, they will receive driver activated complete messages. Completing here will affect this process. Do you wish to continue?')]";////tr[@class='review']/td[7]";
		public static final String txtCompleteMsg_loc="xpath";
		
		public static final String customerAddressDetailsLink="//a[contains(text(),'Customer Address Details')]";
        public static final String customerAddressDetailsLink_Loc="xpath";
        
        public static final String customerAddressDetailsText="/html/body/div[2]/div/form/div";
        public static final String customerAddressDetailsText_Loc="xpath";
		
	
		
}
