package uimap;

public class Header {
	//WaitroseHomePageLogo
		public static final String linkWaitroseLogo = "logo";
		public static final String linkWaitroseLogo_Loc = "className";
		
		
	    //Trolley Icon Text
	    public static final String txtTrolleyIcon = "//div[@id='headerBasket']/a[@class='basket']/span";
	    public static final String txtTrolleyIcon_Loc = "xpath";
	    
	    //Sign in link
	    public static final String linkSignIn = "//input[@value='Sign in']";
	    public static final String linkSignIn_Loc = "xpath";
	    
	    
	    
	    //Search Bar
	    public static final String editboxSearch = "search";
	    public static final String editboxSearch_Loc = "id";
	    
	    //Search button
	    public static final String buttonSearch = "//input[@name='search']";
	    public static final String buttonSearch_Loc = "xpath";
	    
	   //Trolley icon
	    public static final String linkMyTrolley = "//div[@id='headerBasket']/a/span";
	    public static final String linkMyTrolley_Loc = "xpath";
	    
	    //Jotter Multi Search - Assortment landing page
	    public static final String linkJotterMultiSearch1 = "//img[@alt='jotter multi-search']";
	    public static final String linkJotterMultiSearch1_Loc = "xpath";
	    
	    //Jotter Multi Search - Home Page
	    public static final String linkJotterMultiSearch2 = "//p[@id='headerJotterLink']/a";
	    public static final String linkJotterMultiSearch2_Loc = "xpath";
	    
	    
	    //
		public static final String lnkAiselSubLinks_loc="linkText";
		//bread crumb
		public static final String lnkBredcrumb="//div[@class='breadcrumbs standard']/ul/li[@class='current']/span";
		public static final String lnkBredcrumb_loc="xpath";

		 //Level 1 Header aisle(Nanditha-Updated on 25-July-13)
	    public static final String linkHeaderAisle = "//div[@class='logged-out-trolley mega-menu']/ul/li";
	    public static final String linkHeaderAisle_Loc = "xpath";
		
	    public static final String tabaisels="//div[@class='mega-menu has-trolley']/ul/li[ItemIndex]/a";
	    public static final String tabaisels_Loc="xpath";
	    
	   

           	    //password field(Nanditha -Updated on 25-July-13)
	    public static final String txtPassword = "//input[@id='logonPassword']";
	    public static final String txtPassword_Loc ="xpath";
	    
	    //e-mail field(Nanditha -Updated on 25-July-13)
	    public static final String txtEmail = "//input[@id='logonId']";
	    public static final String txtEmail_Loc = "xpath";
	    //PageURL (Nanditha - Created on 31-July-13)
	    public static String urlPageBeforeLogin = "";
	    public static String urlPageAfterLogin = "";
		
	    
	    //Checkout button(Nanditha - Created on 31-July-13)
	//    public static final String buttonChkOut = "//a[contains(text(),'Checkout')]";
	    public static final String buttonChkOut = "//button[contains(text(),'Checkout')]";
	    public static final String buttonChkOut_Loc = "xpath";


	   //Offers link(Nanditha - Created on 31-July-13)
	    public static final String lnkOffers = "//a[contains(text(),'Offers')]";
	    public static final String lnkOffers_Loc = "xpath";
	    
	  //checkout
	    public static final String linkCheckout="//div[@id='headerMiniTrolley']//button[text()='Checkout']";
	    public static final String linkCheckout_Loc="xpath";
	    
	
	   

//Gajapathy(Updated on 060813)
	    public static final String lnkAiselSubLinks_Loc="linkText";

//See all offers link
	    public static final String lnkSeealloffers="See all offers";
	    public static final String lnkSeealloffers_loc="linkText";
	    //View all previous orders
	    public static final String lnkViewallpreord="View all previous orders";
        public static final String lnkViewallpreord_loc="linkText";
        //Previous Orders Text
        public static final String txtPreviousOrders="h2[contains(text(),'Previous Orders')]";
        public static final String txtPreviousOrders_loc="xpath";
        // Jotter multi-search  left hand panel link
        public static final String lnkLeftJtrMultSrch="//h2[contains(text(),'Jotter multi-search')]";
        public static final String lnkLeftJtrMultSrch_loc="xpath";
       //Jotter note pad
        public static final String tabNotepad="//div[@class='jotter-pad']";
        public static final String tabNotepad_loc="xpath";
        
       //Product grid
	    public static final String tabProductGrid="//div[@class='products-grid']/div[@class='products-row']";
	    public static final String tabProductGrid_loc="xpath";
	    
	    
        
       
        

        //Selected Slot link(Nanditha - Created on 20-Aug-13) 
//        public static final String lnkSelectedSlot = "//div[@id='headerDelivery']/p[2]/a";
        public static final String lnkSelectedSlot = "//*[contains(text(),'Edit slot')]";
        public static final String lnkSelectedSlot_Loc = "xpath";

        //Edit Slot button
        public static final String btnEditSlot = "//*[contains(text(),'Edit slot')]";
        public static final String btnEditSlot_Loc = "xpath";


 //Change Slot button(Nanditha - Created on 20-Aug-13)
        //public static final String btnChangeSlot = "//a[contains(text(),'Change slot')]"; // identifier object changed in 6.2 (jan 27)
        public static final String btnChangeSlot = "//*[contains(text(),'Change slot')]";
        public static final String btnChangeSlot_Loc = "xpath";
        

      //GroceriesLink(Updated on 20-Aug-13)
      	    public static final String linkGroceries = "//a[contains(text(),'Groceries')]";
      	    public static final String linkGroceries_Loc = "xpath";
      	//Sublink count
      	    public static final String linksubcategorycount="//div[@class='merchandising espot_in_left_nav']/nav/ul/li[2]/h4";
      	  public static final String linksubcategorycount_loc="xpath";



      	  //Entertaining link (Nanditha - Created on 05-Sep-13)
      	  public static final String lnkEntertaining = "//a[contains(text(),'Entertaining')]";
      	  public static final String lnkEntertaining_Loc = "xpath";


      	//All SUBLINKS OF PERTICULAR SUB AISEL varying
      	public static final String txtallLinks="//ul/li[@class='regular hover']/div/div[1]/div[1]/ol/li[LinkIndex]";
      	public static final String txtallLinks_loc="xpath";
      	//for count
      	public static final String txtTotLinks="//ul/li[@class='regular hover']/div/div[1]/div[1]/ol/li";
      	public static final String txtTotLinks_loc="xpath";
      	//First sublink on mouse over
      	public static final String linkFirstSublink="//div[@class='mega-menu-holder']/div[1]/div[1]/ol/li/h4/a";
      	public static final String linkFirstSublink_loc="xpath";
      	
      	

        //all left navigation links
      	  public static final String linkAllleftPanel="//div[@class='merchandising espot_in_left_nav']/nav/ul/li[2]/ul/li/a";
      	public static final String linkAllleftPanel_loc="xpath";

      //all left navigation links varying
    	  public static final String linkAllleftPanelvary="//div[@class='merchandising espot_in_left_nav']/nav/ul/li[2]/ul/li[Index]/a";
    	public static final String linkAllleftPanelvary_loc="xpath";
    	//Perticular left link
      	public static final	String	SpecificLeftLink="//nav[@class='refinement groceries']/ul/li[2]/ul/li/a[contains(text(),'ValToReplace')]";
      	public static final	String	linkSpecificLeftLink_loc="xpath";

 //Favourites Link
	    public static final String linkFavourites = "Favourites";//a[@href='/shop/MyFavourites']";
	    public static final String linkFavourites_Loc = "linkText";
	    

// All left panle links varying
      	public static final String txtAllLeftPanelLinks="//nav[@class='refinement groceries']/ul/li[2]/ul/li[LinkIndex]";
      	public static final String txtAllLeftPanelLinks_loc="xpath";
      	// All left panle links const
      	public static final String txtTotLeftPanelLinks="//nav[@class='refinement groceries']/ul/li[2]/ul/li";
      	public static final String txtTotLeftPanelLinks_loc="xpath";
      	

      //myWaitrose Savings in header(Nanditha - created on 10-Oct-13)
            	public static final String myWaitroseSavings = "//span[@class='trolley-savings']";
            	public static final String myWaitroseSavings_Loc = "xpath";
            	
            	                
               
                //public static final String btnBookDelivery="//a[contains(text(),'Book a slot')]"; // comments as not working in 6.2 changes
            	 public static final String btnBookDelivery="//*[contains(text(),'Book a slot')]";
            	public static final String btnBookDelivery_Loc="xpath";
                
                //-checking flyout(kathir - created on- 10-Jul-14)
                public static final String gridFlyout = "//div[(@class='mega-menu-holder') and contains(@style,'block')]";
				
				// Flyout menu item header link(Kathir - created on 15-Jul-14)
                public static final String lnkMenuHeaderFlyout = "//div[@class='mega-menu-item']/ol/li/h4/a";
                

              //Message under checkout button
              public static final String msgUnderChkout="p.static";//div[@class='min-spend']/p";
              public static final String msgUnderChkout_loc="cssSelector";	
              //mouse hover on checkout button
              public static final String txtMsgOnChkout="//div[@class='tooltip is-showing']/p";
              public static final String txtMsgOnChkout_loc="xpath";
              //CHECKOUT BUTTON
              public static final String btnChkOut="//div[@id='headerBasket']/div[2]";
              public static final String btnChkOut_loc="xpath";

              //Change service
      //        public static final String linkChangeService="Change service";
              public static final String linkChangeService="//button[contains(text(),'Change service')]";
              public static final String linkChangeService_loc="xpath";
              
            //Go to your Favourites(uPdated on 060814)
      	    public static final String lnkGotoyourFavourites="View Favourites";
      	    public static final String lnkGotoyourFavourites_loc="linkText";
      	   
		  

		//jotter landing for logged in user(Updated on 200814)
			    public static final String tabjottersec="//div[@class='jotter-pad']/./form";
			    public static final String tabjottersec_loc="xpath";

		public static String linkFavPreOrd="//li[@class='tab-button']/div/a/h2";
				  public static String linkFavPreOrd_loc="xpath";
				
				
				  
				
				  public static final String linkEntertaining = "//a[contains(text(),'Entertaining')]";
				  public static final String linkEntertaining_Loc = "xpath"; 
				  

				//Reservation Confirmation Header Text in Overlay(Created By JAyandran on 13/11/2014)
								public static final String txtReservationHeaderText="//p[contains(text(),'Great - Your seasonal item has now been reserved')]";
								public static final String txtReservationHeaderText_loc="xpath";
								 
								    
								//Reservation Confirmation Body Text(Please Note) in Overlay(Created By JAyandran on 13/11/2014)
								public static final String txtReservationBodyText1="//p[contains(text(),'Please note.')]";
								public static final String txtReservationBodyText1_loc="xpath";
								    
								//Reservation Confirmation Body Text in Overlay(Created By JAyandran on 13/11/2014)
								public static final String txtReservationBodyText2="//p[contains(text(),'You have added this product to your order, but you will need to collect and pay for it in store - payment')]";
								public static final String txtReservationBodyText2_loc="xpath";
								    
								//Reservation Confirmation Body Text in Overlay(Created By JAyandran on 13/11/2014)
								public static final String txtReservationBodyText3="//p[contains(text(),'You will need to check this order out in order to secure your reservation.')]";
								public static final String txtReservationBodyText3_loc="xpath";
								    
								//Reservation Confirmation Checkbox in Overlay(Created By JAyandran on 13/11/2014)
								public static final String btnReservationCheckBox="checkres";
								public static final String btnReservationCheckBox_loc="id"; 
								
								//Reservation Confirmation Overlay - Continue Button Disabled Status(Created By JAyandran on 13/11/2014)
								public static final String btnContinueReservationStatus="//*[@class='button content-button no-image resrvButton is-disabled']";
								public static final String btnContinueReservationStatus_loc="xpath"; 
								
								
								//Reservation Confirmation Overlay - Continue Button(Created By JAyandran on 14/11/2014)
								public static final String btnContinueReservation="//div[@class='button content-button no-image resrvButton']/a[text()='Continue']";
								public static final String btnContinueReservation_loc="xpath";


				//UnavailableItem Overlay - View Trolley Button(Created By JAyandran on 17/11/2014)
								public static final String btnViewTrolley="(//*[contains(text(),'View trolley')])";
								public static final String btnViewTrolley_loc="xpath"; 
								
								//UnavailableItem Overlay - ChangeSlot Button(Created By JAyandran on 17/11/2014)
								public static final String btnChangeSlotInUnAvailableOverlay="(//*[contains(text(),'Change slot')])";
								public static final String btnChangeSlotInUnAvailableOverlay_loc="xpath";
								
								
								
								
								//UnavailableItem Overlay - RemoveFromTrolley(Created By JAyandran on 18/11/2014)
								public static final String linkRemoveFromTrolley="(//a[@id='conflict-product-remove']//span[@class='txt-hardconflict'])";
								public static final String linkRemoveFromTrolley_loc="xpath";
								

								//bread crumb list
								public static final String lnkBredcrumblist="//div[@class='breadcrumbs standard']/ul/li[index]";
								public static final String lnkBredcrumblist_loc="xpath";
								
								
								

								//offer bread crumb created on 05-nov-2014

								public static final String linkOfferBreadcrumb="//div[@class='breadcrumbs standard']/ul/li[3]";
								public static final String linkOfferBreadcrumb_loc="xpath";
								
								//UnavailableItem Overlay - Continue Button(Created By JAyandran on 18/11/2014)
								public static final String btnContinue="(//a[@id='conflicts-overlay-continue'])[2]";
								public static final String btnContinue_loc="xpath";
                               // Unavailable items message in overlay
								
								public static final String msgunavailableoverlay = "(//div[@class='conflict-order-content']/h2[contains(text(),'Some offers will have finished before your delivery date')])[2]";
								public static final String msgunavailableoverlay_Loc = "xpath";
								
								//header message
								public static final String txtdc="//p[@class='delivery-charge']";
								public static final String txtdc_loc="xpath";
								//delivery charge
								public static final String txtDelivC="//p[@class='delivery-charge']/strong[1]";
								public static final String txtDelivC_loc="xpath";
								//banner value
								public static final String txtbanner="//p[@class='delivery-charge']/strong[2]";
								public static final String txtbanner_loc="xpath";
								//close 
								public static final String linkClose="//a[@class='close']";
								public static final String linkClose_loc="xpath";
								
								//Trolley Amounts
								public static final String trolleyAmt = "//span[@class='trolley-total']"; //div[@class='totals']//span";
								public static final String trolleyAmt_Loc = "xpath";
								public static double trolleyAmount = -1;
								public static String slotDate;
								public static String slotTime;
								
								
								// product unavailable alert header 
								
								public static final String txtProductUnavailable = "(//div[@class='section-content conflicts-overlay-inner']//h2[contains(text(),'Unavailable items')])[2]";
						
								public static final String txtProductUnavailable_Loc = "xpath";
								
								// continue button disabled
								
								public static final String btnContinueDisabled = "//div[@class='button content-button is-disabled']";
								public static final String btnContinueDisabled_Loc = "xpath";
								
								
								//Browse Shop
								
								public static final String btnBrowseShop = "//div[@id='megaMenu']//button[text()='Browse Shop']";
								public static final String btnBrowseShop_loc = "xpath";
								
								//Groceries tab under BrowseShop
								public static final String lnkGroceriesBS = "//div[@class='dropdown-menu megaMenu']//a[text()='Groceries']";
								public static final String lnkGroceriesBS_loc = "xpath";
							
								//Entertaining tab under BrowseShop
								public static final String lnkEntertainingBS = "//div[@class='dropdown-menu megaMenu']//a[text()='Entertaining']";
								public static final String lnkEntertainingBS_loc = "xpath";
								 
								//admin phone symbol in mainsite
								public static final String logoOFPhone = "//div[@class='services-bar']//li[@class='phone']";
								public static final String logoOFPhone_loc = "xpath";
}
