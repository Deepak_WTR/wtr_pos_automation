package uimap;

public class FastMail { 



	//Email (Created by : Prabhakaran Sankar)
	public static final String Fastemail = "//input[@name='username']";
	public static final String Fastemail_Loc = "xpath";

	//Password (Created by : Prabhakaran Sankar)
	public static final String Fastpassword = "//input[@type='password']";
	public static final String Fastpassword_Loc = "xpath";

	//Login button (Created by : Prabhakaran Sankar)
	public static final String LoginButton = "//span[contains(text(),'Log In')]";
	public static final String LoginButton_Loc = "xpath";
	//Total value
	public static final String txtItemTotal="(//strong[contains(text(),'Total')])[1]";
	public static final String txtItemTotal_loc="xpath";
	// item total	
	public static String txttotal="(//table/tbody/tr/td/strong[contains(text(),'Total')]/../../td[2])[1]";
	public static String txttotal_loc="xpath";
	public static String total="";
	public static String dcvalue="";
	//delivery charge
	public static String txtdcValue="(//table/tbody/tr/td[contains(text(),'Delivery charge')]/../td[2])[1]";
	public static String txtdcValue_loc="xpath";
	


	// Use by date in order confirmation mail
	public static final String txtusebydate = "//small[contains(text(),'Use by date')]";
	public static final String txtusebydate_Loc = "xpath";


	// My waitrose savings 

	public static final String txtMywaitroseoffers = "//strong[contains(text(),'myWaitrose Savings')]";
	public static final String txtMywaitroseoffers_Loc = "xpath";

	

	// Branch rule 12 

	public static final String txtbranchrule1 = "//td[contains(text(),'Please note that your right to return products does not apply to goods made to your specification, that have been clearly personalise')]";

	public static final String txtbranchrule1_loc = "xpath";


	public static final String txtbranchrule2 = "//td[contains(text(),'Please note that you are entitled to cancel this contract if you so wish')]";

	public static final String txtbranchrule2_loc = "xpath";


	public static final String txtbranchrule3 = "//td[contains(text(),'For further information please read our')]//a";

	public static final String txtbranchrule3_loc = "xpath";
	
	
	// mail content
	

	public static final String txtmailcontent1 = "//td[contains(text(),'For further information please read our')]//a";

	public static final String txtmailcontent1_loc = "xpath";
	public static final String txtmailcontent2 = "//td[contains(text(),'For further information please read our')]//a";

	public static final String txtmailcontent2_loc = "xpath";
	public static final String txtmailcontent3 = "//td[contains(text(),'For further information please read our')]//a";

	public static final String txtmailcontent3_loc = "xpath";

	
	// search 
	
	public static final String inputsearch = "//input[@placeholder='Search mail']";
	public static final String inputsearch_loc = "xpath";
	
	// your registration details
	

	
	public static final String subregdetails = "//span[contains(text(),'Your registration details')]";
	public static final String subregdetails_loc = "xpath";
	

	// your registration details
	

	
	public static final String txtCollectionLocker = "//td[contains(text(),'We�ll now pick and pack your order with care, ready for you to collect from your locker')]";
	public static final String txtCollectionLocker_loc = "xpath";

// return date for loan items 

	public static final String txtreturndateforloanitems = "//td[contains(text(),'Please return them')]";
	public static final String txtreturndateforloanitems_loc = "xpath";
	
	
	// Order Details Text

		public static final String txtOrderDetails = "//h2[contains(text(),'Order Details')]";
		public static final String txtOrderDetails_loc = "xpath";
		
		// confirmation on your offer 
		
		public static final String lnkConfirmationoffer = "(//span[contains(text(),'your myWaitrose Offers')])[1]";
		                                                   
		public static final String lnkConfirmationoffer_Loc = "xpath";
		
		
		// My offer savings 
		
		public static final String tabMyOfferSavings = "(//table/tbody/tr/td/strong[contains(text(),'My Offers Savings')]/../../td[2])[1]";
public static final String tabMyOfferSavings_Loc = "xpath";
}

