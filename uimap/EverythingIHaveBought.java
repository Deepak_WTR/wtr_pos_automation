package uimap;

public class EverythingIHaveBought {


	//Content Management Text 1
	public static final String txtContentManagement1 = "//div[@class='l-content']/p";
	public static final String txtContentManagement1_Loc = "xpath";
	
	//Content Management Text 2
		public static final String txtContentManagement2 = "//div[@class='r-content']/p[1]";
		public static final String txtContentManagement2_Loc = "xpath";
	
		//Content Management Text 3
		public static final String txtContentManagement3 = "//div[@class='r-content']/p[2]";
		public static final String txtContentManagement3_Loc = "xpath";
		
		//Add Button
		public static final String buttonAdd = "button add-button";
		public static final String buttonAdd_Loc = "className";
	

}
