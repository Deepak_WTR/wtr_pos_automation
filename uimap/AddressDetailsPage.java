package uimap;

public class AddressDetailsPage {
	//Address details page checkout heading
	public static final String addressDetailsHeading="//h2[contains(text(),'Delivery address details')  or contains(text(),'Your address details')]";
	public static final String addressDetailsHeading_loc="xpath";
	
	//Save and Continue button
	public static final String btnSaveAndContinue="//input[@value='Save and continue']";
	public static final String btnSaveAndContinue_loc="xpath";
	
	
	public static final String txtDriverNotes = "notes";
	public static final String txtDriverNotes_loc = "id";
	
	public static final String chkAlternateAddress = "alternativeAddress";
	public static final String chkAlternateAddress_loc = "id";
	
	public static final String txtAlternateContact = "alternativeContact";
	public static final String txtAlternateContact_loc = "id";
}

