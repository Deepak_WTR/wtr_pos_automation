package uimap;



public class OfCustomerRegistration {

	//shop in branch we deliver
	public final static String linkSIBWD="Shop in Branch we deliver";
	public final static String linkSIBWD_Loc="linkText";
		
	//customer registration
	public final static String linkCustReg="Customer Registration";
	public final static String linkCustReg_Loc="linkText";

	//enter postcode
	public final static String txtPostcode="postCode";
	public final static String txtPostcode_Loc="id";
	
	//click postcode
	public final static String btnPostcode="checkPostCode";
	public final static String btnPostcode_Loc="name";
	
	//continue to register
	public final static String btnContinueToRegister="continueRegister";
	public final static String btnContinueToRegister_Loc="name";
	
	// logon id
	public final static String txtLogin="logonId";
	public final static String txtLogin_Loc="id";
	
	// password
	public final static String txtPassword="password";
	public final static String txtPassword_Loc="id";
	
	// confirm password
	public final static String txtConfirmPassword="verifyPassword";
	public final static String txtConfirmPassword_Loc="id";
	
	// title
	public final static String txtTitle="personTitle";
	public final static String txtTitle_Loc="id";
	
	// firstname
	public final static String txtFirstName="firstName";
	public final static String txtFirstName_Loc="id";
	
	// lastname
	public final static String txtLastName="lastName";
	public final static String txtLastName_Loc="id";
	
	// nickName
	public final static String txtNickName="nickName";
	public final static String txtNickName_Loc="id";
	
	// phoneNumber
	public final static String txtphoneNumber="phoneNumber";
	public final static String txtphoneNumber_Loc="id";

	// termsAndConditions
	public final static String btntermsAndConditions="termsAndConditions";
	public final static String btntermsAndConditions_Loc="id";
	
	// printAcceptance
	public final static String btnprintAcceptance="printAcceptance";
	public final static String btnprintAcceptance_Loc="id";
	
	// submitThisForm
	public final static String btnsubmitThisForm="submitThisForm";
	public final static String btnsubmitThisForm_Loc="name";
	
	// selectedAddress
	public final static String btnselectedAddress="selectedAddress";
	public final static String btnselectedAddress_Loc="id";
	
	// address line 1
	public final static String txtAddress1="addressLine1";
	public final static String txtAddress1_Loc="id";
	
	// country
	public final static String txtCountry="country";
	public final static String txtCountry_Loc="id";

	// Mobile Number
	public final static String txtMobileNumber="mobileNumber";
	public final static String txtMobileNumber_Loc="id";
	
	// Delivery Note
	public final static String txtDeliveryNote="deliveryNote";
	public final static String txtDeliveryNote_Loc="id";
	
	// Screen Name
	public final static String txtScreenName="screenName";
	public final static String txtScreenName_Loc="id";
		
	// Describe Yourself Text
	public final static String txtDescribeYourself="describeYourself";
	public final static String txtDescribeYourself_Loc="id";	
		
	// Favourite food text 
	public final static String txtFavouriteFood="favoriteFood";
	public final static String txtFavouriteFood_Loc="id";
		
	// Location text
	public final static String txtLocation="location";
	public final static String txtLocation_Loc="id";
		
	
	
	// customer number
	public final static String labelcustomernumber="//td/span";
	public final static String labelcustomernumber_Loc="xpath";
}

