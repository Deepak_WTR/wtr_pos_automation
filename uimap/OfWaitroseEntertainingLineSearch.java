package uimap;



public class OfWaitroseEntertainingLineSearch {

	//Line bumber search - Prabhakaran Sankar - 13-march-15
	public final static String lineNumberSearch="//input[@name='prnbr']";
	public final static String lineNumberSearch_Loc="xpath";
		
	
	//Line bumber search - Prabhakaran Sankar - 13-march-15
	public final static String branchNumber="//input[@name='branch_rn']";
	public final static String branchNumber_Loc="xpath";
	
	//WAitrose Entertaining Line search page header  - Prabhakaran Sankar - 13-march-15
	public final static String wtrEntLineSearchPageHeader="//td[contains(text(),'WaitroseEntertaining Line Search')]";
	public final static String wtrEntLineSearchPageHeader_Loc="xpath";
	
	//Search button - Prabhakaran Sankar - 13-march-15
	public final static String btnSearch="//td[@class='buttonCenter']//a[contains(text(),'search')]";
	public final static String btnSearch_Loc="xpath";
	
	
	//Select fisrst order - Prabhakaran Sankar - 13-march-15
	public final static String selectOrderNo="(//input[@name='order'])[1]/../a";
	public final static String selectOrderNo_Loc="xpath";
	
	//Select fisrst order - Prabhakaran Sankar - 13-march-15
		public final static String selectOrderNo_Updated="(//input[@name='order'])[1]/../a";
		public final static String selectOrderNo_Updated_Loc="xpath";
	
	
}
