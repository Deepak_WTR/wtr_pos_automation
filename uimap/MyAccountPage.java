package uimap;

public class MyAccountPage {
	//My Account Heading	
	public static final String txtMyAccHeading="//h2[contains(text(),'My Account')]";
	public static final String txtMyAccHeading_loc="xpath";

	//My Shopping Heading	
	public static final String txtMyShoppingHeading="//*[contains(text(),'My Shopping')]";
	public static final String txtMyShoppingHeading_loc="xpath";

	//My Waitrose Heading	
	public static final String txtMyWaitroseHeading="//*[contains(text(),'myWaitrose')]";
	public static final String txtMyWaitroseHeading_loc="xpath";
	// MyOrders link
//	public static final String lnkMyOrder="My Orders";
	public static final String lnkMyOrder="(//a[contains(text(),'My Orders')])[2]";//"//a[@class='myaccountlink'][contains(text(),'My Orders')]";
//	public static final String lnkMyOrder_loc="linkText";
	public static final String lnkMyOrder_loc="xpath";
	
	public static final String lnkMyOrderhmg="//li/a[contains(text(),'My Orders')]";//"//a[@class='myaccountlink'][contains(text(),'My Orders')]";
//	public static final String lnkMyOrder_loc="linkText";
	public static final String lnkMyOrderhmg_loc="xpath";
	
	//MyFavorites Link
	public static final String lnkMyfavorites="My Favourites";
	public static final String lnkMyfavorites_loc="linkText";
	//Mylist link
	public static final String lnkMyLists="My Lists";
	public static final String lnkMyLists_loc="linkText";
	//My details TEXT
	public static final String txtMydetailsHdng="//*[contains(text(),'My Details')]"; 
	public static final String txtMydetailsHdng_loc="xpath";
	//My login Details link
	public static final String lnkMyloginDetails="My login details";
	public static final String lnkMyloginDetails_loc="linkText";
	//My Personal Details link
	public static final String linkPersonalDetails="Personal details";
	public static final String linkPersonalDetails_loc="linkText";
	//My addressess link
	public static final String linkMyAddressess="My Addresses";
	public static final String linkMyAddressess_loc="linkText";
	//My Current address link
	public static final String linkCurrentAdd="View your current delivery address";
	public static final String linkCurrentAdd_loc="linkText";
	//Add New Address link 
	public static final String linkNewAddress="Add a new delivery address"; 
	public static final String linkNewAddress_loc="linkText";
	//Paymentdetails link
	public static final String lnkPayment="Payment Details & Billing Information"; 
	public static final String lnkPayment_loc="linkText";
	//MY Order heading
	public static final String txtMyOrderHdng="//h1[contains(text(),' My Orders')]";
	public static final String txtMyOrderHdng_loc="xpath";
	//MY Waitrose Link
	public static final String lnkmyWaitrose="myWaitrose";
	public static final String lnkmyWaitrose_loc="linkText";

	//My favorites tabs
	public static final String txtMyFavoriteTabs="//*[@id='favourites-page']/div/ul"; 
	public static final String txtMyFavoriteTabs_loc="xpath"; 

	public static final String txtMyListsHdng="//h2[contains(text(),'Your Lists')]";
	public static final String txtMyListsHdng_loc="xpath";
	//Create New list text field
	public static final String txtCreateNewList="list_new";
	public static final String txtCreateNewList_loc="id";
	//CreateNew list button
	public static final String btnCreateList="//input[@value='Create list']";
	public static final String btnCreateList_loc="xpath"; 

	public static final String btnOK="//a[contains(text(),'OK')]";
	public static final String btnOK_loc="xpath";
	//MyAccount button in left panel
	public static final String btnMyAccount="//a[contains(text(),'My Account')]";
	public static final String btnMyAccount_loc="xpath";
	//View button in list
	public static final String btnView="//a[contains(text(),'View')]";
	public static final String btnView_loc="xpath";

	public static final String txtEmptylist="//*[@id='favourites-page']/div/div[3]/div/p[3]";
	public static final String txtEmptylist_loc="xpath";
	//Delete Button 
	public static final String btnDeleteList="//a[contains(text(),'Delete')]";
	public static final String btnDeleteList_loc="xpath";

	public static final String btnDeleteListInPopup="//a[contains(text(),'Delete list')]";
	//Cancel order LINK
	public static final String lnkCancelthisOrder="//span[contains(text(),'Cancel this order')]";////table/tbody/tr[2]/td[1]/a/span[2]  
	public static final String lnkCancelthisOrder_loc="xpath";
	//reshcedule order in pop up
	public static final String lnkReShOrder="reschedule your order";
	public static final String lnkReShOder_loc="linkText";
	//yes cancel order in pop
	public static final String btnYesImSure="Yes, I'm sure";
	public static final String btnYesImSure_loc="linkText";
	//Amend Order button 
	public static final String btnAmendOrder="//table/tbody/tr[1]/td[5]/div/div[2]/a[contains(text(),'Amend order')]";
	public static final String btnAmendOrder_loc="xpath";

	public static final String txtAmenOrderPopup="h2[contains(text(),'Amending order #')]";
	public static final String txtAmenOrderPopup_loc="xpath";

	public static final String btnContiue="Continue";
	public static final String btnContinue_loc="linkText";

	public static final String txtamendHdng="//h1[contains(text(),'My Trolley - Amending order #')]";
	public static final String txtamendHdng_loc="xpath";

	public static final String lnkAddmoreShpng="Add more shopping";
	public static final String lnkAddmoreShpng_loc="xpath";

	public static final String btnCheckout="Checkout";
	public static final String btnCheckout_loc="linkText";

	public static final String lnkaddalltofav="Add all to Favourites";
	public static final String lnkaddalltofav_loc="linkText";

	public static final String lnkaddalltoList="Add all to a List";
	public static final String lnkaddalltoList_loc="linkText";

	public static final String lnkEmptyTrolley="Empty trolley";
	public static final String lnkEmptyTrolley_loc="linkText";

	public static final String btnContShpng="Continue shopping";
	public static final String lnkRemovefromTrolley="//div[@class='m-product-cell m-product-cell-manipulation']/div/div/div[3]/p[2]/a/span[2]";

	public static final String lnkRemovefromTrolley_loc="xpath";
	public static final String btnfinishnnextstep="Finished here. Next step";

	public static final String btnfinishnnextstep_loc="linkText";
	public static final String btncntCheckout="Continue to checkout";

	public static final String btncntCheckout_LOC="linkText";
	public static final String btncntshpng="Continue shopping";

	public static final String btncntshpng_loc="linkText";
	public static final String btncntPayment="Continue to payment details";

	public static final String btncntPayment_loc="linkText";
	public static final String btncntPayOrder="Place your order";

	public static final String btncntPayOrder_loc="linkText";
	public static final String txtOrderConfirm="//h2[contains(text(),'Order Confirmation')]";

	public static final String txtOrderConfirm_loc="xpath";
	public static final String lnkCancelChange="Cancel changes";

	public static final String lnkCancelChange_loc="linkText";

	public static final String btnBookdelivery="Book delivery";
	public static final String btnBookdelivery_loc="linkText";

	public static final String txtpostcode="//input[@id='postcode_lookup_input']";
	public static final String txtpostcode_loc="xpath";

	public static final String btnOk1="//input[@value='Ok']";
	public static final String btnOk1_loc="xpath";

	public static final String rdoSelectaddress="//input[@name='address_radio_group']";
	public static final String rdoSelectaddress_loc="xpath";

	public static final String selectslot="//*[@id='slot_181390']";
	public static final String selectslot_loc="xpath";

	public static final String btnstrShpng="Start shopping";
	public static final String btnstrShpng_loc="linkText";

	public static final String lnkaddalltopendingOrder="Add all to pending order";
	public static final String lnkaddalltopendingOrder_loc="linkText";

	public static final String btnConfirm="Confirm";
	public static final String btnConfirm_loc="linkText";

//	public static final String lnkPendingViewOrder="//*[@id='content']/div[2]/div[1]/table/tbody/tr[1]/td[1]/a";
	public static final String lnkPendingViewOrder="//div[@id='content']//div[1]/table/tbody/tr[1]/td[1]/a";
	public static final String lnkPendingViewOrder_loc="xpath";



	public static final String btnViewList="//*[@id='listName']";
	public static final String btnViewList_loc="xpath";

	public static final String btnFirstListname="//tbody/tr[1]/td[1]/a";
	public static final String btnFirstListname_loc="xpath";



	//MyAccount Modal Page UI
	//Personal details
	//duplicate
	//title
	public static final String txtTitle ="title";
	public static final String txtTitle_Loc ="id";
	//first name
	public static final String txtFirstName ="firstName";
	public static final String txtFirstName_Loc="id";
	//last name
	public static final String txtLastName ="lastName";
	public static final String txtLastName_Loc="id";
	//Phone 1
	public static final String txtPhone1 ="phone1";
	public static final String txtPhone1_Loc="id";
	//Phone 2
	public static final String txtPhone2 ="phone2";
	public static final String txtPhone2_Loc="id";
	//Tick if you would prefer not to receive information about John Lewis Department Stores
	public static final String chbjlstores="jlstores";
	public static final String chbjlstores_Loc="id";
	//Tick if you would prefer not to receive information about Greenbee, Jouh Lewis Insurance and John Lewis Financial Services Ltd
	public static final String chbjlfinance="jlfinance";
	public static final String chbjlfinance_Loc="id";	
	//Tick if you would prefer not to receive any information about Waitrose
	public static final String chbwaitrose="waitrose";
	public static final String chbwaitrose_Loc="id";
	//Save
	public static final String btnSave="//input[@value='Save']";
	public static final String btnSave_Loc="xpath";

	//Card Details
	//Payment Details & Billing Information
	public static final String linkPaymentDetail ="Payment Details & Billing Information";
	public static final String linkPaymentDetail_Loc ="linkText";
	//card number
	public static final String txtCardNumber ="card_number";
	public static final String txtCardNumber_Loc = "id";
	//issue month
	public static final String ddbtnIssueMonth ="issueMonth";
	public static final String ddbtnIssueMonth_Loc ="name";
	//issue year
	public static final String ddbtnIssueYear ="card_valid_from";
	public static final String ddbtnIssueYear_Loc ="id";	

	//Card Name
	public static final String txtCardName ="card_name";
	public static final String txtCardName_Loc = "id";

	//login details
	//my login details
	public static final String linkMyLoginDetails ="My login details";
	public static final String linkMyLoginDetails_Loc = "linkText";
	//email
	public static final String txtEmail="email";
	public static final String txtEmail_Loc="id";
	//email confirm
	public static final String txtEmailConfirm="email-confirm";
	public static final String txtEmailConfirm_Loc="id";
	//current password
	public static final String txtCurrentPass="current_password";
	public static final String txtCurrentPass_Loc="id";
	//new password
	public static final String txtNewPass="password";
	public static final String txtNewPass_Loc="id";
	// confirm new password
	public static final String txtNewPassConfirm="password-confirm";
	public static final String txtNewPassConfirm_Loc="id";

	//Address Details
	//my Address
	public static final String linkMyAddress="My Addresses";
	public static final String linkMyAddress_Loc="linkText";
	//add a new address
	public static final String linkAddNewAddress="Add a new address";
	public static final String linkAddNewAddress_Loc="linkText";
	
	public static final String MyAddNewAddress="(//div/h3/a[@class='myaccountlink'])[7]";
			public static final String MyAddNewAddress_Loc="xpath";
	//postcode
	public static final String txtPostcode ="postcode";
	public static final String txtPostcode_Loc ="id";
	//check postcode
	public static final String btnPostcode ="//input[@value='Check postcode']";
	public static final String btnPostcode_Loc ="xpath";

	//nick name
	public static final String txtNickname="//input[@name='nickName']";
	public static final String txtNickname_Loc="xpath";
	
		//title
		public static final String Nickname="//input[@name='addressNickName']";
		public static final String Nickname_Loc="xpath";
		/*//FirstName
		public static final String txtFirstname="firstName";
		public static final String txtFirstname_Loc="id";
		//lastname
		public static final String txtLastname="lastName";
		public static final String txtLastname_Loc="id"; */
	//save new billing address
	public static final String chbSaveNewBillingAdd="//input[@name='alternativeAddress']";
	public static final String chbSaveNewBillingAdd_Loc="xpath";
	
	public static final String secText="//textarea[@name='alternativeContact']";
	public static final String secText_Loc="xpath";
	
	//save this address
	public static final String btnSaveAddress="//input[@value='Save this address']";
	public static final String btnSaveAddress_Loc="xpath";


	////Public Profile Heading
	public static final String txtPublicProfileHdng="//h1[contains(text(),'Your Scrapbook')]";
	public static final String txtPublicProfileHdng_loc="xpath";





	//My Articles Heading
	public static final String lnkArticlesHdng="//h1[contains(text(),'My Articles')]";
	public static final String lnkArticlesHdng_loc="xpath";




	//Save BUTTON
	// public static final String btnSave="//input[@value='Save']";



	//My First list name link
	public static final String txtMyFirstList="//tbody[@class='shoppingList-container']/tr[1]/td/a";
	public static final String txtMyFirstList_loc="xpath";
	// My login details Heading(Gajapathy on050813)
	public static final String txtMyloginHdng="//h1[contains(text(),'My Login Details')]";
	public static final String txtMyloginHdng_loc="xpath";
	//Change Slot button (Nanditha - created on 5-Aug-13)
	public static final String btnChangeSlot = "//a[contains(text(),'Change slot')]";
	public static final String btnChangeSlot_Loc = "xpath";


	//View Order link -My Orders(Nanditha - Created on 05-Aug-13)
	public static final String lnkViewOrder = "//a[contains(text(),'View order no. Number')]";
	public static final String lnkViewOrder_Loc = "xpath";





	//add new billing address in paymnet page
	public static final String rdoaddnewbillingaddress = "newBillingAddressRadio";
	public static final String rdoaddnewbillingaddress_Loc = "id";



	//zipcode
	public static final String txtZipcode ="zipCode";
	public static final String txtZipcode_Loc ="id";
	//add a new address
	public static final String linkNewBillAddress="Find my address";
	public static final String linkNewBillAddress_Loc="linkText";

	//save and continue(210813)
	public static final String btnSaveContinue="//input[@value='Save and continue']";
	public static final String btnSaveContinue_loc="xpath";




	//Updated Nanditha on 300813
	public static final String lnkCancelViewOrder="//*[@id='content']/div[2]/div[2]/table/tbody/tr[2]/td[1]/a";
	public static final String lnkCancelViewOrder_loc="xpath";

	//Updated Nanditha on 300813
	public static final String txtCancelled="//*[@id='content']/div[2]/div[2]/table/tbody/tr[2]/td[4]";
	public static final String txtCancelled_loc="xpath";






	//Recipes & Cookbooks Heading updated on(300813)
	public static final String lnkRecipesCookbooksHdng="//h1[contains(text(),'Recipes and cookbooks')]";
	public static final String lnkRecipesCookbooksHdng_loc="xpath";

	//My Comments Heading(Updated on 300813)
	public static final String lnkMyCommentsHdng="//h1[contains(text(),'My Comments')]";
	public static final String lnkMyCommentsHdng_LOC="xpath";

	//select address(Nanditha -Updated on 300713)Gajapathy-300813

	//My Public Profile
	public static final String txtMyPublicProfile="//h1[contains(text(),'My Public Profile')]";
	public static final String txtMyPublicProfile_loc="xpath";

	//StartingHeading
	public static final String txtSrtHeading="//h1[contains(text(),'Weekly Shopping at waitrose.com is quick, easy and great value')]";
	public static final String txtSrtHeading_loc="xpath";


	//Screen Name2
	public static final String txtScreenName1="//input[@id='screen_name']";
	public static final String txtScreenName1_loc="xpath";

	//Save public profile
	public static final String btnSavePublicprofile="//input[@value='Save public profile']";
	public static final String btnSavePublicprofile_loc="xpath";
	//ALL LIST NAMES UPDATED ON 010913
	public static final String txtAllListNames="//tr[index]/td[@class='listName']/a";
	public static final String txtAllListNames_loc="xpath";



	//Delete button (Nanditha - Created on 06-Sep-13)
	public static final String btnDeleteCard = "//a[contains(text(),'Delete')]";
	public static final String btnDeleteCard_Loc = "xpath";

	//Yes button (Nanditha -Created on 06-Sep-13)
	public static final String btnYes = "//a[contains(text(),'Yes')]";
	public static final String btnYes_Loc = "xpath";

	//Card edit box (Nanditha - Created on 06-Sep-13)
	public static final String txtboxCard = "//input[@id='card_number']";
	public static final String txtboxCard_Loc = "xpath";
	//Expire Month
	public static final String ddbtnExpireMonth ="endMonth";
	public static final String ddbtnExpireMonth_Loc ="id";

	//Expire Year
	public static final String ddbtnExpireYear ="endYear";
	public static final String ddbtnExpireYear_Loc ="id";


	//Billing Address(Nirmal -created on 24-SEP-2013)
	public static final String txtBillingAddress_loc="xpath";
	public static final String txtBillingAddress="//h2[contains(text(),'Billing address')]";


	//View Order Overlay-Product Name (Nanditha - Updated on 03-10-2013)

	public static final String lnkViewOrderPrdName_Loc ="xpath";


	// MemberShip status(Nirmal on 7-Oct-2013)
	public static final String txtMembershipStatus_loc = "xpath";
	public static final String txtMembershipStatus = "//span[2][contains(text(),'Membership activated')]";


	//public static final String popupClose_loc = "linkText";
	public static final String popupClose_loc = "xpath";
	public static final String popupClose = "//a[@class='close']";

	public static final String txtMembershipStatus1_loc = "xpath";
	public static final String txtMembershipStatus1 = "//span[2][contains(text(),'Membership not activated')]";

	public static final String linkJoinmyWaitRose_loc = "xpath";
	public static final String linkJoinmyWaitRose = "//a[contains(text(),'Join myWaitrose')]";

	public static final String linkmyWaitRoseCard_loc = "xpath";
	public static final String linkmyWaitRoseCard = "//a[contains(text(),'Link myWaitrose Card')]";

	// Link up myWaitrose Card page(Nirmal-7-OCt-2013)
	public static final String txtWtrCardNumber_loc = "id";
	public static final String txtWtrCardNumber = "myWaitroseCard";

	public static final String btnWtrCardUpdate_loc = "id";
	public static final String btnWtrCardUpdate = "myWaitroseActivateSubmit";




	public static final String linkOptoutMembership_loc = "linktext";

	public static final String linkOptoutMembership = "Opt out of myWaitrose";//Opt-out of myWaitrose";

	public static final String btnOptoutMembership_loc = "xpath";
	public static final String btnOptoutMembership = "//input[@value='Opt out of myWaitrose']";

	//Individual Add button in List details page(Nanditha - created on 26-Nov-13)																						
	public static final String btnAddSinglePrd = "//div[2]/div/div[2]/a";
	public static final String btnAddSinglePrd_Loc = "xpath";


	//check box for individual product in List details page(Nanditha - creatd on 26-Nov-13)
	public static final String chkboxSinglePrd = "//div[@class='m-product-padding']/div/input";
	public static final String chkboxSinglePrd_Loc = "xpath";







	//Import now link (Nanditha - Created on 13-Jan-14)
	public static final String lnkImportProducts = "//a[contains(text(),'Import products')]";
	public static final String lnkImportProducts_Loc = "xpath";

	//MIKE OF
	//return to admin
	public final static String linkReturnToadmin="//a[contains(text(),'Phone')]";
	public final static String linkReturnToadminPer="//a[contains(text(),'Person')]";
	public final static String linkReturnToadmin_Loc="xpath";	

	public static final String btnAmendOrders="//td[@class='last']/div/div[2]/a";
	public static final String btnAmendOrders_loc="xpath";
	public static final String btnAmendOrdersvary="(//td[@class='last']/div/div[2]/a)[Index]";
	public static final String btnAmendOrdersvary_loc="xpath";

	//cancel order message
	public static final String txtCancelMsg="//tr[@class='colspan']/td/a[contains(text(),'ValuetoReplace')]//..//..//./td[4]";
	public static final String txtCancelMsg_loc="xpath";
	//
	//View Order Overlay-Product Name (Nanditha - Updated on 20-March-2014)
	public static String lnkViewOrderPrdName = "//div[6]/div/div[2]/div/div/div/section[2]/div[2]/div[Index]/div/div/div[2]/p/a" ;
	/////"(//a[contains(text(),'Name')])[2]"




	//close overlay(Nanditha -Updated on 20-Mar-14)

	public static final String lnkCloseOverlay1 = "//body/div[6]/div/div/a";
	public static final String lnkCloseOverlay_Loc = "xpath";

	//cancel the latest order link
	public static final String cancelLatestOrder="(//a/span[contains(text(),'Cancel this order')])[index]";
	public static final String cancelLatestOrder_loc="xpath";
	//cancel the orders links
	public static final String cancelOrders="//a/span[contains(text(),'Cancel this order')]";
	public static final String cancelOrders_loc="xpath";







	//Screen Name updated
	public static final String txtScreenName="//input[@id='screen-name-1']";//input[@id='screen-name']";
	public static final String txtScreenName_loc="xpath";



	//(Gajapathy updated on 210314)
	public static final String txtDeletelistInPopup="//p/span";
	public static final String txtDeletelistInPopup_loc="xpath";



	//close overlay(Nanditha -Updated Lekshmi May 2014)
	public static final String lnkCloseOverlay = "(//a[contains(text(),'Close')])[2]";


	//Amend Order from View Order (Nanditha - Updated Lekshmi)
	public static final String btnAmendOrderFrmViewOrder = "//div[6]/div/div[2]/div/div/div/section/div[2]/div[4]/div/a";
	public static final String btnAmendOrderFrmViewOrder_Loc = "xpath";


	//Address selection from list



	//select address(Nanditha -Updated on 300713)Lekshmi June 2014
	public static final String listAddress="//div[7]/div/div/ul/li[2]";
	public static final String listAddress_Loc="xpath";

	// to be updated by nirmal(updated Lekshmi June 2014)
	//address selection from List






	public static final String btnAmendOrderPop="(//a[@class='amend-order-button firstIndex'][contains(text(),'Amend order')])[2]";
	public static final String btnAmendOrderPop_loc="xpath";
	


	//public static final String listAddress1="//div[6]/div/div[1]/ul/li[2]";//div[5]/div/div[2]/ul/li[2]";//(Updated on 190514)
	//public static final String listAddress1_Loc="xpath";


	public static final String listAddress3 = "//div[6]/div/div/ul/li";

	public static final String listPayAddress1="//div[6]/div/div/ul/li[4]";

	public static final String Wtrpostcode_loc = "id";
	public static final String Wtrpostcode = "postcode";





	//Permanent Card radio button (kathir - created on 28-jul-14)
	public static final String rdoPermanentCard = "perm_card";
	public static final String rdoPermanentCard_loc = "id";	

	//My Favorites heading(Updated on 200814)
	public static final String txtMyFavoriteHdng="//li[@id='favtab']/a"; 
	public static final String txtMyFavoriteHdng_loc="xpath";





	public static final String listAddress2_Loc="xpath";
	public static final String listAddress2 = "//ul[@class='addresses']/li[@class='address']";

	//My Account link in drop down(Nanditha - created on 06-Aug-13)(Jayandran - updated on 19-Nov-14)
	public static final String lnkMyAccountInDrpDwn = "//div[@class='fly-out-content']//a[contains(text(),'My Account')]";
	public static final String lnkMyAccountInDrpDwn_Loc = "xpath";


	// Error message invalid mywaitrose card number

	public static final String msginvalidmywaitrosecardno = "//div[@class='error-msg']//p";
	public static final String msginvalidmywaitrosecardno_Loc = "xpath";



	// import products in my account link 

//	public static final String lnkmyaccountimportproducts = "//div[@class='fly-out']//a[contains(text(),'Import')]";
	public static final String lnkmyaccountimportproducts = "//a[contains(text(),'Import')]";
	public static final String lnkmyaccountimportproducts_Loc = "xpath"; 






	//Updated By Jayandran on 16/12/2014
	public static final String listAddress1_Loc="xpath";
	public static final String listAddress1="(//ul[@class='addresses']//li)[1]";

	// cancel first order from my orders

	public static final String lnkcancelthisortder = "(//span[contains(text(),'Cancel this order')])[1]";
	public static final String lnkcancelthisortder_Loc = "xpath";


	//Articles

	public static final String lnkArticles="//li[@class='articles']/h3/a";
	public static final String lnkArticles_loc="xpath";

	//My Comments

	public static final String lnkMyComments="//li[@class='my-comments']/h3/a";
	public static final String lnkMyComments_LOC="xpath";


	//Recipes & Cookbooks

	public static final String lnkRecipesCookbooks="//li[@class='recipes']/h3/a";
	public static final String lnkRecipesCookbooks_loc="xpath";

	//Public Profile updated o

	public static final String lnkPublicProfile="//li[@class='public-profile']/h3/a";
	public static final String lnkPublicProfile_loc="xpath";
	//qty insert
	public static final String txtQty="//input[@class='quantity-input']";
	public static final String txtQty_loc="xpath";
	//add
	public static final String btnAdd="//a[@title='Add a single item to trolley']";
	public static final String btnAdd_Loc="xpath";

	//Click update button - Prabhakaran Sankar
	public static final String btnUpdate ="//input[@value='Update']";
	public static final String btnUpdate_Loc ="xpath";

	//Click button OptOutMyWaitrose - Prabhakaran Sankar
	public static final String buttonOptOutMyWaitrose ="//input[@value='Opt out of myWaitrose']";
	public static final String buttonOptOutMyWaitrose_Loc ="xpath";

	//myWaitrose Landing Page - Register Your Card Button	
	//commenting below reference as it is not working as part of 5.7 changes- deusmeus
	//public static final String btnRegisterYourCard = "//img[contains(@title,'RegisterYourCard')]";;//"(//*[@class='button cmsbutton'])[1]/a";////"//img[contains(@title,'btns_2')]"; 
	public static final String btnRegisterYourCard = "//img[contains(@title,'myWaitrose-btns_2') or @title='RegisterYourCard']";
	
	
	
	//"//img[contains(@title,'RegisterYourCard')]"	;
	// "//div[@class='waitroseimage componentBottomMargin']/a/img[contains(@title,'register-mywtr')]";
	public static final String btnRegisterYourCard_loc = "xpath";
	//myWaitrose Landing Page - SignIn Button
	//commenting below reference as it is not working as part of 5.7 changes- deusmeus
	public static final String btnSignIn ="//img[contains(@title,'Signin')]";//a[contains(text(),'Log-in here')]";//;//"//img[contains(@title,'btns_3')]";//	"//div[@class='waitroseimage componentBottomMargin']/a/map/area";//div[@class='waitroseimage componentBottomMargin']//img[contains(@title,'Sign')]";
	//public static final String btnSignIn ="//img[contains(@title,'myWaitrose-btns_3')]";
	public static final String btnSignIn_loc = "xpath";
	//myWaitrose Landing Page - JoinMyWaitrose Button
	//commenting below reference as it is not working as part of 5.7 changes- deusmeus
	public static final String btnJoinMyWaitrose = "//img[contains(@title,'JoinMywaitrose')]";//a[contains(text(),'Sign up here')]";//;//"//img[contains(@title,'btns_1')]" ;//	"//a[contains(text(),'Join myWaitrose no')]";//div[@class='waitroseimage componentBottomMargin']//img[contains(@title,'Join')]";
	//public static final String btnJoinMyWaitrose = "//img[contains(@title,'myWaitrose-btns_1')]";
	public static final String btnJoinMyWaitrose_loc = "xpath";
	//myWaitrose Address Details Page - Non-Editable Text
	public static final String txtNonEditable = 	"//*[text()='It is not possible to amend your myWaitrose address whilst an order is being placed or is pending fulfillment. Amendments can be made once all orders have been checked out and fulfilled.']";
	public static final String txtNonEditable_loc = "xpath";
	//myWaitroseCard link for new registration (kathir - created on 28-jul-14)(Jayandran Updated on 20-nov-2014)
	//not working as part of 5.7 changes- deusmeus
	//public static final String lnkmyWaitroseCard = "Register myWaitrose card";
	public static final String lnkmyWaitroseCard = "//h3/a[contains(text(),'I would like to join')]";
	public static final String lnkmyWaitroseCard_loc = "xpath";


	//myWaitroseCard link to update new card number(Jayandran - 19-Nov-2014)
	//not working as part of 5.7 changes- deusmeus
	//public static final String lnkUpdateMyWaitroseCard ="myWaitrose Card";
	public static final String lnkUpdateMyWaitroseCard ="myWaitrose card number"; 
	public static final String lnkUpdateMyWaitroseCard_Loc = "linkText";




	// list text quanity 
	public static final String inputqty = "(//div[@class='m-product-cell']//input[@type='number'])";
	public static final String inputqty_Loc = "xpath";
	// list add button 

	public static final String btnadd = "(//div[@class='m-product-cell']//div[@class='button add-button'])";
	public static final String btnadd_loc = "xpath";

	//card types
	public static final String txtCardtypes="//span[@class='accepted-payment-methods']/span[index]";
	public static final String txtCardtypes_loc="xpath";


	// savings in latest order - my account page

	public static final String txtsavings = "(//p[@class='savings'])[2]";
	public static final String txtsavings_Loc = "xpath";

	// Delivery charges in order details page 

	public static final String txtdelivery = "/html/body/div[6]/div/div[2]/div/div/div/section[1]/div[1]/p[3]";
	public static final String txtdelivery_loc = "xpath";

	// close button 

	public static final String lnkclosebutton = "(//a[@class='vieworder-overlay-close'])[2]";
	public static final String lnkclosebutton_Loc = "xpath";


	//order number link first order 

	public static final String lnkorderno = "(//a[contains(text(),'View order no.')])[1]";
	public static final String lnkorderno_Loc = "xpath";

	//order number link previous order 

	public static final String lnkpreviousorder = "(//a[contains(@class,'previous ')])[1]";
	public static final String lnkpreviousorder_Loc = "xpath";

	public static final String txtproduct = "(//div[@class='row last'])";
	
	// SHop from the latest order - Prabhakaran Sankar
	public static final String btnShopFromThisOrderPop="(//div[@class='button content-button']//a[@class='firstIndex'])[2][contains(text(),'Shop from this order')]";
	public static final String btnbtnShopFromThisOrderPop_loc="xpath";
	
	// SHop from the latest order - Prabhakaran Sankar
		public static final String btnAddAllItemsToTrolley="//div[@id='add_to_trolley']//a[contains(text(),'Add all items to trolley')]";
		public static final String btnAddAllItemsToTrolley_loc="xpath";
		
		//Contact preferences - radiobtn - OrderUpdatesByTextMessageToYes 
		public static final String radiobtnOrderUpdatesByTextMessageToYes="txtMsgs";
		public static final String radiobtnOrderUpdatesByTextMessageToYes_loc="id";
		
		//Contact preferences - radiobtn - OrderUpdatesByTextMessageToNo 
		public static final String radiobtnOrderUpdatesByTextMessageToNo="noTxtMsgs";
		public static final String radiobtnOrderUpdatesByTextMessageToNo_loc="id";
		
		//Contact preferences - button - submit or save 
		public static final String btnOrderUpdatesSaveButton="//input[@value='Save']";
		public static final String btnOrderUpdatesSaveButton_loc="xpath";
		
		public static final String lnkMyAddressInMyAccount = "//div[@class='fly-out']//a[contains(text(),'Address')]";
		public static final String lnkMyAddressInMyAccount_Loc = "xpath";
		public static final String lnkMyAddressText = "existing-address";
		
		//Join MyWaitrose page heading 
		public static final String joinMyWHeading = "//h1[text()='Join myWaitrose']";
		public static final String joinMyWHeading_loc = "xpath";
		
		//myWaitrose card number
		public static final String txtMyWCardNumber = "myWaitroseCard";
		public static final String txtMyWCardNumber_loc = "id";
		
		//myWaitrose card number
				public static final String registercardOnline = "//div[@id='myWaitroseRegister']/input/following-sibling::label";
				public static final String registercardOnline_loc = "xpath";

}
