package uimap;

public class HomePage {
	//My Account link
	public static final String linkMyAccount ="//a[contains(text(),'My Account')]";
	public static final String linkMyAccount_Loc ="xpath";



	//Start shopping button
	public static final String buttonStartShopping = "//form[@id='start-shopping-home-form']/div/input";
	public static final String buttonStartShopping_Loc = "xpath";

	//My Orders Button
	public static final String buttonMyOrders = "//a[contains(text(),'My orders')]";
	public static final String buttonMyOrders_Loc = "xpath";


	//Email text field
	public static final String txtEmail="logonId";
	public static final String txtEmail_loc="id";
	//Password text field
	public static final String txtPassword="logonPassword";
	public static final String txtPassword_loc="id";
	//SignIn link
	public static final String lnkSignIn="//input[@value='Sign in']"; 
	public static final String lnkSignIn_loc="xpath";
	//Groceries tab 
	public static final String linkMegamenuTab="Groceries";//div[@id='headerShoppingSection']//a[contains(.,'')] 
	public static final String linkMegamenuTab_loc="linkText";

	//Goback button 
	public static final String linkbredcrum="         go back.... (todo)       "; 
	// Aisels
	public static final String linksubMegamenuTab="Fruit & Veg"; 
	public static final String linksubMegamenuTab_loc="linkText";

	//Category link
	public static final String linkCategory="//a[contains(text(),'Vegetables ')]";
	public static final String linkCategory_loc="xpath";
	//sub category
	public static final String linksubcategory="//a[contains(text(),'Green Vegetables ')]";
	public static final String linksubcategory_loc="xpath";
	//Add button
	public static final String linkAddtocart="Add to trolley";
	//Add button list
	public static final String btnAddlist="//div[@class='button add-button']/a";
	public static final String btnAddlist_loc="xpath";
	//Qty list
	public static final String txtqtylist="//div[@class='quantity-append with-cta']/input";
	public static final String txtqtylist_loc="xpath";
	//Close button in mini trolley
	public static final String linkCloseForselectOption="Close"; 
	public static final String linkCloseForselectOption_loc="linkText";

	public static final String txtMessage="//div[@class='lightbox-container lightbox-error']";
	public static final String txtMessage_loc="xpath";


	//waitrose image link
	public static final String linkHome="Waitrose";
	public static final String linkHome_loc="linkText";
	//public static final String linkCategory_loc//*[@id='content']/div/div[3]/nav/ul
	//Add button
	public static final String btnAdd="Add";
	public static final String btnAdd_loc="linkText";
	//My Account Link ////li[@id='headerMyAccountMenu']/a[text()='My Account']
	public static final String lnkMyaccount="//li[@id='headerMyAccountMenu']/a[contains(.,'My Account')]";
	public static final String lnkMyaccount_loc="xpath";


	//Recipes Heading (updated by vaishnavi)
		public static final String txtRecipesHdng="//h1[@class='pageTitle' and contains(text(),'Recipes')]";
		public static final String txtRecipesHdng_loc="xpath";
	//Inspiration link
	public static final String lnkInspiration="//a[contains(text(),'Inspiration')]";
	public static final String lnkInspiration_loc="xpath";
	//inspiration heading
	public static final String txtInpirationHdng="//h1[contains(text(),'Inspiration')]";
	public static final String txtInpirationHdng_loc="xpath";


	//Logout link
	//	public static final String lnkLogout="//div[@class='button']//a[contains(text(),'Sign out')]";
	//	public static final String lnkLogout_Loc="xpath";
	//waitroseTv link
	public static final String lnkWaitroseTV="//a[contains(text(),'Waitrose TV Live - recipes')]";
	public static final String lnkWaitroseTV_loc="xpath";
	// New user post code text field
	public static final String txtNewuserPostcode="start-shopping-postcode";
	public static final String txtNewuserPostcode_loc="id";
	//Start shopping button
	public static final String btnStartShopping="//input[@value='Start shopping']";
	public static final String btnStartShopping_loc="xpath";

	//Choosing Delivery types


	//Post code 
	public static final String txtpostcode="postcode_lookup_input";
	public static final String txtpostcode_loc="id";
	//button ok
	public static final String btnOk1="//input[@value='Ok']";
	public static final String btnOk1_loc="xpath";
	//Book slot Heading
	public static final String txtShpnDelvHdng="//h2[contains(text(),'Where would you like your shopping delivered?')]";
	public static final String txtShpnDelvHdng_loc="xpath";
	//shop online u collect link
	public static final String lnkShpOnlYouColl="//a[contains(text(),'shop online, you collect')]";
	public static final String lnkShpOnlYouColl_loc="xpath";

	//Start shopping button in delivery pop up
	public static final String lnkStrtSpng="//a[contians(text(),'Start shopping')]";
	public static final String lnkStrtSpng_loc="xpath";
	//Collection tabs
	public static final String tabscollection="//*[@id='book-collection-slot']/div/ul";
	public static final String tabscollection_loc="xpath";
	//Entertain tab Details
	public static final String txtEntertainMsg="//*[@id='book-collection-slot-tab1']/div/p[1]";
	public static final String txtEntertainMsg_loc="xpath";
	//Groceries tab
	public static final String tabGroceries="//a[contains(text(),'Groceries Collection')]";
	public static final String tabGroceries_loc="xpath";
	//Groceries tab Details
	public static final String txtGroceriesMsg="//*[@id='book-collection-slot-tab2']/div/p[1]";
	public static final String txtGroceriesMsg_loc="xpath";
	//have your shopping delivered link
	public static final String txtHaveUrShpngDlvry="//a[contains(text(),'have your shopping delivered')]";
	public static final String txtHaveUrShpngDlvry_loc="xpath";
	//Shop in branch we deliver in shop online you collect page
	public static final String lnkselDeliveryService="//a[contains(text(),'shop in branch,we deliver')]";
	public static final String lnkselDeliveryService_loc="xpath";
	//Pick another branch button
	public static final String btnAnothrBranch="//a[contains(text(),'Pick another branch')]";
	public static final String btnAnothrBranch_loc="xpath";

	//homepage heading after choose slot
	public static final String txthomeHdng="//h1[contains(text(),'Shopping online at Waitrose.com is quick, easy and great value')]";
	public static final String txthomeHdng_loc="xpath";
	//delivery details in home page
	public static final String deliverydetails="//*[@id='headerDelivery']/p";
	public static final String deliverydetails_loc="xpath";

	//Find A Branch 

	//find a branch link
	public static final String lnkFindBranch="Find a Branch";
	public static final String lnkFindBranch_loc="linkText";
	//find a branch heading
	public static final String txtFindBranchHdng="//h2[contains(text(),'Find a branch')]";
	public static final String txtFindBranchHdng_loc="xpath";
	//fnd brnch post code
	public static final String txtfndBrnchPstCode="global-form-postcode";
	public static final String txtfndBrnchPstCode_loc="id";
	//select brch option
	public static final String dpdnSelectbranch="global-form-select-branch";
	public static final String dpdnSelectbranch_loc="id";
	//find branch button
	public static final String btnfndBrch="//input[@value='Find branch']";
	public static final String btnfndBrch_loc="xpath";
	//welcome message branch heading
	public static final String txtfndBrnchMsg="//h1[contains(text(),'Welcome to Waitrose')]";
	public static final String txtfndBrnchMsg_loc="xpath";
	//Branch drop down list
	public static final String drpbrnchlist="//*[@id='global-form-select-branch']/option";
	public static final String drpbrnchlist_loc="xpath";
	//Branch name heading
	public static final String txtBrnchNameHdng="//h1[contains(text(),'Welcome to Waitrose')]";
	public static final String txtBrnchNameHdng_loc="xpath";
	//Search text field
	public static final String txtSearch="search";
	public static final String txtSearch_loc="id";
	//Search button
	public static final String btnSearch="search";
	public static final String btnSearch_loc="name";  
	
//	public static final String keywordSearch="//*[@id='header-search-form']/div[2]/div/section[1]/article[1]";
	public static final String keywordSearch="//form[@id='header-search-form']//article[@class='autosuggest-keyword-list']";
	public static final String keywordSearch_loc="xpath";  
	
	public static final String categorySearch="//form[@id='header-search-form']//article[@class='autosuggest-category-list']";
	public static final String categorySearch_loc="xpath";  
	
	public static final String productSearch="//form[@id='header-search-form']//h3[@class='suggested-product-title']";
	public static final String productSearch_loc="xpath";  
	
	//Auto suggest overlay
	public static final String autoSuggestOverlayJQuery = "$('#autosuggestBackdrop').click()";
	public static final String autoSuggestOverlay = "//div[@id='autosuggestBackdrop']";
	public static final String autoSuggestOverlay_loc = "xpath";

	//Homepage UI
	//post code 
	public static final String txtPostCode = "start-shopping-postcode";
	public static final String txtPostCode_Loc ="id";
	//start shopping 
	//duplicate

	//my account sublink
	public static final String linkSubMyAccount ="//li[@id='my-account-drop-down']/div/ul/li[4]/a";
	public static final String linkSubMyAccount_Loc ="xpath";


	//Sign out link(Nanditha -Created on 31-July-13)
	public static final String lnkSignOut = "(//a[contains(text(),'Sign out')])[2]";
	public static final String lnkSignOut_Loc = "xpath";




	//Recipes link(Updated on 050813)
	public static final String lnkRecipes="Recipes";
	public static final String lnkRecipes_loc="linkText";

	public static final String linkMyFavourites = "//img[@alt='my favourites']";
	public static final String linkMyFavourites_Loc = "xpath";


	//Book collection  for register user
	public static final String btnBookCollection="book a collection";
	public static final String btnBookCollection_loc="linkText";

	//pick a bracnch
	public static final String btnPickABranch="Pick another branch";
	public static final String btnPickABranch_loc="linkText";



	// Heading in Grocery page(Updated on070813)
	public static final String txtGroceryHeading="//h1[contains(text(),'Weekly Shopping at waitrose.com is quick, easy and great value')]";
	public static final String txtGroceryHeading_loc="xpath";

	public static final String btnBookDelivery1="Book a Groceries delivery";


	//Entertain tab
	public static final String btnEntertain="Entertaining Collection";
	public static final String btnEntertain_loc="linkText";


	//Shop in branch we deliver text
	public static final String txtShpBrc="//h2[contains(text(),'Book your shop in branch, we deliver slot')]";
	public static final String txtShpBrc_loc="xpath";
	//Confirm reservation


	public static final String btnViewFullBranch="View full branch details";
	public static final String btnViewFullBranch_loc="linkText";

	public static final String btnBookCollection1="Book collection";




	//My Favourites button Shopping Panel(Nanditha -Created on 01-Sep-2013)
	public static final String btnMyFavourites = "//a[contains(text(),'My Favourites')]";
	public static final String btnMyFavourites_Loc = "xpath";




	//my account link(Nanditha - Updated on 03-Sep-13)
	public static final String linkMyAccount1 ="My Account";
	public static final String linkMyAccount1_Loc ="linkText";






	//Myaccount under list(Updated on 16/09/13)
	public static final String lnkMyaccountUnderList="My Account";//div[@class='fly-out']/ul/li[3]/a";
	public static final String lnkMyaccountUnderList_loc="linkText";

	//updated on 170913
	public static final String txtQtykg="//div[@class='quantity-append']/input[@class='quantity-input']";//div[@class='quantity-append with-cta']/input[@class='quantity-input']";//Quantity text field 
	public static final String txtQtykg_loc="xpath";  	

	//LeftNavigation link list on MegamenuPage(Nirmal-created on 19-sep-2013
	public static final String leftNavLinkList_loc="xpath";
	public static final String leftNavLinkList="//nav[@class='refinement']/ul/li";
	public static final String leftNavLink_loc="partialLinkText";


	//Load More Button-Nanditha Created on 04-Oct-13)
	public static final String btnLoadMore = "Load more";//"//a[contains(text(),'Load more')]";
	public static final String btnLoadMore_Loc = "linkText";

	//Search Info - Nanditha Created on 04-Oct-13)
	public static final String txtSearchInfo = "//p[@class='search-info']";
	public static final String txtSearchInfo_Loc = "xpath";

	//Add button of Single item (Nanditha created on 04-Oct-13)
	public static final String btnAddPrd = "Add";
	public static final String btnAddPrd_Loc = "linkText";

	//Link See all (Nanditha -Created on 04-oct-13)
	public static final String lnkSeeAll = "//div[2]/div[3]/p/a";
	public static final String lnkSeeAll_Loc = "xpath";


	// My Account Dropdown Homepage(NIRMAL Created 4-oct-2013)

	//public static final String ddoMyAccount_loc = "id";
	//public static final String ddoMyAccount = "my-account-drop-down";

	//Updated for 6.2 Change - Jai
	public static final String ddoMyAccount_loc = "xpath";
	public static final String ddoMyAccount = "//li[@id='headerMyAccountMenu']/a[contains(.,'My Account')]"; 


	// MyWaitrose Card Homepage(Nanditha Updated 21-oct-2013) ( updated by vaishnavi ) 

	public static final String linkmyWaitroiseCard_loc = "xpath";
	public static final String linkmyWaitroiseCard ="//a[contains(text(),'myWaitrose')]";
	//commented this as part of 5.7 changes (deusmeus)
	//public static final String linkmyWaitroiseCard = "//a[contains(text(),'myWaitrose') and contains(text(),'card')]";
//	public static final String linkmyWaitroiseCard ="//nav[@id='headertopnav']//a[contains(text(),'myWaitrose')]";//"//a[@class='my-waitrose']"; //Commented on 21 Feb
	//public static final String linkmyWaitroiseCard = "//a[contains(text(),'myWaitrose')]";////a[contains(text(),'myWaitrose')];//a[@class='my-waitrose']
	//left panel of each category(Updated on 181013)
	public static final String linksLeftPanel="//*[@id='content']/div/div/nav/ul";//*[@id='content']/div/div[2]/div/nav/ul
	public static final String linksLeftPanel_loc="xpath";

	//Post code edit box(Updated Lekshmi May 2014)
	public static final String editboxPostCode = "//form[@id='start-shopping-home-form']/fieldset/input";
	public static final String editboxPostCode_Loc = "xpath";


	//Continue Shopping button(Nanditha - Updated on 05-Sep-2013)(updated Lekshmi June 2014)
	public static final String btnContinueShopping_Loc = "xpath";
	public static final String btnContinueShopping = "//a[contains(text(),'Continue shopping')]";


	//myaccount from left panel
	public static final String lnkMyaccountfromleft="//a[contains(text(),'My Account')]";
	public static final String lnkMyaccountfromleft_loc="xpath";
	//MyShopping section
	public static final String tabMyShopping="(//div[@class='account-container']/div/h2)[1]";
	public static final String tabMyShopping_loc="xpath";

	//Explorer by Offer Type
	public static final String txtOfferType="//nav[@class='refinement offers']/h3[1]";
	public static final String txtOfferType_loc="xpath";
	//Explorer by Offer Section
	public static final String txtOfferSec="//nav[@class='refinement offers']/ul[1]";
	public static final String txtOfferSec_loc="xpath";
	//Offer section links
	public static final String linksOfferSec="//nav/ul[1]/li/ul/li[index]/a";
	public static final String linksOfferSec_loc="xpath";

	//Book Delivery  for register user(Updated on 050814 - kathir)
	public static final String btnBookDelivery="Book a slot";	
	public static final String btnBookDelivery_loc="linkText";

	//shop in branch, we deliver link updated on 020914
	public static final String lnkShpInBrnchWeDelvr="//a[contains(text(),'shop in branch, we deliver')]";//a[contains(text(),'shop in branch, we deliver')]";
	public static final String lnkShpInBrnchWeDelvr_loc="xpath";

	// user logged out  popup - BY:  prabhakaran Sankar
	public static final String popUpCloseButton="//a[@class ='close']";
	public static final String popUpCloseButton_loc="xpath";



	//Confirm reservation updated on 29/11/2014

	public static final String btnConfirmResrvation="(//a[contains(text(),'Confirm reservation')])[2]";
	public static final String btnConfirmResrvation_loc="xpath";

	//Order Confirmation

	public static final String btnOrdConf="(//a[@id='processnext'])[2]";
	public static final String btnOrdConf_loc="xpath";


	//MyWaitroseOffer link in Offer Page ( created by Jayandran on 10/12/2014)

	public static final String linkMyWaitroseOffer="//nav[@class='refinement offers']//*[contains(text(),'myWaitrose')]";
	public static final String linkMyWaitroseOffer_loc="xpath";
	//checkout class
	public static final String classcheckout ="//div[@id='headerBasket']/div[4]";

	//Footer page
	public static final String linkCompanyInfo="footer-wrapper";//"footer-tab";
	public static final String linkCompanyInfo_loc="class";

	//Error Message Validation in FindBranch Page ( created by Jayandran on 7/1/2015)

	public static final String txtInvalidBranchErrorMessage="//p[text()='No results were found, please try a different search term.']";
	public static final String txtInvalidBranchErrorMessage_loc="xpath";

	// Multi buy in offers type 

	public static final String lnkMultibuyOffers = "(//a[@class='category js-category clearfix'])[1]";
	public static final String lnkMultibuyOffers_loc = "xpath";

	//buy 1 get 1 in offers type 

	public static final String lnkbuy1get1Offers = "//a[contains(text(),'Buy 1 get 1 free')]";
	public static final String lnkbuy1get1Offers_loc = "xpath";


	//3 for 2  in offers type 

	public static final String lnk3for2Offers = "//a[contains(text(),'3 for 2')]";
	public static final String lnk3for2Offers_loc = "xpath";

	// help in home page

	public static final String lnkhelp = "(//a[contains(text(),'Help')])[1]";
	public static final String lnkhelp_loc = "xpath";

	// Flowers in home page

	public static final String lnkflowers = "(//a[contains(text(),'Flowers')])[1]";
	public static final String lnkflowers_loc = "xpath";

	// Garden in home page

	public static final String lnkgarden = "(//a[contains(text(),'Garden')])[1]";
	public static final String lnkgarden_loc = "xpath";

	// Cellar in home page

	public static final String lnkcellar = "(//a[contains(text(),'Cellar')])[1]";
	public static final String lnkcellar_loc = "xpath";

	// warrent 1 in home page 

	public static final String imgwarrent1 = "//img[@class='warrant1']";
	public static final String imgwarrent1_Loc = "xpath";

	// warrent 2 in home page 

	public static final String imgwarrent2 = "//img[@class='warrant2']";
	public static final String imgwarrent2_Loc = "xpath";


	// footer home page links 

	public static final String lnkfooter = "//div[@class='footer-column']";
	public static final String lnkfooter_loc = "xpath";

	// footer home page links 

	public static final String lnkfootercontact = "//div[@class='footer-column last']";
	public static final String lnkfootercontact_loc = "xpath";

	// Wine cases heading
	public static final String txtwinecasesheading="//span[contains(text(),'Wine Cases')]";
	public static final String txtwinecasesheading_loc="xpath";



	//Florist link
	public static final String lnkflorist="//nav[@class='primary']//a[contains(text(),'Florist')]";
	public static final String lnkflorist_loc="xpath";
	
	//Gifts link
		public static final String lnkgifts="(//a[contains(text(),'Gifts')])[1]";
		public static final String lnkgifts_loc="xpath";
	
	//Inspiration link
		public static final String lnkWinecases_loc="//a[contains(text(),'Wine Cases')]";
		public static final String lnkWinecases="xpath";

	// Flowers & Gifts heading
	public static final String txtFlowers="//span[contains(text(),'Florist')]";
	public static final String txtFlowers_loc="xpath";
	// Gifts heading
		public static final String txtGifts="//span[contains(text(),'Gifts')]";
		public static final String txtGifts_loc="xpath";
	
	//Logout link
//	public static final String lnkLogout="//a[contains(text(),'Sign out')]";
//	public static final String lnkLogout="//div[@class='signout keyline-top']//a[contains(text(),'Sign out')]";
	public static final String lnkLogout= "(//a[contains(text(),'Sign out')])[1]";
	public static final String lnkLogout_Loc="xpath";

	// TV home symbol
	public static final String TVhome="//a[@class='home']";
	public static final String TVhome_loc="xpath";

	// TV link home page
	public static final String TVlnk="//a[contains(text(),'TV')]";
	public static final String TVlnk_loc="xpath";

	// Welcome back message
	public static final String textWelcomeBack="//h2[contains(text(),'Welcome back')]";
	public static final String textWelcomeBack_loc="xpath";
	
	// Customer Information Notice Link
	public static final String linkCustomerInformationNotice="Customer information notices";
	public static final String linkCustomerInformationNotice_loc="linkText";
	
	// Customer Information Notice Link
		public static final String linkJohnLewis="John Lewis";
		public static final String linkJohnLewis_loc="linkText";
		
		//John Lewis Logo
				public static final String johnLewisLogo="//img[@title='John Lewis']";
				public static final String johnLewisLogo_loc="xpath";
				

	// Partnership link - Waitrose Garden
		public static final String linkWaitroseGarden="//a[@href='http://www.waitrosegarden.com/']";
		public static final String linkWaitroseGarden_loc="xpath";
		
	// Partnership link - Waitrose direct
		public static final String linkWaitrosedirect="//a[@href='http://www.waitrosedirect.com/']";
		public static final String linkWaitrosedirect_loc="xpath";
		
	// Partnership link - John Lewis Insurance
		public static final String linkJohnLewisInsurance="//a[@href='http://www.johnlewis-insurance.com#track=JLwr0009c']";
		public static final String linkJohnLewisInsurance_loc="xpath";
		
		// FacebookLogo 
		public static final String facebookLogo="//i[@class='fb_logo img sp_Z_xw_7FDtNv sx_b6c073']";
		public static final String facebookLogo_loc="xpath";
		
		// TwitterLogo 
				public static final String twitterLogo="//h3[@id='signup-dialog-header']";
				public static final String twitterLogo_loc="xpath";
				
				
				// mywairtrose header in my waitrose card page
				
				public static final String txtmywaitroseheader = "//nav[@class='secondary']//a[contains(text(),'myWaitrose')]";
				public static final String txtmywaitroseheader_Loc = "xpath";
				
//				public static final String slotHeader = "//*[@id='bookslotForm']";
//				public static final String slotHeader_Loc = "xpath";
				
				public static final String slotHeader = "//*[@id='_atssh971']";
				public static final String slotHeader_Loc = "xpath";
				
				//Entertaining Megamenu
				public static final String lnkEntertainingMegamenu = "Entertaining";
				public static final String lnkEntertainingMegamenu_loc = "linkText";
				
				//AutoSuggest Keywords
				public static final String lnkAutoSuggestKeywords = "//article[@class='autosuggest-keyword-list']//a[@class='autosuggestion keyword']";
				

				//My Account link
				public static final String checkAvailability ="//*[@value='Check Availability']";
				public static final String checkAvailability_Loc ="xpath";
				
				// My Homepageimg link
				/*public static final String clickimg  = "//a[@href='http://www.waitrose.com/']";
				public static final String clickimg_Loc = "xpath";*/
}
