package uimap;

public class ProductPortrait 
{
	//Product Grid
	public static final String linkProductPortraitGrid="//div[@class='products-grid']/div[@class='products-row'][RowIndex]/div[@class='m-product-cell'][CellIndex]";
	public static final String linkProductPortraitGrid_Loc = "xpath";
	//Search results product portrait
	public static final String linkSearchResultsProductGrid = "//div[@class='products-grid']/div[RowIndex]/div[CellIndex]";
	public static final String linkSearchResultsProductGrid_Loc = "xpath";
	//Bread Crumb Count
	//public static final String linkBreadCrumb = "//span[@id='current-breadcrumb']";
	//public static final String linkBreadCrumb_Loc = "xpath";	 
	public static final String linkBreadCrumb = "//div[@class='breadcrumbs standard']/ul/li[@class='current']/span[@id='current-breadcrumb']";
	public static final String linkBreadCrumb_Loc = "xpath";
	
	public static final String lblCurrentBreadCrumb = "current-breadcrumb";
	public static final String lblCurrentBreadCrumb_loc = "id";
	//Bread Crumb Name
	public static final String linkAisleName = "//a[contains(text(),'Name')]";
	public static final String linkAisleName_Loc = "xpath";
	//LHS navigation
	public static final String linkNavigation = "//nav[@class='refinement']/ul/li";
	public static final String linkNavigation_Loc = "xpath";
	//Product Name 
	public static String SelectedProductName="";
	//Filters:
	//Filters - Product Listing Page.
	public static final String linkFilter = "//div[@id='content']/div/div[2]/nav/form/fieldset/ul/li";
	public static final String linkFilter_Loc = "xpath";
	//Filter Name - Product Listing Page.
	public static final String txtFilterName ="//div[@id='content']/div/div[2]/nav/form/fieldset/ul/li[iFilterCount]/label";
	public static final String txtFilterName_Loc ="xpath";
	public static String FiterNameAttribute = "for";
	public static String gname;
	//Filter Checkbox - Product Listing Page.
	public static final String chkboxFilters = "//div[@id='content']/div/div[2]/nav/form/fieldset/ul/li[iFilterCount]/input";
	public static final String chkboxFilters_Loc = "xpath";
	// Filtered Products row : 
	public static final String linkFilteredProductsRow = "//div[@class='l-content']/div[@class='products-grid']/div[@class='products-row']";
	public static final String linkFilteredProductsRow_Loc ="xpath";
	//Product Row
	public static final String linkProductRow ="//div[@class='products-grid']/div[@class='products-row']";
	public static final String linkProductRow_Loc = "xpath";
	//Product Add button(Created on 26-July-13)
	public static final String buttonAdd="(//a[contains(text(),'Add')])[Index]";
	public static final String buttonAdd_Loc = "xpath";
	//Product Qty edit box (Nanditha - Created on 2-Aug-13)
	public static final String linkProductQtyeditbox = "//div[@class='products-grid']/div[@class='products-row'][RowIndex]/div[@class='m-product-cell'][CellIndex]/div/div[2]/div/div/input";
	public static final String linkProductQtyeditbox_Loc ="xpath";
	public static final String linkProductQtyeditboxAttribute = "className";
	public static final String linkproductQtyeditboxAttributeValue = "quantity-input";
	//Product Offer Tag(Nanditha - Updated on 02-Aug-13)
	public static final String linkProductOfferTag = "//div[@class='products-grid']/div[@class='products-row'][RowIndex]/div[@class='m-product-cell'][CellIndex]/div/div/div[2]/a[2]";
	public static final String linkProductOfferTag_Loc ="xpath";
	//Product name link in Product modal (Gajapathy on 050813)
	public static final String linkProductNameInModal ="//div[@class='top-wrapper']/h1/a/em";
	public static final String linkProductNameInModal_loc = "xpath";
	
    //Quantity text list
	public static final String txtquantity ="//div[@class='m-product-details-container']//a[contains(text(),'ValToReplace')]//..//..//..//input[@class='quantity-input']";
    public static final String txtquantity_loc="xpath";
    //First product name
    public static final String linkFirstPrdName="//div[@class='products-grid']/div[1]/div[1]/div/div[1]/div[2]/a[1]";
    public static final String linkFirstPrdName_loc="xpath";
    //Product Counts in page
    public static final String txtnoproducts="//div[@class='products-grid']/div[@class='products-row']/div";
    public static final String txtnoproducts_loc="xpath";
    //Load more button
    public static final String btnLoadMore="Load more";
    public static final String btnLoadMore_loc="linkText";
    //Offer filter checked
    public static final String chkSelectOffer="//div[@id='filter-section-rolled']/ul/li[@class='selected']/input[@checked='checked']";
    public static final String chkSelectOffer_loc="xpath";
    //Clear All link 
    public static final String linkClearAll="clear all";
    public static final String linkClearAll_loc="linkText";
    //All Product Price( updated on 120813)
    public static final String txtAllProdRrice="//div[@class='products-grid']/div[@class='products-row']/div/div/div/div[3]/span[1]";
    public static final String txtAllProdRrice_loc="xpath";
    //to be updated on 140813
//    public static final String txtAllProdPrice="//div[@class='products-grid']/div[@class='products-row']/div/div/div/div[3]/span[1]";
    public static final String txtAllProdPrice="//div[@class='m-product-cell']//span[@class='price trolley-price']";
    public static final String txtAllProdPrice_loc="xpath";
    
    public static final String buttonAdd1="Add";
    public static final String buttonAdd1_Loc = "linkText";
    //Close link
 //   public static final String linkClose="Close";
 //   public static final String linkClose_loc="linkText";
    
    public static final String linkClose="//a[@class='close']";
    public static final String linkClose_loc="xpath";
    
    //Sort drop down
    public static final String lnkSort="sort";//div[@class='tab groceries']/div/div[2]/form/fieldset/select[@id='sort']";
    public static final String lnkSort_loc="id";
   
	//Enter valid qty Updated on 030913
	public static final String txtvalidQty="//div[@class='lightbox-container lightbox-error']/p";//p[contains(text(),'Please enter a valid quantity.')]";
	public static final String txtvalidQty_loc="xpath";
    //Add button (Nanditha - Created on o5-Sep-13)
	public static final String btnAddVariationPrd = "//a[contains(text(),'Add to trolley')]";
	public static final String btnAddVariationPrd_Loc = "xpath";
	//Spinner button (Nanditha - Created on 05-Sep-13)
	public static final String lnkIncrementVariationPrd = "//a[@id='plus-']";
	public static final String lnkIncrementVariationPrd_Loc  = "xpath";
	//Entertaining tab in Search results page (Nanditha - Created on 05-Sep-13)
	public static final String tabWESearchResults = "//div[2]/div/ul/li[2]/a";
	public static final String tabWESearchResults_Loc = "xpath";
	//Heart Icon(NandithaUpdated on 19-Sep-13)
	public static final String linkProductUnFav="//a[contains(text(),'Remove from Favourites')]";
	public static final String linkProductUnFav_Loc = "xpath";
	//OnOffer filter
//	public static final String chkOfferfilter="(//input[@id='On_Offer/true'])[1]";//On_Offer";//div[@id='filter-section-rolled']/ul/li/input[@id='On_Offer/true']";
	public static final String chkOfferfilter="//li[@class='selected']//input[@class='tickable']";
	public static final String chkOfferfilter_loc="xpath";		
	//Continue shopping on personalised greeting overlay (Nanditha-Updated on 24Sep13)
	public static final String btnContinueShopping = "//a[contains(text(),'Save and continue')]";
	public static final String btnContinueShopping_Loc = "xpath";
	//Add button updated on 170913
	public static final String btnAdd="Add";//a[@title='Add a single item to trolley']";
	public static final String btnAdd_loc="linkText";						
    //Product Portrait Offer Name (Nanditha - Updated on 22-Oct-13)
	public static final String lnkProductPortraitOfferName = "//div[@class='l-content']/div[@class='products-grid']/div[RowIndex]/div[CellIndex]/div/div/div[2]/div[@class='offerDetails']/a";
	public static final String lnkProductPortraitOfferName_Loc = "xpath";
	//Mywaitrose Label
	public static final String imgmyWaitroseLabel="(//a[@class='label myWaitrose'])[1]";
	public static final String imgmyWaitroseLabel_loc="xpath";
	//Add all to my trolley
	public static final String btnAddalltomytrolley="Add all to my trolley";
	public static final String btnAddalltomytrolley_loc="linkText";
	//At aglance
	public static final String txtAtGlance="//h2[contains(text(),'At a glance')]";
	public static final String txtAtGlance_loc="xpath";
    //to be updated by gaja
	public static final String linkBack="//a[@class='back-link']";
	public static final String linkBack_loc="xpath";
	//Items not added from list overlay (Nanditha - Created on 13-Nov-13)
	public static final String itemNotAddedOverlay = "//body[@class='groceries reduced']/div[7]/div/div[2]";
	public static final String itemNotAddedOverlay_Loc = "xpath";
	//Product Name displayed as link(Nanditha - Created on 5-Dec-13)////The product name is retrieved from data Table.
	public static final String lnkPrdName1_Loc = "linkText";
	//myWaitrose link updated on 041213
	//public static final String linkmyWaitrose="//a[contains(text(),'myWaitrose')]";
	//public static final String linkmyWaitrose_loc="xpath";
	//WE Item -Add button (Nanditha - Created on 10-Dec-13)
	public static final String addSingleWEbutton = "//div[@class='r-content']/div[@class='products-grid-entertaining']/div[@class='products-grid']/div[RowIndex]/div[CellIndex]/div/div[2]/div/div[2]/a";
	public static final String addSingleWEbutton_Loc = "xpath";
	//myWaitrose link updated on 170114
	public static final String linkmyWaitrose="(//a[contains(text(),'myWaitrose')])[2]";
    public static final String linkmyWaitrose_loc="xpath";
    //Gajapathy -Updated on 270114
    public static final String lnkFillHeartIconUpdated1 = "(//a[@title='Add to Favourites'])[41]";
    public static final String lnkFillHeartIconUpdated1_Loc = "xpath";	
   
    //Jotter Scroll Image(Nanditha-Jotter Search Summary Page) Gajapathy updated on 140214
	public static final String imgCarouselLoadButton = "//div[@class='panel']";//div[@class='panel'/img[@alt='[loading]'";
	public static final String imgCarouselLoadButton_Loc = "xpath";
    //JotterNotePad (Nanditha-Jotter Landing Page) Gajapathy updated on 140214
	public static final String txtJotterNotePad="jotter-search1";//jotter-search";
	public static final String txtJotterNotePad_Loc="id";//name";
	//Product Name(Updated on 29-Jan-14)
	public static final String linkProductName = "//div[@class='l-content']/div[@class='products-grid']/div[@class='products-row'][RowIndex]/div[@class='m-product-cell'][CellIndex]/div/div/div[2]/a";
	public static final String linkProductName_Loc ="xpath";
	//Prd Qty Restriction Error Msg(Updated-Nanditha on 20-Mar-14)
	public static final String txtErrorMsg = "//body/div[@class='lightbox-container lightbox-error']/p";
	public static final String txtErrorMsg_Loc = "xpath";
	//Analgesic overlay close link (Nanditha - Created on 20-Mar-14)
	public static final String lnkCloseAnalgesicOverlay_Loc = "xpath";
	public static final String lnkCloseAnalgesicOverlay = "//body/div[6]/a";
    //personalised greetings (Nanditha - Created on 24-Mar-14)
	//		public static final String txtPersonalisedGreetings = "//textarea";
	
	//Product Name displayed as link(Nanditha - Updated on 20-Mar-14)////The product name is retrieved from data Table.
	public static final String lnkPrdName = "//a[contains(text(),'Search')]" ;
	public static final String lnkPrdName_Loc = "linkText"; 
	public static final String txttrolleyitem="//a[@class='basket']/span[contains(text(),'item')]";
	public static final String txttrolleyitem_loc="xpath";

	//Restricted overlay close link (Nanditha - Updated  on 04-Sep-13)(updated Lekshmi June 2014)
	public static final String lnkCloseRestrictedOverlay_Loc = "xpath";
	public static final String lnkCloseRestrictedOverlay = "//div[@class='lightbox-container lightbox-error']/a";//div[6]/a";
	public static final String lnkCloseRestrictedOverlayfromlist=" //div[9]/div/div/a";
	//Prd Qty Restriction Error Msg verifided by OK button(Lekshmi June 2014)
    public static final String txtErrorMsginlist = "(//a[contains(text(),'Ok')])[8]";
	public static final String txtErrorMsginlist_Loc = "xpath";
	
	
	//Zero error message
	public static final String txtZeroErrMsg="(//div[@class='lightbox-container lightbox-error'])/p";
	public static final String txtZeroErrMsg_loc="xpath";
	
	//close overllay from list for analgesic product
	public static final String closeItemNotAddedOverlay = "//body[@class='groceries reduced']/div[9]/div/div/a";
	public static final String closeItemNotAddedOverlay_Loc = "xpath";
	//Select option
	public static final String lnkSelectOption = "//div[@class='radiogroup']/input[@name='variant'][3]";
	public static final String lnkSelectOption_Loc = "xpath";
	public static final String lnkSelectOption1 = "//input[@name='variation'][2]";
    //specific offer Type links
	public static final String linkspecificOffertype="(//a[contains(text(),'data')])[1]";
    public static final String linkspecificOffertype_loc="xpath";
    //same Offer link for all products
    public static final String txtSameOffer="//div[@class='offerDetails']/a[1]";
    public static final String txtSameOffer_loc="xpath";
    //Total num of products
    public static final String txtTotPrds="//div[@class='m-product-details-container']";
    public static final String txtTotPrds_loc="xpath";
    //hotspot products section
    public static final String secHotSpot="//div[@class='carousel espot-carousel-container espot3']";
    public static final String secHotSpot_loc="xpath";
    //Anchor link
    public static final String linkAnchor="Back to top";
    public static final String linkAnchor_loc="linkText";
    
    //To store First product price(Kathir - Created on 22-jul-14)
    public static String firstProductPrice = "";
    
    //First product price element(Kathir - Created on 22-jul-14)
    public static final String eleFirstPrice = "(//span[@class='price trolley-price'])[1]";
    public static final String eleFirstPrice_loc = "xpath";		

   
    

    //Total price in header(Kathir - Updated on 25-jul-14)
    public static final String txtTotalPrice = "//div[@class='totals']/span[@class='value']";
    public static final String txtTotalPrice_loc = "xpath";
    

  //Feature label
      public static final String txtFeatureLAabel="//a[@class='label feaOffer' and contains(text(),'Featured Product')]";
      public static final String txtFeatureLAabel_loc="xpath";
      //Feature offer link
      public static final String linkFetPrd="//div[@class='offerDetails']/a[@class='offer' and contains(text(),'Featured Product')]";
      public static final String linkFetPrd_loc="xpath";
      //Mix and match link
      public static final String linkOffer="//div[@class='offerDetails']/a";
      public static final String linkOffer_loc="xpath";
      //Offer logo
      public static final String imgOfferLogo="//div/a[@class='offer']";
      public static final String imgOfferLogo_loc="xpath";
    //selectoption for variation pdct(updated Lekshmi Aug 2014)

      public static final String btnSelectOption = "//a[contains(text(),'Select option')]";
      public static final String btnSelectOption_Loc = "xpath";
      
   
      

public static final String lnkSelectOptiongrid = "//input[@name='variant'][3]";
      
public static final String  btnBookslotOverlay_Loc="xpath";
public static final String btnBookslotOverlay="//a[@class='bookslot-button']";


//Book a slot button from new overlay(Lekshmi Sept 2014)
     
     public static final String  btnBookslot_Loc="xpath";
     public static final String btnBookslot="(//a[contains(text(),'Book a slot')])[2]";
     
     public static final String btnOffAdd="//a[@class='offer'][contains(.,'ValueSearch1')]/../../..//div//a[@class='m-product-open-details'][contains(text(),'ValueSearch2')]/../../../..//div[@class='button add-button']/a[@title='Add a single item to trolley']";
 	//a[@class='offer'][contains(.,'ValueSearch1')]//..//..//a[@class='m-product-open-modal'][contains(text(),'ValueSearch2')]//..//..//..//..//div[@class='button add-button']/a[@title='Add a single item to trolley']";//a[@class='offer'][contains(.,'Buy 2 for �2')]//..//..//a[@class='m-product-open-modal'][contains(text(),'Lurpak Danish unsalted butter')]//..//..//..//..//div[@class='button add-button']/a[@title='Add a single item to trolley']";
 	public static final String btnOffAdd_loc="xpath";
 	
 	//Add Single item(Nanditha -Created on 01 Sep 2013)(Kathir - Updated on 07 Oct 2014)
 		public static final String btnAddSingleItem = "//div[@class='products-grid']/div[@class='products-row'][RowIndex]/div[@class='m-product-cell'][CellIndex]/div/div[2]/div/div[@class='button add-button']/a";
 		public static final String btnAddSingleItem_Loc ="xpath";
 		
 		public static final String txtPersonalisedGreetings = "//fieldset/textarea";
		public static final String txtPersonalisedGreetings_Loc = "xpath";
		

		//Product Title with add button (Updated on 101214)

		public static final String btnAddforSpecificPrdName="//div[@class='m-product-details-container']//a[contains(text(),'ValToReplace')]//..//..//..//..//a[@title='Add a single item to trolley']";
		public static final String btnAddforSpecificPrdName_loc="xpath";
		
		//increment button
		public static final String btnPlus="//a[@title='Add a single item to trolley']";
		public static final String btnPlus_loc="xpath";
		

		// shelf life in PLP 
				
				public static final String imgShelfLifePLP = "//*[contains(@class,'shelf')]//img";
				public static final String imgShelfLifePLP_Loc = "xpath";

			// My waitrose offer logo in PLP page
						
						public static final String imgmywaitroselogo = "(//a[@class='label myWaitrose'])[1]";
						public static final String imgmywaitroselogo_Loc = "xpath";
						
			// offer logo in PLP page
						
						public static final String imgofferLogo = "(//a[@class='label offer'])[1]";
						public static final String imgofferLogo_Loc = "xpath";
						
			// red text link in offer PLP page
						
						public static final String lnkredtextoffer = "//div[@class='offerDetails']//a[@class='offer']";
						public static final String lnkredtextoffer_loc = "xpath";	
						
						//Our brand filteres
						public static final String linkOurBrandView="(//div[@class='filter-view-control'])[1]/a[2]";
						public static final String linkOurBrandView_loc="xpath";
						//offertesxt
						public static final String txtOfffil="(//div[@id='filter-section-rolled'])[1]/ul/li/label";
						public static final String txtOfffil_loc="xpath";
						
								
						//offercheck
						public static final String chkOffer="((//div[@id='filter-section-rolled'])[1]/ul/li/input)[1]";
						public static final String chkOffer_loc="xpath";
						
						//offercheck
						public static final String linkCanonical="//link[@rel='canonical']";
						public static final String linkCanonical_loc="xpath";
						
						
						// PYO offer sticker
						
						public static final String labelPYOoffer = "//div[@class='l-content']//span[@class='label pyoOffer']";
						public static final String labelPYOoffer_Loc = "xpath";
						
						
		// PYO offer 20% off red link 
						
						public static final String linkPYOoffer = "//div[@class='l-content']//div[@class='offerDetails']//a[@title='PYO Offer Detail']";
						public static final String linkPYOoffer_Loc = "xpath";
						

						public static final String productList = "(//div[@class='m-product-cell'])";
						public static final String productList_Loc = "xpath";
						
						public static final String filteredCategory = "//a[@class='inactiveLink']";
						public static final String filteredCategory_loc = "xpath";
						
						public static final String backToSearch = "//a[contains(text(),'Back to Search Results')]";
						public static final String backToSearch_loc = "xpath";

}