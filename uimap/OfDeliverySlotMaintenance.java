package uimap;



public class OfDeliverySlotMaintenance {

	//branch
	public static final String linkDeliverySlotMain="Delivery Slot Maintenance";
	public static final String linkDeliverySlotMain_loc="linkText";
	
	//edit slot
	public static final String linkEditSlot="//td[@id='option_1']";
	public static final String linkEditSlot_loc="xpath";
	
	//branch drop down
	public static final String drpBranch="branch_rn";
	public static final String drpBranch_loc="name";
	
	//service type drop down
	public static final String drpServiceType="service_type";
	public static final String drpServiceType_loc="name";

	//go button
	public static final String btnGo="go";
	public static final String btnGo_loc="linkText";
	
	//back to slot button
	public static final String btnbacktoslot="back to slots homepage";
	public static final String btnbacktoslot_loc="linkText";
	
	//delivery slot results
	public static final String linkRightPadded="//tr[4]/td/table/tbody/tr/td";
	public static final String linkRightPadded_loc="xpath";
	
	//sunday
	public static final String btnSun="Sun";
	public static final String btnSun_loc="linkText";
	
	//monday
	public static final String btnMon="Mon";
	public static final String btnMon_loc="linkText";
	
	//tuesday
	public static final String btnTue="Tue";
	public static final String btnTue_loc="linkText";
	
	//wednesday
	public static final String btnWed="Wed";
	public static final String btnWed_loc="linkText";
	
	//thursday
	public static final String btnThu="Thu";
	public static final String btnThu_loc="linkText";
	
	//friday
	public static final String btnFri="Fri";
	public static final String btnFri_loc="linkText";
	
	//saturday
	public static final String btnSat="Sat";
	public static final String btnSat_loc="linkText";

	public static final String linkSelect="select";
	public static final String linkSelect_loc="linkText";

	public static final String txtPatternNo_loc="xpath";
	public static final String txtPatternNo="//form[@action='admin']//tbody/tr[1]/td[2]";

	public static final String txtRestrictedCap_loc="xpath";
	public static final String txtRestrictedCap="//input[@name='capacity2_*' and @value='']";

	public static final String txtOverallCap_loc="xpath";
	public static final String txtOverallCap="//input[@name='capacity_*' and @value='']";

	public static final String btnCont_loc="xpath";
	public static final String btnCont="//a[@title='Click here to continue with your changes.']";


	public static final String slctRadio_loc="xpath";
	public static final String slctRadio="(//input[@name='date_range'])[2]";

	public static final String slctEndDate_loc="xpath";
	public static final String slctEndDate="//select[@name='end_date_select']";

	public static final String btnSaveChngs_loc="xpath";
	public static final String btnSaveChngs="//a[contains(text(),'save changes')]";	
	//right arrow

	
	public static final String btnRArrow="//td/table/tbody/tr/td/a";
	public static final String btnRArrow_loc="xpath";
	
	//left arrow
	public static final String btnLArrow="//td[28]/a/img";
	public static final String btnLArrow_loc="xpath";
	
	//btnBackToSlot
	public static final String btnBackToSlot="back to slots overview";
	public static final String btnBackToSlot_loc="linkText";
	
	//changeslot
	public static final String btnChangeSlot="//a[contains(text(),'change slot times')]";
	public static final String btnChangeSlot_loc="xpath";
	
	//service
	public static final String lblService="//tr[2]/td[2]";
	public static final String lblService_loc="xpath";
	
	//slot drop down
	public static final String drpSlot="num_slots";
	public static final String drpSlot_loc="name";
	
	//start time drop down
	public static final String drpearliest_start="earliest_start";
	public static final String drpearliest_start_loc="name";
	
	//end time drop down
	public static final String drplatest_end="num_slots";
	public static final String drplatest_end_loc="name";
	
	//first go button
	public static final String btnGo1="(//a[contains(text(),'go')])[1]";
	public static final String btnGo1_loc="xpath";
	
	//second go button
	public static final String btnGo2="(//a[contains(text(),'go')])[2]";
	public static final String btnGo2_loc="xpath";
	
	//pattern number
	public static final String txtPattern="template_rn";
	public static final String txtPattern_loc="name";
	
	//verify pattern number
	public static final String lblPattern="//td/div/table/tbody/tr/td";
	public static final String lblPattern_loc="xpath";
	
	//cancel button
	public static final String btnCancel="cancel";
	public static final String btnCancel_loc="linkText";
	
	//View currently defined
	public static final String linkVIewCurrentDefined="//tr/td[contains(text(),'View currently defined customer slots')]";//"View currently defined customer slots";
	public static final String linkVIewCurrentDefined_Loc="xpath";
	
	//Editslot for multi branches
	public static final String linkEditSlotForMultiBrnch="Edit slots for multiple branches";
	public static final String linkEditSlotForMultiBrnch_loc="linkText";
	
	//Editslot for multi branches
	public static final String linkViewEditSlots="//tr/td[contains(text(),'View / edit slots for a branch')]";//View / edit slots for a branch";
	public static final String linkViewEditSlots_loc="xpath";
	
	//Maintain slot pattern
	public static final String linkMaintainSLotPattern="//tr/td[contains(text(),'Maintain slot patterns')]";//Maintain slot patterns";
	public static final String linkMaintainSLotPattern_Loc="xpath";
	
	//Refresh customer
	public static final String linkRefreshCustomer="Refresh customer site slots for a branch";
	public static final String linkRefreshCustomer_loc="linkText";
					
	//Slot charging
	public static final String linkMaintainSlotCharging="Maintain slot charging";
	public static final String linkMaintainSlotCharging_loc="linkText";
		    

	//Slot Capacity
		public static final String txtTotalCapacity="//td[@class='normalCentered']//input[contains(@name,'capacity')]";
		public static final String txtTotalCapacity_Loc="xpath";
		public static int txtTotalCapacityValue= 0;
		
		//slot capacity - first slot
		public static final String txtSlotCapacityForFirstSlot="(//td[@class='normalCentered']//input[contains(@name,'capacity')])[1]";
		public static final String txtSlotCapacityForFirstSlot_Loc="xpath";
		public static int txtSlotCapacityValueForFirstSlot= -1;
		public static int txtRestrictedSlotCapacityValueForFirstSlot= -1;
		public static int txtRestrictedPlacedSlotCapacityValueForFirstSlot= -1;
		public static int txtRestrictedFreeSlotCapacityValueForFirstSlot= -1;
		//Delivery Slot Maintenance - Continue Button
				public static final String btnContinue="//a[text()='continue']";
				public static final String btnContinue_Loc="xpath";
				
				//Delivery Slot Maintenance - Continue Button
				public static final String btnSaveChanges="//a[text()='save changes']";
				public static final String btnSaveChanges_Loc="xpath";
		

				//edit slot for Multiple Branch
				public static final String linkEditSlotForMultipleBranch="//*[contains(text(),'Edit slots for multiple branches')]";
				public static final String linkEditSlotForMultipleBranch_loc="xpath";
				
				//Alphabetical
				public static final String btnAlphabetical="//a[text()='alphabetically']";
				public static final String btnAlphabetical_loc="xpath";

				//Branch Name
				public static final String txtBranchName="(//td[@class='smallText'])";
				public static final String txtBranchName_loc="xpath";
							
				
				//Branch Slots Mass Page
				public static final String pageBranchSlotMass="//td[contains(text(),'Branch Slots Mass')]";
				public static final String pageBranchSlotMass_loc="xpath";
				
				//Branch Slots Mass Page - slot Type 
				public static final String dropdownSlotType="//select[@name='slot_type']";
				public static final String dropdownSlotType_loc="xpath";
				public static final String dropdownDefaultSlotType="//select[@name='slot_type']//option[@selected]";
				
				//Branch Slots Mass Page - slot Type 
				public static final String dropdownActionType="//select[@name='action_select']";
				public static final String dropdownActionType_loc="xpath";
				public static final String dropdownDefaultActionType="//select[@name='action_select']//option[@selected]";
				
				//Start Date Dropdown
				public static final String txtStartDate="//select[@name='start_date_select']";
				public static final String txtStartDate_loc="xpath";
				
				//End Date Dropdown
				public static final String txtEndDate="//select[@name='end_date_select']";
				public static final String txtEndDate_loc="xpath";
				
				//All Day Radio Button
				public static final String radioAllDay="//td[text()='all day']";
				public static final String radioAllDay_loc="xpath";
				
				//After Radio Button
				public static final String radioAfter="//td[text()='after']";
				public static final String radioAfter_loc="xpath";
		
				//Before Radio Button
				public static final String radioBefore="//td[text()='before']";
				public static final String radioBefore_loc="xpath";
				
				//Branch Slots Mass Page - Capacity Type 
				public static final String dropdownCapacityType="//select[@name='coll_select']";
				public static final String dropdownCapacityType_loc="xpath";
				
				//Branch Slots Mass Page - Capacity Type 
				public static final String dropdownBranchType="//select[@name='branch_rn']//option";
				public static final String dropdownBranchType_loc="xpath";
				
				
				//Delivery Slot Maintenance Page - Refresh Customer
				public static final String linkRefreshCustomerSite="//td[contains(text(),'Refresh customer')]";
				public static final String linkRefreshCustomerSite_loc="xpath";
				
		
		
			
}
