package uimap;

public class M_Login {
	
	public static final String buttonSignIn = "(//button[contains(text(),'Sign in')])[2]";
	public static final String buttonSignIn_Loc = "xpath";
	
	public static final String linknewuser = "//button[contains(text(),'New to Waitrose.com? Register here')]";
	public static final String linknewuser_Loc = "xpath";

	public static final String txtEmail="(//div[@class='inputHolder']/input)[3]";
	public static final String txtEmail_loc="xpath";
	
	public static final String txtPassword="(//div[@class='inputHolder']/input)[4]";
	public static final String txtPassword_loc="xpath";
	
	public static final String txtconfirmPassword="(//div[@class='inputHolder']/input)[5]";
	public static final String txtconfirmPassword_loc="xpath";
	
	public static final String btnReg="//button[@class='btnPrimaryCTA unselectable']";
	public static final String btnReg_loc="xpath";
	
	public static final String btncont="(//button[contains(text(),'Continue')])[1]";
	public static final String btncont_loc="xpath";
	
	public static final String txtregEmail="(//div[@class='inputHolder']/input)[1]";
	public static final String txtregEmail_loc="xpath";
	
	public static final String txtregPassword="(//div[@class='inputHolder']/input)[2]";
	public static final String txtregPassword_loc="xpath";
	
	public static final String btnLogin="//button[contains(text(),'Login')]";
	public static final String btnLogin_loc="xpath";
}
