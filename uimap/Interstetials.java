package uimap;

public class Interstetials {

	
	//MissedOffer Inteststial Page
	//enter qty 
	public static final String txtMissedOfferQty="//input[@value='1']";
	public static final String txtMissedOfferQty_Loc="xpath";
	//click add button
	public static final String linkMissedOfferAdd="Add";
	public static final String linkMissedOfferAdd_Loc="linkText";
	
	//BeforeYouGo Inteststial Page
	//enter qty 
	public static final String txtBeforeYouGoQty="//input[@value='1']";
	public static final String txtBeforeYouGoQty_Loc="xpath";
	//click add button
	public static final String linkBeforeYouGoAdd="Add";
	public static final String linkBeforeYouGoAdd_Loc="linkText";
	
	
		
		//did you forget Page
		//click finished here next step
		public static final String linkFinishedHere="(//button[contains(text(),'Finished here')])[1]";//"(//a[contains(text(),'Finished here. Next step')])[1]";
		public static final String linkFinishedHere_Loc="xpath";
		
		public static final String linkFinishedHere2="(//button[contains(text(),'Finished here')])[1]";
		public static final String linkFinishedHere2_Loc="xpath";
		
		//Missed offer text (Nanditha -created on 1-Aug-13)
				public static final String txtMissedOffer = "//div[@class='offer-heading']/h2/a";
				public static final String txtMissedOffer_Loc = "xpath";
				public static final String txtMissedOffer_Attribute = "tagName";
				
				public static final String txtMissedOfferInfo = "//div[@class='offer-heading']/span";
				public static final String txtMissedOfferInfo_Loc = "xpath";
				
				//unfavourite a product
				public static final String linkUnFav="Remove from Favourites";
				public static final String linkUnFav_Loc="linkText";
				
				//unfavouite product check 
				public static final String linkUnFavcheck="Toggle favourite";
				public static final String linkUnFavcheck_Loc="linkText";
				//image of product (Nanditha - Created on 12-Aug-13)
				public static final String lnkPrdImage = "//img[@class='m-product-img']";
				public static final String lnkPrdImage_Loc = "xpath";



				//Product Part Number(Nanditha - Created on 13-Aug-13)
				public static String ProductFromInterstitial = "";
				
				//Add button Single Product(Nanditha - Created on 13-Aug-13)
				public static final String buttonAddInterstitial = "Add";
				public static final String buttonAddInterstitial_Loc = "linkText";
				
				

//Delivery address details 
				public static final String txtDelAdd="//h2[contains(text(),'Delivery address details')]";
				public static final String txtDelAdd_loc="xpath";
				//Find my address
				public static final String linkFindmyaddress="Find my address";
				public static final String linkFindmyaddress_loc="linkText";
				
//PromoOFFER POPUP
			
				//Continue
				public static final String linkContinue="Continue";
				public static final String linkContinue_loc="linkText";
				
				//to be updated by Nanditha 24 october
				public static final String lnkFavIcon="";
				public static final String lnkFavIcon_Loc="";
				public static final String lnkAdd="";
				public static final String lnkAdd_Loc="";
				
				//continue to checkout button
				public static final String linkContinuetocheckout="//a[contains(text(),'Continue to checkout')]";
				public static final String linkContinuetocheckout_loc="xpath";
				
				//PromoOFFER POPUP(Update on 090614)
				public static final String txtPopUp="(//p[@class='conflict-msg'])[2]";
				public static final String txtPopUp_loc="xpath";
				

				//Hotspot check 
				public static final String hotspotchkgroceries="//div[@class='carousel espot-carousel-container espot4']/div/div[@class='carousel-items']/div";
				public static final String hotspotchkgroceries_Loc="xpath";
								
				//Hotspot check offr
				public static final String hotspotchkoffers="//div[@class='l-content']/div[2]/div/div";
				public static final String hotspotchkoffers_Loc="xpath";
						
				//Hotspot check BYG
				public static final String hotspotchkBYG="//div[@class='beforeyougo-offer']/div[1]/div/div";
				public static final String hotspotchkBYG_Loc="xpath";

				//sorting option
					public static final String sortingchck="//select[@id='sort']";
					public static final String sortingchck_Loc="xpath";
				//sorting individual option
				public static final String indvsortingchck="//select[@id='sort']/option[count]";
				public static final String indvsortingchck_Loc="xpath";
				//filter option
				public static final String filterchck="//div[@class='wrapper']/div[@id='content']/div/div[2]/nav/div";
				public static final String filterchck_Loc="xpath";

				//filteroptions
				public static final String filteroptionchck="//div[@class='wrapper']/div[@id='content']/div/div[2]/nav/div/fieldset[count]/div[1]/div/div/div/span";
				public static final String filteroptionchck_Loc="xpath";
				
				// Seasonal Reserved popup checkbox(Kathir created on 050814)
				public static final String chkSeasonalReserved="//input[@id='checkres']";
				public static final String chkSeasonalReserved_Loc="xpath";
				
				//Seasonal Reserved popup Continue button(Kathir created on 050814)
				public static final String btnReservedcontinue="//a[@class='continueRes']";
				public static final String btnReservedcontinue_Loc="xpath";	
				
				//click + button
				public static final String linkMissedOfferPlus="//div[2]/div/div[4]/a";////div[@class='button add-button ']/a
				public static final String linkMissedOfferPlus_Loc="xpath";
				
				//total inter products

				public static final String totalDidYFav="//tr[@class='two-col l-content']/div/div[@class='products-row']/div";
				public static final String totalDidYFav_loc="xpath";

				//Total products in did you forget page

				public static int didUFFavts=0;

				//seasonal reserve overlay

				public static final String txtSeasonalResv="//div[@class='resText']/p[1]";
				public static final String txtSeasonalResv_loc="xpath";

				//seasonal check box

				public static final String chkSeasonal="checkres";
				public static final String chkSeasonal_loc="id";

				//continue reserve

				public static final String btnSeasonalContinue="//a[@class='continueRes']";
				public static final String btnSeasonalContinue_loc="xpath";
				

				// text Waitrose kitchen - Prabhakaran Sankar
								public static final String textMyWaitroseKitchen = "//p[contains(text(),'Waitrose kitchen')]";
								public static final String textMyWaitroseKitchen_Loc = "xpath";
								

								
										

										//Before GO Page - 1st Product (Jayandran)
																				public static final String btnFirstProduct="(//div[@class='products-row']//div[@class='m-product-cell'])[1]";
																				public static final String btnFirstProduct_loc="xpath";
																				
																				//Before GO Page - 1st Product_Add Button(Jayandran)
																				public static final String btnAddFirstProduct="(//div[@class='products-row']//a[text()='Add'])[1]";
																				public static final String btnAddFirstProduct_loc="xpath";
																				
																				//Before GO Page - 1st Product_Input Button(Jayandran)
																				public static final String txtQtyFirstProduct="(//div[@class='products-row']//input[@class='quantity-input'])[1]";
																				public static final String txtQtyFirstProduct_loc="xpath";
																				
																				//Order Complete (Jayandran)

																				public static final String txtOfferComplete="//span[contains(text(),'Offer complete')]";
																				public static final String txtOfferComplete_loc="xpath";
																				
																				public static final String txtDeliveryBanner="//div[@class='IntHeaderMsg']/h1";
																				public static final String txtDeliveryBanner_loc="xpath";
																				
																				public static final String txtDeliveryBanner1="//div[@class='byg-messaging']";
																				public static final String txtDeliveryBanner1_loc="xpath";

																				//Created By Jayandran
																					public static double bannerPendingAmountForDeliveryCharge =0;

																					// dynamic qualify message in BYG page
																					
																					public static final String txtDCQualifyMsg  = "//div[@class='IntHeaderMsg']";
																					public static final String txtDCQualifyMsg_Loc = "xpath";

}
