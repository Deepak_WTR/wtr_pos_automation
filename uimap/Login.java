package uimap;



public class Login {

	//PostCode Link(Nanditha-Book A Slot Page)
	public static String linkPostCode = "//div[@class='required']/input[@id='postcode_lookup_input']";
	public static String linkPostCode_Loc = "xpath";

	//'OK' Button(Nanditha-Book A Slot Page)
	public static String buttonOk = "//input[@value='Ok']";
	public static String buttonOk_Loc = "xpath";

	//User-id Text(Nanditha-Book A Slot Page)
	public static String txtUserName = "email";
	public static String txtUserName_Loc = "id";

	//Password Text(Nanditha-Book A Slot Page)
	public static String txtPassWord = "password";
	public static String txtPassWord_Loc = "id";

	//CheckBox(Nanditha-Book A Slot Page)
	public static String CheckBox = "pass-1";
	public static String CheckBox_Loc = "id";

	//Sign in Button(Nanditha-Book A Slot Page)
	public static String buttonSignIn = "button-sign-in";
	public static String buttonSignIn_Loc = "id";

	//Select Slot Link(Nanditha-Book A Slot Page)
	public static String linkSelectSlot = "//div[@class='slots']/ul[4]/li[3]/a";
	public static String linkSelectSlot_Loc = "xpath";

	//Start shopping button
	//public static String buttonStartShopping = "//div[@class='button content-button start-shopping no-image']/a[@class='start-shopping-click']";
	//public static String buttonStartShopping_Loc = "xpath";

	//My Account link
	public static String linkMyAccount = "//a[contains(text(),'My account')]";
	public static String linkMyAccount_Loc = "xpath";

	//Start shopping overlay
	public static String txtStartShoppingOverlay = "//div[@class='toolbox-close']";
	public static String txtStartShoppingOverlay_Loc = "xpath";

	//Heading - Book a Slot
	public static final String txtLoginPageHeading ="//div[@class='l-content']/h1";
	public static final String txtLoginPageHeading_Loc = "xpath";

	//Mike Variables

	//Login Page UI
	public static final String linkGroceries ="Groceries";
	public static final String txtUsername ="logonId";
	public static final String txtUsername_Loc = "name";
	public static final String txtPassword = "logonPassword";
	public static final String txtPassword_Loc ="name";
	public static final String btnSubmit ="//input[@value ='Sign in']";
	public static final String btnSubmit_Loc ="xpath";

	//CheckBox(Nanditha-Book A Slot Page)(Updated On 24-July-13)
	public static String chkBoxRegisteredUser = "pass-1";
	public static String chkBoxRegisteredUser_Loc = "id";

	//CheckBox(Nanditha-Book A Slot Page)(Added on 24-July-13)
	public static String chkBoxNewUser = "pass-0";
	public static String chkBoxNewUser_Loc = "id";

	//Sign in Button(Nanditha-Created on 30-July-13)
	public static String buttonContinue = "button-register";
	public static String buttonContinue_Loc = "id";

	//Confirmed User-id  Text(Nanditha-Created on 30-July-13)
	public static String txtUserNameConfirm = "email-confirm";
	public static String txtUserNameConfirm_Loc = "id";

	//Choose Password Text(Nanditha-Created on 30-July-13)
	public static String txtPassWordChoose = "password-choose";
	public static String txtPassWordChoose_Loc = "id";


	//legal statement
	public static String linkLegalStatement = "Legal statement";
	public static String linkLegalStatement_Loc = "linkText";

	//Channel Island
	public static String linkChannelIsland = "Channel Islands";
	public static String linkChannelIsland_Loc = "linkText";

	//Website Cookies
	public static String linkWebsiteCookies = "Website Cookies";
	public static String linkWebsiteCookies_Loc = "linkText";




	//Invalid Password error message
	public static String txtErrPwd="(//div[@class='error-msg']/p)[2]";
	public static String txtErrPwd_loc="xpath";



	//Start shopping button(Gajapathy updated on 210314)
	//public static String buttonStartShopping = "(//a[@id='continueButton1'])[2]";//(//a[contains(text(),'Continue')])[5]";
	//public static String buttonStartShopping_Loc = "xpath";	



	public static String btnStartShoppingColle = "(//a[@id='continueButton2'])[2]";
	public static String btnStartShoppingColle_loc = "xpath";


	public static String txtWelcmWait="//h1[contains(text(),'Welcome to Waitrose')]";
	public static String txtWelcmWait_loc="xpath";

	public static String buttonStartShopping = "(//a[contains(text(),'Continue')])[9]";
	public static String buttonStartShopping_Loc = "xpath";

	public static String linkPrivacyPolicy = "Privacy policy";
	public static String linkPrivacyPolicy_Loc = "linkText";


	//PostCode Link for collection(Lekshmi Aug-2014)
	public static String linkPostCodecoll = "//input[@id='townname']";
	public static String linkPostCodecoll_Loc = "xpath";

	//'Find' Button(Lekshmi Aug-2014)
	public static String buttonFind = "//input[@id='update-location']";
	public static String buttonFind_Loc = "xpath";



	//Start shopping button(Lekshmi updated August-2014)
	public static String buttonGCStartShopping = "(//a[@id='continueButton2'])[2]";
	public static String buttonGCStartShopping_Loc = "xpath";


	//"collect" from here.
	public static String buttoncollfrmhere = "(//a[contains(text(),'Collect from here')])[1]";//a[contains(text(),'Collect from here')]";
	public static String buttoncollfrmhere_Loc = "xpath";

	 
	public static String btnstartshopping = "//a[contains(text(),'Continue')]";
	public static String btnstartshopping_Loc = "xpath";
	
	//not working as expected changing xpath in 5.7
	public static String btnstartshopping2 = "//a[contains(text(),'Confirm slot change')]";
	//public static String btnstartshopping ="//a[@id='continueButton2']";
	public static String btnstartshopping_Loc2 = "xpath";
	
	
	// forgotten Password 
	
	public static final String lnkForgottenPassword = "//a[contains(text(),'Forgotten your password?')]"; 
	public static final String lnkForgottenPassword_Loc = "xpath";
	
	// forgot password page
	
	public static final String txtforgotDetails = "//span[contains(text(),'Forgot login details')]";
	public static final String txtforgotDetails_Loc = "xpath";
	
	// email address field -  prepopulated field
	
	public static final String txtboxemailAddress = "//input[@id='email']";
	public static final String txtboxemailAddress_Loc = "xpath";
	
	
	// Welcome to waitrose header 
	
	public static final String txtWelocmeToWaitrose = "//h1[contains(text(),'Welcome to Waitrose')]";
	public static final String txtWelocmeToWaitrose_Loc = "xpath";
	
	//Left Search top header panel  
	public static final String leftPanelSearchTopHeader ="//div[@class='leftNavCategory  js-collapsible']/ul/li/a";   // Old Obj Identifier - "//nav[@class='refinement']/ul/li/a";
	public static final String leftPanelSearchTopHeader_Loc = "xpath";
	
	public static final String leftPanelFilterLinks = "//div[@role='tabpanel' and @style='display: block;']//div[@id='filter-section-rolled']/li/a";
	public static final String leftPanelFilterLinks_loc = "xpath";
	
	public static final String recipesGroupHeaderComp = "//div[@class='filter-section']//span[contains(@class, 'arrow-down')]";
	public static final String recipesGroupHeaderComp_loc = "xpath";
	
	public static final String chkleftPanelFilterRecipes = "//div[@class='filter-section']//div[@id='filter-section-rolled' and @style='display: block;']//input[@type='checkbox']";
	public static final String chkleftPanelFilterRecipes_loc = "xpath";
	
	public static final String chkleftPanelUnFilterRecipes = "//div[@class='filter-section']//div[@id='filter-section-rolled' and contains(@class, 'block')]//input[@type='checkbox']";
	public static final String chkleftPanelUnFilterRecipes_loc = "xpath";
	
	public static final String chkleftPanelFilterInspiration = "//nav[@class='refinement']//input[@type='checkbox']";
	public static final String chkleftPanelFilterInspiration_loc = "xpath";
	
	//Left Search left Panel Search First Level Parent Product
	public static final String leftPanelSearchFirstLevelParentProduct = "//*[@id='content']/div/div[2]/nav/nav/ul/li/a";
	public static final String leftPanelSearchFirstLevelParentProduct_Loc = "xpath";
		
	//Left Search left Panel Search Next Level Product 
	public static final String leftPanelSearchNextLevelProduct = "//*[@id='content']/div/div[2]/nav/nav/ul/li/ul[1]/li/a";
	public static final String leftPanelSearchNextLevelProduct_Loc = "xpath";
	
	//Product panel row 
	public static final String productContainerRow = "//div[@class='products-row']";
	public static final String productContainerRow_Loc = "xpath";
	
	//postcode field on overlay
	
	public static final String txtPostcodeOverlay = "logon-postcode";
	public static final String txtPostcodeOverlay_loc = "id";
	
	//check postcode button on overlay
	public static final String btnCheckPostcodeOverlay = "input[value='Check']";
	public static final String btnCheckPostcodeOverlay_loc = "cssSelector";
	
	//Check Delivery Service unavailable Message
	public static final String DelServiceUnavailabeMessage = "//div[@class='register-postcode module hidden']/header/h1";
	public static final String DelServiceUnavailabeMessage_loc = "xpath";	
	
	//Click Collection Slot page
	public static final String clickBookCollectionButton = "book-collection-slot";
	public static final String clickBookCollectionButton_loc = "id";
	
	//Product Unavailable Message
	public static final String productUnavailabelMessage = "//p[contains(text(),'Sorry, this product is currently unavailable for Ordering')]";
	public static final String productUnavailabelMessage_loc =  "xpath";
	
	
}
