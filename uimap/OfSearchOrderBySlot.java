package uimap;


public class OfSearchOrderBySlot {

	//branch
		public static final String drpdbranch="branch_rn";
		public static final String drpdbranch_loc="name";
	//Service type
		public static final String drpdserviceType="service_select";
		public static final String drpdserviceType_loc="name";
	//Hide Advance search
		public static final String btnHideSrch="//tr[@id='advancedSearchOpen']/td/table/tbody/tr/td/img";
		public static final String btnHideSrch_loc="xpath";
	//Start date
		public static final String drpdSrtDate="start_date_select";
		public static final String drpdSrtDate_loc="name";
	//End DATE
		public static final String drpdEndDate="end_date_select";
		public static final String drpdEndDate_loc="name";
	//Start Calendar
		public static final String btnSrtCalender="//tbody/tr[5]/td[1]/a/img";
		public static final String btnSrtCalender_loc="xpath";
	//Start Calendar
		public static final String btnEndCalender="//tbody/tr[5]/td[2]/a/img";
		public static final String btnEndCalender_loc="xpath";	
	//Order Status
		public static final String drpdOrdStatus="status";
		public static final String drpdOrdStatus_loc="name";
	//Search Order
		public static final String btnSrchOrder="(//img[@title='Click here to start your search.'])[2]";
		public static final String btnSrchOrder_loc="xpath";
	//Select dropdown list value
		public static final String drpdBranschValue="//select[@name='branch_rn']/option[contains(text(),'SearchValue')]";
		public static final String drpdBranschValue_loc="xpath";
		//Search result table
		public static final String labelsearchresult="//form[2]/table";
		public static final String labelsearchresult_loc="xpath";
		//click review
		public static final String btnReview="a[title='Click here to view this order.']";
		public static final String btnReview_loc="cssSelector";
		//craft labels
		public static final String linkCraftLabel="Crate Labels";
		public static final String linkCraftLabel_loc="linkText";
		//click back
		public static final String imgBack="//img[@alt='back']";
		public static final String imgBack_loc="xpath";
		//click print schedule
		public static final String imgPrintSchedule="(//img[@alt='print schedule'])[1]";
		public static final String imgPrintSchedule_Loc="xpath";
		//craft labels
		public static final String linkReviewNew="Review New / Amended Orders";
		public static final String linkReviewNew_loc="linkText";
		//customer
		public static final String labelCustomer="//td[6]/table/tbody/tr/td";
		public static final String labelCustomer_loc="xpath";
		public static String labelCustomerValue="";
		/*public static String txtBranchNameDropdown_Loc;
		public static String txtBranchNameOptionDropdown;
		public static String firstOrderTagButton;
		public static String firstOrderTag_loc;*/
		///////////
		public static final String orderSearchlnk="//a[contains(@href, '/wdeliver/app/admin?op=orderSearch')]";
		public static final String orderSearchlnk_loc="xpath";
		
		public static final String orderEntryBox="order_rn";
		public static final String orderEntryBox_loc="name";
		
		public static final String orderSearchbtn="(//img[@alt='Search'])[2]";
		public static final String orderSearchbtn_loc="xpath";
		
		public static final String orderReviewlnk="//img[@title='Click here to review this order.\"']";
		public static final String orderReviewlnk_loc="xpath";
		
		public static final String orderReviewChkBox="is_reviewed_order";
		public static final String orderReviewChkBox_loc="name";
		
		public static final String orderReviewSaveChanges="td.centeredText > input[name=\"submit\"]";
		public static final String orderReviewSaveChanges_loc="cssSelector";
		
		//itemdetails in OrderSummary Table(Created by Jayandran on 25/11/2014)
				public static final String txtLoanItemName="(//div[@id='ordered_details']//td[@class='text'])";
				public static final String txtLoanItemName_loc="xpath";
				
				//OrderSummary Table(Created by Jayandran on 25/11/2014)
				public static final String tableOrderSummary ="ordered_details";
				public static final String tableOrderSummary_loc ="id";
				
				//Hide Advance Search button (Created by Prabhakaran sankar on 12/03/2015)
				public static final String hideAdvanceSearch ="//td[@title='Click here to hide the advanced search options.']//img";
				public static final String hideAdvanceSearch_loc ="xpath";
				
				//Show Advance Search button (Created by Prabhakaran sankar on 12/03/2015)
				public static final String showAdvanceSearch ="//td[@title='Click here to show the advanced search options.']//img";
				public static final String showAdvanceSearch_loc ="xpath";
				
				
				
				//verify review button new and Ammend order page(Created by Prabhakaran sankar on 12/03/2015)
				public static final String reviewButton ="(//a[@title='Click here to review this order.']//img)[1]";
				public static final String reviewButton_loc ="xpath";
				
// Clear search button 
				
				public static final String btnClearSearch = "//img[@title='Click here to clear reset your search.']";
				public static final String btnClearSearch_Loc = "xpath";
				
				
				// Advice note 
				//public static final String chckbxAdviceNote = "//td[@class='centeredText']//input[@name='advice']";
				public static final String chckbxAdviceNote = "//div[@id='ordered_details']//input[@type='checkbox']";
				public static final String chckbxAdviceNote_Loc = "xpath";
				// Advice note 
				public static final String listStatus = "//select[@name='status']";
				public static final String listStatus_Loc = "xpath";
				
				
				// Order slot date validation
				
				public static final String txtslotdate = "//form[@name='selectSlots']//td[@class='text'and @width='50%']";
				public static final String txtslotdate_Loc = "xpath";
				// End date - option 2
				public static final String drpdownslotStartdate = "//select[@name='start_date_select']/option[1]";
				public static final String drpdownslotStartdate_Loc = "xpath";
				
				// End date - option 2
				public static final String drpdownslotEnddate = "//select[@name='start_date_select']/option[2]";
				public static final String drpdownslotEnddate_Loc = "xpath";
				
				
				// first order in order search 			
				
				public static final String lnkorderID ="(//a[@title='Click here to view this order.'])[1]";
				public static final String lnkorderID_Loc = "xpath";
				
				//verify first Order Tag in Slot Screen Page
				public static final String firstOrderTagButton ="//*[contains(text(),'1st')]";//"//span[contains(text(),'1st')]";
				public static final String firstOrderTag_loc ="xpath";
				
				//Select Dropdown - Branch Name 
				public final static String txtBranchNameDropdown="branchSelection";
				public final static String txtBranchNameDropdown_Loc="id";
				
				//Select Dropdown - Branch Name - Option values
				public final static String txtBranchNameOptionDropdown="//select[@id='branchSelection']//option";
				public final static String txtBranchNameOptionDropdown_Loc="xpath";
				
				


				
}
