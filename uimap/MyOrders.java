package uimap;

import allocator.Allocator;

public class MyOrders {

	 //Verify Order reference number i My order page. - Prabhakaran sankar
		public static  String verifyOrderNumber = "//a[contains(text(),'" +Allocator.GlblVar+"')]"; 

		public static  String txtVerifyOrderrefNo="//a[contains(text(),'" +Allocator.GlblVar+"')]";// verifyOrderNumber;
		public static final String txtVerifyOrderrefNo_loc="xpath";
		
		//My Orders link
		public static final String linkMyOrders="//div[contains(text(),'My Orders')]";
		public static final String linkMyOrders_loc="xpath";
		//My pending Order
		public static final String linkPendinOrd="//div[contains(text(),'Order Status: Pending')]";
		public static final String linkPendinOrd_loc="xpath";
		//Edit order
		public static final String btnEditOrder="//button[contains(text(),'Edit order')]";
		public static final String btnEditOrder_loc="xpath";
		//Edit Order Overlay
		public static final String btnEditOrderOverlay="//button[@class='dialogButton' and @tabindex='0']";
		public static final String btnEditOrderOverlay_loc="xpath";
		//Cancel InEdit order overlay
		public static final String btnCancelOverlay="//button[@class='dialogButton' and @tabindex='1']";
		public static final String btnCancelOverlay_loc="xpath";
		
		//Product link
		public static final String linkProdut="//li[@class='TrolleyListItem mblListItem']";
		public static final String linkProdut_loc="xpath";
		//Plus button
		public static final String linkPlusButton="(//i[@class='fa fa-plus'])[4]";
		public static final String linkPlusButton_loc="xpath";
		//minus product button
		public static final String btnMinus="(//i[@class='fa fa-minus'])[4]";
		public static final String btnMinus_loc="xpath";
		//Checkout 
		public static final String btnCheckout="//span[contains(text(),'CHECKOUT')]";
		public static final String btnCheckout_loc="xpath";
		//Empty trolley
		public static final String btnEmprtyTrolley="//button[contains(text(),'Empty trolley')]";
		public static final String btnEmprtyTrolley_loc="xpath";
		//Empty trolley Yes button
		public static final String btnYes="//button[contains(text(),'Yes')]";
		public static final String btnYes_loc="xpath";
		
		
		
		public static final String btnAllItemsToTrolley = "//a[text()='Add all items to trolley']";
		public static final String btnAllItemsToTrolley_Loc = "xpath";
		
		
		
		
		
		
		
		
		
}