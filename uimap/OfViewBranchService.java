package uimap;



public class OfViewBranchService {

	//selecting First Edit button - Prabhakaran Sankar - 19-march-15
	public final static String clickFirstEditButton="(//td[@class='buttonCenter']//a[@title='Edit this branch service'])[1]";
	public final static String clickFirstEditButton_Loc="xpath";
	
	//Save changes button - Prabhakaran Sankar - 19-march-15
		public final static String saveChanges="//a[@title='Save changes']";
		public final static String saveChanges_Loc="xpath";
	
	//selecting checkbox Delivery - Prabhakaran Sankar - 19-march-15
	public final static String deliveryCheckbox="//input[@name='isdelivery']";
	public final static String deliveryCheckbox_Loc="xpath";
	
	//selecting checkbox Collection - Prabhakaran Sankar - 19-march-15
	public final static String collectionCheckbox="//input[@name='iscollection']";
	public final static String collectionCheckbox_Loc="xpath";
		
}
