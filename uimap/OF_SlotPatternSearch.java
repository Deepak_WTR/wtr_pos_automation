package uimap;

public class OF_SlotPatternSearch 
{
	public static final String textSlotPatternSearch="//td[@class='pageHeading' and contains(text(),'Slot Pattern Search')]";
	public static final String textSlotPatternSearch_loc="xpath";
	
	public static final String textOverviewOfExistingSlot="//td[@class='pageHeading' and contains(text(),'Overview Of Existing Slots')]";
	public static final String textOverviewOfExistingSlot_loc="xpath";
	
	public static final String textBranchSlotOverview="//td[@class='pageHeading' and contains(text(),'Branch Slots Overview')]";
	public static final String textBranchSlotOverview_loc="xpath";
	
	public static final String textSlotsMassChanges="//td[@class='pageHeading' and contains(text(),'Branch Slots Mass Changes')]";
	public static final String textSlotsMassChanges_loc="xpath";
	
	public static final String textSlotAdmin="//td[@class='pageHeading' and contains(text(),'Slot Admin Homepage')]";
	public static final String textSlotAdmin_LOC="xpath";
	
	public static final String textSlotcharge="//div[@class='page_title heading']";
	public static final String textSlotcharge_loc="xpath";
	//pattern Heading
	public static final String HdngPattern="//tr/th[contains(text(),'pattern')]";
	public static final String HdngPattern_loc="xpath";
	
	//Pattern numbers
	public static final String textPatterns="(//tr[@class='normal']/td[1])[index]";
	public static final String textPatterns_loc="xpath";
	
	//Pattern numbers
		public static final String textPatterns1="(//tr[@class='normal']/td[1])[1]";
		public static final String textPatterns1_loc="xpath";
		
	
	//Pattern address
	public static final String textPatternAddrs="(//tr[@class='normal']/td[2])[index]";
	public static final String textPatternAddrs_loc="xpath";
	// services
	public static final String textService="(//tr[@class='normal']/td[3])[index]";
	public static final String textService_loc="xpath";
	//slots
	public static final String textSlots="(//tr[@class='normal']/td[4])[index]";
	public static final String textSlots_loc="xpath";
	//start time
	public static final String textStartTime="(//tr[@class='normal']/td[5])[index]";
	public static final String textStartTime_loc="xpath";
	//end time
	public static final String textEndtime="(//tr[@class='normal']/td[6])[index]";
	public static final String textEndtime_loc="xpath";
	//Select link
	public static final String textSelectLink="(//tr[@class='normal']/td[7])[index]";
	public static final String textSelectLink_loc="xpath";
	//first select link
	public static final String textFirstSelectLink="(//tr[@class='normal']/td[7])[1]";
	public static final String textFirstSelectLink_loc="xpath";
	//create new slot pattern
	public static final String linkCreateNewslotPattern="//a[contains(text(),'create new slot pattern')]";
	public static final String linkCreateNewslotPattern_loc="xpath";
	//create new slot pattern page heading
	public static final String textcreatenewSlotHeading="//td[@class='pageHeading' and contains(text(),'Create A New Slot Pattern')]";
	public static final String textcreatenewSlotHeading_loc="xpath";
	//patternName
	public static final String textPatternName="//input[@name='short_desc']";
	public static final String textPatternName_loc="xpath";
	//servicetype
	public static final String drpServiceType="//select[@name='service_type']";
	public static final String drpServiceType_loc="xpath";
	//numberof slots
	public static final String textNumSlots="num_slots";
	public static final String textNumSlots_loc="name";
	//create go
	public static final String btnCreateGo="//a[contains(text(),'go')]";
	public static final String btnCreateGo_loc="xpath";
	//Starttiem
	public static final String drpStartTime="earliest_start";
	public static final String drpStartTime_loc="name";
	
	 //Endtime
		public static final String drpEndTime="latest_end";
		public static final String drpEndTime_loc="name";
		
	//Starttiem_New slot page
		public static final String drpStartTimeNSP="start_time";
		public static final String drpStartTimeNSP_loc="name";
    //Endtime New slot page
	public static final String drpEndTimeNSP="end_time";
	public static final String drpEndTimeNSP_loc="name";
	//description
	public static final String textDescription="long_desc";
	public static final String textDescription_loc="name";
	//Editable description
	public static final String textEditableDescription="//textarea[@class='smallText' and @name='long_desc']";
	public static final String textEditableDescription_loc="xpath";
	//Save CHANGES DESCRIPTION
	public static final String textSaveDescription="//td[@class='smallText']/div";
	public static final String textSaveDescription_loc="xpath";
	
	//cancel button
	public static final String btnCancel="//a[contains(text(),'cancel')]";
	public static final String btnCancel_loc="xpath";
	//Save change button
	public static final String btnSaveChange="//a[contains(text(),'save changes')]";
	public static final String btnSaveChange_loc="xpath";
	//Edit save change button
	public static final String btnEditSaveChange="//a[@title='Click here to save your changes']";
	public static final String btnEditSaveChange_loc="xpath";
	//Number of slots
	public static final String countNumSlots="//tr[@class='normal']";
	public static final String countNumSlots_loc="xpath";
	//service counter
	public static final String countServiceCounter="//tr[@class='normal']/td[5]/select/option[@value='y']";//tr[@class='normal']/td[5]";
	public static final String countServiceCounter_loc="xpath";
	//edit slot pattern details
	public static final String textEditSlot="//td[@class='pageHeading' and contains(text(),'Edit Slot Pattern Details')]";
	public static final String textEditSlot_loc="xpath";
	//back to slot pattern
	public static final String btnbackToSlotpattern="//a[@title='Click here to return to the slot pattern search']";
	public static final String btnbackToSlotpattern_loc="xpath";
    //Edit slot pattern
	public static final String btnEditslotPattern="//a[@title='Click here to edit the details of this slot pattern']";
	public static final String btnEditslotPattern_loc="xpath"; 
	//delete pattern
	public static final String btnDeletePattern="//a[@title='Click here to delete this slot pattern.']";
	public static final String btnDeletePattern_loc="xpath";
	//getpattern name
	public static final String textgetPatternName="//tr/td[contains(text(),'pattern name:')]/../td[2]";
	public static final String textgetPatternName_loc="xpath";
	//editablegetpattern
	public static final String textEditPatternName="//tr/td[contains(text(),'pattern name:')]/../td[2]/input";
	public static final String textEditPatternName_loc="xpath";
	
	//get service type
	public static final String textGetServiceType="//tr/td[contains(text(),'service type:')]/../td[2]";
	public static final String textGetServiceType_loc="xpath";
	//get start and end time
	public static final String textStartNEndtime="//tr/td[contains(text(),'start/end time:')]/../td[2]";
	public static final String textStartNEndtime_loc="xpath";
	//det description
	public static final String textDescripton="//tr/td[contains(text(),'description:')]/../td[2]";
	public static final String textDescripton_loc="xpath";
	
	//get slot pattern
	public static final String textgetSlotPattern="//tr/td[contains(text(),'slot pattern:')]/../td[2]";
	public static final String textgetSlotPattern_loc="xpath";
	
	//dynamic slot pattern
	public static String textDynamicPatterNumber="";
	//pattern text field
	public static final String textPatternField="template_rn";
	public static final String textPatternField_loc="name";
	//SECOND GO
	public static final String btnSECONDGO="(//a[contains(text(),'go')])[2]";
	public static final String btnSECONDGO_LOC="xpath";
	
	
	//pattern vary number
	public static String patternNumber="";
	public static int patterInterger;
	public static String serviceaddress="";
	public static String slotVa="";
	public static int slot;
	public static String startTime="";
	public static String endTime="";
	
	
	
	

}