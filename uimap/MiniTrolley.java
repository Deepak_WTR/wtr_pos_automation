package uimap;

public class MiniTrolley {
	//MiniTrolley Item(Nanditha-Mini-Trolley )
	public static final String imgMiniTrolleyItem = "//article[ItemIndex]/div/a/img";
	public static final String imgMiniTrolleyItem_Loc = "xpath";

	//MiniTrolley Flyout(Nanditha-Mini-Trolley)
	public static final String linkMiniTrolleyFlyout = "//div[@class='flyout content-wrapper has-image trolley-flyout']";
	public static final String linkMiniTrolleyFlyout_Loc = "xpath";

	//Unfilled Heart Icon(Nanditha-Mini-Trolley Flyout)
	public static final String linkHeartIconUnfilled = "favourite not-favourite";
	public static final String linkHeartIconUnfilled_Loc="className";

	//Filled Heart Icon(Nanditha-Mini-Trolley Flyout)
	public static final String linkHeartIconFilled= "favourite is-favourite";
	public static final String linkHeartIconFilled_Loc = "className";

	//MiniTrolley Item Name(Nanditha-Mini-Trolley Flyout)
	public static final String linkTrolleyItemName="//div[@class='flyout-inner-wrap ']/div[@class='title']/div[@class='l-content']";
	public static final String linkTrolleyItemName_Loc="xpath";

	//Mini Trolley Product(Nanditha-Mini-Trolley )
	public static String MiniTrolleyProduct = "";
	//all trolley items
	public static final String minitrolleyframe="trolley-items"; 
	public static final String minitrolleyframe_loc="className";
	//all images in mini trolley
	public static final String imgtrolley="//article[ItemIndex]/div/a/img";
	//" + " button
	public static final String btnincrease="//article[ItemIndex]/div[4]/div[2]/p/a";
	public static final String btnincrease_loc="xpath";
	//"-" button
	public static final String btndeccrease_loc="xpath";
	public static final String btndeccrease="//article[ItemIndex]/div[4]/div[4]/p/a";
	// close button
	public static final String btnClose="//article[ItemIndex]/div[4]/div[1]/p/a";
	public static final String btnClose_loc="xpath";
	//input Quantity
	public static final String txtinpuqty="//article[ItemIndex]/div[4]/div[3]/p";
	public static final String txtinpuqty_loc="xpath";

	//All product images
	public static final String imgAllprdcts="//div[@class='trolley-items']/article/div[1]/a/img";
	public static final String imgAllprdcts_loc="xpath";


	//All product list updated(30/07/13)
	public static final String txtProctsList="//div[@class='trolley-items']/article[contains(text(),'')]/div[4]/div[3]";
	public static final String txtProctsList_loc="xpath";


	//Empty trolley text 
//	public static final String txtEmptyTrolley="//h2[contains(text(),'Your trolley is empty')]";
	public static final String txtEmptyTrolley="//h2[@class='keyline-bottom']";
	public static final String txtEmptyTrolley_loc="xpath";

	//Mini Trolley Qty (Nanditha - Created on 13-Aug-13)
	public static int MiniTrolleyQty = 0;



	//Offer not availed msg (Nanditha - Created on 21-Aug-13)
	public static final String txtOfferMsg = "//div[@class='banner']/div/span";
	public static final String txtOfferMsg_Loc = "xpath";

	//Spinner button in Mini Trolley flyout (Nanditha - Created on 21-Aug-13)
	public static final String  btnIncrement = "//a[contains(text(),'+')]";
	public static final String btnIncrement_Loc = "xpath";

	//Spinner button in Mini Trolley flyout (Nanditha - Created on 21-Aug-13)

	public static final String  btnDecrement = "//a[@class='minus'][contains(text(),'-')]";//a[contains(text(),'-')]";
	public static final String btnDecrement_Loc = "xpath";

	//Import products link when the trolley is empty (Gajapathy) updated on 22-01-14
	public static final String lnkImportProducts = "//div[@id='importproducts']/a/span";
	public static final String lnkImportProducts_Loc = "xpath";

	//decrease icon in minitrolly for first item(Kathir created on 070814)
	public static final String lnkdeccrease_loc="xpath";
	public static final String lnkdeccrease="(//a[@class='decrease'])[1]";

	//product in Mini Trolley(Jayandran on 28/11/2014)
	public static final String imgProduct_loc="xpath";
	public static final String imgProduct="(//article[contains(@class,'trolley-item has-prod-img')])";


	//First product image
	public static final String imgFirstPrdct="//article[1]/div/a/img";
	public static final String imgFirstPrdct_loc="xpath";

	//First product close button
	public static final String imgCloseFirstPrdct="//article[1]/div[4]/div[1]/p/a";
	public static final String imgCloseFirstPrdct_loc="xpath";
	//First product  increment
	public static final String btnPlus="//article[1]/div[4]/div[2]/p/a";
	public static final String btnPlus_loc="xpath";

	//Qty update in minitrolley fly out
	public static final String txtQty="//div[@class='quantity-append']/input[@class='quantity-input']";
	public static final String txtQty_loc="xpath";

	public static double productPrice = -1;
	public static final String productPrice1_Loc = "xpath"; 
	public static final String productPrice1 = "//p[@class='price trolley-price']";


	//unit of measure in minitrolley fly out - Prabhakaran Sankar (30-March-2015)
	public static final String unitOfMeasure="//select[@class='quantity-select']";
	public static final String unitOfMeasure_loc="xpath";

	//product in Mini Trolley(Jayandran on 28/11/2014)
	public static final String trolleyProduct_loc="xpath";
	public static final String trolleyProduct="(//div[contains(@class,'m-product grid')])";


}
