package uimap;



public class OfBranchDeliverydate {

	//check box Change date - Prabhakaran Sankar - 13-march-15
	public final static String checkChangeDate="//input[@id='TopOfPageCheck']";
	public final static String checkChangeDate_Loc="xpath";
	
	
	//Button Save changes - Prabhakaran Sankar - 13-march-15
		public final static String btnSaveChanges="//a[@title='Click here to save your changes.']";
		public final static String btnSaveChanges_Loc="xpath";
		
	//enter date - Prabhakaran Sankar - 13-march-15
		public final static String newDeliveryDate="//input[@name='date_into_branch']";
		public final static String newDeliveryDate_Loc="xpath";	
		
	//enter date - Prabhakaran Sankar - 13-march-15
	public final static String btnBackToOrderSummary="//a[@title='Click here to go back to the order summary.']";
	public final static String btnBackToOrderSummary_Loc="xpath";
	
	//verify aditional Column - Prabhakaran Sankar - 13-march-15
	public final static String additionalColumn="(//tr[@class='grey'])[2]";
	public final static String additionalColumn_Loc="xpath";
	
	//button ShowConsignmentDetails button - Prabhakaran Sankar - 13-march-15
		public final static String btnShowConsignmentDetails="//td[@class='buttonCenter']//a[contains(text(),'show consignment details')]";
		public final static String btnShowConsignmentDetails_Loc="xpath";
	
}
