package uimap;

public class RecipesPage 
{
	
	public static final String lnkFacebook="//a[contains(text(),'Facebook')]";
	public static final String lnkFacebook_loc="xpath";
	
	public static final String lnkTwitter="//a[contains(text(),'Twitter')]";
	public static final String lnkTwitter_loc="xpath";
	
	public static final String lnkPinterest="//a[contains(text(),'Pinterest')]";
	public static final String lnkPinterest_loc="xpath";
	
	public static final String lnkInstagram="//a[contains(text(),'Instagram')]";
	public static final String lnkInstagram_loc="xpath";
	
	public static final String lnkstepbystep="Step by step";
	public static final String lnkstepbystep_loc="linkText";
	
	public static final String txtstepbystep_Hdng="//h1[contains(text(),'Step by step')]";//heading 
	public static final String txtstepbystep_Hdng_loc="xpath";
	
	public static final String lnkmakeRecipe="How to prepare an avocado";
	public static final String lnkmakeRecipe_loc="linkText";
	
	public static final String txtmakeRecipeHgng="How to prepare an avocado";
	public static final String txtmakeRecipeHgng_loc="//h1[contains(text(),'How to prepare an avocado')]";
	
	public static final String tblRating="//div[@class='ratingsystem']";
	public static final String tblRating_loc="xpath";
	
	public static final String txtRatign="//ul[@class='rate']/li/a";
	public static final String txtRating_loc="xpath";
	
	public static final String lnkRecipeweek="Recipes of the week";
	public static final String lnkRecipeweek_loc="linkText";

	public static final String linkWriteNote_loc="xpath";
	public static final String linkWriteNote="//span[contains(text(),'Write note')]";

	public static final String btnSaveNote_loc="xpath";
	public static final String btnSaveNote="//div[@class='tool-option-content overlay-popup active-toolbox-element']//input[@value='Save']";
	
	public static final String txtNoteSaved_loc="xpath";
	public static final String txtNoteSaved="//div[@class='lightbox-container content-wrapper']/p";
	
	public static final String linkCloseSavedNoteLB_loc="xpath";
	public static final String linkCloseSavedNoteLB="//div[@class='lightbox-container content-wrapper']/a";
	

	public static final String linkEditNote_loc="xpath";
	public static final String linkEditNote="//span[@class='addeditnotelabel']";
	
	public static final String btnPubProfOk_loc="xpath";
	public static final String btnPubProfOk="//a[@class=' mypublicProfileOK']";

	public static final String txtPublicProfile_loc="xpath";
	public static final String txtPublicProfile="//h1[contains(text(),'My Public Profile')]";
	

	
	
	public static final String btnSavePublicPro_loc="xpath";
	public static final String btnSavePublicPro="//*[@value='Save public profile']";
	

	
	


	public static final String txtEnterNote_loc="xpath";
	public static final String txtEnterNote="//div[@class='tool-option-content overlay-popup active-toolbox-element']//textarea";

	public static final String txtROTW_loc="xpath";
	public static final String txtROTW="//div[@class='r-content']//a[contains(text(),'Recipes of the week')]";

	public static final String linkViewROFW_loc="xpath";
	public static final String linkViewROFW="//div[@class='l-content']//a[contains(text(),'Recipes of the week')]";
	
	public static final String lnkratinglink="Courgettes with crunchy cheese topping";
	public static final String lnkratinglink_loc="linkText";

	public static final String linkViewRecipie_loc="xpath";
	public static final String linkViewRecipie="(//div[@class='r-content']//div[@class='parsys_column cq-colctrl-lt0']//a)[7]";

	public static final String txtRecipieName_loc="xpath";
	public static final String txtRecipieName="//h1[contains(text(),'*')]";
	
	public static final String txtRecipeweekHdng="//h1[contains(text(),'Recipes of the week')]";
	public static final String txtRecipeweekHdng_loc="xpath";
	
	public static final String txtThankMsg="//*[@id='ratingsThanks']/p";
	
	public static final String lnkSavetoyourscrapbook="recipe-scrapbook-savelink";
	public static final String lnkSavetoyourscrapbook_loc="id";
	
	public static final String txtScreenName="displayName";//Screen name text Field
	public static final String txtScreenName_loc="id";
	
	public static final String btnSaveAndContinue="saveAndContinueButton";//Save and continue
	public static final String btnSaveAndContinue_loc="id";
	//Waiterose tv
	public static final String lnkWaitroseTV="//a[contains(text(),'Waitrose TV Live - recipes')]";
	public static final String lnkWaitroseTV_loc="xpath";
    //waitrose tv heading
	public static final String txtWaitroseTVHdng="//h1[contains(text(),'Waitrose TV Live - recipes')]";
	public static final String txtWaitroseTVHdng_loc="xpath";
	//view recipes
	public static final String lnkViewRecipe="//div[@id='content']/nav/ul/li[10]/a";
	public static final String lnkViewRecipe_loc="xpath";
	
	public static final String lnkFirstListName="//tbody/tr[1]/td[1]/a";
	public static final String lnkFirstListName_loc="xpath";
	
	public static final String lnkRating="//ul[@class='rate']/li/a[@data-rate='+i+']";
	public static final String lnkRating_loc="xpath";
	
	//Write Note
		public static final String lnkWriteNote="Write note";
		public static final String lnkWriteNote_loc="linkText";
		//Text area in pop up
		public static final String txtMsg="(//textarea[@id='add-recipe-note'])[2]";
		public static final String txtMsg_loc="xpath";//textarea[@id='add-recipe-note'][2]
		//SaveBUTTON
		public static final String btnSave="(//input[@value='Save'])[2]";
		public static final String btnSave_loc="xpath";
		
		//Add comment Button
		public static final String btnAddComment="//a[contains(text(),'Add comment')]";
		public static final String btnAddComment_loc="xpath";
		//Txt area for Add Comment
		public static final String txtCommentMsg="(//*[@id='add-comment-text'])[2]";
		public static final String txtCommentMsg_loc="xpath";
		//Submit Button
		public static final String btnSubmit="(//*[@value='Submit'])[4]";
		public static final String btnSubmit_loc="xpath";
		//SignIn button
		public static final String btnSingIn="button-sign-in";
		public static final String btnSingIn_loc="id";
		//Password
		public static final String txtPassword="password";
		public static final String txtPassword_loc="id";

		//Recipe link in home page
				public static final String linkRecipe="Recipes";
				public static final String linkRecipe_loc="linkText";

		
				
				//Recipe name
				public static final String txtRecipeName="//div[@class='m-col-wide']/h1";
				public static final String txtRecipeName_loc="xpath";
		//ADDNIOTE
				public static final String txtAddNote="//div[@class='centerrecipenote']/div";
				public static final String txtAddNote_loc="xpath";
		

		

		//addcomment
				public static final String txtAddCmnt="//*[@id='comments']/ul/div[1]/li/p[2]";
				public static final String txtAddCmnt_loc="xpath";


				
								

								//Header of recipe

								public static final String txtHdrRecipe="//div[@class='title']/h1";//div[@class='textContent']/h2/a";

								public static final String txtHdrRecipe_loc="xpath";

								//Recipe tabs
								public static final String tabRecipes="(//div[@class='recipie-title']/h3)[1]";//div[4]/div[1]/div/div/h2";
								public static final String tabRecipes_loc="xpath";

								//Close Button
								public static final String btnClose="(//a[contains(text(),'Close')])[8]";//(//a[contains(text(),'Close')])[7]";
								public static final String btnClose_loc="xpath";

}
