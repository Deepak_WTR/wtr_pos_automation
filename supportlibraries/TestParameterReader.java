package supportlibraries;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TestParameterReader {


	public Map<String, String> getCSV(String path) {

		String csvFile = path;
		BufferedReader br = null;
		String line = "";
		Map <String, String> validationTags = new HashMap<String, String>();
		
		try {

			br = new BufferedReader(new FileReader(csvFile));
			
			while ((line = br.readLine()) != null) {

				String[] column = line.split(",");
				System.out.println("Validation tag csv file : " + column[0] +","+column[1]);
				validationTags.put(column[0], column[1]);
			}

		} catch (FileNotFoundException e) {
			System.out.println("Validation tag csv file not found at: " + path);
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return validationTags;
	}

}

