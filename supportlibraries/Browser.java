package supportlibraries;

/**
 * Enumeration to represent the browser to be used for execution
 * @author Cognizant
 */
public enum Browser
{
	Android("android"),
	Chrome("chrome"),
	Firefox("firefox"),
	Firebug("firefox"),
	HtmlUnit("htmlunit"),
	InternetExplorer("iexplore"),
	iPhone("iphone"),
	iPad("ipad"),
	Opera("opera"),
	Safari("safari"), 
	Selendroid("selendroid"),
	MobileChrome("mobilechrome"),
	app("app");
	
	private String value;
	
	Browser(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return value;
	}
	
	/*@Override
	public String toString()
	{
		return value;
	}*/
}