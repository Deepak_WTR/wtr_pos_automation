package supportlibraries;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileGrabber {
	
	public String getHdrFileText(){
		File file = null;
		File[] paths;
		String fileContent = "";

		try{      
			// create new file
			Thread.sleep(2000);
			file = new File(new File(System.getProperty("user.dir")).getAbsolutePath()+"\\Miscellaneous\\Omniture\\harFiles");

			// returns pathnames for files and directory
			paths = file.listFiles();
			// for each pathname in pathname array
			for(File path:paths)
			{
				byte[] encoded = Files.readAllBytes(Paths.get(path.getAbsolutePath()));
				System.out.println("path:"+path.getAbsolutePath());
				fileContent = StandardCharsets.UTF_8.decode(ByteBuffer.wrap(encoded)).toString();
				System.out.println("Content in hrd file:"+fileContent);
				path.delete();
				break;
			}
		}catch(Exception e){
			// if any error occurs
			System.out.println("Error getting hdr file.");
			return null;
		}
		return fileContent;
	}

	public boolean deleteHdrFiles(){
		try{
			File file = null;
			File[] paths;
			file = new File(new File(System.getProperty("user.dir")).getAbsolutePath()+"\\Miscellaneous\\Omniture\\harFiles");

			// returns pathnames for files and directory
			paths = file.listFiles();
			// for each pathname in pathname array
			for(File path:paths)
			{
				if(path.delete() == false){
					return false;
				}
			}
		}
		catch(Exception ex){
			System.out.println("could not delete hdr file");
			return false;
		}
		return true;

	}

}
