package supportlibraries;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;



public class OmnitureTagExtracter {

	private final static String OMNITURE_URL = "http://site.waitrose.com/b/ss";
	//Gson gson = new Gson();
	private Container container;
	
	public OmnitureTagExtracter(String json){
		container = new Gson().fromJson(json, Container.class);
	}
	//TODO passing expected tags is only for testing, should actually get all tags as any extra tags should be caught and fail the test
	public Map<String, String> getOmnitureTagValue(Map<String, String> expectedTags){
		
		Map<String, String> actualTags = new HashMap<String, String>();
		List<Entry> entries =  container.getLog().getEntries();
		for (Entry entry : entries){
			if(entry.getRequest().url.startsWith(OMNITURE_URL)){
				Iterator<String> it = expectedTags.keySet().iterator();
				while (it.hasNext()){
					String tagName = it.next().toString();
					System.out.println("expected tag name:"+tagName);
					for (QueryString query : entry.getRequest().getQueryString()){
						if (tagName.equals(query.getName())){
							System.out.println("actual tag name:"+query.getName());
							actualTags.put(tagName, query.value);
							System.out.println("exp query value:"+tagName);
							System.out.println("actual query valur:"+query.value);
						}
					}
				}
				break;
			}
		}	
		return actualTags;
	}
	
	//Gson representation classes
	public class Container {
	    private Log log;
	    
	    public Log getLog(){
	    	return log;
	    }

	}
	
	public class Log{
		private List<Entry> entries;
		
	    public List<Entry> getEntries(){
	    	return entries;
	    }
	}

	public class Entry {
		private Request request;
		
		public Request getRequest(){
			return request;			
		}
	}
	
	public class Request{
	    private String url;
	    private List<QueryString> queryString;
	    
		public String getUrl(){
			return url;			
		}
		public List<QueryString> getQueryString(){
			return queryString;			
		}
	}
	
	public class QueryString{
		private String name;
		private String value;

		public String getName(){
			return name;
		}
		
		public String getValue(){
			return value;
		}
	}
}


