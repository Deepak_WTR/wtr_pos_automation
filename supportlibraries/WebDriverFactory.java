package supportlibraries;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
//import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
//import com.gargoylesoftware.htmlunit.WebClient;
import org.openqa.selenium.ie.InternetExplorerDriver;
//import com.opera.core.systems.OperaDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.remote.*;

import com.cognizant.framework.FrameworkException;
import com.cognizant.framework.Settings;


/**
 * Factory class for creating the {@link WebDriver} object as required
 * @author Cognizant
 */
public class WebDriverFactory
{
	private static Properties properties;
	//public static SelendroidLauncher selendroidServer = null;
	private WebDriverFactory()
	{
		// To prevent external instantiation of this class
	}


	/**
	 * Function to return the appropriate {@link WebDriver} object based on the parameters passed
	 * @param browser The {@link Browser} to be used for the test execution
	 * @return The corresponding {@link WebDriver} object
	 */
	/*public static FirefoxDriver createFirefoxDriver() 
	{
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities = setProxyCapability(capabilities);
		capabilities.setCapability("binary", "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");
		ProfilesIni profile = new ProfilesIni();
		FirefoxProfile ffprofile = profile.getProfile("proxyfreeuser");

	//	return new FirefoxDriver(ffprofile);
	}
*/


	public static DesiredCapabilities setProxyCapability(DesiredCapabilities cap) 
	{

		String user = System.getProperty("proxy.user", "tpumukum");
		String password = System.getProperty("proxy.password", "Suresh1");
		String proxyHost = System.getProperty("proxy.host", "jlp-deskinet");
		String proxyPort = System.getProperty("proxy.port", "80");


		if (user != null && password != null && proxyHost != null && proxyPort != null) 
		{
			Proxy proxy = new Proxy();
			proxy.setHttpProxy("https://" + user + ":" + password + "@" + proxyHost + ":" + proxyPort);
			//proxy.setHttpProxy("http://devdelen:monday876@192.168.211.17:80");

			//		   	cap.setCapability("proxy", proxy);
		}

		return cap;
	}
	public static WebDriver getDriver(Browser browser) throws MalformedURLException
	{	
		WebDriver driver = null;
		properties = Settings.getInstance();
		boolean proxyRequired =
				Boolean.parseBoolean(properties.getProperty("ProxyRequired"));

		switch(browser) {
		case Chrome:
			// Takes the system proxy settings automatically

			System.setProperty("webdriver.chrome.driver",
					new File(System.getProperty("user.dir")).getAbsolutePath()+"\\Miscellaneous\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
			break;

		case Firebug:
			// Takes the system proxy settings automatically
			String pfstatus="Yes";

			/////////////////////////////////rabbani
			if(pfstatus.equalsIgnoreCase("Yes")){
				FirefoxProfile profile = new FirefoxProfile();

				File firebug = new File(new File(System.getProperty("user.dir")).getAbsolutePath()+"\\Miscellaneous\\Omniture\\Extensions\\firebug-1.11.4.xpi");
				File netExport = new File(new File(System.getProperty("user.dir")).getAbsolutePath()+"\\Miscellaneous\\Omniture\\Extensions\\netExport-0.9b3.xpi");

				try {
					profile.addExtension(firebug);
					profile.addExtension(netExport);
				} catch (Exception e) {
					System.out.println("Firebug or netExport is missing from the Extensions folder.");
				}

				// Set default Firefox preferences
				profile.setPreference("app.update.enabled", false);
				String domain = "extensions.firebug.";

				// Set default Firebug preferences
				profile.setPreference(domain + "currentVersion", "1.11.4");
				profile.setPreference(domain + "allPagesActivation", "on");
				profile.setPreference(domain + "defaultPanelName", "net");
				profile.setPreference(domain + "net.enableSites", true);


				// Set default NetExport preferences
				profile.setPreference(domain + "netexport.alwaysEnableAutoExport", true);
				profile.setPreference(domain + "netexport.showPreview", false);
				profile.setPreference(domain + "netexport.defaultLogDir", new File(System.getProperty("user.dir")).getAbsolutePath()+"\\Miscellaneous\\Omniture\\harFiles\\");
				//profile.setPreference("extensions.firebug.netexport.Automation", true); 
				//profile.setPreference("extensions.firebug.netexport.pageLoadedTimeout", 3000); 

		//		driver = new FirefoxDriver(profile);
				//driver = new FirefoxDriver(ffprofile);


			}
			break;
			/////////////////////////////

		case MobileChrome:

			//Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe");
			DesiredCapabilities capabilities = new DesiredCapabilities();			
			capabilities.setCapability(CapabilityType.BROWSER_NAME, "Chrome");
			capabilities.setCapability("platformName", "Android");
			capabilities.setCapability("deviceName", "Android");
			capabilities.setCapability("udid", "04aeb6e70024ad8b");
			capabilities.setCapability("appium-version", "1.4.16.1");
			capabilities.setCapability("newCommandTimeout", "180");
			driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);	
			break; 
			
		case app:
			DesiredCapabilities capabilities1 = new DesiredCapabilities(); 
			capabilities1.setCapability("deviceName", "04aeb6e70024ad8b");
			capabilities1.setCapability(CapabilityType.BROWSER_NAME, "Android");
			capabilities1.setCapability(CapabilityType.VERSION, "5.0.1");
			capabilities1.setCapability("platformName", "Android");
			capabilities1.setCapability("appPackage", "com.waitrose.WaitroseQuickCheck");
			capabilities1.setCapability("appActivity", "com.mdcinternational.myiscan.ControllerActivity");
			driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities1);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			break; 

		case Firefox:
			//driver = new FirefoxDriver();	
	//		driver= createFirefoxDriver();

			break;

			//case HtmlUnit:03157df3b406bd20
			//		// Does not take the system proxy settings automatically!
			//		// NTLM authentication for proxy supported
			//		
			//		if (proxyRequired) {
			//			driver = new HtmlUnitDriver() {
			//			@Override
			//			protected WebClient modifyWebClient(WebClient client) {
			//				DefaultCredentialsProvider credentialsProvider = new DefaultCredentialsProvider();
			//				credentialsProvider.addNTLMCredentials(properties.getProperty("Username"),
			//														properties.getProperty("Password"),
			//														properties.getProperty("ProxyHost"),
			//														Integer.parseInt(properties.getProperty("ProxyPort")),
			//														"", properties.getProperty("Domain"));
			//				client.setCredentialsProvider(credentialsProvider);
			//				return client;
			//				}
			//			};
			//			
			//			((HtmlUnitDriver) driver).setProxy(properties.getProperty("ProxyHost"),
			//										Integer.parseInt(properties.getProperty("ProxyPort")));
			//		} else {
			//			driver = new HtmlUnitDriver();
			//		}
			//		
			//		break;

		case InternetExplorer:
			// Takes the system proxy settings automatically

			System.setProperty("webdriver.ie.driver",
					new File(System.getProperty("user.dir")).getAbsolutePath()+"\\Miscellaneous\\drivers\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			break;



		case Safari:
			// Takes the system proxy settings automatically

			driver = new SafariDriver();
			break;

			//by rabbani
			/*case Android:
			try{
				Process proc2 = Runtime.getRuntime().exec(new String[]{properties.getProperty("AndroidPath")+"/adb", "-s", properties.getProperty("AndroidMachineID"), "shell", "am", "start", "-a", "android.intent.action.MAIN", "-n", "org.openqa.selenium.android.app/.MainActivity"});
				Thread.sleep(3000);
				Process proc3 = Runtime.getRuntime().exec(new String[]{properties.getProperty("AndroidPath")+"/adb", "-s", properties.getProperty("AndroidMachineID"), "forward", "tcp:8080", "tcp:8080"});
				Thread.sleep(3000);
				DesiredCapabilities capability = DesiredCapabilities.android();
				 capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				driver = new AndroidDriver();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
		case Selendroid:			
			SelendroidConfiguration config = new SelendroidConfiguration();

		    selendroidServer = new SelendroidLauncher(config);
		    selendroidServer.launchSelendroid();

		    DesiredCapabilities caps = SelendroidCapabilities.android();

		    try {
				driver = new SelendroidDriver(caps);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/

		case iPad:
			try{

				driver = new RemoteWebDriver(new URL(properties.getProperty("iDriverPath")), DesiredCapabilities.ipad());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 

		case iPhone:
			try{

				driver = new RemoteWebDriver(new URL(properties.getProperty("iDriverPath")), DesiredCapabilities.iphone());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			///

		default:
			//throw new FrameworkException("Unhandled browser!");
		}

		return driver;
	}

	private static DesiredCapabilities getProxyCapabilities()
	{
		Proxy proxy = new Proxy();
		proxy.setProxyType(ProxyType.MANUAL);

		properties = Settings.getInstance();
		String proxyUrl = properties.getProperty("ProxyHost") + ":" +
				properties.getProperty("ProxyPort");

		proxy.setHttpProxy(proxyUrl);
		proxy.setFtpProxy(proxyUrl);
		proxy.setSslProxy(proxyUrl);
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setCapability(CapabilityType.PROXY, proxy);

		return desiredCapabilities;
	}

	/**
	 * Function to return the appropriate {@link WebDriver} object based on the parameters passed
	 * @param browser The {@link Browser} to be used for the test execution
	 * @param remoteUrl The URL of the remote machine to be used for the test execution
	 * @return The corresponding {@link WebDriver} object
	 */
	public static WebDriver getDriver(Browser browser, String remoteUrl)
	{
		return getDriver(browser, null, null, remoteUrl);
	}

	/**
	 * Function to return the appropriate {@link WebDriver} object based on the parameters passed
	 * @param browser The {@link Browser} to be used for the test execution
	 * @param browserVersion The browser version to be used for the test execution
	 * @param platform The {@link Platform} to be used for the test execution
	 * @param remoteUrl The URL of the remote machine to be used for the test execution
	 * @return The corresponding {@link WebDriver} object
	 */
	public static WebDriver getDriver(Browser browser, String browserVersion,
			Platform platform, String remoteUrl)
	{
		// For running RemoteWebDriver tests in Chrome and IE:
		// The ChromeDriver and IEDriver executables needs to be in the PATH of the remote machine
		// To set the executable path manually, use:
		// java -Dwebdriver.chrome.driver=/path/to/driver -jar selenium-server-standalone.jar
		// java -Dwebdriver.ie.driver=/path/to/driver -jar selenium-server-standalone.jar

		properties = Settings.getInstance();
		boolean proxyRequired =
				Boolean.parseBoolean(properties.getProperty("ProxyRequired"));

		DesiredCapabilities desiredCapabilities = null;
		if ((browser.equals(Browser.Android) ||browser.equals(Browser.HtmlUnit) || browser.equals(Browser.Opera))
				&& proxyRequired) {
			desiredCapabilities = getProxyCapabilities();
		} else {
			desiredCapabilities = new DesiredCapabilities();
		}

		desiredCapabilities.setBrowserName(browser.getValue());


		if (browserVersion != null) {
			desiredCapabilities.setVersion(browserVersion);
		}
		if (platform != null) {
			desiredCapabilities.setPlatform(platform);
		}

		desiredCapabilities.setJavascriptEnabled(true);	// Pre-requisite for remote execution

		URL url;
		try {
			url = new URL(remoteUrl);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new FrameworkException("The specified remote URL is malformed");
		}

		return new RemoteWebDriver(url, desiredCapabilities);
	}
}