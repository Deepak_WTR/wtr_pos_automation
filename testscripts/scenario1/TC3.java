package testscripts.scenario1;

import java.net.MalformedURLException;

import org.testng.annotations.Test;

import supportlibraries.DriverScript;
import supportlibraries.TestCase;


/**
 * Test for register new user and login using the registered user
 * @author Cognizant
 */
public class TC3 extends TestCase
{
	@Test
	public void runTC3() throws MalformedURLException
	{
		testParameters.setCurrentTestDescription("Test for register new user and login using the registered user");
		
		driverScript = new DriverScript(testParameters);
		driverScript.driveTestExecution();
	}
}