package allocator;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.openqa.selenium.Platform;

import supportlibraries.Browser;
import supportlibraries.ResultSummaryManager;
import supportlibraries.SeleniumTestParameters;

import com.cognizant.framework.DynamicReportGenerator;
import com.cognizant.framework.ExcelDataAccess;
import com.cognizant.framework.FrameworkException;
import com.cognizant.framework.IterationOptions;


/**
 * Class to manage the batch execution of test scripts within the framework
 * @author Cognizant
 */
public class Allocator extends ResultSummaryManager
{
	
	private List<SeleniumTestParameters> testInstancesToRun;
	
	public static String currentbrowser ="";
	public static String currentplatform ="";
	public static String GlblVar ="";
	public static String[] savedAddress;
	public static String flagJoinMyWLogin = "";

	public static String[] OFItem = null;
	public static String[] MyOrderItem = null;
	public static String[] Item = null;
	
	public static void main(String[] args)
	{
		Allocator allocator = new Allocator();
		allocator.driveBatchExecution();
	}
	
	private void driveBatchExecution()
	{	
		//--------FOR DYNAMIC REPORT: TO SHARE THE REPORT IN THE BELOW PATH 
		//DynamicReportGenerator.shareReport("C:/WCSReports");
		//-----------------------------------------------------------------
		
		
	  setRelativePath();
		initializeTestBatch();
		
		int nThreads = Integer.parseInt(properties.getProperty("NumberOfThreads"));
		initializeSummaryReport(properties.getProperty("RunConfiguration"), nThreads);
		
		try {
			setupErrorLog();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new FrameworkException("Error while setting up the Error log!");
		}
				executeTestBatch();
		wrapUp();
		launchResultSummary();
	}
	
	@Override
	protected void initializeTestBatch()
	{
		super.initializeTestBatch();
		testInstancesToRun = getRunInfo(properties.getProperty("RunConfiguration"));		
		
	}
	
	private List<SeleniumTestParameters> getRunInfo(String sheetName)
	{
		ExcelDataAccess runManagerAccess = new ExcelDataAccess(frameworkParameters.getRelativePath(), "Run Manager");			
		runManagerAccess.setDatasheetName(sheetName);
		
		int nTestInstances = runManagerAccess.getLastRowNum();
		List<SeleniumTestParameters> testInstancesToRun = new ArrayList<SeleniumTestParameters>();
		
		for (int currentTestInstance = 1; currentTestInstance <= nTestInstances; currentTestInstance++) {
			String executeFlag = runManagerAccess.getValue(currentTestInstance, "Execute");
			
			if (executeFlag.equalsIgnoreCase("Yes")) {
				String currentScenario = runManagerAccess.getValue(currentTestInstance, "TestScenario");
				String currentTestcase = runManagerAccess.getValue(currentTestInstance, "TestCase");
				SeleniumTestParameters testParameters =
						new SeleniumTestParameters(currentScenario, currentTestcase);
				
				testParameters.setCurrentTestDescription(runManagerAccess.getValue(currentTestInstance, "Description"));
				
				String iterationMode = runManagerAccess.getValue(currentTestInstance, "IterationMode");
				if (!iterationMode.equals("")) {
					testParameters.setIterationMode(IterationOptions.valueOf(iterationMode));
				} else {
					testParameters.setIterationMode(IterationOptions.RunAllIterations);
				}
				
				String startIteration = runManagerAccess.getValue(currentTestInstance, "StartIteration");
				if (!startIteration.equals("")) {
					testParameters.setStartIteration(Integer.parseInt(startIteration));
				}
				String endIteration = runManagerAccess.getValue(currentTestInstance, "EndIteration");
				if (!endIteration.equals("")) {
					testParameters.setEndIteration(Integer.parseInt(endIteration));
				}
				
				String browser = runManagerAccess.getValue(currentTestInstance, "Browser");
				currentbrowser = browser;
				if (!browser.equals("")) {
					testParameters.setBrowser(Browser.valueOf(browser));
				} else {
					testParameters.setBrowser(Browser.valueOf(properties.getProperty("DefaultBrowser")));
				}
				String browserVersion = runManagerAccess.getValue(currentTestInstance, "BrowserVersion");
				if (!browserVersion.equals("")) {
					testParameters.setBrowserVersion(browserVersion);
				}
				String platform = runManagerAccess.getValue(currentTestInstance, "Platform");
				currentplatform = platform;
				if (!platform.equals("")) {
					testParameters.setPlatform(Platform.valueOf(platform));
				} else {
					testParameters.setPlatform(Platform.valueOf(properties.getProperty("DefaultPlatform")));
				}
				
				testInstancesToRun.add(testParameters);
			}
		}
		
		return testInstancesToRun;
	}
	
	
	/**
	 * Reading Dynamic Report and Jira Upload Configuration
	 *
	 * @param sheetName
	 * @return
	 */
	
//	@SuppressWarnings("unused")
//	private HashMap<String,String> getDynamicReportSettings(String sheetName)
//	{
//		ExcelDataAccess runManagerAccess = new ExcelDataAccess(frameworkParameters.getRelativePath(), "Run Manager");			
//		runManagerAccess.setDatasheetName(sheetName);
//		HashMap<String,String> settings = new HashMap<String,String>();
//		int nTestInstances = runManagerAccess.getLastRowNum();
//		
//		for (int currentTestInstance = 1; currentTestInstance <= nTestInstances; currentTestInstance++) {
//			String executeFlag = runManagerAccess.getValue(currentTestInstance, "Use");
//			if (executeFlag.equalsIgnoreCase("Yes")) {
//				if(runManagerAccess.getValue(currentTestInstance, "Make Shared Copy").equalsIgnoreCase("Yes")){
//					String SharedPath = runManagerAccess.getValue(currentTestInstance, "Shared Path");
//					settings.put("Shared Path", SharedPath);
//				}else{
//					settings.put("Shared Path", "nil");
//				}
//				
//				if(runManagerAccess.getValue(currentTestInstance, "Upload To Jira").equalsIgnoreCase("Yes")){
//					String val = runManagerAccess.getValue(currentTestInstance, "Jira Root Path");
//					if (val != null && !val.isEmpty()){
//						settings.put("Jira Root Path", val);
//					}else{
//						settings.put("Jira Root Path", "nil");
//					}
//					
//					
//					val = runManagerAccess.getValue(currentTestInstance, "Common Root Path");
//					if (val != null && !val.isEmpty()){
//						settings.put("Common Root Path", val);
//					}else{
//						settings.put("Common Root Path", settings.get("Jira Root Path"));
//					}
//					
//					
//					val = runManagerAccess.getValue(currentTestInstance, "Jira UserName");
//					if (val != null && !val.isEmpty()){
//						settings.put("Jira UserName", val);
//					}else{
//						settings.put("Jira UserName", "nil");
//					}
//					
//					
//					val = runManagerAccess.getValue(currentTestInstance, "Jira Password");
//					if (val != null && !val.isEmpty()){
//						if(val.startsWith("%") && val.endsWith("%")){
//							settings.put("Jira Password", val);
//						}else{	
//							String temp = DynamicReportGenerator.encode(val);
//							runManagerAccess.setValue(currentTestInstance, "Jira Password", temp);
//							settings.put("Jira Password", temp);
//						}
//						
//					}else{
//						settings.put("Jira Password", "nil");
//					}
//					
//					
//					val = runManagerAccess.getValue(currentTestInstance, "Project Key");
//					if (val != null && !val.isEmpty()){
//						settings.put("Project Key", val);
//					}else{
//						settings.put("Project Key", "nil");
//					}
//					val = runManagerAccess.getValue(currentTestInstance, "Issue Type");
//					if (val != null && !val.isEmpty()){
//						settings.put("Issue Type", val);
//					}else{
//						settings.put("Issue Type", "nil");
//					}
//					
//					val = runManagerAccess.getValue(currentTestInstance, "Built");
//					if (val != null && !val.isEmpty()){
//						settings.put("Built", val);
//					}else{
//						settings.put("Built", "DEFAULT");
//					}
//					
//					val = runManagerAccess.getValue(currentTestInstance, "Environment");
//					if (val != null && !val.isEmpty()){
//						settings.put("Environment", val);
//					}else{
//						settings.put("Environment", "N/A");
//					}
//					
//					val = runManagerAccess.getValue(currentTestInstance, "Use Proxy");
//					if (val != null && !val.isEmpty()){
//						settings.put("Use Proxy", val);
//					}else{
//						settings.put("Use Proxy", "nil");
//					}
//					
//					
//					val = runManagerAccess.getValue(currentTestInstance, "Proxy Host");
//					if (val != null && !val.isEmpty()){
//						settings.put("Proxy Host", val);
//					}else{
//						settings.put("Proxy Host", "nil");
//					}
//					
//					
//					val = runManagerAccess.getValue(currentTestInstance, "Proxy Port");
//					if (val != null && !val.isEmpty()){
//						settings.put("Proxy Port", val);
//					}else{
//						settings.put("Proxy Port", "0");
//					}
//					
//										
//					val = runManagerAccess.getValue(currentTestInstance, "Proxy UserName");
//					if (val != null && !val.isEmpty()){
//						settings.put("Proxy UserName", val);
//					}else{
//						settings.put("Proxy UserName", "nil");
//					}			
//					
//					val = runManagerAccess.getValue(currentTestInstance, "Proxy Password");
//					if (val != null && !val.isEmpty()){
//						if(val.startsWith("%") && val.endsWith("%")){
//							settings.put("Proxy Password", val);
//						}else{	
//							String temp = DynamicReportGenerator.encode(val);
//							runManagerAccess.setValue(currentTestInstance, "Proxy Password", temp);
//							settings.put("Proxy Password", temp);
//						}
//						
//					}else{
//						settings.put("Proxy Password", "nil");
//					}					
//					
//
//				}
//				break;	
//			}
//
//		}
//
//		return settings;
//	}
	
	
	private void executeTestBatch()
	{
		int nThreads = Integer.parseInt(properties.getProperty("NumberOfThreads"));
		ExecutorService parallelExecutor = Executors.newFixedThreadPool(nThreads);
		for (int currentTestInstance = 0; currentTestInstance < testInstancesToRun.size() ; currentTestInstance++ ) {
			ParallelRunner testRunner =	new ParallelRunner(testInstancesToRun.get(currentTestInstance), summaryReport);
			parallelExecutor.execute(testRunner);
			if(frameworkParameters.getStopExecution()) {
				break;
			}
		}
		
		parallelExecutor.shutdown();
		while(!parallelExecutor.isTerminated()) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}