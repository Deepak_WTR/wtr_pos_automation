package businesscomponents;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uimap.Header;
import uimap.HomePage;
import uimap.M_EnterDetails;
import uimap.M_BookSlot;
import uimap.OfBranchDeliverydate;
import uimap.OfLogin;
import uimap.UserRegisteration;
import uimap.M_Login;
import allocator.Allocator;

import com.cognizant.framework.Status;
//import com.thoughtworks.selenium.SeleniumException;


public class Mobile_BookSlot extends ReusableLibrary {

	

	public Mobile_BookSlot(ScriptHelper scriptHelper) {
		super(scriptHelper);
		
		// TODO Auto-generated constructor stub
	}
	

	/********************************************************
	 *FUNCTION    :clickGoTomyWaitrose_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :28-12-2016
	 *DESCRIPTION :click the Go to my Waitrose button 
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void clickGoTomyWaitrose_mob() {
		if (ReusableComponents.isElementPresent(driver, M_BookSlot.btnmyW_loc, M_BookSlot.btnmyW)){
			ReusableComponents.clickButton(driver, M_BookSlot.btnmyW_loc, M_BookSlot.btnmyW);	
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    report.updateTestLog("Go to my waitrose", "Go to myWaitrose button is clicked", Status.PASS);}
		else
			  report.updateTestLog("Go to my waitrose", "Go to myWaitrose button is not clicked", Status.FAIL);
		
	}
	
	/********************************************************
	 *FUNCTION    :clickBookASlot_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :28-12-2016
	 *DESCRIPTION :click the Book a slot button
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void clickBookASlot_mob() throws InterruptedException {
		if (ReusableComponents.isElementPresent(driver, M_BookSlot.btnbookaslot_loc, M_BookSlot.btnbookaslot)){
		   ReusableComponents.clickButton(driver, M_BookSlot.btnbookaslot_loc, M_BookSlot.btnbookaslot);
		   driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		   Thread.sleep(5000);
		   report.updateTestLog("Book A Slot", "Book A Slot button is clicked", Status.PASS);}
		else
		   report.updateTestLog("Book A Slot", "Book A Slot button is not clicked", Status.FAIL);
	}
	
	/********************************************************
	 *FUNCTION    :clickBookASlot_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :28-12-2016
	 *DESCRIPTION :click the Book a slot button
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void clickBookASlotChoice_mob() throws InterruptedException {
		if (ReusableComponents.isElementPresent(driver, M_BookSlot.btnbookaslot_loc, M_BookSlot.btnbookaslot)){
		   ReusableComponents.clickButton(driver, M_BookSlot.btnbookaslot_loc, M_BookSlot.btnbookaslot);
		   driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		   Thread.sleep(5000);}
		else
			{
			if (ReusableComponents.isElementPresent(driver, M_BookSlot.btnbookaslotchoice_loc, M_BookSlot.btnbookaslotchoice)){
			   ReusableComponents.clickButton(driver, M_BookSlot.btnbookaslotchoice_loc, M_BookSlot.btnbookaslotchoice);
			   driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);}
			}
			   
		}
	
	/********************************************************
	 *FUNCTION    :clickBookDelivery_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :28-12-2016
	 *DESCRIPTION :choose delivery option
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void clickBookDelivery_mob() {
		if (ReusableComponents.isElementPresent(driver, M_BookSlot.btnbookdelivery_loc, M_BookSlot.btnbookdelivery)){
		    ReusableComponents.clickButton(driver, M_BookSlot.btnbookdelivery_loc, M_BookSlot.btnbookdelivery);
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    report.updateTestLog("Book Delivery", "Book Delivery is clicked", Status.PASS);}
		else
			   report.updateTestLog("Book Delivery", "Book Delivery is not clicked", Status.FAIL);
	}
	
	
	/********************************************************
	 *FUNCTION    :clickEditSlot_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :28-12-2016
	 *DESCRIPTION :choose delivery option
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void clickEditSlot_mob() {
		if (ReusableComponents.isElementPresent(driver, M_BookSlot.btnEditslot_loc, M_BookSlot.btnEditslot)){
		    ReusableComponents.clickButton(driver, M_BookSlot.btnEditslot_loc, M_BookSlot.btnEditslot);
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    report.updateTestLog("Edit Stot", "Edit Stot is clicked", Status.PASS);}
		else
			clickBookDelivery_mob();
			
	}
	/********************************************************
	 *FUNCTION    :selectDeliveryAddress_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :28-12-2016
	 *DESCRIPTION : selecting the delivery address
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void selectDeliveryAddress_mob() throws InterruptedException {
		String Daddress = dataTable.getData("General_Data","DeliveryAddress");
		System.out.println(Daddress);
		ReusableComponents.SelectValuefromDropDown(driver,M_BookSlot.dropdowndeliveryadd_loc,M_BookSlot.dropdowndeliveryadd,Daddress);
		report.updateTestLog("Select Delivery address", "Delivery address is selcted as : "+ Daddress, Status.PASS);
		Thread.sleep(5000);
		/*if(Daddress.equalsIgnoreCase("Add new address"))
		{
			
		}
		
		else
		{
			//div[@class='WRgridItem2b']/button[@class='mblButton btnPrimaryCTA unselectable']
		}
		
	}*/
}
	/********************************************************
	 *FUNCTION    :chooseYourDeliverySlot_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :28-12-2016
	 *DESCRIPTION :clicking the book slot option
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void chooseYourDeliverySlot_mob() {
		if (ReusableComponents.isElementPresent(driver, M_BookSlot.btndeliveryslot_loc, M_BookSlot.btndeliveryslot)){
		   ReusableComponents.clickButton(driver, M_BookSlot.btndeliveryslot_loc, M_BookSlot.btndeliveryslot);
		   driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		   report.updateTestLog("Book A Slot", "Book A Slot button is clicked", Status.PASS);}
		else
		   report.updateTestLog("Book A Slot", "Book A Slot button is not clicked", Status.FAIL);
	}
	
	/********************************************************
	 *FUNCTION    :clickday_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :28-12-2016
	 *DESCRIPTION :Book a slot - Any Day Slot Booking
	 *PAGE        :Select Slot Page.
	 * @throws InterruptedException 
	 ********************************************************/
	
	
	public void clickday_mob() throws InterruptedException 
	{
		for(int i=1;1<=10;i++)
		{
			if(ReusableComponents.isElementPresent(driver, "xpath", "(//button[contains(text(),'Select')])[1]")) 
			{
			   ReusableComponents.clickButton(driver, "xpath", "(//button[contains(text(),'Select')])[1]");
			        if(ReusableComponents.isElementPresent(driver, "xpath", "(//span[contains(text(),'Select')])[1]"))
			        {
			        	ReusableComponents.clickButton(driver, "xpath", "(//span[contains(text(),'Select')])[1]"); 
			        	ReusableComponents.clickButton(driver, "xpath", "(//button[contains(text(),'Book Slot')])[1]");
			        	Thread.sleep(10000);
			        	break;
			        	
			         }
			         
			} 
			else {
				ReusableComponents.clickButton(driver, "xpath", "(//button[@class='mblButton nextCalendarDates arrowCalendar arrowMarkCalender unselectable'])[1]");  
			}	              
	
		}
	}

	
/********************************************************
 *FUNCTION    :clickdayforediting slot_mob
 *AUTHOR      :Chandru Kumar 
 *DATE        :28-12-2016
 *DESCRIPTION :Book a slot - Any Day Slot Booking ( for editing slot )
 * @throws InterruptedException 
 ********************************************************/

public void clickdayforeditingslot_mob() throws InterruptedException {
	ReusableComponents.clickButton(driver, "xpath", "(//button[@class='mblButton nextCalendarDates arrowCalendar arrowMarkCalender unselectable'])[1]");
	for(int i=1;1<=10;i++)
	{
		if(ReusableComponents.isElementPresent(driver, "xpath", "(//button[contains(text(),'Select')])[1]")) 
		{
		   ReusableComponents.clickButton(driver, "xpath", "(//button[contains(text(),'Select')])[1]");
		        if(ReusableComponents.isElementPresent(driver, "xpath", "(//span[contains(text(),'Select')])[1]"))
		        {
		        	ReusableComponents.clickButton(driver, "xpath", "(//span[contains(text(),'Select')])[1]"); 
		        	ReusableComponents.clickButton(driver, "xpath", "(//button[contains(text(),'Book Slot')])[1]");
		        	Thread.sleep(10000);
		        	break;
		        	
		         }
		         
		} 
		else {
			ReusableComponents.clickButton(driver, "xpath", "(//button[@class='mblButton nextCalendarDates arrowCalendar arrowMarkCalender unselectable'])[1]");  
		}	              

	}
}
}	
	


