package businesscomponents;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.By;

import com.cognizant.framework.Status;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.SeleniumTestParameters;

public class MobileApp_ProductScan  extends ReusableLibrary {

	public String userDir = System.getProperty("user.dir");
	int aaa=123;
	static JLabel label = new JLabel();
	static JFrame frame = new JFrame();


	public MobileApp_ProductScan(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}

	
	/********************************************************
	 *FUNCTION    :scanMultiProducts
	 *AUTHOR      :DeepakPrabhu A
     *DATE        :19-10-2017
	 *DESCRIPTION :Used for scanning multiple products by using the dataTable Qty value
	 ********************************************************/
	
	
	public void scanMultiProducts() 
	{
		
		int prodQty = Integer.parseInt(dataTable.getData("Items", "ProdQuantity"));
	
		int counter =1;

		try{
			while(counter<=prodQty)
			{

				(new MobileApp_testing(scriptHelper)).clickScanButton();
				scanProduct();
				counter++;

			}
			
					
		}catch(Exception e)
		{
			report.updateTestLog("Product scan", "Multiple product scan was not successful", Status.FAIL);
		}

		
		
		
	}
	
	
	
	
	/********************************************************
	 *FUNCTION    :removeProduct
	 *AUTHOR      :DeepakPrabhu A
     *DATE        :19-10-2017
	 *DESCRIPTION :used to remove a product
	 ********************************************************/
	
	public void removeProduct() throws Exception
	{
		scanProduct();
	}

	
	
	/********************************************************
	 *FUNCTION    :scanproduct
	 *AUTHOR      :Chandru Kumar S
	 *MODIFIED BY :DeepakPrabhu A
	 *DATE        :19-10-2017
	 *DESCRIPTION :scanning the product barcode image by fetching the product details from the Items sheet (General_Data)
	 ********************************************************/
	public void scanProduct() throws Exception 
	{	
		String product = dataTable.getData("Items", "ProdName");
		System.out.println("Launch of Barcode Image for " +product+ " - Initiated!!");
		String filePath = userDir+"\\ProductImages\\"+product+".png";
		String myOS = System.getProperty("os.name").split(" ")[0];
		String scanIconXpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout"
				+ "/android.widget.FrameLayout/android.widget.LinearLayout"
				+ "/android.widget.RelativeLayout[2]/android.widget.LinearLayout"
				+ "/android.widget.LinearLayout[2]/android.widget.TextView";
	
		

		int attempts =1;
		do{

		
			
			System.out.println("Product scan AttemptNo: "+attempts);
			
			if(attempts>=2)
			{
				driver.navigate().back();
				(new MobileApp_testing(scriptHelper)).clickScanButton();
			}

			if(attempts==4){
				break;
			}
			attempts++;

			switch(myOS)
			{


			case "Windows":

			{
				openImageInWindows(filePath);

				break;
			}
			
		

			case "Mac":
			{

				try{

					filePath=filePath.replaceAll("\\\\", "//");
					System.out.println(filePath);
					File imgFile = new File(filePath);
					openImageInMac(imgFile);


				} catch(Exception e)

				{

					System.out.println("Exception in reading PNG file in MAC OS: "+ e);

				}
				break;
			}
		
			}	
	
		
			
		}while((ReusableComponents.isElementPresent(driver,"xpath",scanIconXpath)==false)
				&&(ReusableComponents.isElementPresent(driver, "id", "android:id/message")==false));
	}




	

	/********************************************************
	 *FUNCTION    :scanmyWaitroseCardNumber
	 *AUTHOR      :Chandru Kumar S
	 *MODIFIED BY :DeepakPrabhu A
	 *DATE        :19-10-2017
	 *DESCRIPTION :scanning the barcode image of my Waitrose Card Number
	 ********************************************************/

	public void scanmyWaitroseCardNumber() throws Exception 
	{

		String myOS = System.getProperty("os.name");
		System.out.println("myWaitrose card barcode image launch - Initiated");

		switch(myOS)
		{

		case "Windows":
		{

			openImageInWindows(userDir+"\\ProductImages\\myWaitroseCardNumber.PNG");

			break;
		}

		case "Mac":
		{

			try{


				String filePath = userDir+"/ProductImages/myWaitroseCardNumber.PNG";
				File imgFile = new File(filePath);
				openImageInMac(imgFile);
			

			} catch(Exception e)

			{

				System.out.println("Exception in reading PNG file in MAC OS: "+ e);

			}
			break;
		}

		}


	}



	/********************************************************
	 *FUNCTION    :scanEndofTransaction
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :11-1-2017
	 *DESCRIPTION :Clicking the scan end of transaction button
	 ********************************************************/

	public void scanEndofTransaction() throws Exception 
	{


		String myOS = System.getProperty("os.name").split(" ")[0];

		String filePath = userDir+"\\ProductImages\\EndofTransaction.png";
		System.out.println("Launch of Barcode for EndofTransaction - Initiated!!");
		switch(myOS)
		{


		case "Windows":
		{


			filePath = userDir+"\\ProductImages\\EndofTransaction.png";
			openImageInWindows(filePath);
			report.updateTestLog("Verify EOT scan","End of transaction performed successfully", Status.PASS);
			break;
		}
		case "Mac":
		{
			try{

				File imgFile = new File(filePath.replace("\\", "/"));
				openImageInMac(imgFile);
						
		
			}catch(Exception e)
			{

				System.out.println("Exception in reading PNG file in MAC OS: "+ e);

			}
			break;
		
		}
	
	
		}
		
		
		
		
		// Ignore the pop up (Click OK)
		try{
			
			
			driver.findElement(By.xpath("//android.widget.Button[@resource-id='android:id/button1' and @text='OK']")).click();
		}catch(Exception e)
		{
			System.out.println("No pop up found while scanning EOT in QR code session");
		}
		
		
		
		
		
	}






		/********************************************************
		 *FUNCTION    :openImageInWindows
		 *AUTHOR      :DeepakPrabhu A
		 *DATE        :11-10-2017
		 *DESCRIPTION :General method for opening a image file windows platform
		 * @throws IOException 
		 * @throws InterruptedException 
		 ********************************************************/

		public void openImageInWindows(String filePath) throws IOException, InterruptedException
		{

			Runtime.getRuntime().exec("cmd /c start "+filePath);
			
			Thread.sleep(3000);
			
			(new MobileApp_testing(scriptHelper)).closewindow();

		}



		/********************************************************
		 *FUNCTION    :openImageInMac
		 *AUTHOR      :DeepakPrabhu A
		 *DATE        :11-10-2017
		 *DESCRIPTION :General method for opening a image file Mac platform
		 * @throws IOException 
		 * @throws InterruptedException 
		 ********************************************************/

		public void openImageInMac(File file) throws IOException, InterruptedException
		{
			label.setPreferredSize(new Dimension(250, 650));
			frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
			BufferedImage image = null;
			image = ImageIO.read(file);
			frame.add(label);
			label.setIcon(new ImageIcon(image));
			frame.getContentPane().setLayout(new FlowLayout());
			label.setVisible(true);
			frame.setVisible(true);
			frame.addNotify();
			Thread.sleep(5000);	
			label.hide();
			frame.dispose();


		}

		
		
		
		/********************************************************
		 *FUNCTION    :verifyPleaseDeclareToCashierPrompt
		 *AUTHOR      :DeepakPrabhu A
		 *DATE        :11-10-2017
		 ********************************************************/

	/*	public void verifyPleaseDeclareToCashierPrompt()
		{

			String promptText = ReusableComponents.getText(driver, "id", "android:id/message");
			System.out.println(promptText);
			
		if(ReusableComponents.getText(driver, "id", "android:id/message").contains("Please declare to cashier'. Item not added to basket. has to be displayed"))
		{
			This barcode is not found. Please ensure you show this item to the cashier at the end of your shop.
		
			
			report.updateTestLog("Invalid product scan", "Pop up with the instruction message <b> "+promptText+" </b> is displayed as expected", Status.PASS);	
			ReusableComponents.clickButton(driver, "id", "android:id/button1");

		} else{
			
			report.updateTestLog("Invalid product scan", "Pop up is not displayed with the expected instructions", Status.FAIL);	
		}

		}*/
		
		
		


		/********************************************************
		 *FUNCTION    :verifyErrorMessageForInvalidMyWScan
		 *AUTHOR      :DeepakPrabhu A
		 *DATE        :11-10-2017
		 ********************************************************/

		public void verifyErrorMessageForInvalidMyWScan()
		{

		if(ReusableComponents.getText(driver, "id", "android:id/message").equals("Please scan your customercard"))
		{
			report.updateTestLog("Error Message for Invalid myW card scan", "Error message is displayed as expected", Status.PASS);	
			ReusableComponents.clickButton(driver, "id", "android:id/button1");

		} else{
			
			report.updateTestLog("Error Message for Invalid myW card scan", "Error message is not displayed", Status.FAIL);	
		}

		}

		
		
		
	}