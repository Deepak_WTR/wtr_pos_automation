package businesscomponents;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uimap.M_PaymentDetails;
import uimap.Mobile_appFieldvalidation;
import uimap.Mobile_appBackofficeLogin;

public class MobileApp_BackofficeLogin extends ReusableLibrary {

	public MobileApp_BackofficeLogin(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}

	
	/********************************************************
	 *FUNCTION    :enterBranchNo
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :28-4-2017
	 *DESCRIPTION :Enter the Branch Number
	 ********************************************************/
	public void enterBranchNo()
	{
		ReusableComponents.isElementPresent(driver, Mobile_appBackofficeLogin.branchNo_Loc, Mobile_appBackofficeLogin.branchNo);
		String branchno = dataTable.getData("General_Data","BranchNumber");
		System.out.println(branchno);
		ReusableComponents.typeinEditbox(driver, Mobile_appBackofficeLogin.branchNo_Loc, Mobile_appBackofficeLogin.branchNo,branchno);
	}
	
	
	/********************************************************
	 *FUNCTION    :enterUserNo
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :28-4-2017
	 *DESCRIPTION :Enter the User Number
	 ********************************************************/
	public void enterUserNo()
	{
		ReusableComponents.isElementPresent(driver, Mobile_appBackofficeLogin.userNo_Loc, Mobile_appBackofficeLogin.userNo);
		String userno = dataTable.getData("General_Data","UserName");
		System.out.println(userno);
		ReusableComponents.typeinEditbox(driver, Mobile_appBackofficeLogin.branchNo_Loc, Mobile_appBackofficeLogin.branchNo,userno);
	}
	
	/********************************************************
	 *FUNCTION    :enterpassword
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :28-4-2017
	 *DESCRIPTION :Enter the Password
	 ********************************************************/
	public void enterpassword()
	{
		ReusableComponents.isElementPresent(driver, Mobile_appBackofficeLogin.pwd_Loc, Mobile_appBackofficeLogin.pwd);
		String password = dataTable.getData("General_Data","Password");
		System.out.println(password);
		ReusableComponents.typeinEditbox(driver, Mobile_appBackofficeLogin.pwd_Loc, Mobile_appBackofficeLogin.pwd,password);
	}
	
	/********************************************************
	 *FUNCTION    :clickLogin
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :28-4-2017
	 *DESCRIPTION :Click the Login Button
	 ********************************************************/
	
	public void clickLogin()
	{
		ReusableComponents.isElementPresent(driver, Mobile_appBackofficeLogin.clickButton_Loc, Mobile_appBackofficeLogin.clickButton);
		ReusableComponents.clickButton(driver, Mobile_appBackofficeLogin.clickButton_Loc, Mobile_appBackofficeLogin.clickButton);
	}
	
	
	/********************************************************
	 *FUNCTION    :clickAudit
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :28-4-2017
	 *DESCRIPTION :Click the Login Button
	 ********************************************************/
	
	public void clickAudit()
	{
		ReusableComponents.isElementPresent(driver, Mobile_appBackofficeLogin.clickAuditButton_Loc, Mobile_appBackofficeLogin.clickAuditButton);
		ReusableComponents.clickButton(driver, Mobile_appBackofficeLogin.clickAuditButton_Loc, Mobile_appBackofficeLogin.clickAuditButton);
	}
	
	
	/********************************************************
	 *FUNCTION    :clickEndofday
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :28-4-2017
	 *DESCRIPTION :Click the End of day Reductions Button
	 ********************************************************/
	
	public void clickEndofday()
	{
		ReusableComponents.isElementPresent(driver, Mobile_appBackofficeLogin.clickEODRbutton_Loc, Mobile_appBackofficeLogin.clickEODRbutton);
		ReusableComponents.clickButton(driver, Mobile_appBackofficeLogin.clickEODRbutton_Loc, Mobile_appBackofficeLogin.clickEODRbutton);
	}
	
	
	/********************************************************
	 *FUNCTION    :clickEndofdayMain
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :28-4-2017
	 *DESCRIPTION :Click the End of day Reductions Maintenace Button
	 ********************************************************/
	
	public void clickEndofdayMain()
	{
		ReusableComponents.isElementPresent(driver, Mobile_appBackofficeLogin.clickEODRMainbutton_Loc, Mobile_appBackofficeLogin.clickEODRMainbutton);
		ReusableComponents.clickButton(driver, Mobile_appBackofficeLogin.clickEODRMainbutton_Loc, Mobile_appBackofficeLogin.clickEODRMainbutton);
	}
	
	
	/********************************************************
	 *FUNCTION    :clickAddButton
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :28-4-2017
	 *DESCRIPTION :Click the Add Button
	 ********************************************************/
	
	public void clickAddButton()
	{
		ReusableComponents.isElementPresent(driver, Mobile_appBackofficeLogin.clickAddbutton_Loc, Mobile_appBackofficeLogin.clickAddbutton);
		ReusableComponents.clickButton(driver, Mobile_appBackofficeLogin.clickAddbutton_Loc, Mobile_appBackofficeLogin.clickAddbutton);
	}
	
	
	
	/********************************************************
	 *FUNCTION    :enterEODRname
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :28-4-2017
	 *DESCRIPTION :Click the EODR name
	 ********************************************************/
	
	public void enterEODRname()
	{
		ReusableComponents.isElementPresent(driver, Mobile_appBackofficeLogin.eodrName_Loc, Mobile_appBackofficeLogin.eodrName);
		String name = dataTable.getData("General_Data","EODRname");
		System.out.println(name);
		ReusableComponents.typeinEditbox(driver, Mobile_appBackofficeLogin.eodrName_Loc, Mobile_appBackofficeLogin.eodrName,name);
	}
	
	/********************************************************
	 *FUNCTION    :clickNext
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :28-4-2017
	 *DESCRIPTION :Click the Next
	 ********************************************************/
	
	public void clickNext()
	{
		ReusableComponents.isElementPresent(driver, Mobile_appBackofficeLogin.clickNextButton_Loc, Mobile_appBackofficeLogin.clickNextButton);
		ReusableComponents.clickButton(driver, Mobile_appBackofficeLogin.clickNextButton_Loc, Mobile_appBackofficeLogin.clickNextButton);
	}
	
	
	/********************************************************
	 *FUNCTION    :enterTime
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :28-4-2017
	 *DESCRIPTION :Enter the current time
	 ********************************************************/
	
	public void enterTime()
	{   
		
		ReusableComponents.isElementPresent(driver, Mobile_appBackofficeLogin.enterTime_Loc, Mobile_appBackofficeLogin.enterTime);
		ReusableComponents.clickButton(driver, Mobile_appBackofficeLogin.enterTime_Loc, Mobile_appBackofficeLogin.enterTime);
	}
}
