package businesscomponents;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import supportlibraries.Asserts;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

import com.cognizant.framework.Status;
//import com.thoughtworks.selenium.SeleniumException;

public class ReusableComponents extends ReusableLibrary{
	public ReusableComponents(ScriptHelper scriptHelper) {
		super(scriptHelper);
		}

	private static Asserts asserts = new Asserts();
	public static boolean isAlertPresent(WebDriver driver) {
		try {
			driver.switchTo().alert();
			return true;
		} // try
		catch (NoAlertPresentException Ex) {
			return false;
		} // catch
	}

	
	// #############################################################################
	// Function Name : verifyPage
	// Description : Function to validate text in a page
	// Input Parameters : Text to Verify
	// Return Value : None
	// Author : Cognizant
	// Date Created : 05/16/2012
	// #############################################################################
	public static boolean verifyPage(String pgText) {
		int timeout = 3;
		boolean isPresent = false;
		try {
			int x = 0;
			do {
				if (asserts.isTextPresent(driver, pgText)) {
					isPresent = true;
				} else {
					Thread.sleep(1000);
					x = x + 1;
					isPresent = false;
				}
			} while (x < timeout && isPresent == false);

		} catch (Exception e) {

		}
		return (isPresent);
	}
	
	// #############################################################################
		// Function Name : getListCount
		// Description : Function to get List Count
		// Input Parameters : Driver Object, Identifyby, Locator 
		// Return Value : List Count
		// Author : Cognizant
		// Date Created : 08/24/2012
		// #############################################################################
	public static int  getListCount(WebDriver driver, String identifyBy,
			String locator) {
		int intListCount = 0;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				List<WebElement> element=driver.findElements(By.xpath(locator));
				intListCount = element.size();
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				List<WebElement> element=driver.findElements(By.id(locator));
				intListCount = element.size();
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				List<WebElement> element=driver.findElements(By.name(locator));
				intListCount = element.size();
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				List<WebElement> element=driver.findElements(By.cssSelector(locator));
				intListCount = element.size();
			}
		} else if (identifyBy.equalsIgnoreCase("className")) {
			if (isElementPresent(driver, "className", locator)) {
				List<WebElement> element=driver.findElements(By.className(locator));
				intListCount = element.size();
			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			if (isElementPresent(driver, "linkText", locator)) {
				List<WebElement> element=driver.findElements(By.linkText(locator));
				intListCount = element.size();
			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			if (isElementPresent(driver, "partialLinkText", locator)) {
				List<WebElement> element=driver.findElements(By.partialLinkText(locator));
				intListCount = element.size();
			}
		}
		return intListCount;
	}

	
	// #############################################################################
	// Function Name : closeJscriptPopup
	// Description : Function to close the Javascript Popup
	// Input Parameters : driver and alert
	// Return Value : None
	// Author : Cognizant
	// Date Created : 05/16/2012
	// #############################################################################
	public static void ignorePopup(WebDriver driver, Alert alert) {
		try {
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			alert = driver.switchTo().alert();
			String str = alert.getText();
			System.out.println("Alert-" + str);
			alert.accept();
		} catch (Exception e) {
			System.out.println("Alert Not appearing");
		}
	}

	public boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} // try
		catch (NoAlertPresentException Ex) {
			return false;
		} // catch
	} // isAlertPresent()

	// #############################################################################
	// Function Name : navigatetoWebpage
	// Description : Function to Navigate to the WebPage
	// Input Parameters : driver and url
	// Return Value : None
	// Author : Cognizant
	// Date Created : 05/16/2012
	// #############################################################################
	public static void navigatetoWebpage(WebDriver driver, String url) {
		driver.get(url);
	}
	// #############################################################################
	// Function Name : getText
	// Description : Function to text from the WebPage
	// Input Parameters : driver and url
	// Return Value : None
	// Author : Cognizant
	// Date Created : 05/16/2012
	// #############################################################################
	public static String  getText(WebDriver driver, String identifyBy,
				String locator) {
			String strText = null;
			if (identifyBy.equalsIgnoreCase("xpath")) {
				if (isElementPresent(driver, "xpath", locator)) {
					strText=driver.findElement(By.xpath(locator)).getText();
				}
			} else if (identifyBy.equalsIgnoreCase("id")) {
				if (isElementPresent(driver, "id", locator)) {
					strText=driver.findElement(By.id(locator)).getText();
				}
			} else if (identifyBy.equalsIgnoreCase("name")) {
				if (isElementPresent(driver, "name", locator)) {
					strText=driver.findElement(By.name(locator)).getText();
				}
			} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
				if (isElementPresent(driver, "cssSelector", locator)) {
					strText=driver.findElement(By.cssSelector(locator)).getText();
				}
			} else if (identifyBy.equalsIgnoreCase("className")) {
				if (isElementPresent(driver, "className", locator)) {
					strText=driver.findElement(By.className(locator)).getText();
				}
			} else if (identifyBy.equalsIgnoreCase("linkText")) {
				if (isElementPresent(driver, "linkText", locator)) {
					strText=driver.findElement(By.linkText(locator)).getText();
				}
			} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
				if (isElementPresent(driver, "partialLinkText", locator)) {
					strText=driver.findElement(By.partialLinkText(locator)).getText();
				}
			}
			return strText;
		}

		
	// #############################################################################
	// Function Name : clickButton
	// Description : Function to click a button
	// Input Parameters : driver, identifier, locator
	// Return Value : None
	// Author : Cognizant
	// Date Created : 05/16/2012
	// #############################################################################
	public static void clickButton(WebDriver driver, String identifyBy,
			String locator) {
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				driver.findElement(By.xpath(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				driver.findElement(By.id(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				driver.findElement(By.name(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				driver.findElement(By.cssSelector(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("className")) {
			if (isElementPresent(driver, "className", locator)) {
				driver.findElement(By.className(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			if (isElementPresent(driver, "linkText", locator)) {
				driver.findElement(By.linkText(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			if (isElementPresent(driver, "partialLinkText", locator)) {
				driver.findElement(By.partialLinkText(locator)).click();
			}
		}
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// #############################################################################
	// Function Name : isElementPresent
	// Description : Function to validate the existence of an element
	// Input Parameters : driver, identifier, locator
	// Return Value : None
	// Author : Cognizant
	// Date Created : 05/16/2012
	// #############################################################################
	public static boolean isElementPresent(WebDriver driver, String identifyBy,
			String locator) {
		int timeout = 100;
		boolean isPresent = false;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.xpath(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.id(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.name(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.linkText(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.partialLinkText(locator))
							.isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.cssSelector(locator))
							.isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("className")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.className(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		}
		return isPresent;

	}

	// #############################################################################
			// Function Name : isElementPresent
			// Description : Function to validate the existence of an element
			// Input Parameters : wait time, driver, identifier, locator
			// Return Value : tpuAmoha
			// Author : Cognizant
			// Date Created : 05/02/2016
			// #############################################################################
			public static boolean isElementPresent(int waitTime, WebDriver driver, String identifyBy,
					String locator) {
				int timeout = waitTime;
				boolean isPresent = false;
				if (identifyBy.equalsIgnoreCase("xpath")) {
					try {
						int x = 0;
						do {
							if (driver.findElement(By.xpath(locator)).isDisplayed()) {
								isPresent = true;
							} else {
								Thread.sleep(1000);
								x = x + 1;
								isPresent = false;
							}
						} while (x < timeout && isPresent == false);

					} catch (Exception e) {

					}
				} else if (identifyBy.equalsIgnoreCase("id")) {
					try {
						int x = 0;
						do {
							if (driver.findElement(By.id(locator)).isDisplayed()) {
								isPresent = true;
							} else {
								Thread.sleep(1000);
								x = x + 1;
								isPresent = false;
							}
						} while (x < timeout && isPresent == false);

					} catch (Exception e) {

					}
				} else if (identifyBy.equalsIgnoreCase("name")) {
					try {
						int x = 0;
						do {
							if (driver.findElement(By.name(locator)).isDisplayed()) {
								isPresent = true;
							} else {
								Thread.sleep(1000);
								x = x + 1;
								isPresent = false;
							}
						} while (x < timeout && isPresent == false);

					} catch (Exception e) {

					}
				} else if (identifyBy.equalsIgnoreCase("linkText")) {
					try {
						int x = 0;
						do {
							if (driver.findElement(By.linkText(locator)).isDisplayed()) {
								isPresent = true;
							} else {
								Thread.sleep(1000);
								x = x + 1;
								isPresent = false;
							}
						} while (x < timeout && isPresent == false);

					} catch (Exception e) {

					}
				} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
					try {
						int x = 0;
						do {
							if (driver.findElement(By.partialLinkText(locator))
									.isDisplayed()) {
								isPresent = true;
							} else {
								Thread.sleep(1000);
								x = x + 1;
								isPresent = false;
							}
						} while (x < timeout && isPresent == false);

					} catch (Exception e) {

					}
				} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
					try {
						int x = 0;
						do {
							if (driver.findElement(By.cssSelector(locator))
									.isDisplayed()) {
								isPresent = true;
							} else {
								Thread.sleep(1000);
								x = x + 1;
								isPresent = false;
							}
						} while (x < timeout && isPresent == false);

					} catch (Exception e) {

					}
				} else if (identifyBy.equalsIgnoreCase("className")) {
					try {
						int x = 0;
						do {
							if (driver.findElement(By.className(locator)).isDisplayed()) {
								isPresent = true;
							} else {
								Thread.sleep(1000);
								x = x + 1;
								isPresent = false;
							}
						} while (x < timeout && isPresent == false);

					} catch (Exception e) {

					}
				}
				return isPresent;

			}
			
			
			// #############################################################################
			// Function Name : isElementPresent
			// Description : Function to validate the existence of an element
			// Input Parameters : wait time, driver, identifier, locator
			// Return Value : tpuAmoha
			// Author : Cognizant
			// Date Created : 05/02/2016
			// #############################################################################
			public static boolean isElementPresent(int waitTime, WebElement driver, String identifyBy,
					String locator) {
				int timeout = waitTime;
				boolean isPresent = false;
				if (identifyBy.equalsIgnoreCase("xpath")) {
					try {
						int x = 0;
						do {
							if (driver.findElement(By.xpath(locator)).isDisplayed()) {
								isPresent = true;
							} else {
								Thread.sleep(1000);
								x = x + 1;
								isPresent = false;
							}
						} while (x < timeout && isPresent == false);

					} catch (Exception e) {

					}
				} else if (identifyBy.equalsIgnoreCase("id")) {
					try {
						int x = 0;
						do {
							if (driver.findElement(By.id(locator)).isDisplayed()) {
								isPresent = true;
							} else {
								Thread.sleep(1000);
								x = x + 1;
								isPresent = false;
							}
						} while (x < timeout && isPresent == false);

					} catch (Exception e) {

					}
				} else if (identifyBy.equalsIgnoreCase("name")) {
					try {
						int x = 0;
						do {
							if (driver.findElement(By.name(locator)).isDisplayed()) {
								isPresent = true;
							} else {
								Thread.sleep(1000);
								x = x + 1;
								isPresent = false;
							}
						} while (x < timeout && isPresent == false);

					} catch (Exception e) {

					}
				} else if (identifyBy.equalsIgnoreCase("linkText")) {
					try {
						int x = 0;
						do {
							if (driver.findElement(By.linkText(locator)).isDisplayed()) {
								isPresent = true;
							} else {
								Thread.sleep(1000);
								x = x + 1;
								isPresent = false;
							}
						} while (x < timeout && isPresent == false);

					} catch (Exception e) {

					}
				} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
					try {
						int x = 0;
						do {
							if (driver.findElement(By.partialLinkText(locator))
									.isDisplayed()) {
								isPresent = true;
							} else {
								Thread.sleep(1000);
								x = x + 1;
								isPresent = false;
							}
						} while (x < timeout && isPresent == false);

					} catch (Exception e) {

					}
				} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
					try {
						int x = 0;
						do {
							if (driver.findElement(By.cssSelector(locator))
									.isDisplayed()) {
								isPresent = true;
							} else {
								Thread.sleep(1000);
								x = x + 1;
								isPresent = false;
							}
						} while (x < timeout && isPresent == false);

					} catch (Exception e) {

					}
				} else if (identifyBy.equalsIgnoreCase("className")) {
					try {
						int x = 0;
						do {
							if (driver.findElement(By.className(locator)).isDisplayed()) {
								isPresent = true;
							} else {
								Thread.sleep(1000);
								x = x + 1;
								isPresent = false;
							}
						} while (x < timeout && isPresent == false);

					} catch (Exception e) {

					}
				}
				return isPresent;

			}
	// #############################################################################
		// Function Name : ElementPresent
		// Description : Function to validate the existence of an element
		// Input Parameters : driver, identifier, locator
		// Return Value : None
		// Author : Cognizant
		// Date Created : 05/16/2012
		// #############################################################################
		public static boolean ElementPresent(WebDriver driver, String identifyBy,
				String locator, String PageLocation) throws InterruptedException {
			int timeout = 5;
			boolean isPresent = false;
			if (identifyBy.equalsIgnoreCase("xpath")) {
				try {
					int x = 0;
					do {
						if (driver.findElement(By.xpath(locator)).isDisplayed()) {
							isPresent = true;
						} else {
							Thread.sleep(1000);
							x = x + 1;
							isPresent = false;
						}
					} while (x < timeout && isPresent == false);

				} catch (Exception e) {

				}
			} else if (identifyBy.equalsIgnoreCase("id")) {
				try {
					int x = 0;
					do {
						if (driver.findElement(By.id(locator)).isDisplayed()) {
							isPresent = true;
						} else {
							Thread.sleep(1000);
							x = x + 1;
							isPresent = false;
						}
					} while (x < timeout && isPresent == false);

				} catch (Exception e) {

				}
			} else if (identifyBy.equalsIgnoreCase("name")) {
				try {
					int x = 0;
					do {
						if (driver.findElement(By.name(locator)).isDisplayed()) {
							isPresent = true;
						} else {
							Thread.sleep(1000);
							x = x + 1;
							isPresent = false;
						}
					} while (x < timeout && isPresent == false);

				} catch (Exception e) {

				}
			} else if (identifyBy.equalsIgnoreCase("linkText")) {
				try {
					int x = 0;
					do {
						if (driver.findElement(By.linkText(locator)).isDisplayed()) {
							isPresent = true;
						} else {
							Thread.sleep(1000);
							x = x + 1;
							isPresent = false;
						}
					} while (x < timeout && isPresent == false);

				} catch (Exception e) {
					System.out.println("exception: "+ e);

				}
			} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
				try {
					int x = 0;
					do {
						if (driver.findElement(By.partialLinkText(locator))
								.isDisplayed()) {
							isPresent = true;
						} else {
							Thread.sleep(1000);
							x = x + 1;
							isPresent = false;
						}
					} while (x < timeout && isPresent == false);

				} catch (Exception e) {

				}
			} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
				try {
					int x = 0;
					do {
						if (driver.findElement(By.cssSelector(locator))
								.isDisplayed()) {
							isPresent = true;
						} else {
							Thread.sleep(1000);
							x = x + 1;
							isPresent = false;
						}
					} while (x < timeout && isPresent == false);

				} catch (Exception e) {

				}
			} else if (identifyBy.equalsIgnoreCase("className")) {
				try {
					int x = 0;
					do {
						if (driver.findElement(By.className(locator)).isDisplayed()) {
							isPresent = true;
						} else {
							Thread.sleep(1000);
							x = x + 1;
							isPresent = false;
						}
					} while (x < timeout && isPresent == false);

				} catch (Exception e) {

				}
			}
			return isPresent;

		}
	// #############################################################################
	// Function Name : clickLink
	// Description : Function to click the Link
	// Input Parameters : driver, identifier, locator
	// Return Value : None
	// Author : Cognizant
	// Date Created : 05/16/2012
	// #############################################################################
	public static boolean clickLink(WebDriver driver, String identifyBy,
			String locator) {
		boolean isPresent=false;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				isPresent = true;
				driver.findElement(By.xpath(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				isPresent = true;
				driver.findElement(By.id(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				isPresent = true;
				driver.findElement(By.name(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			if (isElementPresent(driver, "linkText", locator)) {
				isPresent = true;
				driver.findElement(By.linkText(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			if (isElementPresent(driver, "partialLinkText", locator)) {
				isPresent = true;
				driver.findElement(By.partialLinkText(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				isPresent = true;
				driver.findElement(By.cssSelector(locator)).click();
			}
		}
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isPresent;
	}
	
	
	
	
	// #############################################################################
		// Function Name : clickLink - updated
		// Description : Function to click the Link
		// Input Parameters : driver, identifier, locator
		// Return Value : None
		// Author : Cognizant
		// Date Created : 05/06/2015
		// #############################################################################
		public static boolean clickObject(WebDriver driver, String identifyBy,String locator) {
			boolean isPresent=false;
			if (identifyBy.equalsIgnoreCase("xpath")) {
				if (isElementPresent(driver, "xpath", locator)) {
					isPresent = true;
					WebElement element = driver.findElement(By.xpath(locator));
					JavascriptExecutor executor = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", element);
					//driver.findElement(By.xpath(locator)).click();
				}
			} else if (identifyBy.equalsIgnoreCase("id")) {
				if (isElementPresent(driver, "id", locator)) {
					isPresent = true;
					WebElement element = driver.findElement(By.id(locator));
					JavascriptExecutor executor = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", element);
					}
			} else if (identifyBy.equalsIgnoreCase("name")) {
				if (isElementPresent(driver, "name", locator)) {
					WebElement element = driver.findElement(By.name(locator));
					JavascriptExecutor executor = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", element);
					isPresent = true;
					
				}
			} else if (identifyBy.equalsIgnoreCase("linkText")) {
				if (isElementPresent(driver, "linkText", locator)) {
					WebElement element = driver.findElement(By.linkText(locator));
					JavascriptExecutor executor = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", element);
					isPresent = true;
					
				}
			} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
				if (isElementPresent(driver, "partialLinkText", locator)) {
					WebElement element = driver.findElement(By.partialLinkText(locator));
					JavascriptExecutor executor = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", element);
					isPresent = true;
					}
			} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
				if (isElementPresent(driver, "cssSelector", locator)) {
					WebElement element = driver.findElement(By.cssSelector(locator));
					JavascriptExecutor executor = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", element);
					isPresent = true;
					}
			}
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return isPresent;
		}
	
	// #############################################################################
		// Function Name : clickstrLink
		// Description : Function to click the Link
		// Input Parameters : driver, identifier, locator
		// Return Value : None
		// Author : Cognizant
		// Date Created : 05/16/2012
		// #############################################################################
		public static boolean clickstrLink(WebDriver driver, String identifyBy,
				String locator) {
			boolean isPresent=false;
			if (identifyBy.equalsIgnoreCase("xpath")) {
				if (isElementPresent(driver, "xpath", locator)) {
					isPresent = true;
					driver.findElement(By.xpath(locator)).click();
				}
			} else if (identifyBy.equalsIgnoreCase("id")) {
				if (isElementPresent(driver, "id", locator)) {
					isPresent = true;
					driver.findElement(By.id(locator)).click();
				}
			} else if (identifyBy.equalsIgnoreCase("name")) {
				if (isElementPresent(driver, "name", locator)) {
					isPresent = true;
					driver.findElement(By.name(locator)).click();
				}
			} else if (identifyBy.equalsIgnoreCase("linkText")) {
				if (isElementPresent(driver, "linkText", locator)) {
					isPresent = true;
					driver.findElement(By.linkText(locator)).click();
				}
			} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
				if (isElementPresent(driver, "partialLinkText", locator)) {
					isPresent = true;
					driver.findElement(By.partialLinkText(locator)).click();
				}
			} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
				if (isElementPresent(driver, "cssSelector", locator)) {
					isPresent = true;
					driver.findElement(By.cssSelector(locator)).click();
				}
			}
			
			return isPresent;
		}


	// #############################################################################
	// Function Name : typeinEditbox
	// Description : Function to type in text box
	// Input Parameters : driver, identifier, locator and value to be typed
	// Return Value : None
	// Author : Cognizant
	// Date Created : 05/16/2012
	// #############################################################################
	public static boolean typeinEditbox(WebDriver driver, String identifyBy,
			String locator, String valuetoType) {
		boolean isPresent = false;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				isPresent=true;
				driver.findElement(By.xpath(locator)).clear();
				driver.findElement(By.xpath(locator)).sendKeys(valuetoType);
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				isPresent=true;
				driver.findElement(By.id(locator)).clear();
				driver.findElement(By.id(locator)).sendKeys(valuetoType);
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				isPresent=true;
				driver.findElement(By.name(locator)).clear();
				driver.findElement(By.name(locator)).sendKeys(valuetoType);
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				isPresent=true;
				driver.findElement(By.cssSelector(locator)).clear();
				driver.findElement(By.cssSelector(locator)).sendKeys(
						valuetoType);
			}
		}
		return isPresent;

	}

	// #############################################################################
	// Function Name : selectRadiobutton
	// Description : Function to Select Radio button
	// Input Parameters : driver, identifier, locator
	// Return Value : None
	// Author : Cognizant
	// Date Created : 05/16/2012
	// #############################################################################
	public static void selectRadiobutton(WebDriver driver, String identifyBy,
			String locator) {
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
			//	System.out.println("clicking radio button" +locator);
				driver.findElement(By.xpath(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				driver.findElement(By.id(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				driver.findElement(By.name(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				driver.findElement(By.cssSelector(locator)).click();
			}
		}

	}

	// #############################################################################
	// Function Name : selectCheckbox
	// Description : Function to Select a check box
	// Input Parameters : driver, identifier, locator and check flag to be
	// Switched on/off
	// Return Value : None
	// Author : Cognizant
	// Date Created : 05/16/2012
	// #############################################################################
	public static void selectCheckbox(WebDriver driver, String identifyBy,
			String locator, String checkFlag) {

		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				if ((checkFlag).equalsIgnoreCase("ON")) {
					if (!(driver.findElement(By.xpath(locator)).isSelected())) {
						driver.findElement(By.xpath(locator)).click();
					}
				}
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				if ((checkFlag).equalsIgnoreCase("ON")) {
					if (!(driver.findElement(By.id(locator)).isSelected())) {
						driver.findElement(By.id(locator)).click();
					}
				}
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				if ((checkFlag).equalsIgnoreCase("ON")) {
					if (!(driver.findElement(By.name(locator)).isSelected())) {
						driver.findElement(By.name(locator)).click();
					}
				}
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				if ((checkFlag).equalsIgnoreCase("ON")) {
					if (!(driver.findElement(By.cssSelector(locator))
							.isSelected())) {
						driver.findElement(By.cssSelector(locator)).click();
					}
				}
			}
		}

	}

	/**
	 * Component to verify header. Uses h1 tag in page.
	 * 
	 * @param pgText
	 * @return Returns true if header is found
	 */
	public static boolean verifyHeader(String pgText) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		long start = System.currentTimeMillis();
		boolean isPresent = false;
		try {
			if (isElementPresent(driver,"xpath","//div[@class='jotter-results']/div[@class='r-content']/h1")) {
				String strText = driver.findElement(
						By.xpath("//div[@class='jotter-results']/div[@class='r-content']/h1")).getText();
				System.out.println(strText);
				if (strText.contains(pgText))
					isPresent = true;
			}
		} catch (Exception e) {
		}
		System.out.println("Time taken in this verify header call is "
				+ (System.currentTimeMillis() - start));
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		return (isPresent);
	}

	/**
	 * Convenience function to verify header by Class 'page-title'
	 * 
	 * @FunctionName verifyHeaderByClassPageTitle
	 * @InputParameters None
	 * @Author Cognizant
	 * @DateCreated Jun 25, 2012
	 * @param pgText
	 * @return True of False
	 */
	public boolean verifyHeaderByClassPageTitle(String pgText) {
		long start = System.currentTimeMillis();
		boolean isPresent = false;
		try {
			//if (isElementPresent(driver, "xpath", "//h1[@class='page-title']")) {
				if (driver.findElement(By.tagName("h1")).isDisplayed()) {
				String strText = driver.findElement(
					//	By.xpath("//h1[@class='page-title']")).getText();
				By.tagName("h1")).getText();
				System.out.println(strText);
				if (strText.matches(pgText))
					isPresent = true;
			}
		} catch (Exception e) {
		}
		System.out.println("Time taken in this verify header" + pgText
				+ " call is " + (System.currentTimeMillis() - start));
		return (isPresent);
	}

//#############################################################################
	// Function Name : isElementPresent
	// Description : Function to validate the existence of an element
	// Input Parameters : driver, identifier, locator
	// Return Value : None
	// Author : Cognizant
	// Date Created : 05/16/2012
	// #############################################################################
	public static boolean isImagePresentInTable(WebDriver driver, String identifyBy,
			String locator, int Row) {
		int timeout = 3;
		boolean isPresent = false;		
		locator=locator.replaceAll("<>","["+Row+"]");		
		if (identifyBy.equalsIgnoreCase("xpath")) {
			try {
				
				
					if (driver.findElement(By.xpath(locator)).isDisplayed()) 
						isPresent = true;				

			} catch (Exception e) {

			}
		}/* else if (identifyBy.equalsIgnoreCase("id")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.id(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.name(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.linkText(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.partialLinkText(locator))
							.isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.cssSelector(locator))
							.isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("className")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.className(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		}*/
		return isPresent;

	}


	// #############################################################################
	// Function Name : getElement
	// Description : Function to get Element from the WebPage
	// Input Parameters : driver and url
	// Return Value : None
	// Author : Nanditha
	// Date Created : 25-June-13
	// #############################################################################
	public static WebElement  getElement(WebDriver driver, String identifyBy,
				String locator) {
			WebElement Element = null;
			if (identifyBy.equalsIgnoreCase("xpath")) {
				if (isElementPresent(driver, "xpath", locator)) {
					Element=driver.findElement(By.xpath(locator));
				}
			} else if (identifyBy.equalsIgnoreCase("id")) {
				if (isElementPresent(driver, "id", locator)) {
					Element=driver.findElement(By.id(locator));
				}
			} else if (identifyBy.equalsIgnoreCase("name")) {
				if (isElementPresent(driver, "name", locator)) {
					Element=driver.findElement(By.name(locator));
				}
			} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
				if (isElementPresent(driver, "cssSelector", locator)) {
					Element=driver.findElement(By.cssSelector(locator));
				}
			} else if (identifyBy.equalsIgnoreCase("className")) {
				if (isElementPresent(driver, "className", locator)) {
					Element=driver.findElement(By.className(locator));
				}
			} else if (identifyBy.equalsIgnoreCase("linkText")) {
				if (isElementPresent(driver, "linkText", locator)) {
					Element=driver.findElement(By.linkText(locator));
				}
			} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
				if (isElementPresent(driver, "partialLinkText", locator)) {
					Element=driver.findElement(By.partialLinkText(locator));
				}
			}
			return Element;
		}
	// #############################################################################
	// Function Name : getText from table
	// Description : Function to get text from a table
	// Input Parameters : driver and url
	// Return Value : None
	// Author : Nanditha
	// Date Created : 7-July-13
	// #############################################################################		
	public static String getTextFromTable(WebDriver driver, String identifyBy,
			String locator, int Row) {
		int timeout = 3;
		String text = "";		
		locator=locator.replaceAll("<>","["+Row+"]");		
		if (identifyBy.equalsIgnoreCase("xpath")) {
			try {
				
				
					text =driver.findElement(By.xpath(locator)).getText() ;
										

			} catch (Exception e) {

			}
		}/* else if (identifyBy.equalsIgnoreCase("id")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.id(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.name(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.linkText(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.partialLinkText(locator))
							.isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.cssSelector(locator))
							.isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("className")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.className(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		}*/
		return text;

	}
	// #############################################################################
		// Function Name : click link from table
		// Description : Function to click a link from a table
		// Input Parameters : driver and url
		// Return Value : None
		// Author : Nanditha
		// Date Created : 8-July-13
		// #############################################################################		
		public static void clickLinkFromTable(WebDriver driver, String identifyBy,
				String locator, int Row) {
			int timeout = 3;
			String text = "";		
			locator=locator.replaceAll("<>","["+Row+"]");		
			if (identifyBy.equalsIgnoreCase("xpath")) {
				try {
					
					
						driver.findElement(By.xpath(locator)).click() ;
											

				} catch (Exception e) {

				}
			}/* else if (identifyBy.equalsIgnoreCase("id")) {
				try {
					int x = 0;
					do {
						if (driver.findElement(By.id(locator)).isDisplayed()) {
							isPresent = true;
						} else {
							Thread.sleep(1000);
							x = x + 1;
							isPresent = false;
						}
					} while (x < timeout && isPresent == false);

				} catch (Exception e) {

				}
			} else if (identifyBy.equalsIgnoreCase("name")) {
				try {
					int x = 0;
					do {
						if (driver.findElement(By.name(locator)).isDisplayed()) {
							isPresent = true;
						} else {
							Thread.sleep(1000);
							x = x + 1;
							isPresent = false;
						}
					} while (x < timeout && isPresent == false);

				} catch (Exception e) {

				}
			} else if (identifyBy.equalsIgnoreCase("linkText")) {
				try {
					int x = 0;
					do {
						if (driver.findElement(By.linkText(locator)).isDisplayed()) {
							isPresent = true;
						} else {
							Thread.sleep(1000);
							x = x + 1;
							isPresent = false;
						}
					} while (x < timeout && isPresent == false);

				} catch (Exception e) {

				}
			} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
				try {
					int x = 0;
					do {
						if (driver.findElement(By.partialLinkText(locator))
								.isDisplayed()) {
							isPresent = true;
						} else {
							Thread.sleep(1000);
							x = x + 1;
							isPresent = false;
						}
					} while (x < timeout && isPresent == false);

				} catch (Exception e) {

				}
			} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
				try {
					int x = 0;
					do {
						if (driver.findElement(By.cssSelector(locator))
								.isDisplayed()) {
							isPresent = true;
						} else {
							Thread.sleep(1000);
							x = x + 1;
							isPresent = false;
						}
					} while (x < timeout && isPresent == false);

				} catch (Exception e) {

				}
			} else if (identifyBy.equalsIgnoreCase("className")) {
				try {
					int x = 0;
					do {
						if (driver.findElement(By.className(locator)).isDisplayed()) {
							isPresent = true;
						} else {
							Thread.sleep(1000);
							x = x + 1;
							isPresent = false;
						}
					} while (x < timeout && isPresent == false);

				} catch (Exception e) {

				}
			}*/
			

		}


		

/********************************************************
 *FUNCTION    :switchToWindow
 *AUTHOR      :Gajapathy D.R.
 *DATE        :02-July-13
 *DESCRIPTION :Function to Switch to New Window.
 *PAGE        :Navigating to new window. 
********************************************************/
public static void switchToWindow()throws NoSuchWindowException
{
	try
	{
		Set<String> handles = driver.getWindowHandles();
		String current = driver.getWindowHandle();
		handles.remove(current);
		Thread.sleep(1000);
		String newTab = handles.iterator().next();
	    driver.switchTo().window(newTab);
	    
	}
	catch(Exception e)
	{
		e.printStackTrace();
		report.updateTestLog("Switvh to Window", "Switch to window not appear"+e.toString(), Status.FAIL);
	}

}


/********************************************************
 *FUNCTION    :explicitWait
 *AUTHOR      :Gajapathy D.R.
 *DATE        :30/05/14
 *DESCRIPTION :Function to explicitWait
 *PAGE        :Wait for expected element. 
********************************************************/
public static void explicitWait(WebDriver driver, String identifyBy, String locator,String locatorname)
{
	try
	{
		if(identifyBy.equalsIgnoreCase("id"))
		{
			WebElement elementCheck=(new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.id(locator)));
			if(elementCheck.isDisplayed())
			{
				report.updateTestLog("Expected Element", "Expected element is present in page"+locatorname, Status.PASS);
			}
			else
				report.updateTestLog("Expected Element", "Expected element is not present in page", Status.FAIL);
		}
		if(identifyBy.equalsIgnoreCase("xpath"))
		{
			WebElement elementCheck=(new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
			if(elementCheck.isDisplayed())
			{
				report.updateTestLog("Expected Element", "Expected element is present in page"+locatorname, Status.PASS);
			}
			else
				report.updateTestLog("Expected Element", "Expected element is not present in page", Status.FAIL);
		}
		if(identifyBy.equalsIgnoreCase("linkText"))
		{
			WebElement elementCheck=(new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(locator)));
			if(elementCheck.isDisplayed())
			{
				report.updateTestLog("Expected Element", "Expected element is present in page"+locatorname, Status.PASS);
			}
			else
				report.updateTestLog("Expected Element", "Expected element is not present in page", Status.FAIL);
		}
		if(identifyBy.equalsIgnoreCase("name"))
		{
			WebElement elementCheck=(new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.name(locator)));
			if(elementCheck.isDisplayed())
			{
				report.updateTestLog("Expected Element", "Expected element is present in page"+locatorname, Status.PASS);
			}
			else
				report.updateTestLog("Expected Element", "Expected element is not present in page", Status.FAIL);
		}
		if(identifyBy.equalsIgnoreCase("partialLinkText"))
		{
			WebElement elementCheck=(new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText(locator)));
			if(elementCheck.isDisplayed())
			{
				report.updateTestLog("Expected Element", "Expected element is present in page"+locatorname, Status.PASS);
			}
			else
				report.updateTestLog("Expected Element", "Expected element is not present in page", Status.FAIL);
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
		report.updateTestLog("Expected Element", "Expected element is not present in page"+e.toString(), Status.FAIL);
		
	}
}
/********************************************************
 *FUNCTION    :waitUntilElement
 *AUTHOR      :Kathir
 *DATE        :15-July-14
 *DESCRIPTION :Function to validate the existence and non-Existence of an element
 ********************************************************/ 

public static boolean waitUntilElement(WebDriver driver, String identifyBy,
		String locator, boolean checkPresence, int time) {
	int timeout = time;
	boolean isPresent = false;
	if(checkPresence){
		if (identifyBy.equalsIgnoreCase("xpath")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.xpath(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.id(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.name(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.linkText(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.partialLinkText(locator))
							.isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.cssSelector(locator))
							.isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("className")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.className(locator)).isDisplayed()) {
						isPresent = true;
					} else {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		}
	}else
	{
		if (identifyBy.equalsIgnoreCase("xpath")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.xpath(locator)).isDisplayed()) {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					} else {
						isPresent = true;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.id(locator)).isDisplayed()) {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					} else {
						isPresent = true;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.name(locator)).isDisplayed()) {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					} else {
						isPresent = true;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.linkText(locator)).isDisplayed()) {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					} else {
						isPresent = true;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.partialLinkText(locator))
							.isDisplayed()) {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					} else {
						isPresent = true;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.cssSelector(locator))
							.isDisplayed()) {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					} else {
						isPresent = true;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		} else if (identifyBy.equalsIgnoreCase("className")) {
			try {
				int x = 0;
				do {
					if (driver.findElement(By.className(locator)).isDisplayed()) {
						Thread.sleep(1000);
						x = x + 1;
						isPresent = false;
					} else {
						isPresent = true;
					}
				} while (x < timeout && isPresent == false);

			} catch (Exception e) {

			}
		}
	}
	return isPresent;

}

/********************************************************
 *FUNCTION    :scrollDownTillEndpage
 *AUTHOR      :Gajapathy D.R.
 *DATE        :16-Jul-14
 *DESCRIPTION :Function to scroll down till end of the page
 *PAGE        :Re-usable component. 
********************************************************/
public static void scrollDownTillEndpage()
{
	 try
	 {
		 Actions actions = new Actions(driver);
		 actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
		 report.updateTestLog("Scroll down", "Scroll down till page end", Status.DONE);
	 }
	 catch(Exception e)
	 {
		 e.printStackTrace();
		 report.updateTestLog("Scroll down", "Scroll down is not happened"+e.toString(), Status.FAIL);
	 }
}
/********************************************************
 *FUNCTION    :scrollUpTillToppage
 *AUTHOR      :Gajapathy D.R.
 *DATE        :16-Jul-14
 *DESCRIPTION :Function to scroll up till top of the page
 *PAGE        :Re-usable component. 
********************************************************/
public static void scrollUpTillToppage()
{
	 try
	 {
		 Actions actions = new Actions(driver);
		 actions.keyUp(Keys.CONTROL).sendKeys(Keys.UP).perform();
		 report.updateTestLog("Scroll Up", "Scroll Up till top of the page", Status.DONE);
	 }
	 catch(Exception e)
	 {
		 e.printStackTrace();
		 report.updateTestLog("Scroll Up", "Scroll Up is not happened"+e.toString(), Status.FAIL);
	 }
}
/********************************************************
 *FUNCTION    :navigatePreviousPage
 *AUTHOR      :Gajapathy D.R.
 *DATE        :18-Jul-14
 *DESCRIPTION :Function to scroll up till top of the page
 *PAGE        :Re-usable component. 
********************************************************/
public static void navigatePreviousPage()
{
	 try
	 {
		 driver.navigate().back();
		 report.updateTestLog("Navigate Previous Page", "Navigated to previos page", Status.DONE);
	 }
	 catch(Exception e)
	 {
		 e.printStackTrace();
		 report.updateTestLog("Navigate Previous Page", "Not Navigated to previos page"+e.toString(), Status.FAIL);
	 }
}
//#############################################################################
//Function Name : isEnabled
//Description : Function to check is element is enabled
//Input Parameters : driver, identifier, locator
//Return Value : boolean
//Author : Kathir
//Date Created : 31-Jul-2014
//#############################################################################
public static boolean isEnabled(WebDriver driver, String identifyBy,
		String locator) {
	boolean isPresent = false;
	if (identifyBy.equalsIgnoreCase("xpath")) {
		if (driver.findElement(By.xpath(locator)).isEnabled()) {
			isPresent=true;
		}
	} else if (identifyBy.equalsIgnoreCase("id")) {
		if (driver.findElement(By.id(locator)).isEnabled()) {
			isPresent=true;
		}
	} else if (identifyBy.equalsIgnoreCase("name")) {
		if (driver.findElement(By.name(locator)).isEnabled()) {
			isPresent=true;
		}
	} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
		if (driver.findElement(By.cssSelector(locator)).isEnabled()) {
			isPresent=true;
		}
	}
	return isPresent;

}


//######################## Reusablecomponents.java ######################################



//######################## Reusablecomponents.java ######################################

  //#############################################################################
// Function Name : getAttribute
// Description : Function to get any property value of the element
// Author : Jayandran
// Date Created : 13-nov-2014
// #############################################################################
public static String  getAttribute(WebDriver driver, String identifyBy,
			String locator, String property) {
		String strText = null;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				strText=driver.findElement(By.xpath(locator)).getAttribute(property);
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				strText=driver.findElement(By.id(locator)).getAttribute(property);
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				strText=driver.findElement(By.name(locator)).getAttribute(property);
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				strText=driver.findElement(By.cssSelector(locator)).getAttribute(property);
			}
		} else if (identifyBy.equalsIgnoreCase("className")) {
			if (isElementPresent(driver, "className", locator)) {
				strText=driver.findElement(By.className(locator)).getAttribute(property);
			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			if (isElementPresent(driver, "linkText", locator)) {
				strText=driver.findElement(By.linkText(locator)).getAttribute(property);
			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			if (isElementPresent(driver, "partialLinkText", locator)) {
				strText=driver.findElement(By.partialLinkText(locator)).getAttribute(property);
			}
		}
		return strText;
	}

public static void SelectValuefromDropDown1(WebDriver driver, String identifyBy, String locator, String valuetoSelect)
{
	//WebElement select =(driver.findElement(By.id("unitOfMeasure")));
	//select.sendKeys(value);
	if (identifyBy.equalsIgnoreCase("id")) {
		if (isElementPresent(driver, "id", locator)) {
		//	System.out.println("clicking radio button" +locator);
			Select select1 = new Select(driver.findElement(By.id(locator)));
			select1.selectByIndex(Integer.parseInt(valuetoSelect));
		}
	} 
	else if (identifyBy.equalsIgnoreCase("xpath")) {
		if (isElementPresent(driver, "xpath", locator)) {
		//	System.out.println("clicking radio button" +locator);
			Select select1 = new Select(driver.findElement(By.xpath(locator)));
			select1.selectByIndex(Integer.parseInt(valuetoSelect));
		}
	} 
	else if (identifyBy.equalsIgnoreCase("name")) {
		if (isElementPresent(driver, "name", locator)) {
		//	System.out.println("clicking radio button" +locator);
			Select select1 = new Select(driver.findElement(By.name(locator)));
			select1.selectByIndex(Integer.parseInt(valuetoSelect));
		}
	} 
}


public static void SelectValuefromDropDown(WebDriver driver, String identifyBy, String locator, String valuetoSelect)
{
	//WebElement select =(driver.findElement(By.id("unitOfMeasure")));
	//select.sendKeys(value);
	if (identifyBy.equalsIgnoreCase("id")) {
		if (isElementPresent(driver, "id", locator)) {
		//	System.out.println("clicking radio button" +locator);
			WebElement select=driver.findElement(By.id(locator));
			select.sendKeys(valuetoSelect);
		}
	} 
	else if (identifyBy.equalsIgnoreCase("xpath")) {
		if (isElementPresent(driver, "xpath", locator)) {
		//	System.out.println("clicking radio button" +locator);
			WebElement select=driver.findElement(By.xpath(locator));
			select.sendKeys(valuetoSelect);
		}
	} 
	else if (identifyBy.equalsIgnoreCase("name")) {
		if (isElementPresent(driver, "name", locator)) {
		//	System.out.println("clicking radio button" +locator);
			WebElement select=driver.findElement(By.name(locator));
			select.sendKeys(valuetoSelect);
		}
	} 
}

public static void safeJavaScriptClick(WebElement element) throws Exception {
	try {
		if (element.isEnabled() && element.isDisplayed()) {
			System.out.println("Clicking on element with using java script click");

			((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
		} else {
			System.out.println("Unable to click on element");
		}
	} catch (StaleElementReferenceException e) {
		System.out.println("Element is not attached to the page document "+ e.getStackTrace());
	} catch (NoSuchElementException e) {
		System.out.println("Element was not found in DOM "+ e.getStackTrace());
	} catch (Exception e) {
		System.out.println("Unable to click on element "+ e.getStackTrace());
	}
}

public static void waitUntilClickable(WebDriver driver, String identifyBy, String locator)
{
	WebDriverWait wait = new WebDriverWait(driver, 60);
	WebElement element = null;
	if (identifyBy.equalsIgnoreCase("id")) {
		element = wait.until(ExpectedConditions.elementToBeClickable(By.id(locator)));
	} 
	else if (identifyBy.equalsIgnoreCase("xpath")) {
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
	} 
	else if (identifyBy.equalsIgnoreCase("name")) {
		element = wait.until(ExpectedConditions.elementToBeClickable(By.name(locator)));
	} 
}

public static void waitForPageLoad(int period)
{
	WebDriverWait wait = new WebDriverWait(driver, period);

	wait.until(new ExpectedCondition<Boolean>() {
	    public Boolean apply(WebDriver wdriver) {
	        return ((JavascriptExecutor) driver).executeScript(
	            "return document.readyState"
	        ).equals("complete");
	    }
	});
}



}