package businesscomponents;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uimap.Header;
import uimap.HomePage;
import uimap.M_EnterDetails;
import uimap.M_BookSlot;
import uimap.M_SearchProduct;
import uimap.OfBranchDeliverydate;
import uimap.OfLogin;
import uimap.UserRegisteration;
import uimap.M_Login;
import allocator.Allocator;

import com.cognizant.framework.Status;
//import com.thoughtworks.selenium.SeleniumException;


public class Mobile_SearchProduct extends ReusableLibrary {

	public Mobile_SearchProduct(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	
	}
	
	/********************************************************
	 *FUNCTION    :clickStartShopping_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :29-12-2016
	 *DESCRIPTION :click the Start Shopping button 
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void clickStartShopping_mob() throws InterruptedException {
		if (ReusableComponents.isElementPresent(driver, M_SearchProduct.btnStartShopping_loc, M_SearchProduct.btnStartShopping)){
			ReusableComponents.clickButton(driver, M_SearchProduct.btnStartShopping_loc, M_SearchProduct.btnStartShopping);	
			Thread.sleep(10000);
		    report.updateTestLog("Start Shopping", "Start Shopping button is clicked", Status.PASS);}
		else
			  report.updateTestLog("Start Shopping", "Start Shopping button is not clicked", Status.FAIL);
	}
	
	
	/********************************************************
	 *FUNCTION    :SearchingProduct_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :29-12-2016
	 *DESCRIPTION :click the Start Shopping button 
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void searchingProduct_mob() throws InterruptedException {
		ReusableComponents.clickButton(driver, M_SearchProduct.btnSearch_loc, M_SearchProduct.btnSearch);
		if (ReusableComponents.isElementPresent(driver, M_SearchProduct.txtSearch_loc, M_SearchProduct.txtSearch)){
			String productname = dataTable.getData("General_Data","productname_mob");
			System.out.println(productname);
			ReusableComponents.typeinEditbox(driver, M_SearchProduct.txtSearch_loc, M_SearchProduct.txtSearch,productname);	
			driver.findElement(By.xpath(M_SearchProduct.txtSearch)).sendKeys(Keys.ENTER);
			Thread.sleep(8000);
		    report.updateTestLog("Search Product", "Search product is successfull", Status.PASS);}
		else
			  report.updateTestLog("Search Product", "Search product is not successfull", Status.FAIL);
	}
	
	
	/********************************************************
	 *FUNCTION    :addProduct_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :29-12-2016
	 *DESCRIPTION :click the Start Shopping button 
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void addProduct_mob() throws InterruptedException {
		if (ReusableComponents.isElementPresent(driver, M_SearchProduct.btnadd_loc, M_SearchProduct.btnadd)){
			ReusableComponents.clickButton(driver, M_SearchProduct.btnadd_loc, M_SearchProduct.btnadd);	
			ReusableComponents.isElementPresent(driver, M_SearchProduct.btnno_loc, M_SearchProduct.btnno);
			String productqty = dataTable.getData("General_Data","productquantity_mob");
			System.out.println(productqty);
			ReusableComponents.typeinEditbox(driver, M_SearchProduct.btnno_loc, M_SearchProduct.btnno,productqty);
			//JavascriptExecutor jse=(JavascriptExecutor)driver;
			//jse.executeScript("scroll(0,650)"); 
			ReusableComponents.clickButton(driver, M_SearchProduct.btnaddproduct_loc, M_SearchProduct.btnaddproduct);	
			ReusableComponents.clickButton(driver, M_SearchProduct.btncheckout_loc, M_SearchProduct.btncheckout);
			Thread.sleep(10000);
		    report.updateTestLog("Add Product", "Add button is clicked", Status.PASS);}
		else 	{
			ReusableComponents.isElementPresent(driver, M_SearchProduct.btnno_loc, M_SearchProduct.btnno);
			String productqty = dataTable.getData("General_Data","productquantity_mob");
			System.out.println(productqty);
			ReusableComponents.typeinEditbox(driver, M_SearchProduct.btnno_loc, M_SearchProduct.btnno,productqty);
			//JavascriptExecutor jse=(JavascriptExecutor)driver;
			//jse.executeScript("scroll(0,650)"); 
			ReusableComponents.clickButton(driver, M_SearchProduct.btnaddproduct_loc, M_SearchProduct.btnaddproduct);	
			ReusableComponents.clickButton(driver, M_SearchProduct.btncheckout_loc, M_SearchProduct.btncheckout);
			Thread.sleep(10000);
		    report.updateTestLog("Add Product", "Add button is clicked", Status.PASS);}
		
	}}

	
