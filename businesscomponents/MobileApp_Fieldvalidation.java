package businesscomponents;

import org.openqa.selenium.By;

import com.cognizant.framework.Status;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uimap.M_PaymentDetails;
import uimap.Mobile_appFieldvalidation;
import uimap.Mobile_apptesting;
import uimap.OfOrderSummary;
import businesscomponents.ReusableComponents;

public class MobileApp_Fieldvalidation extends ReusableLibrary {
	String price;
	
	public MobileApp_Fieldvalidation(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	/********************************************************
	 *FUNCTION    :itemnamefield
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :12-1-2017
	 *DESCRIPTION :Field validation - Item name 
	 ********************************************************/
	public void itemnamefield() throws Exception 
	{
		Thread.sleep(2000);
		if(ReusableComponents.isElementPresent(driver, Mobile_appFieldvalidation.itemName_loc, Mobile_appFieldvalidation.itemName)){
			String product = ReusableComponents.getText(driver, Mobile_appFieldvalidation.itemName_loc,Mobile_appFieldvalidation.itemName);
			String[] productname = product.split(" ");
			String strTemp = "";
			for(int i=1;i<productname.length;i++)
			{
			   strTemp = strTemp + " " + productname[i];
			}
			strTemp=strTemp.trim();
			System.out.println("The item name is: " + strTemp);
			report.updateTestLog("Item name", "Item name is present -"+ strTemp , Status.PASS);}
		else
			report.updateTestLog("Item name", "Item name is not present", Status.FAIL);
		}
	
	/********************************************************
	 *FUNCTION    :itempricefield
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :12-1-2017
	 *DESCRIPTION :Field validation - Item price 
	 ********************************************************/
	public void itempricefield() throws Exception 
	{
		if(ReusableComponents.isElementPresent(driver, Mobile_appFieldvalidation.itempricefield_loc, Mobile_appFieldvalidation.itempricefield)){
			String itempricefield = ReusableComponents.getText(driver, Mobile_appFieldvalidation.itempricefield_loc,Mobile_appFieldvalidation.itempricefield);
			 System.out.println(itempricefield);
			 report.updateTestLog("Item Price", "Item Price field is present", Status.PASS);}
		else
			  report.updateTestLog("Item name", "Item Price field is not present", Status.FAIL);
		}
	
	/********************************************************
	 *FUNCTION    :savingsfield
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :12-1-2017
	 *DESCRIPTION :Field validation - savings 
	 ********************************************************/
	public void savingsfield() throws Exception 
	{
		if(ReusableComponents.isElementPresent(driver, Mobile_appFieldvalidation.savingsfield_loc, Mobile_appFieldvalidation.savingsfield)){
			String savingsfield = ReusableComponents.getText(driver, Mobile_appFieldvalidation.savingsfield_loc,Mobile_appFieldvalidation.savingsfield);
			 System.out.println(savingsfield);
			 report.updateTestLog("savings field", "savings field is present", Status.PASS);}
		else
			  report.updateTestLog("savings field", "savings field is not present", Status.FAIL);
	}
	
	/********************************************************
	 *FUNCTION    :itemtotalfield
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :12-1-2017
	 *DESCRIPTION :Field validation - Item total
	 ********************************************************/
	public void itemtotalfield() throws Exception 
	{
		if(ReusableComponents.isElementPresent(driver, Mobile_appFieldvalidation.itemtotalfield_loc, Mobile_appFieldvalidation.itemtotalfield)){
			String itemtotalfield = ReusableComponents.getText(driver, Mobile_appFieldvalidation.itemtotalfield_loc,Mobile_appFieldvalidation.itemtotalfield);
			 System.out.println(itemtotalfield);
			 report.updateTestLog("item total field", "item total field is present", Status.PASS);}
		else
			  report.updateTestLog("item total field", "item total field is not present", Status.FAIL);
		}
	
	/********************************************************
	 *FUNCTION    :overallTotalfield
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :12-1-2017
	 *DESCRIPTION :Field validation - Overall Total
	 ********************************************************/
	public void overallTotalfield() throws Exception 
	{
		if(ReusableComponents.isElementPresent(driver, Mobile_appFieldvalidation.overallTotalfield_loc, Mobile_appFieldvalidation.overallTotalfield)){
			String overallTotalfield = ReusableComponents.getText(driver, Mobile_appFieldvalidation.overallTotalfield_loc,Mobile_appFieldvalidation.overallTotalfield);
			 System.out.println(overallTotalfield);
			 report.updateTestLog("overall Total field", "overall Total field is present", Status.PASS);}
		else
			  report.updateTestLog("overall Total field", "overall Total field is not present", Status.FAIL);
		}
	
	/********************************************************
	 *FUNCTION    :overallsavingsfield
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :12-1-2017
	 *DESCRIPTION :Field validation - Overall savings
	 ********************************************************/ 
	public void overallsavingsfield() throws Exception 
	{
		if(ReusableComponents.isElementPresent(driver, Mobile_appFieldvalidation.overallsavingsfield_loc, Mobile_appFieldvalidation.overallsavingsfield)){
			String overallsavings = ReusableComponents.getText(driver, Mobile_appFieldvalidation.overallsavingsfield_loc,Mobile_appFieldvalidation.overallsavingsfield);
			 System.out.println(overallsavings);
			 report.updateTestLog("overall savings field", "overall savings is "+overallsavings, Status.PASS);}
		else
			  report.updateTestLog("overall savings field", "overall savings is not present", Status.FAIL);
		}
	
	/********************************************************
	 *FUNCTION    :overallsavingsfield_trolleypage
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :2-3-2017
	 *DESCRIPTION :Field validation - Overall savings
	 ********************************************************/ 
	public void overallsavingsfield_trolleypage() throws Exception 
	{
		if(ReusableComponents.isElementPresent(driver, Mobile_appFieldvalidation.overallsavingsfieldtrolley_loc, Mobile_appFieldvalidation.overallsavingsfieldtrolley)){
			String overallsavingsTrolleypage = ReusableComponents.getText(driver, Mobile_appFieldvalidation.overallsavingsfieldtrolley_loc,Mobile_appFieldvalidation.overallsavingsfieldtrolley);
			 System.out.println(overallsavingsTrolleypage);
			 report.updateTestLog("overall savings field", "overall savings is "+overallsavingsTrolleypage, Status.PASS);}
		else
			  report.updateTestLog("overall savings field", "overall savings is not present", Status.FAIL);
		}
	
	/********************************************************
	 *FUNCTION    :noOfItems
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :12-1-2017
	 *DESCRIPTION :Getting the number of items added to the trolley
	 ********************************************************/
	public String getNoOfItems() throws Exception 
	{
		String numberofItems = null;
		if(ReusableComponents.isElementPresent(driver, Mobile_appFieldvalidation.itemQty_loc, Mobile_appFieldvalidation.itemQty)){
		numberofItems = ReusableComponents.getText(driver, Mobile_appFieldvalidation.itemQty_loc,Mobile_appFieldvalidation.itemQty);
			 System.out.println(numberofItems);
			 report.updateTestLog("Item quantity", "Item quantity is"+numberofItems, Status.PASS);}
		else{
			  report.updateTestLog("Item quantity", "Item quantity is not present", Status.FAIL);
		}
	return numberofItems;
	}
	
	
	/********************************************************
	 *FUNCTION    :noOfItemsTrolleyPage
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :10-2-2017
	 *DESCRIPTION :Getting the number of items added to the trolley in the trolley page
	 ********************************************************/
	public void noOfItemsTrolleyPage() throws Exception 
	{
		if(ReusableComponents.isElementPresent(driver, Mobile_appFieldvalidation.itemQtyTrolleypage_loc, Mobile_appFieldvalidation.itemQtyTrolleypage)){
			String numberofItemsTrolleyPage = ReusableComponents.getText(driver, Mobile_appFieldvalidation.itemQtyTrolleypage_loc,Mobile_appFieldvalidation.itemQtyTrolleypage);
			 System.out.println(numberofItemsTrolleyPage);
			 report.updateTestLog("Item quantity Trolley page", "Item quantity is"+numberofItemsTrolleyPage, Status.PASS);}
		else
			  report.updateTestLog("Item quantity Trolley page", "Item quantity is not present", Status.FAIL);
		}
	/********************************************************
	 *FUNCTION    :verifyItemPrice
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :12-1-2017
	 *DESCRIPTION :Verifying the item price with actual price 
	 ********************************************************/
	public void verifyItemPrice() throws Exception 
	{
		//String price = driver.findElement(By.xpath("//android.widget.LinearLayout[@index='3']/android.widget.TextView[@index='0']")).getText();   
		//getItemPrice();
		String price = ReusableComponents.getText(driver, Mobile_appFieldvalidation.itemprice_Loc,Mobile_appFieldvalidation.itemprice);
		System.out.println("The item Price is: " + price);
		String Actualprice = dataTable.getData("Items" , "ProductPrice");
		System.out.println("The original Price is: " + Actualprice);
		if(Actualprice.equals(price))
		{
			report.updateTestLog("Item Price", "item price is "+price,Status.PASS);}
		else{
			report.updateTestLog("Item Price","Item price is not matched", Status.FAIL);
		}
	}
	
	
	/********************************************************
	 *FUNCTION    :verifyItemPrice_OfferPrice
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :15-2-2017
	 *DESCRIPTION :Verifying the item price with actual price including offer
	 ********************************************************/
	public void verifyItemPrice_OfferPrice() throws Exception 
	{
		
		
		if((dataTable.getData("Items" , "ProductOfferTotal").isEmpty())){
		verifyItemPrice();}
		else{
			 verifyItemPrice();
			 String Reducedprice = dataTable.getData("Items" , "ProductOfferTotal");
		     System.out.println("The Reduced Price is: " + Reducedprice);
		     String itemReducedPrice = ReusableComponents.getText(driver, Mobile_appFieldvalidation.itemTotal_Loc,Mobile_appFieldvalidation.itemTotal);
			 System.out.println("The item total is: " + itemReducedPrice);
			 if(Reducedprice.equals(itemReducedPrice))
			    {
			       report.updateTestLog("Item Reduced Price", "item Reduced price is "+itemReducedPrice,Status.PASS);
			    }
			 else
				  report.updateTestLog("Item Reduced Price","Item Reduced price is not matched", Status.FAIL); 
		}	
    }
	
	/********************************************************
	 *FUNCTION    :getItemPrice
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :12-1-2017
	 *DESCRIPTION :getting the Item price  
	 ********************************************************/
	public void getItemPrice()
	{
		String price = ReusableComponents.getText(driver, Mobile_appFieldvalidation.itemprice_Loc,Mobile_appFieldvalidation.itemprice);
	    System.out.println("The item Price is: " + price);
	}
	
	/********************************************************
	 *FUNCTION    :getitemsavings
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :12-1-2017
	 *DESCRIPTION :getting the Item savings  
	 ********************************************************/
	public void getitemsavings() throws Exception 
	{
		//String price = driver.findElement(By.xpath("//android.widget.LinearLayout[@index='3']/android.widget.TextView[@index='0']")).getText();   
	    String itemsaving = ReusableComponents.getText(driver, Mobile_appFieldvalidation.itemsaving_Loc,Mobile_appFieldvalidation.itemsaving);
	    System.out.println("The item Savings is: " + itemsaving);
	    report.updateTestLog("Item Saving", "item saving is "+itemsaving,Status.PASS);  
    }
	
	/********************************************************
	 *FUNCTION    :getitemTotal
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :12-1-2017
	 *DESCRIPTION :getting the individual Item total  
	 ********************************************************/
	
	public void getitemTotal() throws Exception 
	{
		//String price = driver.findElement(By.xpath("//android.widget.LinearLayout[@index='3']/android.widget.TextView[@index='0']")).getText();   
	    String itemtotal = ReusableComponents.getText(driver, Mobile_appFieldvalidation.itemTotal_Loc,Mobile_appFieldvalidation.itemTotal);
	    System.out.println("The item total is: " + itemtotal);
	    report.updateTestLog("Item total", "item total is "+itemtotal,Status.PASS);  
    }
	
	/********************************************************
	 *FUNCTION    :getOverallTotal
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :12-1-2017
	 *DESCRIPTION :getting the overall Item total  
	 ********************************************************/
	
	public void getOverallTotal() throws Exception 
	{
		//String price = driver.findElement(By.xpath("//android.widget.LinearLayout[@index='3']/android.widget.TextView[@index='0']")).getText();   
	    String overalltotal = ReusableComponents.getText(driver, Mobile_appFieldvalidation.overalltotal_Loc,Mobile_appFieldvalidation.overalltotal);
	    System.out.println("The overall total is: " + overalltotal);
	    report.updateTestLog("Overall total", "overall total is "+overalltotal,Status.PASS);  
    }
	

	/********************************************************
	 *FUNCTION    :getTrolleyPageOverallTotal
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :10-2-2017
	 *DESCRIPTION :getting the overall Item total from the trolley page
	 ********************************************************/
	
	public void getTrolleyPageOverallTotal() throws Exception 
	{
		//String price = driver.findElement(By.xpath("//android.widget.LinearLayout[@index='3']/android.widget.TextView[@index='0']")).getText();   
	    String TrolleyPageoveralltotal = ReusableComponents.getText(driver, Mobile_appFieldvalidation.trolleypageoveralltotal_Loc,Mobile_appFieldvalidation.trolleypageoveralltotal);
	    System.out.println("The overall total is: " + TrolleyPageoveralltotal);
	    report.updateTestLog("Overall total", "overall total is "+TrolleyPageoveralltotal,Status.PASS);  
    }
	
	/********************************************************
	 *FUNCTION    :verifyTrolleyPageOverallTotal
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :2-3-2017
	 *DESCRIPTION :verify the overall Item total from the trolley page
	 ********************************************************/
	
	public void verifyTrolleyPageOverallTotal() throws Exception 
	{
		if(ReusableComponents.isElementPresent(driver, Mobile_appFieldvalidation.trolleypageoveralltotal_Loc,Mobile_appFieldvalidation.trolleypageoveralltotal)){
			String TrolleyPageoveralltotal = ReusableComponents.getText(driver, Mobile_appFieldvalidation.trolleypageoveralltotal_Loc,Mobile_appFieldvalidation.trolleypageoveralltotal);
			System.out.println("The overall total is: " + TrolleyPageoveralltotal);
			String[] Total = TrolleyPageoveralltotal.split(": ");
			System.out.println("The TrolleyPageoveralltotal is: " + Total[1]);
			String Overalltotal = dataTable.getData("Items" , "OverallTotal");  
			System.out.println("The overallTotal is: " + Overalltotal);
			if(Overalltotal.equals(Total[1]))
			{
				report.updateTestLog("Overall Total", "Overall Total is "+TrolleyPageoveralltotal,Status.PASS);}
			else
				report.updateTestLog("Overall Total","Overall Total is not matched", Status.FAIL);
		}
	}

	
	/********************************************************
	 *FUNCTION    :getOfferDescription
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :10-2-2017
	 *DESCRIPTION :getting the offer description
	 ********************************************************/
	
	public void getOfferDescription() throws Exception 
	{
		if(ReusableComponents.isElementPresent(driver, Mobile_appFieldvalidation.offerDescription_loc, Mobile_appFieldvalidation.offerDescription))
		{
			String offerdescription = ReusableComponents.getText(driver, Mobile_appFieldvalidation.offerDescription_loc,Mobile_appFieldvalidation.offerDescription);
			if (offerdescription.equals(""))
			   { 
				  System.out.println(offerdescription+"Null");
				  report.updateTestLog("Offer Description", "Offer Description is Null", Status.PASS);
			   }
			else
		       {
			     System.out.println(offerdescription);
                 report.updateTestLog("Offer Description", "Offer Description is"+offerdescription, Status.PASS);
		       }
		 }
		else 
		 {
			report.updateTestLog("Offer Description", "Offer Description is", Status.FAIL);
	     }
			
	}
	
	}
		



