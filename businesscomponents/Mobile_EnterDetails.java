package businesscomponents;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uimap.Header;
import uimap.HomePage;
import uimap.M_EnterDetails;
import uimap.OfBranchDeliverydate;
import uimap.OfLogin;
import uimap.UserRegisteration;
import uimap.M_Login;

import allocator.Allocator;

import com.cognizant.framework.Status;
//import com.thoughtworks.selenium.SeleniumException;

public class Mobile_EnterDetails extends ReusableLibrary {

	public Mobile_EnterDetails(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	
	/********************************************************
	 *FUNCTION    :enterdetails_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :27-12-2016
	 *DESCRIPTION :entering the details
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void enterdetails_mob() throws InterruptedException{

		ReusableComponents.typeinEditbox(driver,M_EnterDetails.txtTitle_loc,M_EnterDetails.txtTitle,dataTable.getData("General_Data" , "Title"));
		ReusableComponents.typeinEditbox(driver,M_EnterDetails.txtFirstname_loc,M_EnterDetails.txtFirstname,dataTable.getData("General_Data" , "FirstName"));
		ReusableComponents.typeinEditbox(driver,M_EnterDetails.txtLastname_loc,M_EnterDetails.txtLastname,dataTable.getData("General_Data" , "LastName"));
		String nick = dataTable.getData("General_Data" , "NickName");
		ReusableComponents.typeinEditbox(driver,M_EnterDetails.txtnickname_loc,M_EnterDetails.txtnickname, nick);
		ReusableComponents.typeinEditbox(driver,M_EnterDetails.txtPostcode_loc,M_EnterDetails.txtPostcode,dataTable.getData("General_Data" , "PostCode1"));
		ReusableComponents.clickButton(driver, M_EnterDetails.btnAddr_loc, M_EnterDetails.btnAddr);
		Thread.sleep(5000);
		ReusableComponents.clickLink(driver, M_EnterDetails.selectaddress_loc, M_EnterDetails.selectaddress );
		ReusableComponents.typeinEditbox(driver,M_EnterDetails.txtPhoneNo_loc,M_EnterDetails.txtPhoneNo,dataTable.getData("General_Data" , "Phone1"));
		ReusableComponents.clickButton(driver, M_EnterDetails.btnJoinmyWr_loc, M_EnterDetails.btnJoinmyWr);
		Thread.sleep(8000);
		report.updateTestLog("New User details Registration", "New User joined the waitrose by updating details", Status.PASS);	
}
	
	/********************************************************
	 *FUNCTION    :AddNewAddress_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :27-12-2016
	 *DESCRIPTION :entering the details for new address
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void addNewAddress_mob() throws InterruptedException{
	ReusableComponents.typeinEditbox(driver,M_EnterDetails.txtTitlenew_loc,M_EnterDetails.txtTitlenew,dataTable.getData("General_Data" , "Title"));
	ReusableComponents.typeinEditbox(driver,M_EnterDetails.txtFirstnamenew_loc,M_EnterDetails.txtFirstnamenew,dataTable.getData("General_Data" , "FirstName"));
	ReusableComponents.typeinEditbox(driver,M_EnterDetails.txtLastnamenew_loc,M_EnterDetails.txtLastnamenew,dataTable.getData("General_Data" , "LastName"));
	ReusableComponents.typeinEditbox(driver,M_EnterDetails.txtnicknamenew_loc,M_EnterDetails.txtnicknamenew,dataTable.getData("General_Data" , "NickName"));
	ReusableComponents.typeinEditbox(driver,M_EnterDetails.txtPostcodenew_loc,M_EnterDetails.txtPostcodenew,dataTable.getData("General_Data" , "PostCode1"));
	ReusableComponents.clickButton(driver, M_EnterDetails.btnAddrnew_loc, M_EnterDetails.btnAddrnew);
	Thread.sleep(5000);
	ReusableComponents.clickLink(driver, M_EnterDetails.selectaddressnew_loc, M_EnterDetails.selectaddressnew );
	ReusableComponents.typeinEditbox(driver,M_EnterDetails.txtPhoneNonew_loc,M_EnterDetails.txtPhoneNonew,dataTable.getData("General_Data" , "Phone1"));
	ReusableComponents.clickButton(driver, M_EnterDetails.btnSaveAddressnew_loc, M_EnterDetails.btnSaveAddressnew);
	report.updateTestLog("New User details Registration", "New User joined the waitrose by updating details", Status.PASS);	
	
}
}