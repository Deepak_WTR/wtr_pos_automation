package businesscomponents;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import com.cognizant.framework.TestParameters;

import supportlibraries.DriverScript;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.SeleniumTestParameters;
import supportlibraries.TestCase;


public class XL extends ReusableLibrary {


	public XL(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}



	String testcaseName;


	
	public  void putOrderNo(String sheetName, String OrdNo) throws IOException
	
	{
		testcaseName = dataTable.getData("Items","TC_ID");
	    
		String userDir = System.getProperty("user.dir");
		
		System.out.println(testcaseName);
		Properties properties = new Properties();
		properties.load(new FileInputStream(userDir+"/Global Settings.properties"));
	    String workbookName = properties.getProperty("Workbook_Name")+".xls";
		
	    System.out.println(workbookName);
	    
		String putDataSheetPath;
	
	    putDataSheetPath = userDir+"\\Datatables\\"+workbookName;
	


		String osName = System.getProperty("os.name");

		if(osName.contains("Mac"))
		{
			putDataSheetPath= putDataSheetPath.replaceAll("\\\\", "//");
		}



		FileInputStream fin = new FileInputStream(putDataSheetPath); 
		HSSFWorkbook wb = new HSSFWorkbook(fin); 
		HSSFSheet ws;


		ws = wb.getSheet(sheetName);


		HSSFRow firstRow= ws.getRow(0);





		int maxCol = firstRow.getLastCellNum();
		int maxRow = ws.getLastRowNum();

		Cell myCell;
		int myRow = 0;
		int myCol = 0;

		try{

			// Finding my TC_ID row

			for (myRow=0;myRow<=maxRow;myRow++)
			{

				Cell cell =  ws.getRow(myRow).getCell(0);

				if(cell.getStringCellValue().equals(testcaseName))
				{

					break;

				}
			}

			// Finding my Trasc_ID column

			for (myCol=0;myCol<=maxCol;myCol++)
			{

				Cell cell =  ws.getRow(0).getCell(myCol);

				if(cell.getStringCellValue().equals("Transaction_Number"))
				{

					break;

				}
			}

		} catch(Exception e)

		{
			System.out.println("Exception in reading the data");

		}



		try{
			// write your data into the cell (myRow, myCol)

			FileOutputStream fos  =new FileOutputStream(putDataSheetPath);


			myCell = ws.getRow(myRow).createCell(myCol);
			myCell = ws.getRow(myRow).getCell(myCol);

			myCell.setCellValue(OrdNo);

			System.out.println("TranscID Put Data successful");



			wb.write(fos);	

			// wb.close();
			fos.close();
			fos.flush();
			fin.close();
		}catch(Exception e)
		{

			System.out.println("Exception in writing the transc ID");
		}







	}

}










