package businesscomponents;

import java.io.File;
import java.io.IOException;
import java.sql.Connection; 
import java.sql.DriverManager; 
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import supportlibraries.Asserts;
import supportlibraries.DriverScript;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


import uimap.Header;
import uimap.Interstetials;
import uimap.JotterFrame;
import uimap.Favourites;
import uimap.HomePage;
import uimap.Login;
import uimap.MainTrolley;
import uimap.MiniTrolley;
import uimap.MyAccountPage;
import uimap.OrderReview;
import uimap.MyList;
import uimap.ProductPortrait;
import uimap.RecipesPage;
import uimap.UserRegisteration;

//import Datatables.CommonData;
import allocator.Allocator;
import businesscomponents.ReusableComponents;
//import com.UtilityFunctions.FindBy;
import com.cognizant.framework.Status;
import com.cognizant.framework.Util;
//import com.thoughtworks.selenium.SeleniumException;

public class DB extends ReusableLibrary {
	private Asserts asserts = new Asserts();
	public static String ForumName="";


	public DB(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

     public static void main(String  rgs[]) throws Exception 
     { 
    	 try {
    		 Class.forName("com.ibm.db2.jcc.DB2Driver");
    		 }
    		  catch (ClassNotFoundException e) {
    		  System.out.println("Please include Classpath  where your DB2 Driver is located");
    		  e.printStackTrace();
    		  return;
    		  }
    		 System.out.println("DB2 driver is loaded successfully");
    		 Connection conn = null;
    		 PreparedStatement pstmt = null;
    		 ResultSet rset=null;
    		 boolean found=false;
    		 try {
    		 conn = DriverManager.getConnection("jdbc:db2://UAAWECD1:50060/dawec00","infos00r","gl4c13r"); 
    		     if (conn != null)
    		 {
    		    System.out.println("DB2 Database Connected");
    		 }
    		    else
    		 {
    		       System.out.println("Db2 connection Failed ");
    		  }
        
        pstmt=conn.prepareStatement("select * from lwecapp.ORDERS where ORDERS_ID = 23753184");
        rset=pstmt.executeQuery();
          if(rset!=null)
        {

         while(rset.next())
        {
         found=true;
         //System.out.println("Class Code: "+rset.getString("clcode"));
         System.out.println("Total Price in Db: "+rset.getString("totalproduct"));
        }
        }
        if (found ==false)
        {
        System.out.println("No Information Found");
        }
                
        rset.close();
        pstmt.close();
        conn.close();
        
        } catch (SQLException e) {
        System.out.println("DB2 Database connection Failed");
        e.printStackTrace();
        return;
        }
        
    	
    } 
      /*
     private static Connection  getConnection() throws ClassNotFoundException, SQLException 
     { 
        //Class. forName ( "COM.ibm.db2os390.sqlj.jdbc.DB2SQLJDriver"  ); 
    	 Class.forName("com.ibm.db2.jcc.DB2Driver");
        Connection  connection = 
                DriverManager.getConnection("jdbc:db2://UAAWECD1:50060/dawec00","infos00r","gl4c13r"); 
      
        System. out .println( "From DAO, connection obtained " );
        return connection;
     } 
     */
     //
     //public static void main(String  rgs[]) throws Exception
     public void executePutty()
     {
    	 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    	 Date date = new Date();
    	 System.out.println(dateFormat.format(date));
    	 report.updateTestLog("Redate order in Putty","Started" , Status.DONE);
    	 try {
    		 try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Process proc1 = Runtime.getRuntime().exec( "C:\\Work\\EggPlantPoc\\putty.exe");
			
			Process proc2 = Runtime.getRuntime().exec( "C:\\Work\\EggPlantPoc\\AutoIT\\Putty.exe "+dateFormat.format(date)+" "+Allocator.GlblVar);
			
			try {
				Thread.sleep(52000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			report.updateTestLog("Redate order in Putty for order "+ Allocator.GlblVar,"Completed successfully" , Status.PASS);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
     }
}