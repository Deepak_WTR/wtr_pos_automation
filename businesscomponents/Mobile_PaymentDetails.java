package businesscomponents;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uimap.Header;
import uimap.HomePage;
import uimap.M_EnterDetails;
import uimap.M_BookSlot;
import uimap.M_PaymentDetails;
import uimap.M_SearchProduct;
import uimap.OfBranchDeliverydate;
import uimap.OfLogin;
import uimap.UserRegisteration;
import uimap.M_Login;
import allocator.Allocator;

import com.cognizant.framework.Status;
//import com.thoughtworks.selenium.SeleniumException;

public class Mobile_PaymentDetails extends ReusableLibrary{

	public Mobile_PaymentDetails(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}


	/********************************************************
	 *FUNCTION    :clickContoPayment_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :29-12-2016
	 *DESCRIPTION :click Continue to Payment details button 
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void clickContoPayment_mob() throws InterruptedException {
		if (ReusableComponents.isElementPresent(driver, M_PaymentDetails.btnContinue_loc, M_PaymentDetails.btnContinue)){
			ReusableComponents.clickButton(driver, M_PaymentDetails.btnContinue_loc, M_PaymentDetails.btnContinue);	
			Thread.sleep(20000);
		    report.updateTestLog("Start Shopping", "Start Shopping button is clicked", Status.PASS);}
		else
			  report.updateTestLog("Start Shopping", "Start Shopping button is not clicked", Status.FAIL);
	}
	
	/********************************************************
	 *FUNCTION    :enterCardDetails_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :29-12-2016
	 *DESCRIPTION :Card details filling and clicking the place your button 
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void enterCardDetails_mob() throws InterruptedException {
		String cardnumber = dataTable.getData("General_Data" , "CardNumber_mob");
		System.out.println(cardnumber);
		ReusableComponents.typeinEditbox(driver, M_PaymentDetails.txtcardno_loc, M_PaymentDetails.txtcardno,cardnumber);
		String month = dataTable.getData("General_Data","ExpireMonth_mob");
		System.out.println(month);
		String year = dataTable.getData("General_Data","ExpireYear_mob");
		System.out.println(year);
		String name = dataTable.getData("General_Data","NameOnCard_mob");
		System.out.println(name);
		ReusableComponents.SelectValuefromDropDown(driver,M_PaymentDetails.dropdownmonth_loc,M_PaymentDetails.dropdownmonth,month);
		ReusableComponents.SelectValuefromDropDown(driver,M_PaymentDetails.dropdownyear_loc,M_PaymentDetails.dropdownyear,year);
		ReusableComponents.typeinEditbox(driver, M_PaymentDetails.txtcardname_loc, M_PaymentDetails.txtcardname,name);
		ReusableComponents.isElementPresent(driver, M_PaymentDetails.enablecard_loc, M_PaymentDetails.enablecard);
		ReusableComponents.clickButton(driver, M_PaymentDetails.enablecard_loc, M_PaymentDetails.enablecard);
		 report.updateTestLog("Card details ", "Card details entered successfully", Status.PASS);
	}
	
	/********************************************************
	 *FUNCTION    :clickPlaceYourOrder_mob
	 *AUTHOR      :Chandru Kumar 
	 *DATE        :29-12-2016
	 *DESCRIPTION :click Place Your Button  
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void clickPlaceYourOrder_mob() throws InterruptedException {
		if(ReusableComponents.isElementPresent(driver, M_PaymentDetails.placeorder_loc, M_PaymentDetails.placeorder)){
			   ReusableComponents.clickButton(driver, M_PaymentDetails.placeorder_loc, M_PaymentDetails.placeorder);
			   Thread.sleep(5000);
			   report.updateTestLog("place your order", "place your order button is clicked", Status.PASS);}
		    else 
			   report.updateTestLog("place your order", "place your order button is not clicked", Status.FAIL);
	}



/********************************************************
 *FUNCTION    :clickPlaceYourOrder_mob
 *AUTHOR      :Chandru Kumar 
 *DATE        :29-12-2016
 *DESCRIPTION :click Place Your Button  
 * @throws InterruptedException 
 * @throws IOException 
 ********************************************************/

public void getReferenceNumber_mob() throws InterruptedException, IOException {
	if(ReusableComponents.isElementPresent(driver, M_PaymentDetails.getReferenceno_loc, M_PaymentDetails.getReferenceno)){
		String  refnum =  ReusableComponents.getText(driver, M_PaymentDetails.getReferenceno_loc, M_PaymentDetails.getReferenceno);
		System.out.println(refnum);
		 
		   Thread.sleep(5000);
		   report.updateTestLog("Reference Number", "Reference Number is retrieved", Status.PASS);}
	    else 
		   report.updateTestLog("Reference Number", "Reference Number is not retrieved", Status.FAIL);
}

}


