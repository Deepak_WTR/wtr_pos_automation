package businesscomponents;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uimap.Header;
import uimap.OfLogin;
import uimap.UserRegisteration;
import uimap.M_Login;
import allocator.Allocator;

import com.cognizant.framework.Status;
//import com.thoughtworks.selenium.SeleniumException;

public class Mobile_HomePage extends ReusableLibrary {

public Mobile_HomePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}


/********************************************************
 *FUNCTION    :openApp_mob
 *AUTHOR      :Chandru Kumar 
 *DATE        :27-12-2016
 *DESCRIPTION :Launching the application
 * @throws InterruptedException 
 ********************************************************/

public void openApp_mob() throws InterruptedException{

	String url = dataTable.getData("General_Data" , "URL_Mob");
	System.out.println(url);
	driver.get(url);
	Thread.sleep(20000);

}



}



