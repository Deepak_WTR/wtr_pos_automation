package businesscomponents;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;


public class Selenium_RL_latest {
	
	private WebDriver driver;
	private String baseUrl;
	DesiredCapabilities capability;
	String errorMsg = "Thanks for Suggesting New route to us.";

	@Before
	public void setUp() throws Exception {
		
		//IE
		//System.setProperty("webdriver.ie.driver","D:\\ProjectWorkSpace\\TestProject\\src\\ieDriver\\IEDriverServer.exe");
		//System.setProperty("webdriver.ie.driver","D:\\ProjectWorkSpace\\TestProject\\ieDriver\\IEDriverServer.exe");
		//System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"/ieDriver/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver","D:\\Drivers\\BrowserDrivers\\IEDriverServer.exe");
		capability = DesiredCapabilities.internetExplorer();
		capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		capability.setCapability("ignoreProtectedModeSettings", true);
		capability.setJavascriptEnabled(true);
		driver = new InternetExplorerDriver(capability);
		
		//Chrome
		/*System.setProperty("webdriver.chrome.driver","D:\\ProjectWorkSpace\\TestProject\\src\\ieDriver\\chromedriver.exe");
		capability = DesiredCapabilities.chrome();
		//capability.setCapability("chrome.binary", ChromeBinaryPath);
        capability.setJavascriptEnabled(true);
        driver = new ChromeDriver();*/
				
		//driver = new FirefoxDriver();
		baseUrl = "http://www.ralphlauren.co.jp/";
		//baseUrl = "http://www.ticketgoose.com/";
	}

	@Test
	public void test() {
		
		try{
			String pageTitle = "Ralph Lauren Japan";
			driver.navigate().to(baseUrl);
			driver.manage().window().maximize();
			Thread.sleep(2000);
			//driver.findElement(By.xpath("//a[@id='WC_CachedHeaderDisplay_links_1']/span")).click();
			driver.findElement(By.xpath("/html/body/div/div/div/ul/li/a/span[2]")).click();
			Thread.sleep(3000);
			String textValue1 = driver.findElement(By.className("magazine_tt")).getText();
			System.out.println("Text Value :" + textValue1);
			highlightElement(driver, driver.findElement(By.className("magazine_tt")));
			driver.navigate().back();
			Thread.sleep(4000);
			driver.navigate().forward();
			Thread.sleep(4000);
			String title = driver.getTitle();
			System.out.println("Title:"+ title);
			if(title != null && title.contains(pageTitle))
				System.out.println("Page Assertion: PASS");
			else
				System.out.println("Page Assertion: FAIL");
			driver.close();
			
			
			//Window handles
			/*String strParentTitle = driver.getTitle();
			String strParentWindow,strChildWindow = null;
			java.util.Set<String> handles;
			handles = driver.getWindowHandles();
			String[] strHandles = new String[2];
			int i = 0;
			for (String handle : handles) {
	              strHandles[i++] = new String(handle);
	        }
			driver.switchTo().window(strHandles[0]);
			if (driver.getTitle().equals(strParentTitle)) {
	              strParentWindow = strHandles[0];
	              strChildWindow = strHandles[1];
	        }
	        driver.switchTo().window(strChildWindow);*/
	        //
	        
	        //Ticket goose;
			/*driver.navigate().to("http://www.ticketgoose.com/");
			driver.manage().window().maximize();
			Thread.sleep(2000);
			WebElement select = driver.findElement(By.id("fs"));
		    List<WebElement> options = select.findElements(By.tagName("option"));
		    for (WebElement option : options) {
		        if("Chennai".equals(option.getText())){
		            option.click();
		            break;
		        }
		      
		    }
		    String val = driver.findElement(By.id("fs")).getAttribute("class");
		    System.out.println("VALUE:" + val);
		    
		    Thread.sleep(2000);
		    WebElement select1 = driver.findElement(By.id("ts"));
		    List<WebElement> options1 = select1.findElements(By.tagName("option"));
		    for (WebElement option : options1) {
		        if("Coimbatore".equals(option.getText())){
		            option.click();
		        	break;
		        }
		    }
		    Thread.sleep(1000);
		    driver.findElement(By.id("tdate")).click();
		    Thread.sleep(1000);
		    driver.findElement(By.partialLinkText("14")).click();
		    Thread.sleep(1000);
		    driver.findElement(By.id("hsearch")).click();
		    Thread.sleep(10000);
		    String pageTitle1 = driver.getTitle();
		    System.out.println("pageTitle1:"+pageTitle1);
		   
		    driver.findElement(By.partialLinkText("Suggest a Route")).click();
		    Thread.sleep(1000);
		    //driver.findElement(By.xpath("//div[contains(@id,'sugrout')]//div[contains(@class,'sugrout')]/form/ul/li[1]/input[@id='from']")).sendKeys("Chennai");
		    Thread.sleep(1000);
		    driver.findElement(By.id("from")).sendKeys("Chennai");
		    Thread.sleep(1000);
		    driver.findElement(By.id("to")).sendKeys("Coimbatore");
		    Thread.sleep(1000);
		    driver.findElement(By.id("operator")).sendKeys("Parveen Travels");
		    Thread.sleep(1000);
		    driver.findElement(By.id("phone")).sendKeys("9990004444");
		    Thread.sleep(1000);
		    driver.findElement(By.id("email")).sendKeys("Testing@test.com");
		    Thread.sleep(2000);
		    driver.findElement(By.xpath("//div[contains(@id,'sugrout')]//div[contains(@class,'sugrout')]/form/center[3]/input[@name='event']")).click();
		    //driver.findElement(By.name("event")).click();
		    Thread.sleep(2000);
		    String verifyText = driver.findElement(By.xpath("//div[contains(@id,'sugrout')]//div[contains(@class,'sugrout')]/form/center[2]/span[@id='errorMsg']")).getText();
			System.out.println("VerifyText:"+verifyText);
			if(verifyText != null && errorMsg.equalsIgnoreCase(verifyText)){
				System.out.println("Scenario: PASS" );
			}else{
				System.out.println("Scenario: FAIL" );
			}*/
				
			/*Select select = new Select(driver.findElement(By.id("fs")));
			Thread.sleep(2000);
			select.selectByVisibleText("Chennai");
			Thread.sleep(3000);*/
			
		}catch(Exception e){
			e.getMessage();
		}
	}
	
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
	
	public void highlightElement(WebDriver driver, WebElement element) {
	    for (int i = 0; i < 3; i++) {
	        JavascriptExecutor js = (JavascriptExecutor) driver;
	        js.executeScript("arguments[0].setAttribute('style', arguments[1]);",
	                element, "color: red; border: 5px solid red;");
	        try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        js.executeScript("arguments[0].setAttribute('style', arguments[1]);",
	                element, "");
	    }
	}
}
