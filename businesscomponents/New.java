package businesscomponents;



import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.junit.*;

import static org.junit.Assert.*;
//import static org.hamcrest.CoreMatchers.*;

import org.openqa.selenium.*;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class New {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    //driver = new FirefoxDriver();
   System.setProperty("webdriver.chrome.driver",
			new File(System.getProperty("user.dir")).getAbsolutePath()+"\\Miscellaneous\\drivers\\chromedriver.exe");
driver = new ChromeDriver();
	  //driver = new RemoteWebDriver(new URL("http://10.20.27.253:3001/wd/hub"), DesiredCapabilities.ipad());
    baseUrl = "https://wtr-mob-test4.tiffani.co.uk";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testX() throws Exception {
    driver.get(baseUrl + "/");
    JOptionPane.showMessageDialog(null, "Action", "Please perform required action", JOptionPane.INFORMATION_MESSAGE);
	  //String x=driver.findElement(By.cssSelector("button.mblButton.greenButton.SlotBookingButton")).getAttribute("outerHTML");
    //String x=driver.findElement(By.cssSelector("input.mblTextBox")).getAttribute("outerHTML");
   // System.out.println(x);
    //driver.findElement(By.cssSelector("input.mblTextBox")).sendKeys("milk");
    
    List<WebElement> Elemnt = driver.findElements(By.cssSelector("button.mblButton.whiteButton"));
    for(WebElement ele : Elemnt){
        System.out.println(ele.getTagName());
        System.out.println(ele.getAttribute("innerHTML"));
       if(ele.getAttribute("innerHTML").equals("My Orders")){
    	   ele.click();
       }
    }
    Thread.sleep(5000);
    if(driver.findElement(By.cssSelector("input.mblTextBox.loginEnterUsername")).isDisplayed()){
    	driver.findElement(By.cssSelector("input.mblTextBox.loginEnterUsername")).sendKeys("rabbani@wtr-test.net");
    	driver.findElement(By.cssSelector("input.mblTextBox.loginEnterPassword")).sendKeys("user1234");
    	
    	Elemnt = driver.findElements(By.cssSelector("button.mblButton.greenButton"));
    	    for(WebElement ele : Elemnt){
    	        System.out.println(ele.getTagName());
    	        System.out.println(ele.getAttribute("innerHTML"));
    	       if(ele.getAttribute("innerHTML").equals("Login")){
    	    	   ele.click();
    	       }
    	    }
    }
    Thread.sleep(15000);
    //driver.findElement(By.cssSelector("input.mblTextBox")).getAttribute("outerHTML");
    
    /*driver.findElement(By.id("logonId")).sendKeys("rabbani@wtr-test.net");
    driver.findElement(By.id("logonPassword")).clear();
    driver.findElement(By.id("logonPassword")).sendKeys("user1234");
    driver.findElement(By.cssSelector("fieldset > input[type=\"submit\"]")).click();
   
    JOptionPane.showMessageDialog(null, "Action", "Please perform required action", JOptionPane.INFORMATION_MESSAGE);
	 
    Thread.sleep(6000);
   
    driver.findElement(By.linkText("Finished here. Next step")).click();
    Thread.sleep(6000);
    driver.findElement(By.linkText("Continue to checkout")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("processnext")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("cardNumber")).clear();
    driver.findElement(By.id("cardNumber")).sendKeys("4444333322221111");
    new Select(driver.findElement(By.id("endMonth"))).selectByVisibleText("04");
    new Select(driver.findElement(By.id("endYear"))).selectByVisibleText("18");
    driver.findElement(By.id("nameOnCard")).clear();
    driver.findElement(By.id("nameOnCard")).sendKeys("asdfg");
    
    driver.findElement(By.id("place-order-upper")).click();*/
    Thread.sleep(6000);
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
