package businesscomponents;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uimap.Header;
import uimap.OfBranchDeliverydate;
import uimap.OfLogin;
import uimap.UserRegisteration;
import uimap.M_Login;
import allocator.Allocator;

import com.cognizant.framework.Status;
//import com.thoughtworks.selenium.SeleniumException;

public class Mobile_LoginPage extends ReusableLibrary {




//private static final TimeUnit SECONDS = null;

public Mobile_LoginPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}


/********************************************************
 *FUNCTION    :clickSignin_mob
 *AUTHOR      :Chandru Kumar 
 *DATE        :27-12-2016
 *DESCRIPTION :register for new user
 * @throws InterruptedException 
 ********************************************************/

public void clickSignin_mob() {
	if(ReusableComponents.clickLink(driver, M_Login.buttonSignIn_Loc, M_Login.buttonSignIn)){
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    report.updateTestLog("Register new user", "The user has logged into the site", Status.PASS);}
	else
		report.updateTestLog("Register new user", "The user has not logged into the site", Status.FAIL);
}


/********************************************************
 *FUNCTION    :enterDetailsforRegisteredUser_mob
 *AUTHOR      :Chandru Kumar 
 *DATE        :27-12-2016
 *DESCRIPTION :register for new user
 * @throws InterruptedException 
 ********************************************************/

public void enterDetailsforRegisteredUser_mob() throws InterruptedException {
	String regEmail = dataTable.getData("General_Data" , "RegUserName");
	System.out.println(regEmail);
	String regPassword = dataTable.getData("General_Data","RegPassword");
	System.out.println(regPassword);
	ReusableComponents.typeinEditbox(driver, M_Login.txtregEmail_loc, M_Login.txtregEmail,regEmail);
	ReusableComponents.typeinEditbox(driver, M_Login.txtregPassword_loc, M_Login.txtregPassword,regPassword);
	ReusableComponents.clickButton(driver, M_Login.btnLogin_loc, M_Login.btnLogin);
	Thread.sleep(10000);
    report.updateTestLog("Register new user", "The user has logged into the site", Status.PASS);
    }
	
/********************************************************
 *FUNCTION    :clickNewToWaitrose_mob
 *AUTHOR      :Chandru Kumar
 *DATE        :27-12-2016
 *DESCRIPTION :clicking the login button
 ********************************************************/

public void clickNewToWaitrose_mob(){

	if(ReusableComponents.clickLink(driver, M_Login.linknewuser_Loc, M_Login.linknewuser)){
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	      report.updateTestLog("click register link", "The link is successfully clicked", Status.PASS);}
	else
		  report.updateTestLog("click register link", "The link is successfully clicked", Status.FAIL);
}

/********************************************************
 *FUNCTION    :registerNewUser_mob
 *AUTHOR      :Chandru Kumar 
 *DATE        :27-12-2016
 *DESCRIPTION :New user registration
 * @throws InterruptedException 
 ********************************************************/

public void registerNewUser_mob() throws InterruptedException{
	String userName = dataTable.getData("General_Data" , "UserName");
	System.out.println(userName);
	String password = dataTable.getData("General_Data","NewPassword");
	System.out.println(password);
	String EmailID;
	
	// Auto generate mail 
	Date TimeStamp = new Date( );
	SimpleDateFormat FormatObj = new SimpleDateFormat ("yyMMddhhmmss");
	String time = FormatObj.format(TimeStamp);
	System.out.println(time);
	
	EmailID = time + userName;
	
	System.out.println(EmailID); 
	ReusableComponents.typeinEditbox(driver, M_Login.txtEmail_loc, M_Login.txtEmail,EmailID);
	ReusableComponents.typeinEditbox(driver, M_Login.txtPassword_loc, M_Login.txtPassword,password);
	ReusableComponents.typeinEditbox(driver, M_Login.txtconfirmPassword_loc, M_Login.txtconfirmPassword,password);
	ReusableComponents.clickButton(driver, M_Login.btnReg_loc, M_Login.btnReg);
	report.updateTestLog("New User Registration", "New User has created an account", Status.PASS);
	Thread.sleep(10000);
	ReusableComponents.clickButton(driver, M_Login.btncont_loc, M_Login.btncont);
}


}
