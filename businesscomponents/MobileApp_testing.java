package businesscomponents;

	import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import uimap.Mobile_appFieldvalidation;
import uimap.Mobile_apptesting;
import uimap.OfOrderSummary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.winium.DesktopOptions;
//import org.openqa.selenium.winium.WiniumDriver;
//import org.testng.annotations.AfterTest;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.ExcelDataAccess;
import com.cognizant.framework.Status;
/*import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;*/
import com.cognizant.framework.Util;


public class MobileApp_testing extends ReusableLibrary {
	Runtime runtime = Runtime.getRuntime();

	MobileApp_ProductScan scan = new MobileApp_ProductScan(scriptHelper);

	static String osName = System.getProperty("os.name");
	static String userDir= System.getProperty("user.dir");
	public MobileApp_testing(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
     
	
	/********************************************************
	 *FUNCTION    :clickScanQRcode
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :11-1-2017
	 *DESCRIPTION :clicking the scan QR button
	 ********************************************************/
	
	public void clickScanQRcode() throws InterruptedException
	{  
		
		
		//TO handle Adhoc myWaitrose scan prompt (Temporary approach)
		
		try{
		/*    driver.findElement(By.xpath("//android.widget.Button[@resource-id='com.waitrose.WaitroseQuickCheck:id/acceptButton' and @text='Agree']")).click();
		    clickScanYourmyWaitroseCard();
			scan.scanmyWaitroseCardNumber();*/
			Thread.sleep(1500);
			ReusableComponents.clickButton(driver, Mobile_apptesting.clickScanQRcode_Loc, Mobile_apptesting.clickScanQRcode);
		
			
		}
		
		catch(Exception e)
		{
		
		ReusableComponents.clickButton(driver, Mobile_apptesting.clickScanQRcode_Loc, Mobile_apptesting.clickScanQRcode);
		}
	}
	
	
	/********************************************************
	 *FUNCTION    :scanQRcode
	 *AUTHOR      :Deepak Prabhu A
	 *DATE        :11-1-2017
	 *DESCRIPTION :opens the QRcode image
	 ********************************************************/
	
	
	
	public void scanQRcode() throws Exception 
	{
		
		
	
		osName = System.getProperty("os.name").split(" ")[0];
		userDir= System.getProperty("user.dir");
		WebDriverWait wait;
		String filePath = userDir+"\\ProductImages\\QRcode_testing.PNG";
		System.out.println("QRcode_testing.PNG launch Initiated");	
		
//		scan.scanEndofTransaction();
//		clickScanQRcode();
		
		
		switch(osName)
		{
		
		case "Windows":
			
		scan.openImageInWindows(filePath);
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/"
				+ "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout"
				+ "/android.widget.TextView[1]")));

		break;
		
		case "Mac":
			filePath =filePath.replace("\\", "/");
			File file = new File(filePath);
		

			scan.openImageInMac(file);
			wait = new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/"
					+ "android.widget.LinearLayout/android.widget.FrameLayout/"
					+ "android.widget.LinearLayout/android.widget.TextView[1]")));

			break;
		}
	}


	
	/********************************************************
	 *FUNCTION    :closewindow
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :11-1-2017
	 *DESCRIPTION :Closing the image window
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void closewindow() throws IOException, InterruptedException
	{
		
		
		String fullOSName = System.getProperty("os.name");
		
		Thread.sleep(3000);
		
		switch(fullOSName){

		case "Windows 7":
		{
			runtime.exec("taskkill /IM dllhost.exe -f");	
			System.out.println("Last PNG File closed!");
			break;
		}
		case "Windows 10":
		{
			runtime.exec("taskkill /f /t /IM Microsoft.Photos.exe");	
			System.out.println("Last PNG File closed!");
			break;
		}
		
		}


		
	} 

	
	
	 	
 	 	
	/********************************************************
	 *FUNCTION    :clickScanButton
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :11-1-2017
	 *DESCRIPTION :Clicking the scan button
	 ********************************************************/

	public void clickScanButton() throws InterruptedException
	{

	
		try{
			Thread.sleep(2000);	
			driver.findElement(By.xpath(Mobile_apptesting.clickScanbutton)).click();
			report.updateTestLog("Scan Button", "Scan Button is clicked successfully", Status.PASS);

		}catch(Exception e){
			report.updateTestLog("Scan Button", "Scan Button is not clicked", Status.FAIL);

		} 

	}
	
	/********************************************************
	 *FUNCTION    :clickRemoveButton
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :11-1-2017
	 *DESCRIPTION :Clicking the remove button
	 ********************************************************/
	public void clickRemoveButton() throws InterruptedException
	{
		 ReusableComponents.clickButton(driver, Mobile_apptesting.clickRemovebutton_Loc, Mobile_apptesting.clickRemovebutton);
		 report.updateTestLog("Remove Button", "Remove Button is clicked successfully", Status.PASS);
		 Thread.sleep(2000);	
	}
	

	
	/********************************************************
	 *FUNCTION    :getReferenceNumber
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :11-1-2017
	 *DESCRIPTION :retrieving the reference number for the transaction
	 * @throws IOException 
	 ********************************************************/
	public void getReferenceNumber() throws InterruptedException, IOException 
	{
		  String result = driver.findElement(By.xpath("//android.widget.TextView[@index='4']")).getText();
		  System.out.println("reference number is: " + result);
		  (new XL(scriptHelper)).putOrderNo("Items", result);
		  report.updateTestLog("Reference Number", "Reference Number is "+result, Status.PASS);
	}
	
	
	
	/********************************************************
	 *FUNCTION    :clickClose
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :11-1-2017
	 *DESCRIPTION :Clicking the close button
	 ********************************************************/
	public void clickClose() throws InterruptedException
	{
		if(ReusableComponents.isElementPresent(driver, "xpath", "//android.widget.TextView[@text='Close' and @index='1']"))
		{
		ReusableComponents.clickButton(driver, "xpath", "//android.widget.TextView[@text='Close' and @index='1']" );
		
		
		}
		//else if(ReusableComponents.isElementPresent(driver, "xpath", "//android.widget.RelativeLayout[@index='0']/android.widget.TextView[@index='1']"));
		//{
		//   ReusableComponents.clickButton(driver, "xpath", "//android.widget.RelativeLayout[@index='0']/android.widget.TextView[@index='1']" );
		//}
		
	}
	
	/********************************************************
	 *FUNCTION    :clickTrolley
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :11-1-2017
	 *DESCRIPTION :Clicking the close button
	 ********************************************************/
	public void clickTrolley() throws InterruptedException
	{
		ReusableComponents.clickButton(driver, "xpath", "//android.widget.TextView[@text='Trolley']" );
	}
	
	/********************************************************
	 *FUNCTION    :clickOk
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :14--2017
	 *DESCRIPTION :Clicking the OK button
	 ********************************************************/
	
	
	
	public void clickOk() throws InterruptedException
	{
		ReusableComponents.clickButton(driver, "xpath", "//android.widget.Button[@resource-id='android:id/button1' and @text='OK']" );
	}
	
	/********************************************************
	 *FUNCTION    :getErrorMessage
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :14-2-2017
	 *DESCRIPTION :verify the error prompt
	 ********************************************************/
	public void getErrorMessage() throws InterruptedException
	{

		if(ReusableComponents.isElementPresent(driver, "xpath","//android.widget.TextView[@resource-id='android:id/message']")){
			String errormessage = ReusableComponents.getText(driver, "xpath","//android.widget.TextView[@resource-id='android:id/message']");
			 System.out.println(errormessage);
			 report.updateTestLog("Error Message", "Error messgae is"+errormessage, Status.PASS);}
		else
			  report.updateTestLog("Error Message", "Error messgae  is not present", Status.FAIL);
	}
	
	
	/********************************************************
	 *FUNCTION    :clickAgree
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :15-2-2017
	 *DESCRIPTION :Clicking the Agree button
	 ********************************************************/
	public void clickAgree() throws InterruptedException
	{

		if(ReusableComponents.isElementPresent(driver, "xpath","//android.widget.Button[@resource-id='com.waitrose.WaitroseQuickCheck:id/acceptButton' and @text='Agree']")){
			ReusableComponents.clickButton(driver, "xpath","//android.widget.Button[@resource-id='com.waitrose.WaitroseQuickCheck:id/acceptButton' and @text='Agree']");
			 report.updateTestLog("Agree Button", "Terms and conditions are agreed succesfully", Status.PASS);}
		else
			  report.updateTestLog("Agree Button", "Agree Button is not clicked", Status.FAIL);
	}
	
	
	/********************************************************
	 *FUNCTION    :checkAgreeButton
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :20-3-2017
	 *DESCRIPTION :Checking the Agree button
	 ********************************************************/
	public void checkAgreeButton() throws InterruptedException
	{

		if(ReusableComponents.isElementPresent(driver, "xpath","//android.widget.Button[@resource-id='com.waitrose.WaitroseQuickCheck:id/acceptButton' and @text='Agree']")){
			 report.updateTestLog("Agree Button", "Agree Button is present", Status.PASS);}
		else
			  report.updateTestLog("Agree Button", "Agree Button is not present", Status.FAIL);
	}
	
	
	/********************************************************
	 *FUNCTION    :checkDisAgreeButton
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :20-3-2017
	 *DESCRIPTION :Checking the Agree button
	 ********************************************************/
	public void checkDisAgreeButton() throws InterruptedException
	{

		if(ReusableComponents.isElementPresent(driver, "xpath","//android.widget.Button[@resource-id='com.waitrose.WaitroseQuickCheck:id/denyButton' and @text='Disagree']")){
			 report.updateTestLog("DisAgree Button", "DisAgree Button is present", Status.PASS);}
		else
			  report.updateTestLog("DisAgree Button", "DisAgree Button is not present", Status.FAIL);
	}
	
	/********************************************************
	 *FUNCTION    :clickAgree
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :15-2-2017
	 *DESCRIPTION :Clicking the Agree button
	 ********************************************************/
	
	public void clickDisAgree() throws InterruptedException
	{

		if(ReusableComponents.isElementPresent(driver, "xpath","//android.widget.Button[@resource-id='com.waitrose.WaitroseQuickCheck:id/denyButton' and @text='Disagree']")){
			ReusableComponents.clickButton(driver, "xpath","//android.widget.Button[@resource-id='com.waitrose.WaitroseQuickCheck:id/denyButton' and @text='Disagree']");
			 report.updateTestLog("DisAgree Button", "Terms and conditions are not agreed succesfully", Status.PASS);}
		else
			  report.updateTestLog("DisAgree Button", "DisAgree Button is not clicked", Status.FAIL);
	}
	
	
	/********************************************************
	 *FUNCTION    :checkTermsandConditionsButton
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :20-3-2017
	 *DESCRIPTION :Checking the Agree button
	 ********************************************************/
	public void checkTermsandConditionsButton() throws InterruptedException
	{

		if(ReusableComponents.isElementPresent(driver, "xpath","//android.widget.TextView[@text='Terms & Conditions']")){
			 report.updateTestLog("Terms and Conditions Button", "Terms and Conditions Button is present", Status.PASS);}
		else
			  report.updateTestLog("Terms and Conditions Button", "Terms and Conditions Button is not present", Status.FAIL);
	}
	
	
	/********************************************************
	 *FUNCTION    :validateTermsandConditions
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :20-3-2017
	 *DESCRIPTION :Checking the Agree button
	 ********************************************************/
	public void validateTermsandConditions() throws InterruptedException
	{

		if(ReusableComponents.isElementPresent(driver, "xpath","//android.widget.ScrollView[@index='1']/android.widget.LinearLayout[@index='0']/android.widget.TextView[@index='0']")){
			String Termsandconditons = ReusableComponents.getText(driver, "xpath","//android.widget.ScrollView[@index='1']/android.widget.LinearLayout[@index='0']/android.widget.TextView[@index='0']");
			 report.updateTestLog("Terms and Conditions Button", "Terms and Conditions is " + Termsandconditons , Status.PASS);}
		else
			  report.updateTestLog("Terms and Conditions", "Terms and Conditions is not present", Status.FAIL);
	}
	
	
	/********************************************************
	 *FUNCTION    :verifyTermsandConditionsRegisteredUser
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :20-3-2017
	 *DESCRIPTION :Checking the Agree button
	 ********************************************************/
	public void verifyTermsandConditionsRegisteredUser() throws InterruptedException
	{

		if(ReusableComponents.isElementPresent(driver, "xpath","//android.widget.ScrollView[@index='1']/android.widget.LinearLayout[@index='0']/android.widget.TextView[@index='0']")){
			String Termsandconditons = ReusableComponents.getText(driver, "xpath","//android.widget.ScrollView[@index='1']/android.widget.LinearLayout[@index='0']/android.widget.TextView[@index='0']");}
			else 
			{
				 report.updateTestLog("Terms and Conditions", "Terms and Conditions is not present for the registered user", Status.PASS);}		
		
	}
	
	

	
	/********************************************************
	 *FUNCTION    :TermsandConditionsdetails
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :20-3-2017
	 *DESCRIPTION :Checking the Agree button
	 ********************************************************/
	public void termsandConditionsdetails() throws InterruptedException
	{

		if(ReusableComponents.isElementPresent(driver, "xpath","//android.widget.ScrollView[@index='1']/android.widget.LinearLayout[@index='0']/android.widget.TextView[@index='0']")){
			String Termsandconditonsdetails = ReusableComponents.getText(driver, "xpath","//android.widget.ScrollView[@index='1']/android.widget.LinearLayout[@index='0']/android.widget.TextView[@index='1']");
			 report.updateTestLog("Terms and Conditions Button", "Terms and Conditions is " + Termsandconditonsdetails , Status.PASS);}
		else
			  report.updateTestLog("Terms and Conditions", "Terms and Conditions is not present", Status.FAIL);
	}
	
	
	/********************************************************
	 *FUNCTION    :verifyMaxValueRchd
	 *AUTHOR      :Deepak Prabhu A
	 *DATE        :25-10-2017
	 *DESCRIPTION :Verify whether the prompt is displayed on reaching the maximum price threshold

	 ********************************************************/
	
	public void verifyMaxValueRchd()
	{

		if(ReusableComponents.isElementPresent(driver, "xpath", "android:id/message"))
		{
			report.updateTestLog("Verify Max value Alert", "Pop up is displayed after reaching the max value as expected", Status.PASS);
		} else {
			report.updateTestLog("Verify Max value Alert", "Pop up is not displayed after reaching the max value", Status.FAIL);

		}

	}
	

	
	/********************************************************
	 *FUNCTION    :validateWelcomeScreen
	 *AUTHOR      :Deepak Prabhu A
	 *DATE        :25-10-2017
	 *DESCRIPTION :Validate all the text fields in the welcome screen after scanning QR code

	 ********************************************************/
	
	public void validateWelcomeScreen()
	{
		
		
		String baseXpath = "/hierarchy/android.widget.FrameLayout"
			+ "/android.widget.LinearLayout"
			+ "/android.widget.FrameLayout"
			+ "/android.widget.LinearLayout";
		
		
		

		//Verify Waitrose Text header
		String waitroseHeader = baseXpath+"/android.widget.RelativeLayout[1]/android.widget.TextView";
	
		
		if(ReusableComponents.getText(driver, "xpath", waitroseHeader).contains("Waitrose"))
		{
			
			
			String waitroseHeaderTxt = ReusableComponents.getText(driver, "xpath", waitroseHeader);
		
		
			report.updateTestLog("Header Text", "Label <b>"+waitroseHeaderTxt+"</b> is present as expected", Status.PASS);
		}else{
			
			System.out.println("Welcome Text not present");

			report.updateTestLog("Header Text", "Label is not present", Status.FAIL);

		}
		
		
		
		
		
		// validating Welcome To text
		
		
		
		

		
		if(ReusableComponents.getText(driver, "xpath", "//android.widget.LinearLayout[@index='0']/android.widget.TextView[@index='2']").equals(" Welcome to"))
		{
			System.out.println("Welcome Text present");
			report.updateTestLog("Verify <b>Welcome to</b> text", "Text is present as expected", Status.PASS);
		}else{
			report.updateTestLog("Verify <b>Welcome to</b> text", "Text is not present", Status.FAIL);

		}

		//  Verifying the Store name label
		String storeLabelXpath = "//android.widget.LinearLayout[@index='0']/android.widget.LinearLayout[@index='3']/android.widget.TextView[@index='1']";

		if(ReusableComponents.getText(driver, "xpath", storeLabelXpath).contains("Waitrose"))
		{
			String storeNameLabel = ReusableComponents.getText(driver, "xpath", storeLabelXpath);


			report.updateTestLog("Store Name  label", "Store label <b>"+storeNameLabel+"</b> is present as expected", Status.PASS);
		}else{
			report.updateTestLog("Store Name  label", "Store label is not present", Status.FAIL);

		}



		// Verifying 'You are now ready to shop' text
		
		String readyToShopXpath = baseXpath+"/android.widget.TextView[2]";
		if(ReusableComponents.getText(driver, "xpath", readyToShopXpath).contains("You are now ready to shop"))
		{
			String readyToShopTxt = ReusableComponents.getText(driver, "xpath", readyToShopXpath);


			report.updateTestLog("Ready to shop label", "Ready to shop label <b>"+readyToShopTxt+"</b> is present as expected", Status.PASS);
		}else{
			report.updateTestLog("Ready to shop label", "Ready to shop label is not present", Status.FAIL);

		}

		//Verify 'Please scan your first item text'
		String pleaseScanFirstItemXpath = baseXpath+"/android.widget.TextView[3]";

		if(ReusableComponents.getText(driver, "xpath", pleaseScanFirstItemXpath).contains("Please scan your first item"))
		{
			String pleaseScanFirstItemTxt = ReusableComponents.getText(driver, "xpath", pleaseScanFirstItemXpath);


			report.updateTestLog("Please scan your First Item text", "Label <b>"+pleaseScanFirstItemTxt+"</b> is present as expected", Status.PASS);
		}else{
			report.updateTestLog("Please scan your First Item text", "Label is not present", Status.FAIL);

		}

	
	
	
	
	
	
	}
	
	/********************************************************
	 *FUNCTION    :VerifyProductNotFoundError
	 *AUTHOR      :Deepak Prabhu A
	 *DATE        :25-10-2017
	 *DESCRIPTION :On scanning an invalid dump product, barcode not found error message should be displayed
	 * @throws InterruptedException 
	 ********************************************************/
	
	public void verifyProductNotFoundError() throws InterruptedException
	{
		
		String expectedErrorPromptMsg = "This barcode is not found. Please ensure you show this item to the cashier at the end of your shop.";
		

		//System.out.println("Actual error msg: "+ReusableComponents.getText(driver,"id","android:id/message"));
		
		
		if(ReusableComponents.getText(driver,"id","android:id/message").contains(expectedErrorPromptMsg))
				{
			
			System.out.println("Pop up is displayed as expected");
			
			report.updateTestLog("Dump Product Error Message", "Error message <b>"+expectedErrorPromptMsg+"</b> is displayed as expected", Status.PASS);
			clickOk();
			
			if(ReusableComponents.isElementPresent(driver, "id", "android:id/button1")==false){
					System.out.println("Pop up closed after clicking OK button"); }
			
	}else{
		report.updateTestLog("Dump Product Error Message", "Error message <b>"+expectedErrorPromptMsg+"</b> is not  displayed", Status.FAIL);

	}
	
	}
	
	/********************************************************
	 *FUNCTION    :clickScanYourmyWaitroseCard
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :15-2-2017
	 *DESCRIPTION :Clicking the OK button
	 ********************************************************/
	public void clickScanYourmyWaitroseCard() throws InterruptedException
	{

		if(ReusableComponents.isElementPresent(driver, "xpath","//android.widget.TextView[@text='Scan your myWaitrose card' and @index='4']")){
			ReusableComponents.clickButton(driver, "xpath","//android.widget.TextView[@text='Scan your myWaitrose card' and @index='4']");
			 report.updateTestLog("Scan Your my Waitrose Card", "Scan Your my Waitrose Card is clicked succesfully", Status.PASS);}
		else
			  report.updateTestLog("Scan Your my Waitrose Card", "Scan Your my Waitrose Card is not clicked", Status.FAIL);
	}
}