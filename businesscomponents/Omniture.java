package businesscomponents;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.openqa.selenium.WebElement;

import supportlibraries.Asserts;
import supportlibraries.DriverScript;
import supportlibraries.FileGrabber;
import supportlibraries.OmnitureTagExtracter;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


import supportlibraries.TestParameterReader;
import uimap.Common;
import uimap.Header;
import uimap.JotterFrame;
import uimap.Favourites;
import uimap.HomePage;
import uimap.Login;
import uimap.MiniTrolley;
import uimap.MyList;
import uimap.ProductDetails;
import uimap.ProductPortrait;
import uimap.UserRegisteration;
//import Datatables.CommonData;
import allocator.Allocator;
import businesscomponents.ReusableComponents;






//import com.UtilityFunctions.FindBy;
import com.cognizant.framework.Status;
import com.cognizant.framework.Util;
//import com.thoughtworks.selenium.SeleniumException;

public class Omniture extends ReusableLibrary {
	private Asserts asserts = new Asserts();
	public static String ForumName="";


	public Omniture(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	
		
	
	/////////////////////Functions below
	public void openAppomniture(){
		//String url = "http://wtr-wec-test.johnlewis.co.uk/";
		//String url = "http://wtr-wec-test2.johnlewis.co.uk/";
		String url = "http://www.wec.tiffani.co.uk/";//dataTable.getData("General_Data" , "URL");
		System.out.println(url);
		//System.out.println(Allocator.currentbrowser);
			
				try {
					//driver.get("http://wtr-wec-test.johnlewis.co.uk/");
					driver.get(url);
					//if(ReusableComponents.isElementPresent(driver, "xpath", "//div[@id='userboxWrapper']/div[@class='userbox']/a[@title='Login']/span"))
					if(ReusableComponents.isElementPresent(driver, Header.linkGroceries_Loc	, Header.linkGroceries))
						report.updateTestLog("Waitrose Application","Successfully opened the URL - " + url, Status.PASS);
					else
						report.updateTestLog("Waitrose Application","Unable to open the URL - " + url, Status.FAIL);
					try{Thread.sleep(8000);}catch(Exception e){}
								
				} catch (Exception e) {
					//driver.get("http://wtr-wec-test.johnlewis.co.uk/");
					driver.get(url);
					
				}
				
				
			}
	public void linkclk(){
		ReusableComponents.clickLink(driver, "linkText", "Fresh");
		report.updateTestLog("Waitrose Click link--Fresh","Successful", Status.PASS);
		try{Thread.sleep(20000);}catch(Exception e){}
	}
public void omnitureValuation(){
	report.updateTestLog("Waitrose omniture validation--"+dataTable.getData("General_Data" , "OmniturePage"),"Start", Status.DONE);
	
	checkResults(new File(System.getProperty("user.dir")).getAbsolutePath()+"\\Miscellaneous\\Omniture\\Tags\\"+dataTable.getData("General_Data" , "OmniturePage")+".csv");
	
		
}
	

public void delfirsthrdfile(){
	File file = null;
	File[] paths;
	
	
	
	
	try{      
		// create new file
		Thread.sleep(2000);
		file = new File(new File(System.getProperty("user.dir")).getAbsolutePath()+"\\Miscellaneous\\Omniture\\harFiles");

		// returns pathnames for files and directory
		paths = file.listFiles();
		// for each pathname in pathname array
		for(File path:paths)
		{
			path.delete();
			break;
		}
	}catch(Exception e){
		// if any error occurs
		System.out.println("Error getting hdr file.");
		
	}
	}
	public void checkResults(String hdrLocation){
		Map<String, String> expectedTagNames = new HashMap<String, String>();
		Map<String, String> actualTagNames = new HashMap<String, String>();
		TestParameterReader testParameterReader = new TestParameterReader();
		expectedTagNames = testParameterReader.getCSV(hdrLocation);
		
		FileGrabber fileGrabber = new FileGrabber();
		OmnitureTagExtracter omnitureTagExtracter = new OmnitureTagExtracter(fileGrabber.getHdrFileText());
		actualTagNames =  omnitureTagExtracter.getOmnitureTagValue(expectedTagNames);
		Iterator<String> itAct = actualTagNames.keySet().iterator();
		Iterator<String> itExp = expectedTagNames.keySet().iterator();
		while (itAct.hasNext()){
			String tagActName = itAct.next().toString();
			String tagActValue= actualTagNames.get(tagActName);
			
			String tagExpName = itExp.next().toString();
			String tagExpValue= expectedTagNames.get(tagExpName);
			
			if (tagActValue.equals(tagExpValue))
				report.updateTestLog("Validate Tag -"+tagExpName,"Match: Expected:"+tagExpValue+", Actual:"+tagActValue, Status.PASS);
			else
				report.updateTestLog("Validate Tag -"+tagExpName,"No Match: Expected:"+tagExpValue+", Actual:"+tagActValue, Status.FAIL);
				
		}
		
	}

}
