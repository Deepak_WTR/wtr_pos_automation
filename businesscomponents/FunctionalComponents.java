package businesscomponents;

import java.util.List;

import org.openqa.selenium.*;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.Select;

import com.cognizant.framework.Status;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


/**
 * Functional Components class
 * @author Cognizant
 */
public class FunctionalComponents extends ReusableLibrary
{
	/**
	 * Constructor to initialize the component library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public FunctionalComponents(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	public void clickLink(){
	    try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    driver.findElement(By.linkText(dataTable.getData("General_Data", "Link"))).click();
	    report.updateTestLog("Link Clicked", dataTable.getData("General_Data", "Link"), Status.PASS);
	    try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    

	}
	public void demofail(){
		report.updateTestLog("Sample failure", "failed case demo", Status.FAIL);
	}
	public void demowarning(){
		report.updateTestLog("Sample Warning", "warning case demo", Status.WARNING);
	}
	public void invokeApplication()
	{
		driver.get(properties.getProperty("ApplicationUrl"));
		//driver.manage().window().maximize();
		report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
								properties.getProperty("ApplicationUrl"), Status.DONE);
	}
	
	//Demo Script
	
	public void existingUserLogin() throws InterruptedException
	{
		System.out.println("Class [FunctionalComponents ] - Method [ExistingUserLogin]");
		//report.updateTestLog("View Pape", "The home page is displayed with no reference to a username", Status.PASS);
		report.updateTestLog("Step 1", "View Pape", Status.PASS);
		driver.findElement(By.xpath("//*[@id='banner']/ul/li[1]/a[2]")).click();
		Thread.sleep(2000);
		
		
		
		//report.updateTestLog("Click Login", "Login register lightbox. Initial state is displayed with all fields blank", Status.PASS);
		report.updateTestLog("Step 2", "Click Login", Status.PASS);
		System.out.println("Testing");
		
		/*try {
			List<WebElement> AlliFrameID = driver.findElements(By.tagName("iframe"));
			System.out.println(AlliFrameID.size());
			for(int i=0;i<=AlliFrameID.size();i++){
				System.out.println("Test:" + AlliFrameID.get(i).getAttribute("class"));
				if("login-iframe".equalsIgnoreCase(AlliFrameID.get(i).getAttribute("class"))){
					driver.switchTo().frame(AlliFrameID.get(i));
					break;
				}
			}
		} catch (Exception e) {
			
		}*/
		
		driver.switchTo().frame(0);
		Thread.sleep(3000);
		/*driver.findElement(By.id("um-email")).click(); //*[@id="um-email"]
		driver.findElement(By.id("um-email")).clear();*/
		driver.findElement(By.id("um-email")).sendKeys("test123@gmail.com");
		Thread.sleep(1000);
		/*driver.findElement(By.id("um-pwd")).click();
		driver.findElement(By.id("um-pwd")).clear();*/
		driver.findElement(By.id("um-pwd")).sendKeys("test123");
		Thread.sleep(500);
		//report.updateTestLog("Enter Valid data", "The data entered by the user is displayed in the relevant fields", Status.PASS);
		report.updateTestLog("Step 3", "Enter a valid email address and password for user that has registered on the site", Status.PASS);
		driver.findElement(By.name("sign-in")).click();
		//report.updateTestLog("Hit Sign-in", "The user should be able to login", Status.PASS);
		report.updateTestLog("Step 5", "Hit Sign-in", Status.PASS);
		Thread.sleep(2000);
		
	}
	
	
	public void registration() throws InterruptedException
	{
		System.out.println("Class [FunctionalComponents ] - Method [Registration]");
		
		String emailAddress = dataTable.getData("General_Data", "Email_address");
		String password = dataTable.getData("General_Data", "Password");
		String confirmPassword = dataTable.getData("General_Data", "ConfirmPsw");
		String accountCreMsg = "Your account has been created!";
		
		//System.out.println("EmailAddress:"+ emailAddress + "PSW:" + password + "Confirm PSW:"+ confirmPassword );
		
		driver.findElement(By.xpath("//*[@id='banner']/ul/li[1]/a[4]")).click();
		report.updateTestLog("Step 1", "Click Register", Status.PASS);
		Thread.sleep(2000);
        driver.switchTo().frame(1);
		Thread.sleep(3000);
        driver.findElement(By.id("um-email-reg")).click();
        driver.findElement(By.id("um-email-reg")).sendKeys(emailAddress);
        driver.findElement(By.id("um-pwd-reg")).sendKeys(password);
        driver.findElement(By.id("um-pwd-reg2")).click();
        driver.findElement(By.id("um-pwd-reg2")).sendKeys(confirmPassword);
        driver.findElement(By.name("create-account")).click();
        Thread.sleep(3000);
        report.updateTestLog("Step 2", "Enter all the mandatory fields and Click on Register", Status.PASS);
        report.updateTestLog("Warning", "Warning popup Testing" , Status.WARNING);
        String accountCreationMsg =  driver.findElement(By.xpath("//div[@class='col-10-center mod-important']")).getText();
        System.out.println("Creation Msg:" + accountCreationMsg);
        if(accountCreMsg.contains(accountCreationMsg))
        {
        	report.updateTestLog("JLRegistration", "Message -" + accountCreMsg + " displayed as expected ", Status.PASS);
        }else{
        	report.updateTestLog("JLRegistration", "Message -" + accountCreMsg + " displayed as expected ", Status.PASS);
        }
        	
        highlightElement(driver, driver.findElement(By.xpath("//div[@class='col-10-center mod-important']")));
        driver.findElement(By.xpath("//div[@id='ssl-register-confirm']/section/a/span")).click();	
        Thread.sleep(3000);
		
	}
	
	public void orderEntry() throws InterruptedException
	{
		System.out.println("Class [FunctionalComponents ] - Method [orderEntry]");
		//String password = dataTable.getData("General_Data", "Password");
		report.updateTestLog("OrderEntry", "To verify the Oredr entry flow till payment page", Status.DONE);
		report.updateTestLog("Verify SearchText", "Search textbox displayed as expected", Status.PASS);
		driver.findElement(By.id("search-keywords")).clear();
		driver.findElement(By.id("search-keywords")).sendKeys("lego watch");
		report.updateTestLog("Enter keyword", "Search keyword entered in search textbox", Status.DONE);
		driver.findElement(By.name("search")).click();
		report.updateTestLog("Click SearchBtn", "Search Button clicked as expected", Status.PASS);
		Thread.sleep(3000);
		
		report.updateTestLog("Search Results", "Click on a product from the search results ", Status.DONE);
		//driver.findElement(By.xpath("//img[@alt='Buy LEGO Star Wars Darth Vader Watch Online at johnlewis.com']")).click();
		driver.findElement(By.xpath("//div[@id='product-grid']/div/div[3]/article/a/img")).click();
		//driver.findElement(By.xpath("//div[@id='product-grid']/div/div[3]/article/a/img")).click();
		//driver.findElement(By.cssSelector("img[alt=\"Buy Lego Star Wars Darth Vader Watch Online at johnlewis.com\"]")).click();
		report.updateTestLog("Search Results", "Clicked on a product from the search results as expected", Status.PASS);
		Thread.sleep(2000);
		//assertEquals("Buy Lego Star Wars Darth Vader Watch online at John Lewis", driver.getTitle());
		
		report.updateTestLog("Product quantity", "Enter a quantity in textbox as exepected ", Status.PASS);
		driver.findElement(By.id("quantity")).clear();
		driver.findElement(By.id("quantity")).sendKeys("2");
		driver.findElement(By.name("/atg/store/order/purchase/CartFormHandler.addItemToOrder")).click();
		report.updateTestLog("Add to Basket", "Click on Add to Basket button as exepected ", Status.PASS);
		Thread.sleep(2000);
		//driver.findElement(By.cssSelector("a.checkout-link.basket-switcher")).click();
		driver.findElement(By.linkText("Checkout")).click();
		report.updateTestLog("Product Checkout", "Click on -Checkout- button as exepected ", Status.PASS);
		
		Thread.sleep(2000);
		//String priceVal = driver.findElement(By.xpath("//td[@class = 'price-col']")).getText();
		String Subtotal = driver.findElement(By.xpath("//div[@id='basket']/form/div/div[1]/div[2]/table/tbody/tr/td[1]/table/tbody/tr/td[4]")).getText();
		System.out.println(Subtotal.substring(1));
		highlightElement(driver, driver.findElement(By.xpath("//div[@id='basket']/form/div/div[1]/div[2]/table/tbody/tr/td[1]/table/tbody/tr/td[4]")));
		report.updateTestLog("Verify SubTotal", "The Subtotal:" + Subtotal.substring(1) + " is calculated as exepected ", Status.PASS);
		
		driver.findElement(By.name("continue-securely")).click();
		report.updateTestLog("Continue securely", "Click on -Continue securely- button in the basket page as exepected ", Status.PASS);
		Thread.sleep(4000);
		
		report.updateTestLog("Secure Checkoutpage", "Enter the email id ", Status.DONE);
		driver.findElement(By.xpath("//input[contains(@id,'txt-email')]")).click();
		driver.findElement(By.xpath("//input[contains(@id,'txt-email')]")).clear();
		driver.findElement(By.xpath("//input[contains(@id,'txt-email')]")).sendKeys("testingapp130@gmail.com");	
		Thread.sleep(3000);
		
		/*driver.findElement(By.id("rad-pwd-yes")).click();
		report.updateTestLog("Secure Checkoutpage", "Select -YES my password- Radio button as expected ", Status.PASS);
		driver.findElement(By.id("txt-pwd")).click();
		driver.findElement(By.id("txt-pwd")).sendKeys(password);
		report.updateTestLog("Secure Checkoutpage", "Enter the corresponding password ", Status.DONE);*/
		driver.findElement(By.name("continue")).click();
		Thread.sleep(500);
		driver.findElement(By.name("continue-1")).click();
		report.updateTestLog("Secure Checkoutpage", "Click on -Continue- button as expected ", Status.PASS);
		
		//Choose Your delivery address - page
		Thread.sleep(3000);
		driver.findElement(By.id("rad-click-and-collect")).click();
		Thread.sleep(1000);
		report.updateTestLog("DeliveryAddress Page", "Choose -Click & Collect- option as expected ", Status.PASS);
		Select select = new Select(driver.findElement(By.name("ddl-region")));
		List<WebElement> selOptions = select.getOptions();
		for(WebElement sel: selOptions){
			if(sel.getText().equalsIgnoreCase("London")){
				System.out.println("INSIDE IF");
				sel.click();
			}
		}
		report.updateTestLog("DeliveryAddress Page", "Select a value for -Region- from the dropdown as expected ", Status.PASS);
		//select.selectByValue("London");
		Thread.sleep(3000);
		Select select1 = new Select(driver.findElement(By.name("ddl-collect-point")));
		List<WebElement> selOptions1 = select1.getOptions();
		for(WebElement sel1: selOptions1){
			if(sel1.getText().equalsIgnoreCase("Brent Cross")){
				System.out.println("INSIDE IF");
				sel1.click();
			}
		}
		report.updateTestLog("DeliveryAddress Page", "Select a value for -Shop- from the dropdown as expected ", Status.PASS);
		//select1.selectByValue("Brent Cross");
		Thread.sleep(1000);
		driver.findElement(By.name("confirm-shop")).click();
		report.updateTestLog("DeliveryAddress Page", "Click on -Confirm Shop- button as expected ", Status.PASS);
		Thread.sleep(3000);
		
		
		//Order Summary Page
		/*String oredrSummaryPgTitle = "John Lewis | Order Summary";
		if(oredrSummaryPgTitle.equalsIgnoreCase(driver.getTitle())){
			report.updateTestLog("To verify Pagetitle", "Page Title is displayed as expected :" + oredrSummaryPgTitle , Status.PASS);
		}
		Thread.sleep(3000);
		driver.findElement(By.name("continue")).click();
		report.updateTestLog("Order Summary", "Click on -Continue- in -Order Summary- page as expected ", Status.PASS);
		Thread.sleep(4000);
		highlightElement(driver, driver.findElement(By.xpath("//div[@id='checkout']/div[2]")));
		 String PaymentPage = driver.getTitle();
		 if(PaymentPage.equalsIgnoreCase(driver.getTitle())){
				report.updateTestLog("To verify Pagetitle", "Page Title is displayed as expected :" + PaymentPage , Status.PASS);
			}
		Thread.sleep(2000);*/
	}
	
	public void summaryPage()throws InterruptedException{
		String oredrSummaryPgTitle = "John Lewis | Order Summary";
		if(oredrSummaryPgTitle.equalsIgnoreCase(driver.getTitle())){
			report.updateTestLog("To verify Pagetitle", "Page Title is displayed as expected :" + oredrSummaryPgTitle , Status.PASS);
		}
		Thread.sleep(3000);
		driver.findElement(By.name("continue")).click();
		report.updateTestLog("Warning", "Warning popup Testing" , Status.WARNING);
		report.updateTestLog("Order Summary", "Click on -Continue- in -Order Summary- page as expected ", Status.PASS);
		Thread.sleep(4000);
		highlightElement(driver, driver.findElement(By.xpath("//div[@id='checkout']/div[2]")));
		 String PaymentPage = driver.getTitle();
		 if(PaymentPage.equalsIgnoreCase(driver.getTitle())){
				report.updateTestLog("To verify Pagetitle", "Page Title is displayed as expected :" + PaymentPage , Status.PASS);
			}
		Thread.sleep(2000);
	}
	
	
	public void orderEntryFlow() throws InterruptedException
	{
		System.out.println("Class [FunctionalComponents ] - Method [orderEntry]");
		String password = dataTable.getData("General_Data", "Password");
		report.updateTestLog("OrderEntry", "To verify the Oredr entry flow till payment page", Status.DONE);
		report.updateTestLog("Verify SearchText", "Search textbox displayed as expected", Status.PASS);
		driver.findElement(By.id("search-keywords")).clear();
		driver.findElement(By.id("search-keywords")).sendKeys("lego watch");
		report.updateTestLog("Enter keyword", "Search keyword entered in search textbox", Status.DONE);
		driver.findElement(By.name("search")).click();
		report.updateTestLog("Click SearchBtn", "Search Button clicked as expected", Status.PASS);
		Thread.sleep(5000);
		
		report.updateTestLog("Search Results", "Click on a product from the search results ", Status.DONE);
		//driver.findElement(By.xpath("//img[@alt='Buy LEGO Star Wars Darth Vader Watch Online at johnlewis.com']")).click();
		driver.findElement(By.xpath("//div[@id='product-grid']/div/div[3]/article/a/img")).click();
		//driver.findElement(By.cssSelector("img[alt=\"Buy Lego Star Wars Darth Vader Watch Online at johnlewis.com\"]")).click();
		report.updateTestLog("Search Results", "Clicked on a product from the search results as expected", Status.PASS);
		Thread.sleep(2000);
		//assertEquals("Buy Lego Star Wars Darth Vader Watch online at John Lewis", driver.getTitle());
		
		report.updateTestLog("Product quantity", "Enter a quantity in textbox as exepected ", Status.PASS);
		driver.findElement(By.id("quantity")).clear();
		driver.findElement(By.id("quantity")).sendKeys("2");
		driver.findElement(By.name("/atg/store/order/purchase/CartFormHandler.addItemToOrder")).click();
		report.updateTestLog("Add to Basket", "Click on Add to Basket button as exepected ", Status.PASS);
		Thread.sleep(3000);
		//driver.findElement(By.cssSelector("a.checkout-link.basket-switcher")).click();
		driver.findElement(By.linkText("Checkout")).click();
		report.updateTestLog("Product Checkout", "Click on -Checkout- button as exepected ", Status.PASS);
		
		Thread.sleep(2000);
		//String priceVal = driver.findElement(By.xpath("//td[@class = 'price-col']")).getText();
		String Subtotal = driver.findElement(By.xpath("//div[@id='basket']/form/div/div[1]/div[2]/table/tbody/tr/td[1]/table/tbody/tr/td[4]")).getText();
		System.out.println(Subtotal.substring(1));
		highlightElement(driver, driver.findElement(By.xpath("//div[@id='basket']/form/div/div[1]/div[2]/table/tbody/tr/td[1]/table/tbody/tr/td[4]")));
		report.updateTestLog("Verify SubTotal", "The Subtotal:" + Subtotal.substring(1) + " is calculated as exepected ", Status.PASS);
		
		driver.findElement(By.name("continue-securely")).click();
		report.updateTestLog("Continue securely", "Click on -Continue securely- button in the basket page as exepected ", Status.PASS);
		Thread.sleep(4000);
		
		report.updateTestLog("Secure Checkoutpage", "Enter the email id ", Status.DONE);
		driver.findElement(By.xpath("//input[contains(@id,'txt-email')]")).click();
		driver.findElement(By.xpath("//input[contains(@id,'txt-email')]")).clear();
		driver.findElement(By.xpath("//input[contains(@id,'txt-email')]")).sendKeys("JLSeleniumAuto3@gmail.com");	
		Thread.sleep(3000);
		
		/*driver.findElement(By.id("rad-pwd-yes")).click();
		report.updateTestLog("Secure Checkoutpage", "Select -YES my password- Radio button as expected ", Status.PASS);
		driver.findElement(By.id("txt-pwd")).click();
		driver.findElement(By.id("txt-pwd")).sendKeys("asdfg123");
		report.updateTestLog("Secure Checkoutpage", "Enter the corresponding password ", Status.DONE);*/
	/*	String browserName = dataTable.getData("General_Data", "Bname");
		System.out.println("Browser Name :" + browserName);
		if(browserName.equalsIgnoreCase("iexplore")){
			driver.findElement(By.name("continue")).click();
		}
		driver.findElement(By.name("continue-1")).click();
		
		report.updateTestLog("Secure Checkoutpage", "Click on -Continue- button as expected ", Status.PASS);
		
		//Choose Your delivery address - page
		Thread.sleep(3000);
		driver.findElement(By.id("rad-click-and-collect")).click();
		Thread.sleep(1000);
		report.updateTestLog("DeliveryAddress Page", "Choose -Click & Collect- option as expected ", Status.PASS);
		Select select = new Select(driver.findElement(By.name("ddl-region")));
		List<WebElement> selOptions = select.getOptions();
		for(WebElement sel: selOptions){
			if(sel.getText().equalsIgnoreCase("London")){
				System.out.println("INSIDE IF");
				sel.click();
			}
		}
		report.updateTestLog("DeliveryAddress Page", "Select a value for -Region- from the dropdown as expected ", Status.PASS);
		//select.selectByValue("London");
		Thread.sleep(3000);
		Select select1 = new Select(driver.findElement(By.name("ddl-collect-point")));
		List<WebElement> selOptions1 = select1.getOptions();
		for(WebElement sel1: selOptions1){
			if(sel1.getText().equalsIgnoreCase("Brent Cross")){
				System.out.println("INSIDE IF");
				sel1.click();
			}
		}
		report.updateTestLog("DeliveryAddress Page", "Select a value for -Shop- from the dropdown as expected ", Status.PASS);
		//select1.selectByValue("Brent Cross");
		Thread.sleep(1000);
		driver.findElement(By.name("confirm-shop")).click();
		report.updateTestLog("DeliveryAddress Page", "Click on -Confirm Shop- button as expected ", Status.PASS);
		Thread.sleep(3000);
		
		
		//Order Summary Page
		String oredrSummaryPgTitle = "John Lewis | Order Summary";
		if(oredrSummaryPgTitle.equalsIgnoreCase(driver.getTitle())){
			report.updateTestLog("To verify Pagetitle", "Page Title is displayed as expected :" + oredrSummaryPgTitle , Status.PASS);
		}
		Thread.sleep(3000);
		driver.findElement(By.name("continue")).click();
		report.updateTestLog("Order Summary", "Click on -Continue- in -Order Summary- page as expected ", Status.PASS);
		Thread.sleep(4000);
		highlightElement(driver, driver.findElement(By.xpath("//div[@id='checkout']/div[2]")));
		 String PaymentPage = driver.getTitle();
		 if(PaymentPage.equalsIgnoreCase(driver.getTitle())){
				report.updateTestLog("To verify Pagetitle", "Page Title is displayed as expected :" + PaymentPage , Status.PASS);
			}*/
		Thread.sleep(2000);
	}
	
	
	
	public void highlightElement(WebDriver driver, WebElement element) {
	    for (int i = 0; i < 2; i++) {
	        JavascriptExecutor js = (JavascriptExecutor) driver;
	        js.executeScript("arguments[0].setAttribute('style', arguments[1]);",
	                element, "color: red; border: 3px solid red;");
	        try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	        js.executeScript("arguments[0].setAttribute('style', arguments[1]);",
	                element, "");
	    }
	}
	
	
	//Demo Script End
	
	public void login()
	{
		String userName = dataTable.getData("General_Data", "Username");
		String password = dataTable.getData("General_Data", "Password");
		
		driver.findElement(By.name("userName")).sendKeys(userName);
		driver.findElement(By.name("password")).sendKeys(password);
		driver.findElement(By.name("login")).click();
		
		report.updateTestLog("Login", "Enter login credentials: " +
										"Username = " + userName + ", " +
										"Password = " + password, Status.DONE);
	}
	
	public void registerUser()
	{
		driver.findElement(By.linkText("REGISTER")).click();
		driver.findElement(By.name("firstName")).sendKeys(dataTable.getData("RegisterUser_Data", "FirstName"));
		driver.findElement(By.name("lastName")).sendKeys(dataTable.getData("RegisterUser_Data", "LastName"));		
		driver.findElement(By.name("phone")).sendKeys(dataTable.getData("RegisterUser_Data", "Phone"));		
		driver.findElement(By.name("userName")).sendKeys(dataTable.getData("RegisterUser_Data", "Email"));	
		driver.findElement(By.name("address1")).sendKeys(dataTable.getData("RegisterUser_Data", "Address"));
		driver.findElement(By.name("city")).sendKeys(dataTable.getData("RegisterUser_Data", "City"));
		driver.findElement(By.name("state")).sendKeys(dataTable.getData("RegisterUser_Data", "State"));
		driver.findElement(By.name("postalCode")).sendKeys(dataTable.getData("RegisterUser_Data", "PostalCode"));
		driver.findElement(By.name("email")).sendKeys(dataTable.getData("General_Data", "Username"));
		String password = dataTable.getData("General_Data", "Password");
		driver.findElement(By.name("password")).sendKeys(password);
		driver.findElement(By.name("confirmPassword")).sendKeys(password);
		driver.findElement(By.name("register")).click();
		report.updateTestLog("Registration", "Enter user details for registration", Status.DONE);
	}
	
	public void clickSignIn()
	{
		driver.findElement(By.linkText("sign-in")).click();
		report.updateTestLog("Click sign-in", "Click the sign-in link", Status.DONE);
	}
	
	public void findFlights()
	{
		driver.findElement(By.name("passCount")).sendKeys((dataTable.getData("Passenger_Data", "PassengerCount")));
		driver.findElement(By.name("fromPort")).sendKeys((dataTable.getData("Flights_Data", "FromPort")));
		driver.findElement(By.name("fromMonth")).sendKeys((dataTable.getData("Flights_Data", "FromMonth")));
		driver.findElement(By.name("fromDay")).sendKeys((dataTable.getData("Flights_Data", "FromDay")));
		driver.findElement(By.name("toPort")).sendKeys((dataTable.getData("Flights_Data", "ToPort")));
		driver.findElement(By.name("toMonth")).sendKeys((dataTable.getData("Flights_Data", "ToMonth")));
		driver.findElement(By.name("toDay")).sendKeys((dataTable.getData("Flights_Data", "ToDay")));
		driver.findElement(By.name("airline")).sendKeys((dataTable.getData("Flights_Data", "Airline")));
		driver.findElement(By.name("findFlights")).click();
		report.updateTestLog("Find Flights", "Search for flights using given test data", Status.DONE);
	}
	
	public void selectFlights()
	{
		driver.findElement(By.name("reserveFlights")).click();
		report.updateTestLog("Select Flights", "Select the first available flights", Status.DONE);
	}
	
	public void bookFlights()
	{
		String[] passengerFirstNames = dataTable.getData("Passenger_Data", "PassengerFirstNames").split(",");
		String[] passengerLastNames = dataTable.getData("Passenger_Data", "PassengerLastNames").split(",");
		int passengerCount = Integer.parseInt(dataTable.getData("Passenger_Data", "PassengerCount"));
		
		for(int i=0; i<passengerCount; i++) {
			driver.findElement(By.name("passFirst" + i)).sendKeys(passengerFirstNames[i]);
			driver.findElement(By.name("passLast" + i)).sendKeys(passengerLastNames[i]);
		}
		driver.findElement(By.name("creditCard")).sendKeys(dataTable.getData("Passenger_Data", "CreditCard"));
		driver.findElement(By.name("creditnumber")).sendKeys(dataTable.getData("Passenger_Data", "CreditNumber"));
		
		report.updateTestLog("Book Tickets", "Enter passenger details and book tickets", Status.SCREENSHOT);
		
		driver.findElement(By.name("buyFlights")).click();
	}
	
	public void backToFlights()
	{
		driver.findElement(By.xpath("//a/img")).click();
	}
	
	public void logout()
	{
		driver.findElement(By.linkText("SIGN-OFF")).click();
		report.updateTestLog("Logout", "Click the sign-off link", Status.DONE);
	}
}