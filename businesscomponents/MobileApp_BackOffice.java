package businesscomponents;
import org.openqa.selenium.By;

import com.cognizant.framework.Status;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import businesscomponents.ReusableComponents;
public class MobileApp_BackOffice extends ReusableLibrary {

	public MobileApp_BackOffice(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	/********************************************************
	 *FUNCTION    :openApp
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :16-1-2017
	 *DESCRIPTION :Field validation - Item name 
	 ********************************************************/
	public void openApp(){
		String url = dataTable.getData("General_Data" , "URL");
		System.out.println(url);
		driver.get(url);
		driver.manage().window().maximize();
	}
	
	
	/********************************************************
	 *FUNCTION    :loginPage
	 *AUTHOR      :Chandru Kumar S
	 *DATE        :16-1-2017
	 *DESCRIPTION :Field validation - Item name 
	 ********************************************************/
	public void loginPage(){
		String BranchNumber = dataTable.getData("General_Data" , "URL");
		System.out.println(BranchNumber);
		String UserNumber = dataTable.getData("General_Data" , "URL");
		System.out.println(UserNumber);
		String Password = dataTable.getData("General_Data" , "URL");
		System.out.println(Password);
		{

			if(ReusableComponents.isElementPresent(driver, "name","//android.widget.TextView[@resource-id='android:id/message']")){
				String errormessage = ReusableComponents.getText(driver, "xpath","//android.widget.TextView[@resource-id='android:id/message']");
				 System.out.println(errormessage);
				 report.updateTestLog("Error Message", "Error messgae is"+errormessage, Status.PASS);}
			else
				  report.updateTestLog("Error Message", "Error messgae  is not present", Status.FAIL);
		}
		
		
	}
	
}
